﻿using UnityEngine;
using System.Collections;

public class PhomConst {

	public const float TURN_TIME = 10;
    public const float MELD_WAIT_TIME = 3;
    public const float END_GAME_WAIT_TIME = 10;

    public const int INVALID_INDEX = -1;
    public const int INVALID_ID = -1;

	public const int SFS_ID_INDEX = 0;
	public const int USER_ID_INDEX = 1;
	public const int USERNAME_INDEX = 2;
	public const int GOLD_INDEX = 3;
	public const int AVATAR_INDEX = 4;
    public const int IS_WIN_INDEX = 5;
    public const int CHANGED_MONEY_INDEX = 6;
    public const int END_GAME_STATUS = 7;
    public const int COMBINATION = 8;
    public const int DISCARDED_CARDS_INDEX = 9;
    public const int EATEN_CARDS_INDEX = 10;
    public const int UPPER_LAID_DOWN_CARDS_INDEX = 11;
    public const int LOWER_LAID_DOWN_CARDS_INDEX = 12;
    public const int IS_WATCHER = 13;
    public const int VIP_POINT = 14;

    public const int ALL_INDEX = 0;
    public const int CHOSEN_MELD_INDEX = 1;
    public const int RUNS_INDEX = 2;
    public const int SETS_INDEX = 3;
    public const int WAITS_INDEX = 4;
    public const int TRASHS_INDEX = 5;
    public const int SCORE_LEFT_INDEX = 6;

    public const int TAKEN_SLOT_ID_INDEX = 0;
    public const int UPPER_SENT_CARDS_INDEX = 1;
    public const int LOWER_SENT_CARDS_INDEX = 2;
    public const int NEW_UPPER_LAID_DOWN_CARDS_INDEX = 3;
    public const int NEW_LOWER_LAID_DOWN_CARDS_INDEX = 4;

    public const int DRAW_BUTTON_INDEX = 0;
    public const int MELD_BUTTON_INDEX = 1;
    public const int DISCARD_BUTTON_INDEX = 2;
    public const int LAY_DOWN_BUTTON_INDEX = 3;
    public const int SEND_CARD_BUTTON_INDEX = 4;
    public const int CHANGE_COMBO_BUTTON_INDEX = 5;

    public const int CARD_NOTICE_MARK_INDEX = 0;
    public const int CARD_MELD_MARK_INDEX = 1;
    public const int CARD_MASK_INDEX = 7;
    public const int CARD_BACK_INDEX = 6;




    public const int NOT_WIN = 0;
    public const int WIN_BY_LEFT_SCORE = 1;
    public const int WIN_BY_9_MELD_CARDS = 2;
    public const int WIN_BY_9_TRASH_CARDS = 3;
    public const int WIN_BY_10_MELD_CARDS = 4;
    public const int WIN_BY_10_TRASH_CARDS = 5;

    /*
    public const int PLAYER_HAND_ZONE_DISTANCE = 35;
    public const int PLAYER_LAY_DOWN_ZONE_DISTANCE = 40;
    public const int HORIZONTAL_DISTANCE = 18;
    public const int VERTICAL_DISTANCE = 40;
    */

    public const float BIG_HORIZONTAL_RATIO = 0.077f;
    public const float SMALL_HORIZONTAL_RATIO = 0.025f;
    public const float VERTICAL_RATIO = 0.065f;

    public const string STATUS_LOSE = "1";
    public const string STATUS_BURN = "2";
    public const string STATUS_LOSE_FOR_ALL = "3";
    public const string STATUS_WIN = "4";
    public const string STATUS_WIN_BY_9_MELD_CARDS = "5";
    public const string STATUS_WIN_BY_9_TRASH_CARDS = "6";
    public const string STATUS_WIN_BY_10_MELD_CARDS = "7";
    public const string STATUS_WIN_BY_10_TRASH_CARDS = "8";

    public const string STATUS_RANK_1 = "9";
    public const string STATUS_RANK_2 = "10";
    public const string STATUS_RANK_3 = "11";
    public const string STATUS_RANK_4 = "12";
}
