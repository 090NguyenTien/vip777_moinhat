﻿using UnityEngine;
using System.Collections;

public class EndGameSprites : MonoBehaviour {

    public Sprite[] Sprites;

    public Sprite Get(int index)
    {
        return Sprites[index];
    }
}
