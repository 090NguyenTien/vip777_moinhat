﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sfs2X.Entities.Data;
using System;
using UnityEngine.UI;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using DG.Tweening;
using SimpleJSON;

public class PhomRoomController : MonoBehaviour
{
    #region Properties

    private SFS sfs;
    private static PhomRoomController instance;
    public static PhomRoomController Instance { get { return instance; } }
    public long TurnTime;
    public GameObject DeckCardBack;
    public GameObject SingleCardBack;
    public Text NumOfCardsLeft;
    public PhomPlayerSlot PlayerSlot;
    public List<PhomEnemySlot> EnemySlots;
    public PhomDeck Deck;
    public GameObject MessageZone;
    public Text Message;
    public List<GameObject> DiscardZones;
    public Text SecondsUntilStartGameText;
    public GameObject EndGameStatusZone;
    public GameObject ScorePanel;
    public GameObject DeckZone;

    public int RoomId { get; set; }
    public int HostId { get; set; }
    public int StartSlotId { get; set; }
    public long BetMoney { get; set; }
    public bool IsAutoPickingCard { get; set; }

    public List<Text> StartCardSets;

    public Text NextCardInDeck;
    public Text RoomIdText;
    public Text BetMoneyText, TxtHostname;
    public Text BoardInfoText;

    private int PreviousSlotId;
    private List<PhomSlot> OldSlots;
    private bool IsGameStarted = false;
    private bool IsShowHand = false;
    private Coroutine countDownAtStartGameCoroutine = null;
    private bool WorkDone = true;
    private bool RunningSubCoroutine = false;
    private Queue<GamePacket> GamePackets = new Queue<GamePacket>();
    private readonly object SyncLock = new object();
    private Stopwatch StopWatch = new Stopwatch();
    private static bool IsGettedGameRoomInfo = false;
    private int CurrentRound;

    [SerializeField]
    PopupInviteManager popupInvite;
    [SerializeField]
    PopupAlertManager popupAlert;
    [SerializeField]
    PopupFullManager popupFull;
    [SerializeField]
    Dictionary<int, int> dictIdPos;

    [SerializeField]
    GameObject ViTriBoomUser, ViTriBoom_1, ViTriBoom_2, ViTriBoom_3;
    [SerializeField]
    GameObject ViTri_CANON_0, ViTri_CANON_1, ViTri_CANON_2, ViTri_CANON_3;
    [SerializeField]
    GameObject ItemBoom_1, ItemBoom_2, ItemBoom_3, ItemBoom_4, ItemBoom_5, ItemBoom_6, PhaBom, Canon, BaiVang, ThanBai, Item_quyphi, QuyPhi, Item_SoaiCa, SoaiCa, Item_HaiPhuong, HaiPhuong;
    [SerializeField]
    GameObject Item_ThietPhien, ThietPhien;

    [SerializeField]
    GameObject ObjAvata_1, ObjAvata_2, ObjAvata_3;
    [SerializeField]
    GameObject ObjBtnInfo_1, ObjBtnInfo_2, ObjBtnInfo_3;

    private int sfsid_BiNem = -1;
    private int SfsLoaiBoom = -1;
    private int sfsid_NemBomPha = -1;
    private int sfsid_BiNemBomPha = -1;

    string IdBangMaster = "";
    string IdBangUser_Nem = "";
    string IdBangUser_BiNem = "";
    string IdBangChinhMinh = "";


    #endregion

    #region Unity Methods

    void Awake()
    {
        instance = this;
        sfs = SFS.Instance;
        SoundManager.PlaySound(SoundManager.ENTER_ROOM);
        IdBangMaster = MyInfo.ID_BANG_MASTER;
        IdBangChinhMinh = MyInfo.ID_BANG;
        LayThongTinThanBai();
    }

    void Start()
    {
        //AllowCheatTool = true;
        popupInvite.Init();
        popupAlert.Init();
        popupFull.Init();
        //Module.Init();
        LoadingManager.Instance.ENABLE = false;
        IsGettedGameRoomInfo = false;
        SendGetGameRoomInfoRequest();
    }

    void Update()
    {
        //ObjBtnInfo_1.SetActive(ObjAvata_1.activeInHierarchy);
        //ObjBtnInfo_2.SetActive(ObjAvata_2.activeInHierarchy);
        //ObjBtnInfo_3.SetActive(ObjAvata_3.activeInHierarchy);

        //ViTriBoom_1.SetActive(ObjAvata_1.activeInHierarchy);
        //ViTriBoom_2.SetActive(ObjAvata_2.activeInHierarchy);
        //ViTriBoom_3.SetActive(ObjAvata_3.activeInHierarchy);
    }

    #endregion

    #region Handle On Click Methods

    public void OnStartGameClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        SendStartGameRequest();
    }

    public void OnDiscardClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PlayerSlot.HideButton(PhomConst.DISCARD_BUTTON_INDEX);
        MessageZone.SetActive(false);
        if (CanDiscard(PlayerSlot, Message))
        {
            int discardedCardId = PlayerSlot.CurrentSelectedCards[0].Id;
            SendDiscardRequest(discardedCardId);
        }
        else
        {
            PlayerSlot.ShowButton(PhomConst.DISCARD_BUTTON_INDEX);
            MessageZone.SetActive(true);
        }
    }

    public void OnDrawClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        HideAllCardNoticeMarks();
        MessageZone.SetActive(false);
        PlayerSlot.HideButton(PhomConst.DRAW_BUTTON_INDEX);
        PlayerSlot.CurrentSelectedCards.Clear();
        SendDrawCardRequest();
    }

    public void OnMeldClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        HideAllCardNoticeMarks();
        MessageZone.SetActive(false);
        PlayerSlot.HideButton(PhomConst.MELD_BUTTON_INDEX);
        PhomSlot previousSlot = GetSlotById(PreviousSlotId);
        PhomCard lastDiscardedCard = previousSlot.DiscardedCards.Last();

        if (CanMeld(PlayerSlot.CurrentSelectedCards, lastDiscardedCard, Message))
        {
            List<PhomCard> eatenCards = PlayerSlot.CurrentSelectedCards;
            SendMeldCardsRequest(eatenCards);
        }
        else
        {
            MessageZone.SetActive(true);
            PlayerSlot.AnimateSortHandZone();
            PlayerSlot.ShowButton(PhomConst.MELD_BUTTON_INDEX);
        }
        PlayerSlot.CurrentSelectedCards.Clear();
    }

    public void OnLayDownClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PlayerSlot.HideButton(PhomConst.LAY_DOWN_BUTTON_INDEX);
        PlayerSlot.HideMeldBorders();
        SendLayDownRequest();
        PlayerSlot.CurrentSelectedCards.Clear();
    }

    public void OnSendCardClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PlayerSlot.HideButton(PhomConst.SEND_CARD_BUTTON_INDEX);
        SendSendCardsRequest();
        PlayerSlot.CurrentSelectedCards.Clear();
    }

    public void OnChangeComboClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PlayerSlot.HideButton(PhomConst.CHANGE_COMBO_BUTTON_INDEX);
        if (PhomUtil.IsMeld(PlayerSlot.CurrentSelectedCards))
        {
            List<int> ids = PhomUtil.ConvertCardsToIds(PlayerSlot.CurrentSelectedCards);
            PlayerSlot.CurrentSelectedCards.Clear();
            SendChangeComboRequest(ids);
        }
        else
        {
            PlayerSlot.CurrentSelectedCards.Clear();
            GamePacket gp = new GamePacket(CommandKey.ChangeCombo);
            gp.Put(ParamKey.CanChangeCombo, false);
            StartCoroutine(ChangeCombo(gp));
        }
    }

    public void OnExitClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        if (IsGameStarted)
        {
			popupFull.Show("Thoát", "Ván dánh chưa kết thúc. Thoát bàn sẽ bị xử thua cháy.", popupFull.Hide, SendExitRoomRequest);
        }
        else
        {
            LoadingManager.Instance.ENABLE = true;
            sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            //SendExitRoomRequest();
            //LoadingManager.Instance.ENABLE = true;
        }
    }

    public void OnInviteClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        popupInvite.Show(SendInviteRequest, OnCloseInviteClick);
        SendInviteRequest();
    }

    public void ItemPlayerInviteOnClick(int _id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        GamePacket param = new GamePacket(CommandKey.INVITE);
        param.Put(ParamKey.USER_ID, _id);
        SFS.Instance.SendRoomRequest(param);
    }

    public void OnCloseInviteClick()
    {
        popupInvite.Hide();
    }

    public void OnChatClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        PopupChatManager.instance.Show();
    }

    public void OnPublicMsg(string _msg)
    {
        int id = int.Parse(_msg.Split('#')[0]);
        int position = -1;
        string vip = "0";
        if (PlayerSlot.SfsId == id)
        {
            position = 0;
            vip = PlayerSlot.Vip;
        }
        else
        {
            for (int i = 0; i < EnemySlots.Count; i++)
            {
                if (EnemySlots[i].IsActive && EnemySlots[i].SfsId == id)
                {
                    vip = EnemySlots[i].Vip;
                    if (i == 0)
                        position = 3;
                    else if (i == 1)
                        position = 2;
                    else
                        position = 1;
                }
            }
        }

        PopupChatManager.instance.RspChat(position, _msg, vip);
    }

    public void OnModuleClick()
    {
        //Module.Show();
    }

    #endregion

    #region On SFS Response

    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)// những command ngoài game.
        {
            case CommandKey.ADD_FRIEND_CONFIRM:
            case CommandKey.ADD_FRIEND_IGNORE:
            case CommandKey.ADD_FRIEND_NOTICE:
            case CommandKey.ADD_FRIEND_REQUEST:
            case CommandKey.ADD_FRIEND_RESULT:
            case CommandKey.ADD_FRIEND_SUCCESS:
            case CommandKey.GET_LIST_FRIEND:

                return;
        }

        switch (gp.cmd)
        {
            case "bomb":
                ResponseNemBoom(gp);
                break;
            case CommandKey.CountDownAtStartGame:
                StopWatch = new Stopwatch();
                StopWatch.Start();
                GamePackets.Enqueue(gp);
                WorkDone = true;
                break;
            case CommandKey.USER_EXIT:
                GamePackets = new Queue<GamePacket>();
                WorkDone = true;
                RspExitGame(gp);
                break;
            case CommandKey.KICK_USER_BY_HOST:
                KickUserByHost(gp);
                return;
            case CommandKey.KICK_USER_ERROR:
                KickUserError(gp);
                return;
            default:
                GamePackets.Enqueue(gp);
                break;
        }

        if (WorkDone)
            HandleNextWorkInQueue();
    }

    private void HandleNextWorkInQueue()
    {
        lock (SyncLock)
            if (GamePackets.Count != 0)
                HandleGamePacketQueue(GamePackets.Dequeue());
    }

    private void HandleGamePacketQueue(GamePacket gp)
    {

        if (!IsGettedGameRoomInfo)
            if (gp.cmd != CommandKey.WatcherGetGameRoomInfoOK && gp.cmd != CommandKey.GET_GAME_ROOM_INFO)
                return;

        switch (gp.cmd)
        {
            case CommandKey.WatcherGetGameRoomInfoOK:
                //ExitGameIfWatcher();
                StartCoroutine(AnimateWatcherGetGameRoomInfoOK(gp));
                break;
            case CommandKey.GET_GAME_ROOM_INFO:
                StartCoroutine(AnimatePlayerGetGameRoomInfoOK(gp));
                break;
            case CommandKey.NOTICE_JOIN_GAME_ROOM:
                StartCoroutine(AnimatePlayerNoticeJoinRoomOK(gp));
                break;
            case CommandKey.StartGameOK:
                StartCoroutine(AnimateStartGameOK(gp));
                break;
            case CommandKey.DiscardNG:
                StartCoroutine(AnimateDiscardNG(gp));
                break;
            case CommandKey.DiscardOK:
                StartCoroutine(AnimateDiscardOK(gp));
                break;
            case CommandKey.StartTurnOK:
                StartCoroutine(AnimateStartTurnOK(gp));
                break;
            case CommandKey.DrawCardOK:
                StartCoroutine(AnimateDrawCardOK(gp));
                break;
            case CommandKey.MeldCardsOK:
                StartCoroutine(AnimateMeldCardsOK(gp));
                break;
            case CommandKey.LayDownOK:
                StartCoroutine(AnimateLayDownOK(gp));
                break;
            case CommandKey.SendCardsOK:
                StartCoroutine(AnimateSendCardsOK(gp));
                break;
            case CommandKey.PlayerEndGameOK:
                StartCoroutine(AnimatePlayerEndGameOK(gp));
                break;
            case CommandKey.PlayerNoticeExit:
                StartCoroutine(AnimatePlayerNoticeExit(gp));
                break;
            case CommandKey.JOIN_GAME_LOBBY_ROOM:
                GameHelper.ChangeScene(GameScene.WaitingRoom);
                break;
            case CommandKey.GET_PLAYERS_INVITE:
                RspGetPlayersInvite(gp);
                break;
            case CommandKey.CountDownAtStartGame:

                StartCoroutine(CountDownAtStartGame(gp));
                break;
            case CommandKey.ChangeCombo:
                StartCoroutine(ChangeCombo(gp));
                break;
            case CommandKey.KickUser:
                StartCoroutine(KickUser(gp));
                break;
            
        }
    }
    private void KickUserError(GamePacket param)
    {
        AlertController.api.showAlert(param.GetString("Message"));
    }
    private void KickUserByHost(GamePacket param)
    {
        AlertController.api.showAlert("Bạn đã bị chủ bàn mời ra khỏi phòng!");
        SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);
    }
    #endregion

    #region Handle Response Methods

    private void RspExitGame(GamePacket gp)
    {
        LoadingManager.Instance.ENABLE = true;
        if (gp.ContainKey(ParamKey.CHIP))
        {
            MyInfo.CHIP = gp.GetLong(ParamKey.CHIP);
            sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            SoundManager.PlaySound(SoundManager.EXIT_ROOM);
        }
        else
        {
            WorkDone = false;
            IsGettedGameRoomInfo = true;
            sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            SoundManager.PlaySound(SoundManager.EXIT_ROOM);
            GameHelper.ChangeScene(GameScene.WaitingRoom);
        }        
        
    }

    void RspGetPlayersInvite(GamePacket _param)
    {
        popupInvite.ShowItems(_param);
    }

    private IEnumerator CountDownAtStartGame(GamePacket gp)
    {

        while (!WorkDone)
            yield return null;
        WorkDone = false;

        StopWatch.Stop();
        int elapsedTime = (int)(StopWatch.ElapsedMilliseconds / 1000) + 1;

        DeckZone.SetActive(false);

        int seconds = gp.GetInt(ParamKey.SecondsUntilStartGame);

        if (countDownAtStartGameCoroutine != null)
            StopCoroutine(countDownAtStartGameCoroutine);

        seconds -= elapsedTime;

        if (GetAllActiveSlots().Count > 1)
        {
			BoardInfoText.text = "Ðang chờ ván mới bắt đầu...";
            countDownAtStartGameCoroutine = StartCoroutine(AnimateCountDownAtStartGame(seconds));
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    #endregion

    #region Send Request Methods

    private void SendGetGameRoomInfoRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.GetGameRoomInfo);
        sfs.SendRoomRequest(gp);
    }

    private void SendHackDealRequest(string strCardSets)
    {
        GamePacket gp = new GamePacket(CommandKey.HackDeal);
        gp.Put(ParamKey.START_CARD_SETS, strCardSets);
        sfs.SendRoomRequest(gp);
    }

    private void SendHackDrawRequest(int nextCardId)
    {
        GamePacket gp = new GamePacket(CommandKey.HackDraw);
        gp.Put(ParamKey.NextCardId, nextCardId);
        sfs.SendRoomRequest(gp);
    }

    private void SendStartGameRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.START_GAME);
        sfs.SendRoomRequest(gp);
    }

    private void SendDiscardRequest(int discardedCardId)
    {
        GamePacket gp = new GamePacket(CommandKey.Discard);
        gp.Put(ParamKey.DiscardedCardId, discardedCardId);
        gp.Put(ParamKey.Round, CurrentRound);
        sfs.SendRoomRequest(gp);
    }

    private void SendDrawCardRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.DrawCard);
        gp.Put(ParamKey.Round, CurrentRound);
        sfs.SendRoomRequest(gp);
    }

    private void SendMeldCardsRequest(List<PhomCard> eatenCards)
    {
        List<int> ids = PhomUtil.ConvertCardsToIds(eatenCards);
        GamePacket gp = new GamePacket(CommandKey.MeldCards);
        gp.Put(ParamKey.EatenCardIds, ids.ToArray());
        gp.Put(ParamKey.Round, CurrentRound);
        sfs.SendRoomRequest(gp);
    }

    private void SendLayDownRequest()
    {
        List<int> ids = PhomUtil.ConvertCardsToIds(PlayerSlot.CurrentSelectedCards);
        GamePacket gp = new GamePacket(CommandKey.LayDown);
        gp.Put(ParamKey.SelectedCardIds, ids.ToArray());
        gp.Put(ParamKey.Round, CurrentRound);
        sfs.SendRoomRequest(gp);
    }

    private void SendSendCardsRequest()
    {
        List<int> ids = PhomUtil.ConvertCardsToIds(PlayerSlot.CurrentSelectedCards);
        GamePacket gp = new GamePacket(CommandKey.SendCards);
        gp.Put(ParamKey.SelectedCardIds, ids.ToArray());
        gp.Put(ParamKey.Round, CurrentRound);
        sfs.SendRoomRequest(gp);
    }

    private void SendChangeComboRequest(List<int> ids)
    {
        GamePacket gp = new GamePacket(CommandKey.ChangeCombo);
        gp.Put(ParamKey.SelectedCardIds, ids.ToArray());
        sfs.SendRoomRequest(gp);
    }

    private void SendExitRoomRequest()
    {
        sfs.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));

    }

    private void SendInviteRequest()
    {
        sfs.SendRoomRequest(new GamePacket(CommandKey.GET_PLAYERS_INVITE));
    }

    #endregion

    #region Check Methods

    private bool CanShowScore(PhomSlot checkingSlot, List<PhomSlot> slots)
    {
        if (checkingSlot == null || slots == null || slots.Count < 2)
            return false;

        if (checkingSlot.EndGameStatusInfo.Equals(PhomConst.STATUS_BURN))
            return false;

        foreach (PhomSlot slot in slots)
        {
            if (slot.IsWatcher)
                continue;

            if (slot.SfsId != checkingSlot.SfsId)
                if (!slot.EndGameStatusInfo.Equals(PhomConst.STATUS_BURN))
                    return true;
        }
        return false;
    }

    public bool CanDiscard(PhomPlayerSlot playerSlot, Text systemMessage)
    {
        systemMessage.text = String.Empty;
        List<PhomCard> selectedCards = playerSlot.CurrentSelectedCards;

        if (selectedCards.Count == 0)
        {
			systemMessage.text = "Vui lòng chọn 1 lá bài.";
            return false;
        }
        else if (selectedCards.Count > 1)
        {
			systemMessage.text = "Chỉ được chọn 1 lá bài.";
            return false;
        }

        PhomCard selectedCard = selectedCards[0];

        if (playerSlot.EatenCards.Contains(selectedCard))
        {
			systemMessage.text = "Không được đánh bài đã an được.";
            return false;
        }

        return true;
    }

    public bool CanMeld(List<PhomCard> selectedCards, PhomCard discardedCard, Text systemMessage)
    {
        systemMessage.text = String.Empty;

        if (PlayerSlot.CurrentSelectedCards.Count < 2)
        {
			systemMessage.text = "Vui lòng chọnn ít nhất 2 lá bài.";
            return false;
        }

        List<PhomCard> newCardSet = PhomUtil.Clone(selectedCards);
        newCardSet.Add(discardedCard);

        if (!PhomUtil.IsMeld(newCardSet))
        {
			systemMessage.text = "Bài đã chọn không tạo thành phỏm được.";
            print("discardedCard");
            PhomUtil.Print(discardedCard);
            print("selectedCards");
            PhomUtil.PrintList(selectedCards);
            return false;
        }

        return true;
    }

    public bool NotDoneEndGameAnimation()
    {
        return EndGameStatusZone.activeSelf;
    }

    #endregion

    #region Animate Methods

    private IEnumerator AnimateWatcherGetGameRoomInfoOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;
        ExitGameIfWatcher();
        yield return null;
        RunningSubCoroutine = true;
        yield return StartCoroutine(AnimatePlayerGetGameRoomInfoOK(gp));
        RunningSubCoroutine = false;

		BoardInfoText.text = "Bàn đang chơi, vui lòng chờ giây lát...";

        string strSlots = gp.GetString(ParamKey.Slots);
        int currentTurnPlayerId = gp.GetInt(ParamKey.CurrentTurnPlayerId);
        int deckSize = gp.GetInt(ParamKey.DeckSize);

        DeckZone.SetActive(true);

        PlayerSlot.IsWatcher = true;
        List<PhomSlot> tempSlots = PhomUtil.ConvertStringToSlots(strSlots);

        foreach (PhomSlot tempSlot in tempSlots)
        {
            if (!tempSlot || !PlayerSlot) continue;
            if (PlayerSlot.SfsId != tempSlot.SfsId)
            {
                PhomEnemySlot enemySlot = (PhomEnemySlot)GetSlotById(tempSlot.SfsId);
                if (!enemySlot) continue;
                enemySlot.DiscardedCards = tempSlot.DiscardedCards;
                enemySlot.EatenCards = tempSlot.EatenCards;
                enemySlot.UpperLaidDownCards = tempSlot.UpperLaidDownCards;
                enemySlot.LowerLaidDownCards = tempSlot.LowerLaidDownCards;
                enemySlot.AnimateSortDiscardZone();
                enemySlot.AnimateSortEatenZone();
                enemySlot.AnimateSortLayDownZone(enemySlot.UpperLaidDownCards, enemySlot.UpperLayDownZone);
                enemySlot.AnimateSortLayDownZone(enemySlot.LowerLaidDownCards, enemySlot.LowerLayDownZone);
            }
        }

        NumOfCardsLeft.text = deckSize.ToString();
        PhomSlot currentTurnPlayer = GetSlotById(currentTurnPlayerId);
        IsGettedGameRoomInfo = true;
        WorkDone = true;
        HandleNextWorkInQueue();
    }
    public void OnModeratorMessage(string _msg)
    {
        JSONNode node = JSONNode.Parse(_msg);
        if (node["is_level_up"].AsInt == 1)
        {
            PlayerSlot.txtLevel.text = node["cur_level"];
            //PlayerSlot.level = node["cur_level"].AsInt;
        }

    }
    List<Hashtable> lstUserVariable;
    public void OnVariableResponse(List<Hashtable> lstDataRes)
    {
        lstUserVariable = lstDataRes;

    }
    private Hashtable GetUserVariableById(string uId)
    {
        //string uid = data["uid"].ToString();
        //string clan_id = data["clanid"].ToString();
        //string nhan_id = data["ringid"].ToString();
        //string gioitinh_id = data["gender"].ToString();
        //string tennguoitinh = data["partner"].ToString();
        foreach (Hashtable uVariable in lstUserVariable)
        {
            if (uVariable["uid"].ToString() == uId)
            {
                return uVariable;
            }
        }
        return null;
    }
    private IEnumerator AnimatePlayerGetGameRoomInfoOK(GamePacket gp)
    {

        if (!RunningSubCoroutine)
        {
            while (!WorkDone)
                yield return null;
            WorkDone = false;
        }

        TurnTime = gp.GetLong(ParamKey.TurnTime);
        RoomId = gp.GetInt(ParamKey.ROOM_ID);
        HostId = gp.GetInt(ParamKey.HostId);
        StartSlotId = gp.GetInt(ParamKey.StartSlotId);
        BetMoney = gp.GetLong(ParamKey.BetMoney);
        string strSlots = gp.GetString(ParamKey.Slots);
        string[] slots = strSlots.Split(DelimiterKey.dollarPack);

        RoomIdText.text = RoomId.ToString();
        BetMoneyText.text = Utilities.GetStringMoneyByLong(BetMoney);

        bool haveUser = false;
        for (int i = 0; i < slots.Length; i++)
        {
            string info = slots[i];

            if (!string.IsNullOrEmpty(info))
            {

                int sfsId = int.Parse(info.Split(DelimiterKey.verticalBarPack)[PhomConst.SFS_ID_INDEX]);

                if (sfsId == MyInfo.SFS_ID)
                {
                    haveUser = true;
                    PlayerSlot.ServerPosition = i;
                    Hashtable uVariable = GetUserVariableById(info.Split(DelimiterKey.verticalBarPack)[PhomConst.USER_ID_INDEX].ToString());
                    PlayerSlot.ProcessSetInfo(info, uVariable);
                    //PlayerSlot.ProcessSetInfo(info);
                    PlayerSlot.AnimateSetInfo(info);
                    break;
                }
            }
        }
        if (haveUser == false)
        {
            ExitGameIfWatcher();

        }else
        {
            for (int i = 0; i < slots.Length; i++)
            {
                string info = slots[i];

                if (!string.IsNullOrEmpty(info))
                {
                    int sfsId = int.Parse(info.Split(DelimiterKey.verticalBarPack)[PhomConst.SFS_ID_INDEX]);

                    if (sfsId != MyInfo.SFS_ID)
                    {
                        PhomEnemySlot enemySlot = PhomUtil.GetEnemySlotByPosition(EnemySlots, PlayerSlot.ServerPosition, i);
                        enemySlot.SlotIndex = PhomUtil.GetEnemySlotIndexByPosition(PlayerSlot.ServerPosition, i);
                        enemySlot.ServerPosition = i;
                        Hashtable uVariable = GetUserVariableById(info.Split(DelimiterKey.verticalBarPack)[PhomConst.USER_ID_INDEX].ToString());
                        enemySlot.ProcessSetInfo(info, uVariable);
                        //enemySlot.ProcessSetInfo(info);
                        enemySlot.AnimateSetInfo(info);
                    }
                }
            }

            AnimateMarkRoundStarter(GetSlotById(StartSlotId));

            if (RunningSubCoroutine)
                IsGettedGameRoomInfo = false;
            else
                IsGettedGameRoomInfo = true;

            WorkDone = true;
            HandleNextWorkInQueue();
        }
        TxtHostname.text = "Chủ Phòng:" + gp.GetString(ParamKey.HOST_NAME);
    }
    private void ExitGameIfWatcher()
    {
        popupAlert.Show("Bàn Chơi Đã Bắt Đầu!", () =>
        {
            SendExitRoomRequest();
            LoadingManager.Instance.ENABLE = true;
        });
    }
    private IEnumerator AnimatePlayerNoticeJoinRoomOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.ENTER_ROOM);

        string strSlots = gp.GetString(ParamKey.Slots);
        string[] slots = strSlots.Split(DelimiterKey.dollarPack);

        for (int i = 0; i < slots.Length; i++)
        {
            string info = slots[i];
            if (!string.IsNullOrEmpty(info))
            {
                int sfsId = int.Parse(info.Split(DelimiterKey.verticalBarPack)[PhomConst.SFS_ID_INDEX]);
                if (sfsId != MyInfo.SFS_ID)
                {
                    PhomEnemySlot enemySlot = PhomUtil.GetEnemySlotByPosition(EnemySlots, PlayerSlot.ServerPosition, i);
                    enemySlot.SlotIndex = PhomUtil.GetEnemySlotIndexByPosition(PlayerSlot.ServerPosition, i);
                    enemySlot.ServerPosition = i;
                    Hashtable uVariable = GetUserVariableById(info.Split(DelimiterKey.verticalBarPack)[PhomConst.USER_ID_INDEX].ToString());
                    enemySlot.ProcessSetInfo(info, uVariable);
                    //enemySlot.ProcessSetInfo(info);

                    // How about comment below code?
                    // enemySlot.RoundStarterImage.gameObject.SetActive(false);
                    enemySlot.AnimateSetInfo(info);
                }
            }
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateStartGameOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        IsGameStarted = true;
        CurrentRound = 1;
        DeckZone.SetActive(true);
        HideAllCardNoticeMarks();

        foreach (PhomSlot slot in GetAllActiveSlots())
            slot.Reset();

        PlayerSlot.HideButton(PhomConst.DRAW_BUTTON_INDEX);
        PlayerSlot.HideButton(PhomConst.MELD_BUTTON_INDEX);
        PlayerSlot.HideButton(PhomConst.DISCARD_BUTTON_INDEX);
        PlayerSlot.HideButton(PhomConst.LAY_DOWN_BUTTON_INDEX);
        PlayerSlot.HideButton(PhomConst.SEND_CARD_BUTTON_INDEX);
        PlayerSlot.HideButton(PhomConst.CHANGE_COMBO_BUTTON_INDEX);

        int startSlotId = gp.GetInt(ParamKey.StartSlotId);
        PhomSlot startSlot = GetSlotById(startSlotId);
        AnimateMarkRoundStarter(startSlot);

        SecondsUntilStartGameText.text = string.Empty;
        BoardInfoText.text = string.Empty;
        StopAllCoroutines();

        // hot fix
        foreach (PhomSlot slot in GetAllSlots())
            if (slot.Avatar.gameObject.activeSelf == false)
                ((PhomEnemySlot)slot).IsActive = false;

        StartCoroutine(AnimateDealCard(gp));
    }

    private IEnumerator AnimateDiscardNG(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

		Message.text = "Không được dánh bài phá phỏm này.";
        MessageZone.SetActive(true);
        PlayerSlot.ShowButton(PhomConst.DISCARD_BUTTON_INDEX);

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateDiscardOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.DANH_1_LA);
        //PlayerSlot.CurrentSelectedCards.Clear();
        MessageZone.SetActive(false);

        int discardSlotId = gp.GetInt(ParamKey.DiscardSlotId);
        int discardedCardId = gp.GetInt(ParamKey.DiscardedCardId);
        string strCombination = gp.GetString(ParamKey.Combination);

        PhomSlot discardSlot = GetSlotById(discardSlotId);
        PhomCard discardedCard = PhomUtil.getCardById(discardedCardId);

        discardSlot.DiscardedCards.Add(discardedCard);
        discardSlot.AnimateSortDiscardZone();

        foreach (PhomSlot slot in GetAllActiveSlots())
            foreach (Transform transform in slot.DiscardZone.transform)
                transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = PhomColor.DiscardedCardColor;

        discardedCard.transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = PhomColor.LastDiscardedCardColor;

        if (discardSlotId == PlayerSlot.SfsId)
        {
            PlayerSlot.Combination = PhomUtil.ToCombination(strCombination);
            PlayerSlot.CurrentSelectedCards.Clear();
            PlayerSlot.AnimateSortHandZone();
        }

        discardSlot.AnimateEndTurn();

        //yield return new WaitForSeconds(0.3f);
        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateStartTurnOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        HideAllCardNoticeMarks();

        CurrentRound = gp.GetInt(ParamKey.Round);
        int startTurnSlotId = gp.GetInt(ParamKey.StartTurnSlotId);
        int previousSlotId = gp.GetInt(ParamKey.PreviousSlotId);
        string recommendedEatenCardIds = gp.GetString(ParamKey.RecommendedEatenCardIds);

        PhomSlot startTurnSlot = GetSlotById(startTurnSlotId);
        List<PhomCard> recommendedEatenCards = PhomUtil.ToCards(recommendedEatenCardIds);

        if (startTurnSlotId == PlayerSlot.SfsId)
        {
            if (recommendedEatenCards != null && recommendedEatenCards.Count > 1)
            {
                PhomSlot previousSlot = GetSlotById(previousSlotId);
                previousSlot.DiscardedCards.Last().transform.GetChild(PhomConst.CARD_NOTICE_MARK_INDEX).gameObject.SetActive(true);
                StartCoroutine(previousSlot.DiscardedCards.Last().AnimateGoUpAndDownNoticeMark());
            }

            PlayerSlot.CurrentSelectedCards.Clear();
            PlayerSlot.AnimateSortHandZone();
        }

        PreviousSlotId = previousSlotId;
        yield return new WaitForSeconds(0.3f);
        startTurnSlot.AnimateStartTurn(false, recommendedEatenCards);
        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateDrawCardOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.DEAL_ONE_CARD);

        int drawnCardSlotId = gp.GetInt(ParamKey.DrawnCardSlotId);
        int deckSize = gp.GetInt(ParamKey.DeckSize);
        int drawnCardId = gp.GetInt(ParamKey.DrawnCardId);
        string strCombination = gp.GetString(ParamKey.Combination);

        PhomSlot drawnCardSlot = GetSlotById(drawnCardSlotId);
        NumOfCardsLeft.text = deckSize.ToString();
        HideDeckIfRunOutOfCards(deckSize);
        drawnCardSlot.AnimateDrawCard();

        yield return new WaitForSeconds(0.3f);

        if (drawnCardSlotId == PlayerSlot.SfsId)
        {
            PhomCard drawnCard = PhomUtil.getCardById(drawnCardId);
            PlayerSlot.Combination = PhomUtil.ToCombination(strCombination);
            drawnCard.transform.SetParent(PlayerSlot.HandZone.transform);
            drawnCard.transform.localPosition = Vector3.zero;
            PlayerSlot.AnimateSortHandZone();
            drawnCard.SetMaskColor(PhomColor.DrawnCardColor);

            PlayerSlot.HideButton(PhomConst.DRAW_BUTTON_INDEX);
            PlayerSlot.HideButton(PhomConst.MELD_BUTTON_INDEX);
            yield return new WaitForSeconds(0.3f);
            PlayerSlot.DecideToShowLayDownOrDiscardButton();
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateMeldCardsOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.PHOM_TAO_PHOM);

        int eatenSlotId = gp.GetInt(ParamKey.EatenSlotId);
        int beEatenSlotId = gp.GetInt(ParamKey.BeEatenSlotId);
        int oldRoundStarterId = gp.GetInt(ParamKey.OldRoundStarterId);
        int newRoundStarterId = gp.GetInt(ParamKey.NewRoundStarterId);
        int beEatenCardId = gp.GetInt(ParamKey.BeEatenCardId);
        int beMovedCardId = gp.GetInt(ParamKey.BeMovedCardId);
        long changedMoney = gp.GetLong(ParamKey.ChangedMoney);
        long eatenSlotNewMoney = gp.GetLong(ParamKey.EatenSlotNewMoney);
        long beEatenSlotNewMoney = gp.GetLong(ParamKey.BeEatenSlotNewMoney);
        int laidOffRound = gp.GetInt(ParamKey.LaidOffRound);
        int gameRound = gp.GetInt(ParamKey.GameRound);
        string strCombination = gp.GetString(ParamKey.Combination);

        PhomSlot eatenSlot = GetSlotById(eatenSlotId);
        PhomSlot beEatenSlot = GetSlotById(beEatenSlotId);
        PhomSlot oldRoundStarter = GetSlotById(oldRoundStarterId);
        PhomSlot newRoundStarter = GetSlotById(newRoundStarterId);
        PhomCard beEatenCard = PhomUtil.getCardById(beEatenCardId);
        PhomCard beMovedCard = PhomUtil.getCardById(beMovedCardId);

        beEatenSlot.DiscardedCards.Remove(beEatenCard);
        beEatenSlot.BeEatenCards.Add(beEatenCard);
        eatenSlot.EatenCards.Add(beEatenCard);

        beEatenSlot.AnimateMinusMoney(changedMoney, beEatenSlotNewMoney);
        eatenSlot.AnimatePlusMoney((long)Math.Round(changedMoney * 0.95f), eatenSlotNewMoney);
        beEatenSlot.AnimateShowBeLaidOffRound(laidOffRound, gameRound);
        eatenSlot.AnimateShowLaidOffRound(laidOffRound, gameRound);

        if (string.IsNullOrEmpty(strCombination))
        {
            PhomEnemySlot enemyEatenSlot = (PhomEnemySlot)eatenSlot;
            beEatenCard.transform.SetParent(enemyEatenSlot.EatenZone.transform);
            beEatenCard.transform.localPosition = Vector3.zero;
            beEatenSlot.AnimateSortDiscardZone();
            enemyEatenSlot.AnimateSortEatenZone();
        }
        else
        {
            Combination combination = PhomUtil.ToCombination(strCombination);
            PlayerSlot.Combination = PhomUtil.Clone(combination);
            beEatenCard.transform.SetParent(PlayerSlot.HandZone.transform);
            beEatenCard.transform.localPosition = Vector3.zero;
            beEatenSlot.AnimateSortDiscardZone();
            PlayerSlot.AnimateSortHandZone();
        }

        if (beMovedCard != null)
        {
            oldRoundStarter.DiscardedCards.Remove(beMovedCard);
            beEatenSlot.DiscardedCards.Add(beMovedCard);
            beMovedCard.transform.SetParent(beEatenSlot.DiscardZone.transform);
            beMovedCard.transform.localPosition = Vector3.zero;
            oldRoundStarter.AnimateSortDiscardZone();
            beEatenSlot.AnimateSortDiscardZone();
        }

        AnimateMarkRoundStarter(newRoundStarter);

        if (!string.IsNullOrEmpty(strCombination))
        {
            PlayerSlot.HideButton(PhomConst.DRAW_BUTTON_INDEX);
            PlayerSlot.HideButton(PhomConst.MELD_BUTTON_INDEX);
            yield return new WaitForSeconds(0.3f);
            PlayerSlot.DecideToShowLayDownOrDiscardButton();
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateLayDownOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.PHOM_HA_PHOM);

        int laidDownSlotId = gp.GetInt(ParamKey.LaidDownSlotId);
        string strUpperLaidDownCards = gp.GetString(ParamKey.UpperLaidDownCards);
        string strLowerLaidDownCards = gp.GetString(ParamKey.LowerLaidDownCards);
        string strRecommendedSentCards = gp.GetString(ParamKey.RecommendedSentCards);
        string strCombination = gp.GetString(ParamKey.Combination);

        PhomSlot laidDownSlot = GetSlotById(laidDownSlotId);
        List<PhomCard> upperLaidDownCards = PhomUtil.ToCards(strUpperLaidDownCards);
        List<PhomCard> lowerLaidDownCards = PhomUtil.ToCards(strLowerLaidDownCards);

        laidDownSlot.UpperLaidDownCards = upperLaidDownCards;
        laidDownSlot.LowerLaidDownCards = lowerLaidDownCards;

        if (laidDownSlotId == PlayerSlot.SfsId)
        {
            ((PhomPlayerSlot)laidDownSlot).AnimateSortLayDownZone(upperLaidDownCards, laidDownSlot.UpperLayDownZone);
            ((PhomPlayerSlot)laidDownSlot).AnimateSortLayDownZone(lowerLaidDownCards, laidDownSlot.LowerLayDownZone);

            PlayerSlot.CurrentSelectedCards.Clear();
            PlayerSlot.HideButton(PhomConst.LAY_DOWN_BUTTON_INDEX);
            List<PhomCard> recommendedSentCards = PhomUtil.ToCards(strRecommendedSentCards);
            PlayerSlot.Combination = PhomUtil.ToCombination(strCombination);
            yield return new WaitForSeconds(0.3f);
            PlayerSlot.AnimateSortHandZone();
            yield return new WaitForSeconds(0.3f);
            PlayerSlot.DecideToShowSendCardOrDiscardButton(recommendedSentCards);
        }
        else
        {
            ((PhomEnemySlot)laidDownSlot).AnimateSortLayDownZone(upperLaidDownCards, laidDownSlot.UpperLayDownZone);
            ((PhomEnemySlot)laidDownSlot).AnimateSortLayDownZone(lowerLaidDownCards, laidDownSlot.LowerLayDownZone);
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimateSendCardsOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        SoundManager.PlaySound(SoundManager.PHOM_HA_PHOM);

        int sentSlotId = gp.GetInt(ParamKey.SentSlotId);
        string strSentGroups = gp.GetString(ParamKey.SentGroups);
        string strCombination = gp.GetString(ParamKey.Combination);

        List<SentGroup> sentGroups = PhomUtil.ConvertStringToSentGroups(strSentGroups);

        foreach (SentGroup sentGroup in sentGroups)
        {
            PhomSlot takenSlot = GetSlotById(sentGroup.TakenSlotId);

            if (takenSlot.SfsId == PlayerSlot.SfsId)
            {
                ((PhomPlayerSlot)takenSlot).AnimateSortLayDownZone(sentGroup.NewUpperLaidDownCards, takenSlot.UpperLayDownZone);
                ((PhomPlayerSlot)takenSlot).AnimateSortLayDownZone(sentGroup.NewLowerLaidDownCards, takenSlot.LowerLayDownZone);
            }
            else
            {
                ((PhomEnemySlot)takenSlot).AnimateSortLayDownZone(sentGroup.NewUpperLaidDownCards, takenSlot.UpperLayDownZone);
                ((PhomEnemySlot)takenSlot).AnimateSortLayDownZone(sentGroup.NewLowerLaidDownCards, takenSlot.LowerLayDownZone);
            }
        }

        yield return new WaitForSeconds(0.3f);

        if (sentSlotId == PlayerSlot.SfsId)
        {
            PlayerSlot.CurrentSelectedCards.Clear();
            PlayerSlot.Combination = PhomUtil.ToCombination(strCombination);
            PlayerSlot.AnimateSortHandZone();
            PlayerSlot.HideButton(PhomConst.SEND_CARD_BUTTON_INDEX);
            PlayerSlot.ShowButton(PhomConst.DISCARD_BUTTON_INDEX);
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimatePlayerEndGameOK(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        IsGameStarted = false;
        EndGameStatusZone.SetActive(true);
        ScorePanel.SetActive(true);

        HideAllCardNoticeMarks();

        foreach (PhomCard card in PlayerSlot.CurrentSelectedCards)
        {
            Vector3 cardPos = card.transform.position;
            card.transform.position = new Vector3(cardPos.x, cardPos.y - Screen.height * PhomConst.VERTICAL_RATIO);
        }

        PlayerSlot.CurrentSelectedCards.Clear();

        string strSlots = gp.GetString(ParamKey.Slots);
        int startSlotId = gp.GetInt(ParamKey.StartSlotId);
        int exitedPlayerId = gp.GetInt(ParamKey.ExitedPlayerId);

        List<PhomSlot> tempSlots = PhomUtil.ConvertStringToSlots(strSlots);
        PlayerSlot.ButtonsZone.SetActive(false);
        PlayerSlot.AnimateEndTurn();
        foreach (PhomSlot slot in EnemySlots)
        {
            slot.AnimateEndTurn();
        }

        if (exitedPlayerId != -1)
        {
            PhomEnemySlot exitedSlot = (PhomEnemySlot)GetSlotById(exitedPlayerId);
            exitedSlot.AnimateExit(Deck);
        }

        foreach (PhomSlot tempSlot in tempSlots)
        {
            if (tempSlot.IsWatcher)
                continue;

            if (PlayerSlot.SfsId == tempSlot.SfsId)
            {
                PlayerSlot.IsWin = tempSlot.IsWin;
                PlayerSlot.GoldInfo = tempSlot.GoldInfo;
                PlayerSlot.ChangedMoneyInfo = tempSlot.ChangedMoneyInfo;
                PlayerSlot.EndGameStatusInfo = tempSlot.EndGameStatusInfo;
                PlayerSlot.Combination = PhomUtil.Clone(tempSlot.Combination);

                //PlayerSlot.AnimateGetDealedCards(Deck);
                yield return new WaitForSeconds(0.1f);
                //PlayerSlot.AnimateSortHandZone();

                if (PlayerSlot.IsWin)
                    PlayerSlot.AnimatePlusMoney(PlayerSlot.ChangedMoneyInfo, PlayerSlot.GoldInfo);
                else
                    PlayerSlot.AnimateMinusMoney(PlayerSlot.ChangedMoneyInfo, PlayerSlot.GoldInfo);

                if (PhomUtil.isSomeoneRummy(tempSlots))
                {
                    if (PhomUtil.isRummyOrLoseForAll(PlayerSlot))
                        PlayerSlot.AnimateShowEndGameStatus();
                }
                else
                {
                    PlayerSlot.AnimateShowEndGameStatus();

                    if (CanShowScore(PlayerSlot, tempSlots))
                        PlayerSlot.AnimateShowScore(PlayerSlot.Combination.GetAllScore());
                }
            }
            else
            {
                PhomEnemySlot enemySlot = (PhomEnemySlot)GetSlotById(tempSlot.SfsId);
                enemySlot.IsWin = tempSlot.IsWin;
                enemySlot.GoldInfo = tempSlot.GoldInfo;
                enemySlot.ChangedMoneyInfo = tempSlot.ChangedMoneyInfo;
                enemySlot.EndGameStatusInfo = tempSlot.EndGameStatusInfo;
                enemySlot.Combination = PhomUtil.Clone(tempSlot.Combination);

                enemySlot.AnimateShowCombination(Deck);
                yield return new WaitForSeconds(0.1f);
                enemySlot.AnimateSortHandZone();

                if (enemySlot.IsWin)
                    enemySlot.AnimatePlusMoney(enemySlot.ChangedMoneyInfo, enemySlot.GoldInfo);
                else
                    enemySlot.AnimateMinusMoney(enemySlot.ChangedMoneyInfo, enemySlot.GoldInfo);

                if (PhomUtil.isSomeoneRummy(tempSlots))
                {
                    if (PhomUtil.isRummyOrLoseForAll(enemySlot))
                        enemySlot.AnimateShowEndGameStatus();
                }
                else
                {
                    enemySlot.AnimateShowEndGameStatus();
                    if (CanShowScore(enemySlot, tempSlots))
                        enemySlot.AnimateShowScore(enemySlot.Combination.GetAllScore());
                }
            }
        }

        if (PlayerSlot.IsWatcher)
        {
            SoundManager.PlaySound(SoundManager.WIN);
        }
        else
        {
            if (PlayerSlot.IsWin)
                SoundManager.PlaySound(SoundManager.WIN);
            else
                SoundManager.PlaySound(SoundManager.LOSE);
        }

        yield return new WaitForSeconds(PhomConst.END_GAME_WAIT_TIME);
        foreach (PhomCard card in Deck.Cards)
        {
            card.transform.SetParent(Deck.transform);
            card.transform.localPosition = Vector3.zero;
        }
        PhomUtil.ResetCardsState(Deck.Cards);

        Deck.gameObject.SetActive(true);
        SingleCardBack.SetActive(true);
        DeckCardBack.SetActive(true);
        NumOfCardsLeft.text = "52";

        // GetAllActiveSlots -> GetAllSlots
        foreach (PhomSlot slot in GetAllSlots())
            slot.Reset();

        StartSlotId = startSlotId;
        AnimateMarkRoundStarter(GetSlotById(StartSlotId));

        EndGameStatusZone.SetActive(false);
        ScorePanel.SetActive(false);
        HideAllCardNoticeMarks();

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private IEnumerator AnimatePlayerNoticeExit(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        int exitedPlayerId = gp.GetInt(ParamKey.ExitedPlayerId);
        int newRoundStarterId = gp.GetInt(ParamKey.NewRoundStarterId);

        PhomEnemySlot exitedSlot = (PhomEnemySlot)GetSlotById(exitedPlayerId);
        int index = 0;

        for (int i = 0; i < EnemySlots.Count; i++)
        {
            if (EnemySlots[i].SfsId == exitedPlayerId)
                index = i;
        }

        AnimateMarkRoundStarter(GetSlotById(newRoundStarterId));
        EnemySlots[index].AnimateExit(Deck);

        EnemySlots[index].Reset();
        EnemySlots[index].IsActive = false;

        yield return new WaitForSeconds(0.3f);

        if (!IsGameStarted)
        {
            StartSlotId = newRoundStarterId;

            int numOfEnemies = 0;
            foreach (PhomEnemySlot enemy in EnemySlots)
                if (enemy.IsActive)
                    numOfEnemies++;
            if (numOfEnemies == 0)
            {
                SecondsUntilStartGameText.text = string.Empty;
                BoardInfoText.text = string.Empty;
            }
        }

        WorkDone = true;
        HandleNextWorkInQueue();
        TxtHostname.text = "Chủ Phòng:" + gp.GetString(ParamKey.HOST_NAME);
    }

    private IEnumerator AnimateCountDownAtStartGame(int secondsUntilStartGame)
    {
        float timeLeft = secondsUntilStartGame;

        while (timeLeft > 0)
        {

            if (GetAllActiveSlots().Count < 2)
                break;

            yield return new WaitForSeconds(1);
            timeLeft -= 1;

            SecondsUntilStartGameText.text = timeLeft.ToString();
            SecondsUntilStartGameText.gameObject.SetActive(true);
            if (SecondsUntilStartGameText.gameObject.activeSelf
                && SecondsUntilStartGameText.text != string.Empty)
                SoundManager.PlaySound(SoundManager.COUNT_DOWN);

            // yield  return null;
        }
        SecondsUntilStartGameText.text = string.Empty;
        BoardInfoText.text = string.Empty;
    }

    private IEnumerator ChangeCombo(GamePacket gp)
    {
        bool canChangeCombo = gp.GetBool(ParamKey.CanChangeCombo);

        if (canChangeCombo)
        {
            string combination = gp.GetString(ParamKey.Combination);
            PlayerSlot.Combination = PhomUtil.ToCombination(combination);
        }
        else
        {
            MessageZone.SetActive(true);
			Message.text = "Không đổi phỏm được!";
            yield return new WaitForSeconds(1);
            Message.text = string.Empty;
            MessageZone.SetActive(false);
        }

        PlayerSlot.CurrentSelectedCards.Clear();
        PlayerSlot.AnimateSortHandZone();
        yield return null;
    }

    private IEnumerator KickUser(GamePacket gp)
    {
        while (!WorkDone)
            yield return null;
        WorkDone = false;

        int[] userList = gp.GetIntArray(ParamKey.USER_LIST);

        foreach (int i in userList)
        {
            //Player
            if (MyInfo.SFS_ID == i)
            {
				popupAlert.Show("Bạn bị mời ra khỏi bàn vì không đủ tiền, vui lòng nạp thêm!", () =>
                {
                    GamePacket gamePacket = new GamePacket(CommandKey.JOIN_GAME_LOBBY_ROOM);
                    gamePacket.Put(ParamKey.GAME_ID, 4);
                    sfs.SendZoneRequest(gamePacket);
                });
            }
            //Enemy
            /* Because server will sent notice user exit later
            else
            {
                PhomEnemySlot enemy = (PhomEnemySlot)GetSlotById(i);
  
                if (enemy != null)
                {
                    enemy.AnimateExit(Deck);
                    yield return new WaitForSeconds(0.3f);
                    enemy.Reset();
                    enemy.SfsId = PhomConst.INVALID_ID;
                    enemy.IsActive = false;
                }
            }
            */
        }

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    #endregion

    #region Sub Animate Methods

    private IEnumerator AnimateDealCard(GamePacket gp)
    {
        SoundManager.PlaySound(SoundManager.DEAL_MANY_CARD);
        List<PhomSlot> slots = GetAllActiveSlots();

        int numOfCardsLeft = 52;

        for (int i = 0; i < 9; i++)
        {
            foreach (PhomSlot player in slots)
            {
                numOfCardsLeft--;
                NumOfCardsLeft.text = numOfCardsLeft + "";
                GameObject temp = Instantiate(SingleCardBack);
                temp.transform.SetParent(Deck.transform);
                temp.transform.localPosition = Vector3.zero;
                temp.transform.localScale = new Vector3(1, 1, 1);
                temp.transform.DOMove(player.Avatar.transform.position, 0.4f, false).SetEase(Ease.Linear);

                Destroy(temp, 0.4f);
                yield return new WaitForSeconds(0.01f);
            }
        }

        int startSlotId = gp.GetInt(ParamKey.StartSlotId);
        int deckSize = gp.GetInt(ParamKey.DeckSize);
        string combination = gp.GetString(ParamKey.Combination);

        PhomSlot startSlot = GetSlotById(startSlotId);
        PlayerSlot.Combination = PhomUtil.ToCombination(combination);

        NumOfCardsLeft.text = deckSize.ToString();

        yield return new WaitForSeconds(0.5f);
        PlayerSlot.AnimateGetDealedCards(Deck);
        PlayerSlot.AnimateSortHandZone();
        PlayerSlot.TimerImage.gameObject.SetActive(false);
        startSlot.AnimateStartTurn(true, null);

        WorkDone = true;
        HandleNextWorkInQueue();
    }

    private void AnimateMarkRoundStarter(PhomSlot newRoundStarter)
    {
        List<PhomSlot> allSlots = GetAllSlots();
        foreach (PhomSlot slot in allSlots)
            slot.RoundStarterImage.gameObject.SetActive(false);

        if (newRoundStarter != null)
            newRoundStarter.RoundStarterImage.gameObject.SetActive(true);
    }

    #endregion

    #region Helper Methods

    private List<PhomSlot> GetAllActiveSlots()
    {
        List<PhomSlot> slots = new List<PhomSlot>();
        slots.Add(PlayerSlot);

        foreach (PhomEnemySlot enemySlot in EnemySlots)
        {

            if (enemySlot.IsActive)
                slots.Add(enemySlot);
        }

        return slots;
    }

    private List<PhomSlot> GetAllSlots()
    {
        List<PhomSlot> slots = new List<PhomSlot>();
        slots.Add(PlayerSlot);

        foreach (PhomEnemySlot enemySlot in EnemySlots)
            slots.Add(enemySlot);

        return slots;
    }

    private PhomSlot GetSlotById(int sfsId)
    {
        if (PlayerSlot.SfsId == sfsId)
            return PlayerSlot;
        foreach (PhomEnemySlot enemy in EnemySlots)
        {
            if (enemy.SfsId == sfsId)
            {
                return enemy;
            }
        }
        return null;
    }
    //private PhomSlot GetSlotBySlotIndex(int slotIndex)
    //{
    //    if (PlayerSlot.SfsId == sfsId)
    //        return PlayerSlot;
    //    foreach (PhomEnemySlot enemy in EnemySlots)
    //    {
    //        if (enemy.SfsId == sfsId)
    //        {
    //            return enemy;
    //        }
    //    }
    //    return null;
    //}

    private void HideDeckIfRunOutOfCards(int deckSize)
    {
        if (deckSize == 0)
        {
            Deck.gameObject.SetActive(false);
            SingleCardBack.SetActive(false);
            DeckCardBack.SetActive(false);
            NumOfCardsLeft.text = string.Empty;
        }
    }

    private void HideAllCardNoticeMarks()
    {
        foreach (PhomCard card in Deck.Cards)
            card.transform.GetChild(PhomConst.CARD_NOTICE_MARK_INDEX).gameObject.SetActive(false);
    }

    public void AllowPickCard()
    {
        IsAutoPickingCard = false;
    }

    #endregion

    public void OnConnectionLost()
    {
        popupAlert.Show(ConstText.ErrorConnection, () => {
            GameHelper.ChangeScene(GameScene.LoginScene);
        });
    }





    #region NÉM ITEM

    #region GỬI REQUEST

    public void SendBomb()
    {
        GamePacket param = new GamePacket("bomb");
        param.Put("sfsid_nem", MyInfo.SFS_ID);
        param.Put("sfsid_binem", sfsid_BiNem);
        param.Put("bomb_type", SfsLoaiBoom);
        SFS.Instance.SendBoomRequest(param);
    }


    public void SendBomPha()
    {
        GamePacket param = new GamePacket("bomb");
        param.Put("sfsid_nem", sfsid_NemBomPha);
        param.Put("sfsid_binem", sfsid_BiNemBomPha);
        param.Put("bomb_type", SfsLoaiBoom);

        if (sfsid_NemBomPha != sfsid_BiNemBomPha)
        {
            SFS.Instance.SendBoomRequest(param);
        }
        sfsid_NemBomPha = -1;
        sfsid_BiNemBomPha = -1;
    }

    #endregion

    #region NHẬN RESPONSE



    public void ResponseNemBoom(GamePacket param)
    {

        int sfsid_Nem = param.GetInt("sfsid_nem");
        int sfsid_BiNem = param.GetInt("sfsid_binem");
        int LoaiBoom = param.GetInt("bomb_type");
        PhomEnemySlot Enemy_Nem = new PhomEnemySlot();
        PhomEnemySlot Enemy_BiNem = new PhomEnemySlot();
        bool NemVaoUser = false;
        //  Debug.Log("Vô ResponseNemBoom--------------sfsid_Nem- " + sfsid_Nem + " sfsid_BiNem = " + sfsid_BiNem + " LoaiBoom = " + LoaiBoom);
        if (sfsid_Nem != MyInfo.SFS_ID)
        {
            foreach (var item in EnemySlots)
            {
                if (item.SfsId == sfsid_Nem)
                {
                    Enemy_Nem = item;
                }
            }

            if (sfsid_BiNem == MyInfo.SFS_ID)
            {
                NemVaoUser = true;
            }
            else
            {
                foreach (var item in EnemySlots)
                {
                    if (item.SfsId == sfsid_BiNem)
                    {
                        Enemy_BiNem = item;
                    }
                }
            }
            int ViTriSinh = Enemy_Nem.Id_ViTriBom;

            if (NemVaoUser == false)  // TRƯỜNG HỢP NGƯỜI THỨ 3
            {
                int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                IdBangUser_Nem = Enemy_Nem.IdBang;
                IdBangUser_BiNem = Enemy_BiNem.IdBang;

                string idUser_Nem = Enemy_Nem.UserId;

                NguoiThuBa(LoaiBoom, ViTriSinh, ViTriNem, IdBangUser_Nem, IdBangUser_BiNem, idUser_Nem);
            }
            else  // TRƯỜNG HỢP NGƯỜI BỊ NÉM
            {
                NguoiBiNem(LoaiBoom, ViTriSinh, Enemy_Nem.IdBang, MyInfo.ID);
            }
        }
        else      // TRƯỜNG HỢP NGƯỜI NÉM
        {
            if (LoaiBoom == 6)
            {
                foreach (var item in EnemySlots)
                {
                    if (item.SfsId == sfsid_BiNem)
                    {
                        Enemy_BiNem = item;
                    }
                }
                int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                IdBangUser_BiNem = Enemy_BiNem.IdBang;

                NguoiNem(ViTriNem, MyInfo.ID);
            }
        }
    }



    void NguoiNem(int ViTriNem, string idUser_ChinhMinh)
    {

        bool check = CheckPhanDame(IdBangUser_BiNem, IdBangChinhMinh);
        //   check = false;
        if (check == false)
        {
            int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

            if (loaiThanBai == -1)
            {
                return;
            }
            else
            {
                GoiThanBai(ViTriNem, 0, 0, loaiThanBai);
            }
        }
        else
        {
            SinhBoom_DiChuyenToiViTri(ViTriNem, 0, 0);
            StartCoroutine(GoiPhanDame(0, ViTriNem));
        }
    }

    void NguoiBiNem(int LoaiBoom, int ViTriSinh, string IdBangNem, string idUser_ChinhMinh)
    {
        if (LoaiBoom != 6)
        {
            IdBangUser_Nem = IdBangNem;
            bool check = CheckPhanDame(IdBangUser_Nem, IdBangChinhMinh);
            //   check = false;
            if (check == false)
            {
                int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

                if (loaiThanBai == -1)
                {
                    SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                }
                else
                {
                    GoiThanBai(ViTriSinh, 0, LoaiBoom, loaiThanBai);
                }
            }
            else
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                StartCoroutine(GoiPhanDame(0, ViTriSinh));
            }
        }
    }

    void NguoiThuBa(int LoaiBoom, int ViTriSinh, int ViTriNem, string IdBangUser_Nem, string IdBangUser_BiNem, string idUser_Nem)
    {
        if (LoaiBoom != 6)
        {
            bool check = CheckPhanDame(IdBangUser_Nem, IdBangUser_BiNem);

            if (check == false)
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
            }
            else
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
                StartCoroutine(GoiPhanDame(ViTriNem, ViTriSinh));
            }

        }
        else
        {
            bool check = CheckPhanDame(IdBangUser_BiNem, IdBangUser_Nem);

            if (check == true)
            {
                SinhBoom_DiChuyenToiViTri(ViTriNem, ViTriSinh, 0);
                StartCoroutine(GoiPhanDame(ViTriSinh, ViTriNem));
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(idUser_Nem);

                if (loaiThanBai == -1)
                {

                }
                else
                {
                    GoiThanBai(ViTriNem, ViTriSinh, 1, loaiThanBai);
                }
            }
        }
    }



    #endregion

    #region SINH ITEM BOOM


    public void SinhBoom_DiChuyenToiViTri(int ViTriSinh, int ViTriNem, int loaiBoom, bool Send = false, int LoaiBoom_PhanDame = 0)
    {
        Transform targetMove = null;
        GameObject MyBoom = null;
        GameObject MyCanon = null;

        // Debug.LogWarning("ViTriSinh - " + ViTriSinh + " ViTriNem - " + ViTriNem + " loaiBoom - " + loaiBoom);

        #region Tao Boom

        if (loaiBoom != 6) // TRƯỜNG HỢP THƯỜNG
        {
            MyBoom = TaoBoomTaiViTriSinh(loaiBoom, ViTriSinh);
        }
        else // TRƯỜNG HỢP PHAN DAME
        {
            MyCanon = TAO_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
            MyBoom = TAO_BOOM_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
        }

        MyBoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        // XÁC ĐỊNH VỊ TRÍ NÉM
        targetMove = XacDinhViTriNem(ViTriNem);

        #endregion

        ItemBoomControll MyItem = MyBoom.GetComponent<ItemBoomControll>();

        MyItem.Init(targetMove);

        if (Send == false)  // TRƯỜNG HỢP CHỈ DIỂN HOẠT KHÔNG GỬI REQUEST
        {
            MyItem.NemBoom(NemThuong, loaiBoom);
        }
        else   // TRƯỜNG HỢP GỬI REQUEST
        {
            if (loaiBoom != 6) // Choi Thuong
            {
                sfsid_BiNem = GetSfsIdEnemy(ViTriNem);
                if (sfsid_BiNem != -1)
                {
                    SfsLoaiBoom = loaiBoom;
                    MyItem.NemBoom(SendBomb, loaiBoom);
                }
            }
            else // Choi Nhung Bi Phan Dame
            {
                sfsid_BiNemBomPha = MyInfo.SFS_ID;
                sfsid_NemBomPha = GetSfsIdEnemy(ViTriSinh);
                if (sfsid_BiNemBomPha != -1)
                {
                    SfsLoaiBoom = loaiBoom;
                    MyItem.NemBoom(SendBomPha, loaiBoom);
                }
            }

        }

    }



    GameObject TaoBoomTaiViTriSinh(int loaiboom, int ViTriSinh)
    {
        GameObject MyBoom = null;
        GameObject ObjBoom = null;
        ObjBoom = XacDinhLoaiBoom(loaiboom);

        if (ViTriSinh == 0)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoomUser.transform) as GameObject;
        }
        else if (ViTriSinh == 1)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_1.transform) as GameObject;
        }
        else if (ViTriSinh == 2)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_2.transform) as GameObject;
        }
        else if (ViTriSinh == 3)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_3.transform) as GameObject;
        }

        return MyBoom;
    }


    GameObject XacDinhLoaiBoom(int loaiboom)
    {
        GameObject ObjBoom = null;

        if (loaiboom == 0)
        {
            ObjBoom = ItemBoom_1;
        }
        else if (loaiboom == 1)
        {
            ObjBoom = ItemBoom_2;
        }
        else if (loaiboom == 2)
        {
            ObjBoom = ItemBoom_3;
        }
        else if (loaiboom == 3)
        {
            ObjBoom = ItemBoom_4;
        }
        else if (loaiboom == 4)
        {
            ObjBoom = ItemBoom_5;
        }
        else if (loaiboom == 5)
        {
            ObjBoom = ItemBoom_6;
        }

        return ObjBoom;
    }




    GameObject TAO_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
    {
        GameObject Canon = null;
        GameObject Obj_Canon = XacDinhLoai_PHANDAME(LoaiBoom_PhanDame);


        if (ViTriSinh == 0)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_0.transform) as GameObject;

        }
        else if (ViTriSinh == 1)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_1.transform) as GameObject;

        }
        else if (ViTriSinh == 2)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_2.transform) as GameObject;

        }
        else if (ViTriSinh == 3)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_3.transform) as GameObject;

        }

        return Canon;
    }


    GameObject TAO_BOOM_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
    {
        GameObject itemboom = null;
        GameObject Obj_itemboom = XacDinhLoai_BOOM_PHANDAME(LoaiBoom_PhanDame);

        if (ViTriSinh == 0)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoomUser.transform) as GameObject;
        }
        else if (ViTriSinh == 1)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_1.transform) as GameObject;
        }
        else if (ViTriSinh == 2)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_2.transform) as GameObject;
        }
        else if (ViTriSinh == 3)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_3.transform) as GameObject;
        }

        return itemboom;

    }


    GameObject XacDinhLoai_BOOM_PHANDAME(int loaiboom)
    {
        GameObject ObjBoom = null;

        String name = loaiboom.ToString() + "_efect";
        ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
        LoadBundleLogin.api.UnloadBundle();

        if (ObjBoom == null)
        {
            ObjBoom = Item_SoaiCa;
        }
        return ObjBoom;
    }

    GameObject XacDinhLoai_PHANDAME(int loaiboom)
    {
        GameObject ObjBoom = null;

        String name = loaiboom.ToString() + "_item";
        ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
        LoadBundleLogin.api.UnloadBundle();

        if (ObjBoom == null)
        {
            ObjBoom = SoaiCa;
        }
        return ObjBoom;

    }


    Transform XacDinhViTriNem(int ViTriNem)
    {
        Transform vitri = null;
        if (ViTriNem == 0)
        {
            vitri = ViTriBoomUser.transform;
        }
        else if (ViTriNem == 1)
        {
            vitri = ViTriBoom_1.transform;
        }
        else if (ViTriNem == 2)
        {
            vitri = ViTriBoom_2.transform;
        }
        else if (ViTriNem == 3)
        {
            vitri = ViTriBoom_3.transform;
        }

        return vitri;
    }

    void NemThuong()
    {

    }

    public int GetSfsIdEnemy(int id_ViTriBom)
    {
        int SfsId = -1;
        foreach (var item in EnemySlots)
        {
            if (item.Id_ViTriBom == id_ViTriBom)
            {
                SfsId = item.SfsId;
                return SfsId;
            }
        }
        return SfsId;
    }




    #endregion

    #region GOI PHẢN DAME

    bool CheckPhanDame(string idBangUser_Nem, string idBangUser_BiNem)
    {
        if (idBangUser_BiNem == "")
        {
            return false;
        }

        if (idBangUser_BiNem == idBangUser_Nem)
        {
            return false;
        }

        if (idBangUser_BiNem != IdBangMaster)
        {
            return false;
        }

        return true;
    }



    public void PhanDame(int Enemy, int LoaiBoom)
    {
        SinhBoom_DiChuyenToiViTri(0, Enemy, LoaiBoom);
        StartCoroutine(PrintfAfter(Enemy, LoaiBoom));
    }

    IEnumerator PrintfAfter(int Enemy, int LoaiBoom)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(Enemy, 0, 6, true);
    }


    IEnumerator GoiPhanDame(int vitri_nem, int vitri_binem)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(vitri_nem, vitri_binem, 6);
    }


    int CheckDicThanBai(string idUser)
    {
        int kq = -1;
        if (DataHelper.DicThanBai.ContainsKey(idUser))
        {
            kq = DataHelper.DicThanBai[idUser];
        }
        return kq;
    }



    public void LayThongTinThanBai()
    {
        API.Instance.RequestGetData_userAvoidItem(RspLayThongTinThanBai);
    }


    void RspLayThongTinThanBai(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        DataHelper.DicThanBai.Clear();

        int cou = node.Count;

        for (int i = 0; i < cou; i++)
        {
            string id = node[i]["user_id"].Value;
            int id_item = int.Parse(node[i]["item_id"].Value);
            DataHelper.DicThanBai.Add(id, id_item);
        }
    }

    public void GoiThanBai(int ViTri_Nem, int Vitri_ThanBai, int loaiboom, int loaiThanBai, bool sentRequest = false)
    {
        SinhBoom_DiChuyenToiViTri(ViTri_Nem, Vitri_ThanBai, loaiboom);

        StartCoroutine(WaitTest(ViTri_Nem, Vitri_ThanBai, loaiThanBai, sentRequest));

    }


    IEnumerator WaitTest(int ViTri_Nem, int Vitri_ThanBai, int loaiThanBai, bool sentRequest = false)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(Vitri_ThanBai, ViTri_Nem, 6, sentRequest, loaiThanBai);
    }

    public void TestGoiThanBai(int loaiThanBai)
    {
        GoiThanBai(3, 1, 2, loaiThanBai);
    }




    #endregion

    #endregion



}