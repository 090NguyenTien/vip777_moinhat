﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Diagnostics;

public class PhomEnemySlot : PhomSlot
{

    public GameObject EatenZone;
    public GameObject enemyInfo, BtnInfo, ViTriBom;

    //[HideInInspector]
    public int SlotIndex = PhomConst.INVALID_INDEX;
    //[HideInInspector]
    public bool IsActive = false;
    [SerializeField]
    PopupInfoEnemy InfoEnemy;

    [SerializeField]
    public int Id_ViTriBom;

    void Awake()
    {
        SfsId = PhomConst.INVALID_ID;
        LogoBang.gameObject.SetActive(false);
    }

    #region Process Methods

    public override void ProcessSetInfo(string slotInfo, Hashtable userVariable)
    {
        base.ProcessSetInfo(slotInfo, userVariable);
        IsActive = true;

        if (userVariable != null)
        {
            if (userVariable.ContainsKey("clanid")) this.IdBang = userVariable["clanid"].ToString();//edit khuong;
            if (userVariable.ContainsKey("ringid")) this.ringid = userVariable["ringid"].ToString();
            if (userVariable.ContainsKey("gender")) this.gender = userVariable["gender"].ToString();
            if (userVariable.ContainsKey("gender")) this.partner = userVariable["partner"].ToString();
        }
    }

    public override void Reset()
    {
        base.Reset();
        //IsActive = true;
    }

    #endregion

    #region Animate Methods

    public new void AnimateSetInfo(string slotInfo)
    {
        base.AnimateSetInfo(slotInfo);
        enemyInfo.SetActive(true);
        if (BtnInfo != null)
        {
            BtnInfo.SetActive(true);
        }
        if (ViTriBom != null)
        {
            ViTriBom.SetActive(true);
        }
        GoldObject.SetActive(true);
    }

    public override void AnimateStartTurn(bool isStartGame, List<PhomCard> recommendedEatenCards)
    {
        CountDownTime();
    }

    public override void AnimateEndTurn()
    {
        StopCountDownTime();
    }

    public void AnimateShowCombination(PhomDeck deck)
    {
        foreach (PhomCard card in Combination.GetSortedCards())
        {
            int id = card.Id;
            deck.Cards[id].transform.SetParent(HandZone.transform);
            deck.Cards[id].transform.localPosition = Vector3.zero;
        }
    }

    public void AnimateSortEatenZone()
    {
        foreach (PhomCard card in EatenCards)
        {
            card.transform.SetParent(EatenZone.transform);
            card.transform.localPosition = Vector3.zero;
            card.transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }

        foreach (Transform transform in EatenZone.transform)
            transform.GetChild(PhomConst.CARD_MASK_INDEX).gameObject.GetComponent<Image>().color = PhomColor.EatenCardColor;

        PhomUtil.SortCardsOrderInZone(EatenCards, EatenZone);
        if (SlotIndex == 0)
            AnimateSortEatenZoneForIndex0();
        else if (SlotIndex == 1)
            AnimateSortEatenZoneForIndex1();
        else
            AnimateSortEatenZoneForIndex2();
    }

    public void AnimateSortHandZone()
    {
        PhomUtil.ResetCardsState(Combination.GetSortedCards());
        PhomUtil.SortCardsOrderInZone(Combination.GetSortedCards(), HandZone);
        if (SlotIndex == 0)
            AnimateSortHandZoneForIndex0();
        else if (SlotIndex == 1)
            AnimateSortHandZoneForIndex1();
        else
            AnimateSortHandZoneForIndex2();
    }

    public void AnimateSortLayDownZone(List<PhomCard> cards, GameObject zone)
    {
        if (SlotIndex == 1)
            AnimateSortLayDownZoneForIndex1(cards, zone);
        else
            AnimateSortLayDownZoneForIndex0And2(cards, zone);
    }

    public void AnimateSortLayDownZoneForIndex1(List<PhomCard> cards, GameObject zone)
    {
        if (cards == null)
            return;

        PhomUtil.ResetCardsState(cards);

        foreach (PhomCard card in cards)
        {
            card.transform.SetParent(zone.transform);
            card.transform.localPosition = Vector3.zero;
            card.transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }

        PhomUtil.SortCardsOrderInZone(cards, zone);
        float distance = Screen.width * PhomConst.SMALL_HORIZONTAL_RATIO;
        Vector3 center = zone.transform.position;
        float x = center.x - zone.transform.childCount / 2 * distance;

        if (zone.transform.childCount % 2 == 0)
            x += distance / 2;

        //Vector3 startPoint = zone.transform.position;
        //float x = startPoint.x;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in zone.transform)
        {
            hash["position"] = new Vector3(x, center.y);
            iTween.MoveTo(t.gameObject, hash);
            x += distance;
        }
    }

    public void AnimateSortLayDownZoneForIndex0And2(List<PhomCard> cards, GameObject zone)
    {
        if (cards == null)
            return;

        PhomUtil.ResetCardsState(cards);

        foreach (PhomCard card in cards)
        {
            card.transform.SetParent(zone.transform);
            card.transform.localPosition = Vector3.zero;
            card.transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }

        PhomUtil.SortCardsOrderInZone(cards, zone);
        float distance = Screen.width * PhomConst.SMALL_HORIZONTAL_RATIO;
        Vector3 startPoint = zone.transform.position;
        float x = startPoint.x;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in zone.transform)
        {
            hash["position"] = new Vector3(x, startPoint.y);
            iTween.MoveTo(t.gameObject, hash);
            x += distance;
        }
    }

    public void AnimateSortEatenZoneForIndex0()
    {
        float distance = Screen.height * PhomConst.VERTICAL_RATIO;
        Vector3 center = HandZone.transform.position;
        float y = center.y + HandZone.transform.childCount / 2 * distance;
        //Vector3 startPoint = EatenZone.transform.position;
        //float y = startPoint.y + (EatenZone.transform.childCount - 1) * distance;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in EatenZone.transform)
        {
            hash["position"] = new Vector3(center.x, y);
            iTween.MoveTo(t.gameObject, hash);
            y -= distance;
        }
    }

    public void AnimateSortEatenZoneForIndex1()
    {
        float distance = Screen.width * PhomConst.SMALL_HORIZONTAL_RATIO;
        Vector3 center = HandZone.transform.position;
        float x = center.x - HandZone.transform.childCount / 2 * distance;
        //Vector3 startPoint = EatenZone.transform.position;
        //float x = startPoint.x - (EatenZone.transform.childCount - 1) * distance;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in EatenZone.transform)
        {
            hash["position"] = new Vector3(x, center.y);
            iTween.MoveTo(t.gameObject, hash);
            x += distance;
        }
    }

    public void AnimateSortEatenZoneForIndex2()
    {
        float distance = Screen.height * PhomConst.VERTICAL_RATIO;
        Vector3 center = HandZone.transform.position;
        float y = center.y + HandZone.transform.childCount / 2 * distance;
        //Vector3 startPoint = EatenZone.transform.position;
        //float y = startPoint.y;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in EatenZone.transform)
        {
            hash["position"] = new Vector3(center.x, y);
            iTween.MoveTo(t.gameObject, hash);
            y -= distance;
        }
    }

    public void AnimateSortHandZoneForIndex0()
    {
        float distance = Screen.height * PhomConst.VERTICAL_RATIO * 0.7f;
        Vector3 center = HandZone.transform.position;
        float y = center.y + HandZone.transform.childCount / 2 * distance;

        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in HandZone.transform)
        {
            hash["position"] = new Vector3(center.x, y);
            iTween.MoveTo(t.gameObject, hash);
            y -= distance;
        }
    }

    public void AnimateSortHandZoneForIndex1()
    {
        float distance = Screen.width * PhomConst.SMALL_HORIZONTAL_RATIO;
        Vector3 center = HandZone.transform.position;
        float x = center.x - HandZone.transform.childCount / 2 * distance;

        if (HandZone.transform.childCount % 2 == 0)
            x += distance / 2;

        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in HandZone.transform)
        {
            hash["position"] = new Vector3(x, center.y);
            iTween.MoveTo(t.gameObject, hash);
            x += distance;
        }
    }

    public void AnimateSortHandZoneForIndex2()
    {
        float distance = Screen.height * PhomConst.VERTICAL_RATIO * 0.7f;
        Vector3 center = HandZone.transform.position;
        float y = center.y + HandZone.transform.childCount / 2 * distance;
        //Vector3 startPoint = HandZone.transform.position;
        //float y = startPoint.y;
        Hashtable hash = new Hashtable();
        hash.Add("time", 0.1f);
        hash.Add("position", null);
        hash.Add("easetype", iTween.EaseType.linear);

        foreach (Transform t in HandZone.transform)
        {
            hash["position"] = new Vector3(center.x, y);
            iTween.MoveTo(t.gameObject, hash);
            y -= distance;
        }
    }

    public void AnimateExit(PhomDeck deck)
    {
        LogoBang.gameObject.SetActive(false);
        //enemyInfo.SetActive(false);
        if (BtnInfo != null)
        {
            BtnInfo.SetActive(false);
        }
        if (ViTriBom != null)
        {
            ViTriBom.SetActive(false);
        }
        UserName.gameObject.SetActive(false);
        Gold.gameObject.SetActive(false);
        GoldObject.SetActive(false);
        if (inviteBtn != null)
            inviteBtn.gameObject.SetActive(true);
        Avatar.gameObject.SetActive(false);
        AvatarBorder.gameObject.SetActive(false);

        TimerImage.SetActive(false);
        RoundStarterImage.gameObject.SetActive(false);

        foreach (PhomCard card in EatenCards)
        {
            card.transform.SetParent(deck.transform);
            card.transform.localPosition = Vector3.zero;
        }

        foreach (PhomCard card in DiscardedCards)
        {
            card.transform.SetParent(deck.transform);
            card.transform.localPosition = Vector3.zero;
        }

        foreach (PhomCard card in UpperLaidDownCards)
        {
            card.transform.SetParent(deck.transform);
            card.transform.localPosition = Vector3.zero;
        }

        foreach (PhomCard card in LowerLaidDownCards)
        {
            card.transform.SetParent(deck.transform);
            card.transform.localPosition = Vector3.zero;
        }

        if (Combination != null && Combination.GetSortedCards() != null)
        {
            foreach (PhomCard card in Combination.GetSortedCards())
            {
                card.transform.SetParent(deck.transform);
                card.transform.localPosition = Vector3.zero;
            }
        }

        IsActive = false;
    }

    #endregion


    bool isNem = false;
    IEnumerator DelayNemCoroutine()
    {
        yield return new WaitForSeconds(2);
        isNem = false;
    }
    public void NemBomVaoEnemy(int vitri)
    {
        if (isNem) return;
        StartCoroutine(DelayNemCoroutine());
        isNem = true;
        //  controller.NemBoomControll(0, 1);
		if (IsActive == true)
        {
            Sprite Ava = this.Avatar.sprite;
            Sprite Khung = this.AvatarBorder.sprite;
            string ten = this.UserName.text;
            string vip = this.Vip;
            string level = this.txtLevel.text;
            string chip = this.Gold.text;
            string gem = Utilities.GetStringMoneyByLong(this.gem);
            string IDBANG = this.IdBang;
            string gioitinh = this.gender;
            int id_gioitinh = int.Parse(gioitinh);
            string ten_nguoi_tinh = this.partner;
            //  Debug.Log("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem);
            InfoEnemy.Init(UserId, Ava, Khung, ten, vip, level, chip, gem, vitri, IDBANG,id_gioitinh, ten_nguoi_tinh, SfsId);
        }


    }

}