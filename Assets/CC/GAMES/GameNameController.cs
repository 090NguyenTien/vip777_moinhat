﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameNameController : MonoBehaviour {

    private void Awake()
    {
        gameObject.GetComponent<Text>().text = GameHelper.getGameName();
        Destroy(this);
    }
}
