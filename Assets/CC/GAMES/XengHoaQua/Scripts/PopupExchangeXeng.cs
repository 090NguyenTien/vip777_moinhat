﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupExchangeXeng : MonoBehaviour {

    [SerializeField]
    Text txtGem, txtEchangePoint, txtPoint, txtExchangeGem;
    [SerializeField]
    XengController xengController;
    [SerializeField]
    HoldButtonHelper holdButtonHelper;
    void Start()
    {
        
    }
	public void Show()
    {
        gameObject.SetActive(true);
        UpdateUI();
    }
    public void Hide()
    {
        xengController.UpdateInfoBar();
        gameObject.SetActive(false);        
    }
    public void UpGem()
    {
        UpGemUI();
        holdButtonHelper.Remove();
    }
    void UpGemUI(int param=-1)
    {
        int val = int.Parse(txtGem.text);
        if (val >= MyInfo.GEM) return;
        txtGem.text = (int.Parse(txtGem.text) + 1).ToString();
        txtEchangePoint.text = (int.Parse(txtGem.text) * XengController.GemToPoint).ToString();
    }
    public void HoldUpGem()
    {
        holdButtonHelper.Run(UpGemUI);
    }


    public void DownGem()
    {
        DownGemUI();
        holdButtonHelper.Remove();
    }
    void DownGemUI(int param=-1)
    {
        int val = int.Parse(txtGem.text);
        if (val < 1) return;
        txtGem.text = (int.Parse(txtGem.text) - 1).ToString();
        txtEchangePoint.text = (int.Parse(txtGem.text) * XengController.GemToPoint).ToString();
    }
    public void HoldDownGem()
    {
        holdButtonHelper.Run(DownGemUI);
    }
    public void ExchangeGemToPoint()
    {
        if (int.Parse(txtGem.text) < 1)
        {
            AlertController.api.showAlert("Chọn Số Kim Cương Bạn Muốn Quy Đổi!");
            return;
        }
        API.Instance.RequestGemToPoint(int.Parse(txtGem.text),GemToPointExchangeComplete);
    }

    private void GemToPointExchangeComplete(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" GemToPointExchangeComplete " + _json);
        if (node["status"].AsInt == 1)
        {
            MyInfo.BAR_POINT = node["bar_point"].AsInt;
            MyInfo.GEM = node["gem"].AsInt;
            AlertController.api.showAlert("Quy Đổi Thành Công!");
            UpdateUI();
        }
    }
    void UpdateUI()
    {
        txtGem.text = "0";
        txtEchangePoint.text = "0";
        txtPoint.text = "0";
        txtExchangeGem.text = "0";
    }
    public void UpPoint()
    {
        UpPointUI();       
        holdButtonHelper.Remove();
    }
    void UpPointUI(int param=-1)
    {
        int val = int.Parse(txtPoint.text);
        if (val >= MyInfo.BAR_POINT) return;
        txtPoint.text = (int.Parse(txtPoint.text) + XengController.PointToGem).ToString();
        txtExchangeGem.text = (int.Parse(txtPoint.text) / XengController.PointToGem).ToString();
    }
    
    public void HoldUpPoint()
    {
        holdButtonHelper.Run(UpPointUI);
    }

    public void DownPoint()
    {
        DownPointUI();
        holdButtonHelper.Remove();
    }
    void DownPointUI(int param=-1)
    {
        int val = int.Parse(txtPoint.text);
        if (val < XengController.PointToGem) return;
        txtPoint.text = (int.Parse(txtPoint.text) - XengController.PointToGem).ToString();
        txtExchangeGem.text = (int.Parse(txtPoint.text) / XengController.PointToGem).ToString();
    }
    public void HoldDownPoint()
    {
        holdButtonHelper.Run(DownPointUI);
    }
    public void ExchangePointToGem()
    {
        if (int.Parse(txtPoint.text) < 1)
        {
            AlertController.api.showAlert("Chọn Số Điểm Bạn Muốn Quy Đổi!");
            return;
        }
        API.Instance.RequestPointToGem(int.Parse(txtPoint.text), PointToGemExchangeComplete);
    }
    private void PointToGemExchangeComplete(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" GemToPointExchangeComplete " + _json);
        if (node["status"].AsInt == 1)
        {
            MyInfo.BAR_POINT = node["bar_point"].AsInt;
            MyInfo.GEM = node["gem"].AsInt;
            AlertController.api.showAlert("Quy Đổi Thành Công!");
            UpdateUI();
        }
    }
}
