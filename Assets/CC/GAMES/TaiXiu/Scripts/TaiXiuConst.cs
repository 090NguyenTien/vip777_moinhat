﻿using UnityEngine;
using System.Collections;

public class TaiXiuConst {

    public const int ROUND_TIME = 15;//40; // 10 - 2
    public const int ROUND_TIME_BAUCUA = 15;//xai ke Const cua Tai Xiu Nha Luoi Code Class moi Qua :D
    public const int BREAK_TIME = 8;

    public const int XIU = -1;
    public const int TAI = 0;
    public const int CONTAIN_1 = 1;
    public const int CONTAIN_2 = 2;
    public const int CONTAIN_3 = 3;
    public const int CONTAIN_4 = 4;
    public const int CONTAIN_5 = 5;
    public const int CONTAIN_6 = 6;
    public const int TOTAL_4 = 7;
    public const int TOTAL_5 = 8;
    public const int TOTAL_6 = 9;
    public const int TOTAL_7 = 10;
    public const int TOTAL_8 = 11;
    public const int TOTAL_9 = 12;
    public const int TOTAL_10 = 13;
    public const int TOTAL_11 = 14;
    public const int TOTAL_12 = 15;
    public const int TOTAL_13 = 16;
    public const int TOTAL_14 = 17;
    public const int TOTAL_15 = 18;
    public const int TOTAL_16 = 19;
    public const int TOTAL_17 = 20;

    public const long BET_MONEY_1 = 1000;
    public const long BET_MONEY_2 = 5000;
    public const long BET_MONEY_3 = 10000;
    public const long BET_MONEY_4 = 100000;
    public const long BET_MONEY_5 = 500000;
    public const long BET_MONEY_6 = 1000000;
    public const long BET_MONEY_7 = 2000000;
    public const long BET_MONEY_8 = 5000000;

    public const long BET_MONEY_9  = 10000000;
    public const long BET_MONEY_10 = 100000000;
    public const long BET_MONEY_11 = 500000000;
}
