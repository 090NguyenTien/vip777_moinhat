﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using System.Linq;
using DG.Tweening;
using Facebook.Unity;
public class TaiXiuRoomController : MonoBehaviour {

    #region Properties

    private SFS sfs;
    private long ChoosingBetMoney;
    private int TimeLeft;

    private bool IsRollingDice1;
    private bool IsRollingDice2;
    private bool IsRollingDice3;
    private bool IsAnimatingEndGame;
    private bool IsShowingMessage;
    private List<bool> IsGettedMoneyList;
    private Dictionary<int, long> CurrentAllBets;

    
    private long PlayerBettedMoney;
    private string LastGameResult;
    private string LastDiceResult;
    private long LastBettedMoney;
    private long LastWonMoney;
    private bool GameEnded;
    private Coroutine countDownRoutine;

    public Text Timer;
    public Image Dice1;
    public Image Dice2;
    public Image Dice3;
    public List<Button> BetButtons;
    public List<Button> BetTypeButtons;
    public Sprite[] DiceSprites;
    public TaiXiuBetChipManager BetChipManager;
    public Text PlayerName;
    public Text PlayerChip;
    public Image PlayerAvatar, AvatarBorder, AvatarBorder_Defaut;
    public Image PlayerAvatarBorder;
    public Text BettedMoney;
    public Text WinMoney;
    public GameObject HistoryPopupPanel;
    public Image HistoryDice1;
    public Image HistoryDice2;
    public Image HistoryDice3;
    public GameObject GrayPanel;
    public Text CheatedDiceResultText;
    public GameObject MessagePanel;

    public GameObject[] DiceResultPoints;
    public GameObject[] DiceRollPoints;
    public GameObject[] OtherPlayerPlaces;

    [SerializeField]
    PopupAlertManager popupAlert;

    public RectTransform Canvas;
    private float Scale;

    #endregion

    #region Unity

    void Awake()
    {
        sfs = SFS.Instance;
        CurrentAllBets = new Dictionary<int, long>();
    }

    void Start()
    {
        Scale = Canvas.lossyScale.x;
        DisableAllBetButtons();
        EnableButton(BetButtons[0]);
        ChoosingBetMoney = 10000;
        PlayerBettedMoney = 0;
        BettedMoney.text = Utilities.GetStringMoneyByLongBigSmall(PlayerBettedMoney);
        WinMoney.text = "0";
        IsAnimatingEndGame = false;
        LastGameResult = string.Empty;
        LastDiceResult = string.Empty;
        LastBettedMoney = 0;
        LastWonMoney = 0;
        popupAlert.Init();
        LoadingManager.Instance.ENABLE = false;
        SendGetGameRoomInfoRequest();
    }

    #endregion

    #region On Click

    public void OnBetTypeButtonClick(Button button)
    {
        if (IsAnimatingEndGame)
            return;

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        if (MyInfo.CHIP < ChoosingBetMoney)
        {            
            if (!IsShowingMessage)
                StartCoroutine(AnimateShowMessage("Bạn không đủ tiền để đặt."));
            return;
        }

        int betType = int.Parse(button.transform.GetChild(0).GetComponent<Text>().text);
        int serverBetType = TXUtil.ToServerBetType(betType);
        SendBetRequest(serverBetType, ChoosingBetMoney);

        long val;
        if (CurrentAllBets.TryGetValue(serverBetType, out val))
        {
            CurrentAllBets[serverBetType] = val + ChoosingBetMoney;
        }
        else
        {
            CurrentAllBets.Add(serverBetType, ChoosingBetMoney);
        }

        StartCoroutine(AnimatePlayerBetMoney(serverBetType, ChoosingBetMoney));
    }

    private float timeCheck = 1f;
    private float timeCheckConfigMin = 1f;
    private float timeCheckConfigMax = 2f;
    private float lastTimeUpdate = 0;
    private void Update()
    {
        if (TimeLeft > 0 && TimeLeft < TaiXiuConst.ROUND_TIME - 2)//con thoi gian
        {
            if (Time.realtimeSinceStartup - lastTimeUpdate >= timeCheck)
            {
                timeCheck = UnityEngine.Random.Range(timeCheckConfigMin, timeCheckConfigMax);
                //timeCheck = 0.5f;
                lastTimeUpdate = Time.realtimeSinceStartup;
                int numPlayer = UnityEngine.Random.Range(0, 5 + 1);


                for (int i = 0; i < numPlayer; i++)
                {
                    StartCoroutine(botWaiting());
                }
            }
        }
    }
    private IEnumerator botWaiting()
    {
        float waitingTime = UnityEngine.Random.Range(0, 2);
        yield return new WaitForSeconds(waitingTime);
        int betType = UnityEngine.Random.Range(0, 1 + 1);

        //Debug.Log("bettype " + betType);


        if (betType == 0)//random -1 ->6
        {
            betType = UnityEngine.Random.Range(-1, 6 + 1);
        }
        else if (betType == 1)//random -1 ->6
        {
            betType = UnityEngine.Random.Range(14, 27 + 1);
        }
        //Debug.Log("bettype " + betType);
        int moneyType = UnityEngine.Random.Range(1, 5 + 1);
        long money = 10000;
        if (moneyType == 2)
        {
            money = 100000;
        }
        else if (moneyType == 3)
        {
            money = 500000;
        }
        else if (moneyType == 4)
        {
            money = 1000000;
        }
        else if (moneyType == 5)
        {
            money = 10000000;
        }
        yield return AnimatePlayerBetMoney(betType, money, true);
    }
    public void OnBetMoneyButtonClick(Button button)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        DisableAllBetButtons();
        EnableButton(button);
        string betMoneyText = button.transform.GetChild(1).GetComponent<Text>().text;
        ChoosingBetMoney = TXUtil.ConvertShortTextToMoney(betMoneyText);
    }

    public void OnHistoryButtonClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        if (!HistoryPopupPanel.activeSelf)
            TaiXiuHistoryManager.Instance.Show();
        else
            TaiXiuHistoryManager.Instance.Hide();
    }

    public void OnCheatButtonClick()
    {
        SendCheatRequest(CheatedDiceResultText.text.Trim());
    }

    public void OnExitClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        if (HistoryPopupPanel.activeSelf)
        {
            OnHistoryButtonClick();
            return;
        }

        SendExitRoomRequest();
        LoadingManager.Instance.ENABLE = true;
    }

    #endregion

    #region Send Request

    private void SendJoinLuckyGameRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.JOIN_LUCKY_GAME);
        sfs.SendZoneRequest(gp);
    }

    private void SendGetGameRoomInfoRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.GET_GAME_ROOM_INFO);
        sfs.SendRoomRequest(gp);
    }

    private void SendBetRequest(int betType, long betMoney)
    {
        GamePacket gp = new GamePacket(CommandKey.BET);
        gp.Put(ParamKey.BET_TYPE, betType);
        gp.Put(ParamKey.BET_MONEY, betMoney);
        sfs.SendRoomRequest(gp);
    }

    private void SendCheatRequest(string cheatedDiceResult)
    {
        //GamePacket gp = new GamePacket(CommandKey.CHEAT);
        //gp.Put(ParamKey.CHEATED_DICE_RESULT, cheatedDiceResult);
        //sfs.SendZoneRequest(gp);
    }

    private void SendExitRoomRequest()
    {
        sfs.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));
    }

    #endregion










    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.JOIN_GAME:
                SendGetGameRoomInfoRequest();
                break;
            case CommandKey.GET_GAME_ROOM_INFO:
                StartCoroutine(AnimateGetGameRoomInfo(gp));
                break;
		case CommandKey.UPDATE:
			Debug.Log ("IS OK - " + !IsAnimatingEndGame);
                if (!IsAnimatingEndGame)
                    StartCoroutine(AnimateUpdate(gp));
                break;
            case CommandKey.END_GAME:
                print("END_GAME: " + DateTime.Now);
                if (!IsShowingMessage)
                    StartCoroutine(AnimateEndGame(gp));
                break;
            case CommandKey.BET:
                //StartCoroutine(AnimatePlayerBetMoney(gp));
                break;
            case CommandKey.USER_EXIT:
                SoundManager.PlaySound(SoundManager.EXIT_ROOM);
                
                GameHelper.ChangeScene(GameScene.HomeSceneV2);
                
                break;
        }
    }






    #region Animate
	public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
	{
		Texture2D mainImage;
		WWW www = new WWW(_url);
		yield return www;

		mainImage = www.texture;
		avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
	}
    private IEnumerator AnimateGetGameRoomInfo(GamePacket gp)
    {
        Debug.LogWarning("GPPPP" + gp.param.ToJson());
        SoundManager.PlaySound(SoundManager.ENTER_ROOM);

        MyInfo.CHIP = gp.GetLong(ParamKey.CHIP);
        PlayerChip.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP);
        
        PlayerName.text = MyInfo.NAME;
		LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
		// Debug.LogError("KhuongTest==========" + loginType);
		if (loginType != LoginType.Facebook) {
            //PlayerAvatar.sprite = DataHelper.GetAvatar(MyInfo.AvatarName);
            StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + MyInfo.AvatarName, PlayerAvatar));
        } else {
			string avatarUrl = "https://graph.facebook.com/me/picture?type=normal&access_token=" + AccessToken.CurrentAccessToken.TokenString;
			StartCoroutine (UpdateAvatarThread (avatarUrl, PlayerAvatar));
		}
        //PlayerAvatar.sprite = DataHelper.GetAvatar(MyInfo.AvatarName);
        //PlayerAvatarBorder.sprite = DataHelper.GetVip(MyInfo.VIPPOINT);




        int id_Bor = MyInfo.MY_BOR_AVATAR;
        if (id_Bor != -1)
        {
            Sprite h = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
            if (h != null)
            {
                AvatarBorder.sprite = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
                AvatarBorder.gameObject.SetActive(true);
                AvatarBorder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                AvatarBorder.gameObject.SetActive(false);
                AvatarBorder_Defaut.gameObject.SetActive(true);
            }

        }
        else
        {
            AvatarBorder.gameObject.SetActive(false);
            AvatarBorder_Defaut.gameObject.SetActive(true);
        }






        string betData = gp.GetString(ParamKey.BET_DATA);

        if (!string.IsNullOrEmpty(betData))
        {
            Dictionary<int, long> bets = TXUtil.ConvertStringToBets(betData);

            foreach (KeyValuePair<int, long> bet in bets)
            {
                StartCoroutine(AnimatePlayerReBetMoney(bet));
                PlayerBettedMoney += bet.Value;
            }
            BettedMoney.text = Utilities.GetStringMoneyByLongBigSmall(PlayerBettedMoney);
        }

        TimeLeft = gp.GetInt(ParamKey.CURRENT_TIME) - 2; // Duy add -2
        
        if (TimeLeft == -1 || TimeLeft == -2) // 1 - 2 = -1
            TimeLeft += TaiXiuConst.ROUND_TIME + TaiXiuConst.BREAK_TIME + 2;
        
        print("TimeLeft: " + TimeLeft);

        if (TimeLeft > TaiXiuConst.ROUND_TIME)
        {
            IsAnimatingEndGame = true;
            while (TimeLeft > TaiXiuConst.ROUND_TIME)
            {
                StartCoroutine(AnimateShowMessage("Vui lòng chờ " + (TimeLeft - TaiXiuConst.ROUND_TIME) + " giây"));
                TimeLeft -= 1;
                yield return new WaitForSeconds(1f);
            }
            IsAnimatingEndGame = false;
        }

        if (countDownRoutine != null)
        {
            StopCoroutine(countDownRoutine);
            countDownRoutine = null;
        }
        countDownRoutine = StartCoroutine(AnimateCountDownTime());
        yield return new WaitForSeconds(1f);
        Timer.gameObject.SetActive(true);
    }

    private IEnumerator AnimateUpdate(GamePacket gp)
    {
		Debug.Log (" F JFJLJLKJJL");
        Dictionary<int, long> oldCurrentAllBets = new Dictionary<int, long>(CurrentAllBets);

        string detailTotalBets = gp.GetString(ParamKey.DETAIL_TOTAL_BETS);
        CurrentAllBets = TXUtil.ConvertStringToBets(detailTotalBets);

        foreach (KeyValuePair<int, long> pair in CurrentAllBets)
            StartCoroutine(AnimateAllPlayerBetMoreMoney(pair.Key,
                TXUtil.GetDifferenceMoney(oldCurrentAllBets, CurrentAllBets, pair.Key)));

        yield return null;
    }

    private IEnumerator AnimateEndGame(GamePacket gp)
    {
        IsAnimatingEndGame = true;
        dicMoneyBet.Clear();
        GameEnded = true;
        TimeLeft = 0;
        Timer.gameObject.SetActive(false);

        string strDiceResult = gp.GetString(ParamKey.DICE_RESULT);
        string gameResult = gp.GetString(ParamKey.GAME_RESULT);
        LastDiceResult = strDiceResult;
        LastGameResult = gameResult;

        int[] diceResult = TXUtil.ConvertStringToDiceResult(strDiceResult);

        //yield return new WaitForSeconds(1f);
        IsRollingDice1 = false;
        IsRollingDice2 = false;
        IsRollingDice3 = false;
  
        StartCoroutine(AnimateSlowRollDice(Dice1, diceResult[0], 0));      
        StartCoroutine(AnimateSlowRollDice(Dice2, diceResult[1], 1));
        StartCoroutine(AnimateSlowRollDice(Dice3, diceResult[2], 2));

        yield return new WaitForSeconds(2f);
        GrayPanel.SetActive(false);
        // Highlight win boxes
        HighlightWinButtons(diceResult[0], diceResult[1], diceResult[2]);

        if (string.IsNullOrEmpty(LastGameResult))
        {
            UpdateHistoryPanel();
            yield return new WaitForSeconds(3f);
        }
        else
        {
            IsGettedMoneyList = new List<bool>();
            //StartCoroutine(AnimatePlayerGetAllMoney());

            long changedMoney = GetChangedMoney(gameResult);
            long winMoney = changedMoney;

            StartCoroutine(AnimatePlayerGetAllMoney(winMoney));
            /*
            if (winMoney > 0)
            {
                WinMoney.text = TXUtil.ConvertMoneyToText(winMoney);
            }
            else
            {
                WinMoney.text = TXUtil.ConvertMoneyToText(0);
            }
            */
            LastWonMoney = winMoney;

            LastBettedMoney = PlayerBettedMoney;

            if (winMoney > PlayerBettedMoney)
                SoundManager.PlaySound(SoundManager.WIN);
            else if (winMoney < PlayerBettedMoney)
                SoundManager.PlaySound(SoundManager.LOSE);

            UpdateHistoryPanel();

            float waitTime = 0;
            while (IsGettedMoneyList[IsGettedMoneyList.Count - 1] != true)
            {
                waitTime += 0.1f;
                yield return new WaitForSeconds(0.1f);
            }

            PlayerBettedMoney = 0;
            MyInfo.CHIP = GetPlayerMoneyAfterEndGame(gameResult);
            yield return new WaitForSeconds(3 - waitTime);

            
            if (winMoney > 0)
            {
                WinMoney.text = Utilities.GetStringMoneyByLongBigSmall(winMoney);
            }
            else
            {
                WinMoney.text = "0";
            }
            
        }

        yield return new WaitForSeconds(2f);
        TimeLeft = TaiXiuConst.ROUND_TIME;

        if (countDownRoutine != null)
        {
            StopCoroutine(countDownRoutine);
            countDownRoutine = null;
        }
        countDownRoutine = StartCoroutine(AnimateCountDownTime());

        yield return new WaitForSeconds(1f);

        RemoveChipsInButtons();

        WinMoney.text = "0";
        BettedMoney.text = "0";
        PlayerChip.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP);
        //MyInfo.CheckMoney(PlayerChip.gameObject);
        Timer.gameObject.SetActive(true);
        

        IsAnimatingEndGame = false;
    }

    private IEnumerator AnimateCountDownTime()
    {
        while (TimeLeft > 0)
        {
            TimeLeft -= 1;
            yield return new WaitForSeconds(1);
            Timer.text = TimeLeft.ToString();
            if (Timer.gameObject.activeSelf && TimeLeft > 0 && TimeLeft < 10)
                SoundManager.PlaySound(SoundManager.COUNT_DOWN);
        }

        IsAnimatingEndGame = true;
        GrayPanel.SetActive(true);
        UnHighlightAllButtons();
        Dice1.transform.parent.SetSiblingIndex(GrayPanel.transform.GetSiblingIndex() + 1);

        Timer.gameObject.SetActive(false);

        Dice1.transform.DOScale(0.9f, 0);
        Dice2.transform.DOScale(0.9f, 0);
        Dice3.transform.DOScale(0.9f, 0);

        Dice1.gameObject.SetActive(true);
        Dice2.gameObject.SetActive(true);
        Dice3.gameObject.SetActive(true);
        //IsRollingDice1 = true;
        //IsRollingDice2 = true;
        //IsRollingDice3 = true;

        Dice1.transform.position = DiceRollPoints[0].transform.position;
        Dice2.transform.position = DiceRollPoints[1].transform.position;
        Dice3.transform.position = DiceRollPoints[2].transform.position;

        GameEnded = false;
        //StartCoroutine(AnimateKickOut());
        StartCoroutine(AnimateRollDice1());
        StartCoroutine(AnimateRollDice2());
        StartCoroutine(AnimateRollDice3());
    }

    private IEnumerator AnimateAllPlayerBetMoreMoney(int type, long differenceMoney)
    {
		Debug.Log ("l kdsjflsk fsl sls");
        int clientBetType = TXUtil.ToClientBetType(type);
        Button button = BetTypeButtons[clientBetType + 1];

        yield return new WaitForSeconds(0.01f);

        long[] numOfAllChipTypes = TXUtil.ConvertMoneyToNumOfAllChipTypes(differenceMoney);

        for (int i = numOfAllChipTypes.Count() - 1; i >= 0; i--)
        {
            for (int j = 0; j < numOfAllChipTypes[i]; j++)
            {
                string text = TXUtil.ConvertIndexToChipText(i);
                TaiXiuBetChip chip = BetChipManager.CreateBetChip(i, text);
                chip.transform.SetParent(OtherPlayerPlaces[UnityEngine.Random.Range(0, 8)].transform); // test
                chip.transform.localPosition = Vector3.zero;
                chip.transform.localScale = new Vector3(1, 1, 1);

                chip.transform.SetParent(button.transform.GetChild(3).transform);

                Vector3 position = button.transform.GetChild(3).transform.position;
                float x = position.x;
                float y = position.y;
                int childCount = button.transform.GetChild(3).transform.childCount;

                if (childCount > 16)
                    y += 45 * Scale;
                else if (childCount > 0)
                    y += childCount * 3 * Scale;

                //chip.transform.DOMove(new Vector3(x, y, 1f), 0.7f, false).SetEase(Ease.InSine);
				chip.transform.DOMove(new Vector3(x, y, 1f), 0.7f);//.SetEase(Ease.InSine);
                yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.3f));
            }
            yield return new WaitForSeconds(UnityEngine.Random.Range(0f, 0.3f));
        }

        yield return new WaitForSeconds(0.7f);
        StartCoroutine(AnimateSortChipsInBetPlace(button.transform.GetChild(3)));
        yield return null;
    }

    private IEnumerator AnimatePlayerReBetMoney(KeyValuePair<int, long> bet)
    {
        int clientBetType = TXUtil.ToClientBetType(bet.Key);
        Button button = BetTypeButtons[clientBetType + 1];

        //foreach (Transform t in button.transform.GetChild(2).transform)
        //    Destroy(t.gameObject);

        //yield return new WaitForSeconds(0.01f);

        long[] numOfAllChipTypes = TXUtil.ConvertMoneyToNumOfAllChipTypes(bet.Value);

        for (int i = numOfAllChipTypes.Count() - 1; i >= 0; i--)
        {
            for (int j = 0; j < numOfAllChipTypes[i]; j++)
            {
                string text = TXUtil.ConvertIndexToChipText(i);
                TaiXiuBetChip betChip = BetChipManager.CreateBetChip(i, text);
                betChip.transform.SetParent(button.transform.GetChild(3).transform);
                betChip.transform.localPosition = Vector3.zero;
                betChip.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        StartCoroutine(AnimateSortChipsInBetPlace(button.transform.GetChild(3)));
        yield return null;
    }

    [SerializeField]
    private Text txtTextBetPref = null;
    private Dictionary<int, long> dicMoneyBet = new Dictionary<int, long>();
    private IEnumerator AnimatePlayerBetMoney(int betType, long betMoney, bool isbotSimulate = false)
    {
        if (isbotSimulate == false)
        {
            
            Debug.LogWarning("MyInfo.CHIP==hahahaahahahah=========" + MyInfo.CHIP);
            if (MyInfo.CHIP >= betMoney)
            {
                MyInfo.CHIP -= betMoney;
                PlayerChip.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP);
                PlayerBettedMoney += betMoney;
                BettedMoney.text = Utilities.GetStringMoneyByLongBigSmall(PlayerBettedMoney);
            }
           
        }
       

        //Debug.Log("bettype " + betType);
        int clientBetType = TXUtil.ToClientBetType(betType);
       
        Button button = GetButtonByBetType(clientBetType);
        //Debug.LogError("TaiXiu=======betMoney========" + betMoney);
        int index = TXUtil.ConvertBetMoneyToSpriteIndex(betMoney);

        string chipText = TXUtil.ConvertMoneyToShortText(ChoosingBetMoney);
        TaiXiuBetChip chip = BetChipManager.CreateBetChip(index, chipText, isbotSimulate);
        chip.transform.SetParent(BetButtons[index].transform);
        chip.transform.localPosition = Vector3.zero;
        chip.transform.localScale = new Vector3(1, 1, 1);

        Transform betcontainer = button.transform.GetChild(3).transform;

        if (isbotSimulate == false)
        {
            bool isFirstBetInRound = false;
            if (dicMoneyBet.ContainsKey(betType) == false)
            {
                isFirstBetInRound = true;
                dicMoneyBet.Add(betType, betMoney);
            }
            else
            {
                dicMoneyBet[betType] = dicMoneyBet[betType] + betMoney;
            }

            Text txtTextBet = null;
            if (isFirstBetInRound == true)
            {
                txtTextBet = Instantiate(txtTextBetPref, betcontainer);
                txtTextBet.rectTransform.anchoredPosition = Vector2.zero;
            }
            else
            {
                for (int i = 0; i < betcontainer.childCount; i++)
                {
                    txtTextBet = betcontainer.GetChild(i).GetComponent<Text>();
                    if (txtTextBet != null)
                    {
                        break;
                    }
                }
                
            }
             txtTextBet.text = Utilities.GetStringMoneyByLongBigSmall(dicMoneyBet[betType]);
        }
        

       

        chip.transform.SetParent(betcontainer);

        Vector3 position = betcontainer.position;
        float x = position.x + 0.1f;
        if (isbotSimulate == true)
        {
            x = position.x - 0.1f;
        }
        float y = position.y;
        int childCount = countBetMoney(betcontainer, isbotSimulate);

        if (childCount > 16)
        {
            y += 45 * Scale;
        }
        else if (childCount > 0)
        {
            y += childCount * 3 * Scale;
        }

        chip.transform.DOMove(new Vector2(x, y), .2f).OnComplete(() => chip.transform.localScale = Vector3.one * 0.7f);
		//chip.transform.DOScale(Vector2.one * 0.7f, .2f);
        yield return new WaitForSeconds(0.2f);
        StartCoroutine(AnimateSortChipsInBetPlace(betcontainer));
        yield return null;
    }

    private int countBetMoney(Transform container, bool countBot)
    {
        int result = 0;
        for (int i = 0; i < container.childCount; i++)
        {
            TaiXiuBetChip tmpBet = container.GetChild(i).GetComponent<TaiXiuBetChip>();
            if (tmpBet != null)
            {
                if (tmpBet.isBot == countBot)
                {
                    ++result;
                }
            }
        }
        return result;
    }

    private IEnumerator AnimateSortChipsInBetPlace(Transform betPlace)
    {
        //float distance = 3 * Scale;
        //Vector2 center = betPlace.transform.position;
        //float y = center.y + 3 * Scale;

        //Hashtable hash = new Hashtable();
        //hash.Add("time", 0.1f);
        //hash.Add("position", Vector2.zero);

        //foreach (Transform t in betPlace.transform)
        //{
        //    if (y >= center.y + 45 * Scale)
        //        hash["position"] = new Vector2(center.x, center.y + 45 * Scale);
        //    else
        //        hash["position"] = new Vector2(center.x, y);

        //    iTween.MoveTo(t.gameObject, hash);
        //    iTween.ScaleTo(t.gameObject, iTween.Hash("scale", Vector3.one * 0.7f, "time", 0.1f) );
        //    y += distance;
        //}

        yield return null;
    }

    private IEnumerator AnimateRollDice1()
    {
        UnityEngine.Random.seed = 9001;
		var timeLeft = 5f;
		Dice1.transform.DOShakeRotation (5, new Vector3(0, 0, 180), 20, 90);
		while (timeLeft > 0)
		{
            if (IsRollingDice1)
            {
                int index = UnityEngine.Random.Range(0, 6);
                Dice1.sprite = DiceSprites[index];
                timeLeft -= 0.1f;
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                timeLeft = 0;
            }
		}
		/*RectTransform.sprite = DiceSprites[diceResult - 1];
        while (IsRollingDice1)
        {
            int index = UnityEngine.Random.Range(0, 6);
            Dice1.sprite = DiceSprites[index];
            RectTransform rectTransform = Dice1.GetComponent<RectTransform>();
            rectTransform.Rotate(new Vector3(0, 0, 30));
            yield return new WaitForSeconds(0.01f);
        }*/
    }

    private IEnumerator AnimateRollDice2()
    {
		UnityEngine.Random.seed = 9001;
		var timeLeft = 5f;
		Dice2.transform.DOShakeRotation (5, new Vector3(0, 0, 180), 20, 90);
		while (timeLeft > 0)
		{
            if (IsRollingDice1)
            {
                int index = UnityEngine.Random.Range(0, 6);
                Dice2.sprite = DiceSprites[index];
                timeLeft -= 0.1f;
                yield return new WaitForSeconds(0.1f);
            }
            else
            {
                timeLeft = 0;
            }
		}
        /*UnityEngine.Random.seed = 9002;
        while (IsRollingDice2)
        {
            int index = UnityEngine.Random.Range(0, 6);
            Dice2.sprite = DiceSprites[index];
            RectTransform rectTransform = Dice2.GetComponent<RectTransform>();
            rectTransform.Rotate(new Vector3(0, 0, 30));
            yield return new WaitForSeconds(0.01f);
        }*/
    }

    private IEnumerator AnimateRollDice3()
    {
		UnityEngine.Random.seed = 9001;
		var timeLeft = 5f;
		Dice3.transform.DOShakeRotation (5, new Vector3(0, 0, 180), 20, 90);
		while (timeLeft > 0)
		{
			if (IsRollingDice1) {
				int index = UnityEngine.Random.Range (0, 6);
				Dice3.sprite = DiceSprites [index];			
				timeLeft -= 0.1f;
				yield return new WaitForSeconds (0.1f);
			}else
				timeLeft = 0;
		}
        /*UnityEngine.Random.seed = 9003;
        while (IsRollingDice3)
        {
            int index = UnityEngine.Random.Range(0, 6);
            Dice3.sprite = DiceSprites[index];
            RectTransform rectTransform = Dice3.GetComponent<RectTransform>();
            rectTransform.Rotate(new Vector3(0, 0, 30));
            yield return new WaitForSeconds(0.01f);
        }*/
    }

    private IEnumerator AnimateSlowRollDice(Image dice, int diceResult, int diceIndex)
    {
        float timeLeft = 2f;
        UnityEngine.Random.seed = 9001;

		dice.transform.DOShakeRotation (2,  new Vector3(0, 0, 180), 20, 90);
        while (timeLeft > 0)
        {
            int index = UnityEngine.Random.Range(0, 6);
            dice.sprite = DiceSprites[index];
            //RectTransform rectTransform = dice.GetComponent<RectTransform>();
            //rectTransform.Rotate(new Vector3(0, 0, 30));
            timeLeft -= 0.1f;
            yield return new WaitForSeconds(0.1f);
        }
        dice.sprite = DiceSprites[diceResult - 1];
        dice.transform.rotation = Quaternion.identity;
        yield return new WaitForSeconds(1f);
        dice.transform.DOScale(0.35f, 0.2f);
        dice.transform.DOMove(DiceResultPoints[diceIndex].transform.position, 0.2f, false).SetEase(Ease.Linear);
        yield return new WaitForSeconds(0.2f);
        Dice1.transform.parent.SetSiblingIndex(GrayPanel.transform.GetSiblingIndex() - 4);
    }

    private IEnumerator AnimatePlayerGetAllMoney(long winMoney)
    {
        string detailResult = LastGameResult.Split(DelimiterKey.hash).ToList()[0];
        string[] betResults = detailResult.Split(DelimiterKey.dollarPack);

        if (winMoney > 0)
            StartCoroutine(AnimateWinMoneyGoUp(winMoney));

        foreach (string betResult in betResults)
        {
            string[] parts = betResult.Split(DelimiterKey.colon);
            int clientBetType = TXUtil.ToClientBetType(int.Parse(parts[0]));
            Button button = BetTypeButtons[clientBetType + 1];
            long money = long.Parse(parts[2]);
            StartCoroutine(AnimatePlayerGetMoney(button, money));
        }
        yield return null;
    }

    private IEnumerator AnimateWinMoneyGoUp(long winMoney)
    {
        long distance = winMoney / 150;
        long currentMoney = 0;
        float timeLeft = 1.5f;
        long moneyLeft = winMoney;

        while (timeLeft > 0)
        {
            moneyLeft -= distance;

            if (moneyLeft < 0)
                break;

            currentMoney += distance;
            WinMoney.text = Utilities.GetStringMoneyByLongBigSmall(currentMoney);
            timeLeft -= 0.01f;
            yield return new WaitForSeconds(0.01f);
        }
        WinMoney.text = Utilities.GetStringMoneyByLongBigSmall(winMoney);
    }

    private IEnumerator AnimatePlayerGetMoney(Button button, long money)
    {
        IsGettedMoneyList.Add(false);
        int addedIndex = IsGettedMoneyList.Count - 1;
        long[] numOfAllChipTypes = TXUtil.ConvertMoneyToNumOfAllChipTypes(money);

        for (int i = numOfAllChipTypes.Count() - 1; i >= 0; i--)
        {
            for (int j = 0; j < numOfAllChipTypes[i]; j++)
            {
                string text = TXUtil.ConvertIndexToChipText(i);
                TaiXiuBetChip betChip = BetChipManager.CreateBetChip(i, text);
                betChip.transform.SetParent(button.transform);
                betChip.transform.localPosition = Vector3.zero;
                betChip.transform.localScale = new Vector3(1, 1, 1);
                betChip.transform.DOMove(WinMoney.transform.position, 0.6f, false);
                yield return new WaitForSeconds(0.1f);
                betChip.transform.SetParent(WinMoney.transform);
            }
        }

        yield return new WaitForSeconds(1.5f);
        foreach (Transform t in WinMoney.transform)
            Destroy(t.gameObject);

        IsGettedMoneyList[addedIndex] = true;

        yield return null;
    }

    private IEnumerator AnimateShowMessage(string message)
    {
        IsShowingMessage = true;
        MessagePanel.transform.GetChild(0).GetComponent<Text>().text = message;
        MessagePanel.SetActive(true);
        
        yield return new WaitForSeconds(1f);
        MessagePanel.SetActive(false);
        IsShowingMessage = false;
        yield return null;
    }

    private IEnumerator AnimateKickOut()
    {
        yield return new WaitForSeconds(5);
        if (!GameEnded)
        {
            popupAlert.Show(ConstText.ErrorConnection, () =>
            {
                sfs.LogOut();
                GameHelper.ChangeScene(GameScene.LoginScene);
            });
        }
    }

    #endregion

    #region Util

    public void RemoveChipsInButtons()
    {
        foreach (Button button in BetTypeButtons)
        {
            foreach (Transform t in button.transform.GetChild(2).transform)
                Destroy(t.gameObject);
            foreach (Transform t in button.transform.GetChild(3).transform)
                Destroy(t.gameObject);
        }
    }

    public void DisableAllBetButtons()
    {
        foreach (Button button in BetButtons)
        {
            button.transform.GetChild(2).gameObject.SetActive(true);
        }
    }

    public void EnableButton(Button button)
    {
        button.transform.GetChild(2).gameObject.SetActive(false);
    }

    private long GetChangedMoney(string strGameResult)
    {
        return long.Parse(strGameResult.Split(DelimiterKey.hash).ToList()[1]);
    }

    private long GetPlayerMoneyAfterEndGame(string strGameResult)
    {
        return long.Parse(strGameResult.Split(DelimiterKey.hash).ToList()[2]);
    }

    private void UpdateHistoryPanel()
    {
        TaiXiuHistoryManager.Instance.UpdateHistory(TXUtil.ConvertStringToDiceResult(LastDiceResult), LastGameResult);
    }

    private Button GetButtonByBetType(int betType)
    {
        return BetTypeButtons[betType + 1];
    }

    private void HighlightWinButtons(int dr1, int dr2, int dr3)
    {
        if (dr1 == dr2 && dr1 == dr3)
            return;

        int total = dr1 + dr2 + dr3;

        if (total <= 10)
            BetTypeButtons[TaiXiuConst.XIU + 1].transform.GetChild(2).gameObject.SetActive(true);
        else
            BetTypeButtons[TaiXiuConst.TAI + 1].transform.GetChild(2).gameObject.SetActive(true);

        BetTypeButtons[dr1 + 1].transform.GetChild(2).gameObject.SetActive(true);
        BetTypeButtons[dr2 + 1].transform.GetChild(2).gameObject.SetActive(true);
        BetTypeButtons[dr3 + 1].transform.GetChild(2).gameObject.SetActive(true);

        BetTypeButtons[total + 4].transform.GetChild(2).gameObject.SetActive(true);
    }

    private void UnHighlightAllButtons()
    {
        foreach (Button button in BetTypeButtons)
            button.transform.GetChild(2).gameObject.SetActive(false);
    }

    #endregion

    public void OnConnectionLost()
    {
        popupAlert.Show(ConstText.ErrorConnection, () => {
            GameHelper.ChangeScene(GameScene.LoginScene);
        });
    }
}