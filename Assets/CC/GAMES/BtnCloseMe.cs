﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnCloseMe : MonoBehaviour
{

    public void CloseMe()
    {
        gameObject.SetActive(false);
    }
    public void DestroyAndClose()
    {
        Destroy(gameObject);
    }
}
