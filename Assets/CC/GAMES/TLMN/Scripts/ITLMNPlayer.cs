﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ITLMNPlayer
{

    void SendCard(List<Card> cards);
    void CountDownTime();
}

public abstract class TLMNBasePlayer : MonoBehaviour, ITLMNPlayer
{
    [HideInInspector]
    public int sfsId;
    [HideInInspector]
    public string userId;
    [HideInInspector]
    public string IdBang;
    [HideInInspector]
    public string ringid;
    [HideInInspector]
    public string gender;
    [HideInInspector]
    public string partner;
    [HideInInspector]
    public long chip;
    [HideInInspector]
    public long gem;
    [HideInInspector]
    public int level;
    public GameObject cardGroup;
    public Text displayName;
    public Image avatar;
    public Image inviteBtn;
    public Image avatarBoder_Defaut;
    public Image avatarBoder;
    public Text gold;
    public string Vip;
    public GameObject timerBackground;

    protected const float fillFrom = 0.1f, fillTo = 0.9f;
    public Text txtLevel, txtExp;
    public GameObject Level;
    public Image timerImage;
    public GameObject sampleCard;
    public Text moneyText;
    public Image rankImage;
    public Image immediateWinImage;
    public TLMNRoomController controller;

    [SerializeField]
    protected GameObject focusPlayer = null;
    [SerializeField]
    protected Text txtResult = null;

    [SerializeField]
    public Image LogoBang;

    public abstract void CountDownTime();
    public abstract void StopCountDownTime();
    public abstract void SendCard(List<Card> hitCards);
    public abstract void SetInfo(string[] info, Hashtable userVariable);
    //public abstract void ShowMoneyEffect(long money);
    public void setResult(long result)
    {
        txtResult.gameObject.SetActive(true);
        txtResult.text = (result > 0 ? "+" : "") + result.ToString();
        if (result > 0)
        {
            txtResult.color = Ulti.ConvertHexStringToColor("FFF135FF");
            txtResult.GetComponent<Outline>().effectColor = Ulti.ConvertHexStringToColor("FF000080");
        }
        else
        {
            txtResult.color = Ulti.ConvertHexStringToColor("929292FF");
            txtResult.GetComponent<Outline>().effectColor = Ulti.ConvertHexStringToColor("0000003D");
        }
    }
    public void hideResult()
    {
        txtResult.gameObject.SetActive(false);
    }

    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        if (mainImage != null && avatar != null)
            avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    public abstract void Reset();
}
