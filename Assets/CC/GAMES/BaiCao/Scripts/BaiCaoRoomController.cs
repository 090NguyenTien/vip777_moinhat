﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

using DG.Tweening;
using UnityEngine;
using Sfs2X.Entities.Data;
using SimpleJSON;

public class BaiCaoRoomController : MonoBehaviour
{
    //Cheat---------------------------
    public Button cheatBtn;
    public InputField input0, input1, input2, input3;
    public Text txtCheat;
    //Cheat---------------------------

    public static BaiCaoRoomController Instance { get; set; }

    private const float CARD_WIDTH = 56;

    public GameObject cardBack;
    public BaiCaoPlayer player;
    public BaiCaoEnemyPlayer[] enemies;
    public Deck deck;
    public Image effectImage;
    public Image cardTypeImage;
    //public GameObject startGameBtn;
    public Sprite[] rankSprite;
    public Sprite[] cardTypeSprites;

    [HideInInspector]
    public List<Card> currentCards;//danh sach card dang danh tren ban choi
    [HideInInspector]
    public bool isFirstGame = false;

    private SFS sfs;
    public bool isStarting = false;
    private int hostId;
    private bool isAnimationStarting = false;
    private Coroutine countDownCoroutine;
    private bool workDone = false;
    private Queue<GamePacket> GamePacketQueue = new Queue<GamePacket>();

    [SerializeField]
    Button btnChat;
    [SerializeField]
    PopupInviteManager popupInvite;
    [SerializeField]
    Text roomIdText;
    [SerializeField]
    Text bettingMoneyText, txtHostname;
    [SerializeField]
    PopupAlertManager popupAlert;
    [SerializeField]
    PopupFullManager popupFullAlert;
    [SerializeField]
    PopupModuleManager Module;
    [SerializeField]
    Text countDownText;
    [SerializeField]
    GameObject statusTextObject, ChipTotal;
    [SerializeField]
    private Text txtGameName = null;
    [SerializeField]
    Text txtTimeGame, TxtTotalBet;
    [SerializeField]
    Image imgAnimationChip;
    [SerializeField]
    GameObject containerChipAni;
    //Vector3 beginPosChipAnimation;


    [SerializeField]
    GameObject ViTriBoomUser, ViTriBoom_1, ViTriBoom_2, ViTriBoom_3, ViTriBoom_4, ViTriBoom_5;

    [SerializeField]
    GameObject ViTri_CANON_0, ViTri_CANON_2, ViTri_CANON_3, ViTri_CANON_4;

    [SerializeField]
    GameObject ItemBoom_1, ItemBoom_2, ItemBoom_3, ItemBoom_4, ItemBoom_5, ItemBoom_6, PhaBom, Canon, BaiVang, ThanBai, Item_quyphi, QuyPhi, Item_SoaiCa, SoaiCa, Item_HaiPhuong, HaiPhuong;

    [SerializeField]
    GameObject Item_ThietPhien, ThietPhien;

    [SerializeField]
    GameObject ObjAvata_1, ObjAvata_2, ObjAvata_3, ObjAvata_4, ObjAvata_5;
    [SerializeField]
    GameObject ObjBtnInfo_1, ObjBtnInfo_2, ObjBtnInfo_3, ObjBtnInfo_4, ObjBtnInfo_5;

    private int sfsid_BiNem = -1;
    private int SfsLoaiBoom = -1;
    private int sfsid_NemBomPha = -1;
    private int sfsid_BiNemBomPha = -1;

    string IdBangMaster = "";
    string IdBangUser_Nem = "";
    string IdBangUser_BiNem = "";
    string IdBangChinhMinh = "";


    [SerializeField]
    private Sprite[] arrSprSmallSuite,
        arrSprBigSuite,
        arrSprValue,
        arrSprJQK;
    void Awake()
    {
        Instance = this;
        LoadingManager.Instance.ENABLE = false;
        sfs = SFS.Instance;

        btnChat.onClick.AddListener(BtnChatOnClick);
        popupInvite.Init();
        popupAlert.Init();
        popupFullAlert.Init();
        SoundManager.PlaySound(SoundManager.ENTER_ROOM);

        IdBangMaster = MyInfo.ID_BANG_MASTER;
        IdBangChinhMinh = MyInfo.ID_BANG;
        LayThongTinThanBai();
        string gameName = "BÀI CÀO";
        txtGameName.text = gameName;
        //beginPosChipAnimation = imgAnimationChip.transform.localPosition;
        imgAnimationChip.gameObject.SetActive(false);
    }

    void Start()
    {
        GetGameRoomInfo();
        txtTimeGame.text = "";
        TxtTotalBet.text = "";
        ChipTotal.gameObject.SetActive(false);
    }


    void Update()
    {

        ObjBtnInfo_1.SetActive(ObjAvata_1.activeInHierarchy);
        ObjBtnInfo_2.SetActive(ObjAvata_2.activeInHierarchy);
        ObjBtnInfo_3.SetActive(ObjAvata_3.activeInHierarchy);
        ObjBtnInfo_4.SetActive(ObjAvata_4.activeInHierarchy);
        ObjBtnInfo_5.SetActive(ObjAvata_5.activeInHierarchy);

        ViTriBoom_1.SetActive(ObjAvata_1.activeInHierarchy);
        ViTriBoom_2.SetActive(ObjAvata_2.activeInHierarchy);
        ViTriBoom_3.SetActive(ObjAvata_3.activeInHierarchy);
        ViTriBoom_4.SetActive(ObjAvata_4.activeInHierarchy);
        ViTriBoom_5.SetActive(ObjAvata_5.activeInHierarchy);
    }



    #region NÉM ITEM

    #region GỬI REQUEST

    public void SendBomb()
    {
        GamePacket param = new GamePacket("bomb");
        param.Put("sfsid_nem", MyInfo.SFS_ID);
        param.Put("sfsid_binem", sfsid_BiNem);
        param.Put("bomb_type", SfsLoaiBoom);
        SFS.Instance.SendBoomRequest(param);
    }


    public void SendBomPha()
    {
        GamePacket param = new GamePacket("bomb");
        param.Put("sfsid_nem", sfsid_NemBomPha);
        param.Put("sfsid_binem", sfsid_BiNemBomPha);
        param.Put("bomb_type", SfsLoaiBoom);

        if (sfsid_NemBomPha != sfsid_BiNemBomPha)
        {
            SFS.Instance.SendBoomRequest(param);
        }
        sfsid_NemBomPha = -1;
        sfsid_BiNemBomPha = -1;
    }


    #endregion

    #region NHẬN RESPONSE





    public void ResponseNemBoom(GamePacket param)
    {

        int sfsid_Nem = param.GetInt("sfsid_nem");
        int sfsid_BiNem = param.GetInt("sfsid_binem");
        int LoaiBoom = param.GetInt("bomb_type");
        BaiCaoEnemyPlayer Enemy_Nem = new BaiCaoEnemyPlayer();
        BaiCaoEnemyPlayer Enemy_BiNem = new BaiCaoEnemyPlayer();
        bool NemVaoUser = false;
        //  Debug.Log("Vô ResponseNemBoom--------------sfsid_Nem- " + sfsid_Nem + " sfsid_BiNem = " + sfsid_BiNem + " LoaiBoom = " + LoaiBoom);
        if (sfsid_Nem != MyInfo.SFS_ID)
        {
            foreach (var item in enemies)
            {
                if (item.sfsId == sfsid_Nem)
                {
                    Enemy_Nem = item;
                }
            }

            if (sfsid_BiNem == MyInfo.SFS_ID)
            {
                NemVaoUser = true;
            }
            else
            {
                foreach (var item in enemies)
                {
                    if (item.sfsId == sfsid_BiNem)
                    {
                        Enemy_BiNem = item;
                    }
                }
            }
            int ViTriSinh = Enemy_Nem.Id_ViTriBom;

            if (NemVaoUser == false)  // TRƯỜNG HỢP NGƯỜI THỨ 3
            {
                int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                IdBangUser_Nem = Enemy_Nem.IdBang;
                IdBangUser_BiNem = Enemy_BiNem.IdBang;
                string idUser_Nem = Enemy_Nem.userId;

                NguoiThuBa(LoaiBoom, ViTriSinh, ViTriNem, IdBangUser_Nem, IdBangUser_BiNem, idUser_Nem);
            }
            else     // TRƯỜNG HỢP NGƯỜI BỊ NÉM
            {
                NguoiBiNem(LoaiBoom, ViTriSinh, Enemy_Nem.IdBang, MyInfo.ID);
            }
        }
        else      // TRƯỜNG HỢP NGƯỜI NÉM
        {
            if (LoaiBoom == 6)
            {
                foreach (var item in enemies)
                {
                    if (item.sfsId == sfsid_BiNem)
                    {
                        Enemy_BiNem = item;
                    }
                }
                int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                IdBangUser_BiNem = Enemy_BiNem.IdBang;

                NguoiNem(ViTriNem, MyInfo.ID);
            }
        }
    }


    void NguoiNem(int ViTriNem, string idUser_ChinhMinh)
    {

        bool check = CheckPhanDame(IdBangUser_BiNem, IdBangChinhMinh);
        //   check = false;
        if (check == false)
        {
            int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

            if (loaiThanBai == -1)
            {
                return;
            }
            else
            {
                GoiThanBai(ViTriNem, 0, 0, loaiThanBai);
            }
        }
        else
        {
            SinhBoom_DiChuyenToiViTri(ViTriNem, 0, 0);
            StartCoroutine(GoiPhanDame(0, ViTriNem));
        }
    }

    void NguoiBiNem(int LoaiBoom, int ViTriSinh, string IdBangNem, string idUser_ChinhMinh)
    {
        if (LoaiBoom != 6)
        {
            IdBangUser_Nem = IdBangNem;
            bool check = CheckPhanDame(IdBangUser_Nem, IdBangChinhMinh);
            //   check = false;
            if (check == false)
            {
                int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

                if (loaiThanBai == -1)
                {
                    SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                }
                else
                {
                    GoiThanBai(ViTriSinh, 0, LoaiBoom, loaiThanBai);
                }
            }
            else
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                StartCoroutine(GoiPhanDame(0, ViTriSinh));
            }
        }
    }

    void NguoiThuBa(int LoaiBoom, int ViTriSinh, int ViTriNem, string IdBangUser_Nem, string IdBangUser_BiNem, string idUser_Nem)
    {
        if (LoaiBoom != 6)
        {
            bool check = CheckPhanDame(IdBangUser_Nem, IdBangUser_BiNem);

            if (check == false)
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
            }
            else
            {
                SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
                StartCoroutine(GoiPhanDame(ViTriNem, ViTriSinh));
            }

        }
        else
        {
            bool check = CheckPhanDame(IdBangUser_BiNem, IdBangUser_Nem);

            if (check == true)
            {
                SinhBoom_DiChuyenToiViTri(ViTriNem, ViTriSinh, 0);
                StartCoroutine(GoiPhanDame(ViTriSinh, ViTriNem));
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(idUser_Nem);

                if (loaiThanBai == -1)
                {

                }
                else
                {
                    GoiThanBai(ViTriNem, ViTriSinh, 1, loaiThanBai);
                }
            }
        }
    }


    #endregion

    #region SINH ITEM BOOM



    public void SinhBoom_DiChuyenToiViTri(int ViTriSinh, int ViTriNem, int loaiBoom, bool Send = false, int LoaiBoom_PhanDame = 0)
    {
        Transform targetMove = null;
        GameObject MyBoom = null;
        GameObject MyCanon = null;

        Debug.LogWarning("ViTriSinh - " + ViTriSinh + " ViTriNem - " + ViTriNem + " loaiBoom - " + loaiBoom);

        #region Tao Boom

        if (loaiBoom != 6) // TRƯỜNG HỢP THƯỜNG
        {
            MyBoom = TaoBoomTaiViTriSinh(loaiBoom, ViTriSinh);
        }
        else // TRƯỜNG HỢP PHAN DAME
        {
            MyCanon = TAO_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
            MyBoom = TAO_BOOM_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
        }

        MyBoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        targetMove = XacDinhViTriNem(ViTriNem);

        #endregion

        ItemBoomControll MyItem = MyBoom.GetComponent<ItemBoomControll>();

        MyItem.Init(targetMove);

        if (Send == false)  // TRƯỜNG HỢP CHỈ DIỂN HOẠT KHÔNG GỬI REQUEST
        {
            MyItem.NemBoom(NemThuong, loaiBoom);
        }
        else
        {
            if (loaiBoom != 6) // Choi Thuong
            {
                sfsid_BiNem = GetSfsIdEnemy(ViTriNem);
                if (sfsid_BiNem != -1)
                {
                    SfsLoaiBoom = loaiBoom;
                    MyItem.NemBoom(SendBomb, loaiBoom);
                }
            }
            else // Choi Nhung Bi Phan Dame
            {
                sfsid_BiNemBomPha = MyInfo.SFS_ID;
                sfsid_NemBomPha = GetSfsIdEnemy(ViTriSinh);
                if (sfsid_BiNemBomPha != -1)
                {
                    SfsLoaiBoom = loaiBoom;
                    MyItem.NemBoom(SendBomPha, loaiBoom);
                }
            }

        }


    }




    GameObject TaoBoomTaiViTriSinh(int loaiboom, int ViTriSinh)
    {
        GameObject MyBoom = null;
        GameObject ObjBoom = null;
        ObjBoom = XacDinhLoaiBoom(loaiboom);

        if (ViTriSinh == 0)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoomUser.transform) as GameObject;
        }
        else if (ViTriSinh == 1)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_1.transform) as GameObject;
        }
        else if (ViTriSinh == 2)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_2.transform) as GameObject;
        }
        else if (ViTriSinh == 3)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_3.transform) as GameObject;
        }
        else if (ViTriSinh == 4)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_4.transform) as GameObject;
        }
        else if (ViTriSinh == 5)
        {
            MyBoom = Instantiate(ObjBoom, ViTriBoom_5.transform) as GameObject;
        }

        return MyBoom;
    }


    GameObject XacDinhLoaiBoom(int loaiboom)
    {
        GameObject ObjBoom = null;

        if (loaiboom == 0)
        {
            ObjBoom = ItemBoom_1;
        }
        else if (loaiboom == 1)
        {
            ObjBoom = ItemBoom_2;
        }
        else if (loaiboom == 2)
        {
            ObjBoom = ItemBoom_3;
        }
        else if (loaiboom == 3)
        {
            ObjBoom = ItemBoom_4;
        }
        else if (loaiboom == 4)
        {
            ObjBoom = ItemBoom_5;
        }
        else if (loaiboom == 5)
        {
            ObjBoom = ItemBoom_6;
        }

        return ObjBoom;
    }




    GameObject TAO_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
    {
        GameObject Canon = null;
        GameObject Obj_Canon = XacDinhLoai_PHANDAME(LoaiBoom_PhanDame);


        if (ViTriSinh == 0)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_0.transform) as GameObject;

        }
        else if (ViTriSinh == 1)
        {
            Canon = Instantiate(Obj_Canon, ViTriBoom_1.transform) as GameObject;

        }
        else if (ViTriSinh == 2)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_2.transform) as GameObject;

        }
        else if (ViTriSinh == 3)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_3.transform) as GameObject;

        }
        else if (ViTriSinh == 4)
        {
            Canon = Instantiate(Obj_Canon, ViTri_CANON_4.transform) as GameObject;

        }
        else if (ViTriSinh == 5)
        {
            Canon = Instantiate(Obj_Canon, ViTriBoom_5.transform) as GameObject;

        }

        return Canon;
    }


    GameObject TAO_BOOM_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
    {
        GameObject itemboom = null;
        GameObject Obj_itemboom = XacDinhLoai_BOOM_PHANDAME(LoaiBoom_PhanDame);

        if (ViTriSinh == 0)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoomUser.transform) as GameObject;
        }
        else if (ViTriSinh == 1)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_1.transform) as GameObject;
        }
        else if (ViTriSinh == 2)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_2.transform) as GameObject;
        }
        else if (ViTriSinh == 3)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_3.transform) as GameObject;
        }
        else if (ViTriSinh == 4)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_4.transform) as GameObject;
        }
        else if (ViTriSinh == 5)
        {
            itemboom = Instantiate(Obj_itemboom, ViTriBoom_5.transform) as GameObject;
        }

        return itemboom;

    }


    GameObject XacDinhLoai_BOOM_PHANDAME(int loaiboom)
    {
        GameObject ObjBoom = null;

        String name = loaiboom.ToString() + "_efect";
        ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
        LoadBundleLogin.api.UnloadBundle();

        if (ObjBoom == null)
        {
            ObjBoom = Item_SoaiCa;
        }
        return ObjBoom;
    }

    GameObject XacDinhLoai_PHANDAME(int loaiboom)
    {
        GameObject ObjBoom = null;

        String name = loaiboom.ToString() + "_item";
        ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
        LoadBundleLogin.api.UnloadBundle();

        if (ObjBoom == null)
        {
            ObjBoom = SoaiCa;
        }
        return ObjBoom;

    }


    Transform XacDinhViTriNem(int ViTriNem)
    {
        Transform vitri = null;
        if (ViTriNem == 0)
        {
            vitri = ViTriBoomUser.transform;
        }
        else if (ViTriNem == 1)
        {
            vitri = ViTriBoom_1.transform;
        }
        else if (ViTriNem == 2)
        {
            vitri = ViTriBoom_2.transform;
        }
        else if (ViTriNem == 3)
        {
            vitri = ViTriBoom_3.transform;
        }
        else if (ViTriNem == 4)
        {
            vitri = ViTriBoom_4.transform;
        }
        else if (ViTriNem == 5)
        {
            vitri = ViTriBoom_5.transform;
        }
        return vitri;
    }

    void NemThuong()
    {

    }


    public int GetSfsIdEnemy(int id_ViTriBom)
    {
        int SfsId = -1;
        foreach (var item in enemies)
        {
            if (item.Id_ViTriBom == id_ViTriBom)
            {
                SfsId = item.sfsId;
                return SfsId;
            }
        }
        return SfsId;
    }



    #endregion

    #region GOI PHẢN DAME

    bool CheckPhanDame(string idBangUser_Nem, string idBangUser_BiNem)
    {
        if (idBangUser_BiNem == "")
        {
            return false;
        }

        if (idBangUser_BiNem == idBangUser_Nem)
        {
            return false;
        }

        if (idBangUser_BiNem != IdBangMaster)
        {
            return false;
        }

        return true;
    }


    public void PhanDame(int Enemy, int LoaiBoom)
    {
        SinhBoom_DiChuyenToiViTri(0, Enemy, LoaiBoom);
        StartCoroutine(PrintfAfter(Enemy, LoaiBoom));
    }

    IEnumerator PrintfAfter(int Enemy, int LoaiBoom)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(Enemy, 0, 6, true);
    }


    IEnumerator GoiPhanDame(int vitri_nem, int vitri_binem)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(vitri_nem, vitri_binem, 6);
    }


    int CheckDicThanBai(string idUser)
    {
        int kq = -1;
        if (DataHelper.DicThanBai.ContainsKey(idUser))
        {
            kq = DataHelper.DicThanBai[idUser];
        }
        return kq;
    }



    public void LayThongTinThanBai()
    {
        API.Instance.RequestGetData_userAvoidItem(RspLayThongTinThanBai);
    }


    void RspLayThongTinThanBai(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        DataHelper.DicThanBai.Clear();

        int cou = node.Count;

        for (int i = 0; i < cou; i++)
        {
            string id = node[i]["user_id"].Value;
            int id_item = int.Parse(node[i]["item_id"].Value);
            DataHelper.DicThanBai.Add(id, id_item);
        }
    }

    public void GoiThanBai(int ViTri_Nem, int Vitri_ThanBai, int loaiboom, int loaiThanBai, bool sentRequest = false)
    {
        SinhBoom_DiChuyenToiViTri(ViTri_Nem, Vitri_ThanBai, loaiboom);

        StartCoroutine(WaitTest(ViTri_Nem, Vitri_ThanBai, loaiThanBai, sentRequest));

    }


    IEnumerator WaitTest(int ViTri_Nem, int Vitri_ThanBai, int loaiThanBai, bool sentRequest = false)
    {
        yield return new WaitForSeconds(0.3f);
        SinhBoom_DiChuyenToiViTri(Vitri_ThanBai, ViTri_Nem, 6, sentRequest, loaiThanBai);
    }



    #endregion



    #endregion

    

    private IEnumerator CountDownAtStartGame(GamePacket gp)
    {
        print("Count down");
        System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        stopwatch.Start();

        //Debug.Log("isAnimationStarting======================================"+ isAnimationStarting);
        //while (isAnimationStarting)
        //{
        // Debug.Log("isAnimationStarting333333======================================" + isAnimationStarting);
        yield return new WaitForSeconds(0.1f);
        //}
        // Debug.Log("isAnimationStarting444444======================================" + isAnimationStarting);
        stopwatch.Stop();

        int elapsedTime = (int)(stopwatch.ElapsedMilliseconds / 1000) + 1;
        int seconds = gp.GetInt(ParamKey.SecondsUntilStartGame);

        if (countDownCoroutine != null)
        {
            StopCoroutine(countDownCoroutine);
            statusTextObject.SetActive(false);
        }

        seconds -= elapsedTime;
        //Debug.LogError("CountCurentPlayer()======================================" + CountCurentPlayer());
        // Debug.LogError("seconds()======================================" + seconds);
        if (CountCurentPlayer() >= 1)
            countDownCoroutine = StartCoroutine(CountDownTimeToStartGame(seconds));
    }

    private IEnumerator CountDownTimeToStartGame(int timeDelay)
    {
        //Debug.LogError("isStarting()======================================" + isStarting);
        if (!isStarting)
        {
            statusTextObject.SetActive(true);
            while (timeDelay > 0)
            {
                if (CountCurentPlayer() < 1)
                    break;
                //if (!isAnimationStarting)
                countDownText.text = timeDelay.ToString();
                //else
                //countDownText.text = string.Empty;
                yield return new WaitForSeconds(1);
                timeDelay--;
            }
            countDownText.text = string.Empty;
        }
    }

    private int[] CastInt(string[] _arr)
    {
        try
        {
            return Array.ConvertAll<string, int>(_arr, int.Parse);
        }
        catch (Exception e)
        {
            return null;
        }
    }
    public void OnModeratorMessage(string _msg)
    {
        JSONNode node = JSONNode.Parse(_msg);
        if (node["is_level_up"].AsInt == 1)
        {
            player.txtLevel.text = node["cur_level"];
            player.level = node["cur_level"].AsInt;
        }
    }
    private void GetGameRoomInfo()
    {
        GamePacket param = new GamePacket(CommandKey.GET_GAME_ROOM_INFO);
        sfs.SendRoomRequest(param);
    }

    private IEnumerator DealCard(IBaiCaoPlayer[] players, int[] playerCards, IBaiCaoPlayer activePlayer)
    {
        SoundManager.PlaySound(SoundManager.DEAL_MANY_CARD);
        //Debug.LogError("playerCards   KKKKKKKKKKKK==========="+ playerCards);
        //Debug.LogError("playerCards.lengh   KKKKKKKKKKKK===========" + playerCards.Length);
        if (playerCards != null)
        {
            //Set cards and create animation for player
            Array.Sort(playerCards, delegate (int x, int y)
            {
                int typeX = (int)deck.cards[x].Type;
                int typeY = (int)deck.cards[y].Type;
                if (deck.cards[x].Value == deck.cards[y].Value)
                {
                    return typeX.CompareTo(typeY);
                }
                return deck.cards[x].Value.CompareTo(deck.cards[y].Value);
            });
            player.cards.Clear();//clear cards user truoc khi add card moi vao
            foreach (int id in playerCards)
            {
                Card card = deck.cards[id];
                card.setBackCardActive(false);
                player.cards.Add(id, card);


                card.transform.SetParent(player.cardGroup.transform);
                card.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                card.gameObject.SetActive(false);
                card.transform.localPosition = Vector3.zero;
            }
            //player.sampleCard.SetActive(false);
            player.SortCards(true);
        }
        float timeDelay = (7 - players.Length) * 0.1f;
        Vector3 posCardMove = Vector3.one;
        for (int i = 0; i < 3; i++)
        {
            foreach (IBaiCaoPlayer iPlayer in players)
            {
                BaiCaoBasePlayer p = iPlayer as BaiCaoBasePlayer;
                p.hideResult();
                GameObject temp = Instantiate(cardBack);
                temp.SetActive(true);
                temp.transform.SetParent(deck.transform);
                temp.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                //getCardPosXByIndex

                if (p == player)
                {
                    //p.sampleCard.GetComponent<RectTransform>().anchoredPosition = 
                    posCardMove = player.cardGroup.transform.position;
                    posCardMove.y = posCardMove.y - 100;
                    posCardMove.x = player.getCardPosXByIndex(i, 3);

                    Hashtable hash = new Hashtable();
                    hash.Add("time", 0.3f);
                    hash.Add("position", posCardMove);
                    hash.Add("easetype", iTween.EaseType.linear);
                    hash.Add("islocal", true);
                    hash.Add("oncompletetarget", gameObject);
                    hash.Add("oncomplete", "showCard");
                    object[] paramsComplete = new object[] { i, temp };
                    hash.Add("oncompleteparams", paramsComplete);
                    iTween.MoveTo(temp.gameObject, hash);


                    //Debug.LogError("move card move to " + posCardMove + "    card index " + i);
                    //temp.transform.DOMove(posCardMove, 10.4f, false).SetEase(Ease.Linear);
                    temp.transform.DOScale(Vector3.one, 0.3f);
                    //Destroy(temp, 0.4f);
                }
                else
                {
                    posCardMove = p.sampleCard.transform.position;
                    temp.transform.DOMove(posCardMove, 0.4f, false).SetEase(Ease.Linear);
                    temp.transform.DOScale(Vector3.one, 0.4f);
                    StartCoroutine(TurnUpCardWhenDeal((BaiCaoEnemyPlayer)p, i));
                    Destroy(temp.gameObject, 0.4f);
                }
                yield return new WaitForSeconds(timeDelay);
                //yield return new WaitForSeconds(1 / 13f);
                //yield return new WaitForSeconds(2);
            }
        }
        cardBack.SetActive(false);



        if (isStarting)
        {
            //activePlayer.CountDownTime();           
        }

        HandleNextWork();
    }
    private void ShowCard(BaseCardItem _cardItem, int _id)
    {
        _cardItem.gameObject.SetActive(true);
        _cardItem.card = BaseCardInfo.Get(_id);

        if (_cardItem.Value == null)
        {
            _cardItem.Value = _cardItem.transform.Find("Card Value").GetComponent<Image>();
            _cardItem.SmallSuite = _cardItem.transform.Find("Upper Type").GetComponent<Image>();
            _cardItem.BigSuite = _cardItem.transform.Find("Main Type").GetComponent<Image>();
        }

        _cardItem.Value.sprite = arrSprValue[(_cardItem.card.Value - 1) % 13];

        _cardItem.Value.color = (int)_cardItem.card.Type < 2 ? Color.black : Color.red;

        //		Debug.Log ("TYPE _ " + (int)_cardItem.card.Type);
        _cardItem.SmallSuite.sprite = arrSprSmallSuite[(int)_cardItem.card.Type];

        if (_cardItem.card.Value < 11 || _cardItem.card.Value > 13)
        {
            _cardItem.BigSuite.sprite = arrSprBigSuite[(int)_cardItem.card.Type];
        }
        else
        {
            _cardItem.BigSuite.sprite = arrSprJQK[(_id / 13) * 3 + (_cardItem.card.Value % 11)];
        }
    }
    private IEnumerator TurnUpCardWhenDeal(BaiCaoEnemyPlayer player, int index)
    {
        yield return new WaitForSeconds(0.4f);
        player.showSampleCardByIndex(index);
    }

    private void showCard(object[] arr)//call from itween
    {
        int index = Convert.ToInt32(arr[0]);
        GameObject tempCard = arr[1] as GameObject;
        Destroy(tempCard.gameObject);
        //Debug.LogError("============index Nenenenenenene============"+ index);
        //Debug.LogError("childCount=======" + player.cardGroup.transform.childCount);
        if (player.cardGroup.transform.childCount == 3)
        {
            Transform cardGroup = player.cardGroup.transform.GetChild(index);
            cardGroup.gameObject.SetActive(true);
        }

    }

    public void OnSFSResponse(GamePacket param)
    {
        Debug.LogWarning("CMD: " + param.cmd + "   " + param.param.ToJson());
        //print (param.param.ToJson());

        switch (param.cmd)// những command ngoài game.
        {
            case CommandKey.ADD_FRIEND_CONFIRM:
            case CommandKey.ADD_FRIEND_IGNORE:
            case CommandKey.ADD_FRIEND_NOTICE:
            case CommandKey.ADD_FRIEND_REQUEST:
            case CommandKey.ADD_FRIEND_RESULT:
            case CommandKey.ADD_FRIEND_SUCCESS:
            case CommandKey.GET_LIST_FRIEND:

                return;
        }

        switch (param.cmd)
        {
            case "bomb":
                ResponseNemBoom(param);
                break;
            case CommandKey.CountDownAtStartGame:
                StartCoroutine(CountDownAtStartGame(param));
                workDone = true;
                //Debug.LogError("==================================KKHHHAHAHAHAKAHKHKAHHA:::");
                break;
            case CommandKey.GET_GAME_ROOM_INFO:
                SetGameRoomInfo(param);
                break;
            case CommandKey.USER_EXIT:
                int userId = param.GetInt(ParamKey.USER_ID);
                if (userId == MyInfo.SFS_ID)
                    HandleUserExit(param);
                else
                    GamePacketQueue.Enqueue(param);
                break;
            case CommandKey.JOIN_GAME_LOBBY_ROOM:
                LoadWatingRoom(param);
                break;
            case CommandKey.START_GAME:
                StartGame(param);
                break;
            case CommandKey.OPEN_CARD:
                OpenCard(param);
                break;
            case CommandKey.GET_PLAYERS_INVITE:
                RspGetPlayersInvite(param);
                break;
            case CommandKey.KICK_USER_BY_HOST:
                KickUserByHost(param);
                return;
            case CommandKey.KICK_USER_ERROR:
                KickUserError(param);
                return;
            default:
                GamePacketQueue.Enqueue(param);
                break;
        }
        if (workDone)
            HandleNextWork();
    }

    private void HandleNextWork()
    {
        Debug.Log("handke next work ne " + GamePacketQueue.Count);
        workDone = true;
        if (GamePacketQueue.Count > 0)
            HandlePacketQueue(GamePacketQueue.Dequeue());
    }

    private void HandlePacketQueue(GamePacket param)
    {
        if (!workDone)
            return;

        workDone = false;
        //UnityEngine.Debug.LogError("param.cmd     " + param.cmd + "   " + param.param.ToJson());
        switch (param.cmd)
        {
            case CommandKey.NOTICE_JOIN_GAME_ROOM:
                NoticePlayerJoinRoom(param);
                break;

            case CommandKey.END_GAME:
                EndGame(param);
                break;

            case CommandKey.KickUser:
                KickUser(param);
                break;
                           
            case CommandKey.USER_EXIT:
                HandleUserExit(param);
                break;
        }
    }
    private void KickUserError(GamePacket param)
    {
        AlertController.api.showAlert(param.GetString("Message"));
    }
    private void KickUserByHost(GamePacket param)
    {
        AlertController.api.showAlert("Bạn đã bị chủ bàn mời ra khỏi phòng!");
        SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);
    }

    private void KickUser(GamePacket param)
    {
        int[] userList = param.GetIntArray(ParamKey.USER_LIST);
        foreach (int i in userList)
        {
            //Player
            if (MyInfo.SFS_ID == i)
            {
				popupAlert.Show("Bạn bị mời ra khỏi bàn vì không đủ tiền, vui lòng nạp thêm!", () =>
                {
                    //Debug.LogError("===============KHUONG===========Moi Ra Khoi Ban Ne");                   
                    SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);
                    //GamePacket gp = new GamePacket(CommandKey.JOIN_GAME_LOBBY_ROOM);
                    //gp.Put(ParamKey.GAME_ID, GAMEID.BaiCao);
                    //sfs.SendZoneRequest(gp);
                });
            }
            //Enemy
            else
            {
                BaiCaoBasePlayer player = GetPlayerBySFSId(i);
                if (player != null)
                    (player as BaiCaoEnemyPlayer).ExitRoom();
            }
        }
        HandleNextWork();
    }

    private void NoticePlayerJoinRoom(GamePacket param)
    {
        //print("Host: " + player.isHost);
        SoundManager.PlaySound(SoundManager.ENTER_ROOM);

        string[] userInfo = param.GetString(ParamKey.USER_INFO).Split('#');
        int position = param.GetInt(ParamKey.POSITION);
        print("Positon: " + position);
        Hashtable uVariable = GetUserVariableById(userInfo[1]);
        GetEnemyByPosition(player.serverPosition, position).SetInfo(userInfo, uVariable);
        //GetEnemyByPosition(player.serverPosition, position).SetInfo(userInfo);
        HandleNextWork();
        //if(!isStarting)
        //    CheckShowStartGameButton();
    }

    List<Hashtable> lstUserVariable;
    public void OnVariableResponse(List<Hashtable> lstDataRes)
    {
        lstUserVariable = lstDataRes;

    }
    private Hashtable GetUserVariableById(string uId)
    {
        //string uid = data["uid"].ToString();
        //string clan_id = data["clanid"].ToString();
        //string nhan_id = data["ringid"].ToString();
        //string gioitinh_id = data["gender"].ToString();
        //string tennguoitinh = data["partner"].ToString();
        foreach (Hashtable uVariable in lstUserVariable)
        {
            if (uVariable["uid"].ToString() == uId)
            {
                return uVariable;
            }
        }
        return null;
    }

    long betMoneyTable;
    private void SetGameRoomInfo(GamePacket param)
    {
        //Debug.LogError("param" + param.ToString());        
        //set level
        /*ISFSObject LevelData = param.GetSFSObject("level");        
        JSONNode node = JSONNode.Parse(LevelData.ToJson());
        MyInfo.CUR_LEVEL = node["cur_level"].AsInt;
        MyInfo.CUR_EXP = node["exp"].AsInt;
        MyInfo.MIN_EXP = node["min_exp"].AsInt;
        MyInfo.MAX_EXP = node["max_exp"].AsInt; */
        //end
        int roomId = param.GetInt(ParamKey.ROOM_ID);
        int hostId = param.GetInt(ParamKey.HOST_ID);
        long betMoney = param.GetLong(ParamKey.BET_MONEY);
        string[] usersInfo = param.GetString(ParamKey.USER_INFO).Split('$');
        //print ("=========== " + param.GetString (ParamKey.USER_INFO));
        this.hostId = hostId;
		roomIdText.text = "Bàn số: " + roomId.ToString();
		bettingMoneyText.text = "Mức cược: " + Utilities.GetStringMoneyByLong(betMoney);
        betMoneyTable = betMoney;

        for (int i = 0; i < usersInfo.Length; i++)
        {
            if (usersInfo[i].Equals(""))
                continue;
            if (int.Parse(usersInfo[i].Split('#')[0]) == MyInfo.SFS_ID)
            {
                player.serverPosition = i;
                break;
            }
        }

        for (int i = 0; i < usersInfo.Length; i++)
        {
            string userInfo = usersInfo[i];
            if (!userInfo.Equals(""))
            {
                print(usersInfo[i]);
                string[] info = userInfo.Split('#');
                Hashtable uVariable = GetUserVariableById(info[1]);
                if (int.Parse(info[0]) == MyInfo.SFS_ID)
                {
                    //Set player info
                    player.SetInfo(info, uVariable);
                }
                else
                {
                    //Set enemy info
                    GetEnemyByPosition(player.serverPosition, i).SetInfo(info, uVariable);
                }
            }
        }

        HandleNextWork();
        txtHostname.text = "Chủ Phòng:" + param.GetString(ParamKey.HOST_NAME);
    }

    private BaiCaoEnemyPlayer GetEnemyByPosition(int playerPos, int enemyPos)
    {
        print("My position: " + playerPos);
        print("Enemy position: " + enemyPos);



        switch (playerPos)
        {
            case 0:
                switch (enemyPos)
                {
                    case 1:
                        return enemies[0];
                    case 2:
                        return enemies[1];
                    case 3:
                        return enemies[2];
                    case 4:
                        return enemies[3];
                    case 5:
                        return enemies[4];
                }
                break;
            case 1:
                switch (enemyPos)
                {
                    case 0:
                        return enemies[4];
                    case 2:
                        return enemies[0];
                    case 3:
                        return enemies[1];
                    case 4:
                        return enemies[2];
                    case 5:
                        return enemies[3];
                }
                break;
            case 2:
                switch (enemyPos)
                {
                    case 0:
                        return enemies[3];
                    case 1:
                        return enemies[4];
                    case 3:
                        return enemies[0];
                    case 4:
                        return enemies[1];
                    case 5:
                        return enemies[2];
                }
                break;
            case 3:
                switch (enemyPos)
                {
                    case 0:
                        return enemies[2];
                    case 1:
                        return enemies[3];
                    case 2:
                        return enemies[4];
                    case 4:
                        return enemies[0];
                    case 5:
                        return enemies[1];
                }
                break;
            case 4:
                switch (enemyPos)
                {
                    case 0:
                        return enemies[1];
                    case 1:
                        return enemies[2];
                    case 2:
                        return enemies[3];
                    case 3:
                        return enemies[4];
                    case 5:
                        return enemies[0];
                }
                break;
            case 5:
                return enemies[enemyPos];
        }
        return null;
    }

    private BaiCaoBasePlayer GetPlayerBySFSId(int sfsId)
    {
        if (player.sfsId == sfsId)
            return player;
        foreach (BaiCaoEnemyPlayer enemy in enemies)
            if (enemy.actived && enemy.sfsId == sfsId)
                return enemy;
        return null;
    }



    public void RequestStartGame()
    {
        GamePacket param = new GamePacket(CommandKey.START_GAME);
        sfs.SendRoomRequest(param);
    }
    private void OpenCard(GamePacket param)
    {
        BaiCaoBasePlayer activePlayer = GetPlayerBySFSId(param.GetInt("sfsid"));
        //activePlayer.show
    }
    private ISFSArray dataEndGame;
    private void StartGame(GamePacket param)
    {
        if (countDownCoroutine != null)
        {
            StopCoroutine(countDownCoroutine);
            statusTextObject.SetActive(false);
        }
        countDownText.text = "";
        txtTimeGame.text = "";
        ChipTotal.gameObject.SetActive(false);
        StopCoroutine("ResetGame");
        processReset();
        statusTextObject.SetActive(false);
        isStarting = true;
        Debug.Log("chia bai ");
        int second = (int)param.GetLong("time_waiting") / 1000;
        StartCoroutine(showTimeNextTurn(second));
        //"{\"data\":[{\"final_chip\":50070002,\"changed_chip\":0,\"card\":\"2_18_36\",\"sfsid\":3},
        //{\"final_chip\":100500000,\"changed_chip\":0,\"card\":\"46_21_24\",\"sfsid\":2}]}"
        //startGameBtn.SetActive(false);
        int userActive = MyInfo.SFS_ID;

        ISFSArray dataUsers = param.GetSFSArray("data");
        dataEndGame = dataUsers;
        //TxtTotalBet.text = param.GetLong("total_bet_money").ToString();
        int index = 0;
        int[] ids = new int[3];
        foreach (SFSObject user in dataUsers)
        {
            string cardsList = "";
            if (user.ContainsKey("card")) cardsList = user.GetUtfString("card");
            else continue;
            if (user.GetInt("sfsid") == MyInfo.SFS_ID)
            {
                string[] cardsId = cardsList.Split('_');
                for (int i = 0; i < 3; i++)
                {
                    ids[index] = int.Parse(cardsId[i]);
                    index++;
                }
            }
        }
        List<IBaiCaoPlayer> players = new List<IBaiCaoPlayer>();
        players.Add(player);
        foreach (BaiCaoEnemyPlayer enemy in enemies)
            if (enemy.actived)
                players.Add(enemy);
        BaiCaoBasePlayer activePlayer = GetPlayerBySFSId(userActive);

        StartCoroutine(DealCard(players.ToArray(), ids, activePlayer));

        CollectMoneyStartGame(players.ToArray(), param.GetLong("bet_money"), param.GetLong("total_bet_money"));
    }

    private void CollectMoneyStartGame(IBaiCaoPlayer[] players, long betMoney, long totalBetMoney)
    {
        StartCoroutine(ShowTotalBetAfterAniCollectMoney(totalBetMoney));

        foreach (IBaiCaoPlayer iPlayer in players)
        {
            BaiCaoBasePlayer p = iPlayer as BaiCaoBasePlayer;
            //Debug.LogError("player.chip=============" + player.chip + "_"+ totalBetMoney);
            if (p == player)
            {
                player.gold.text = Utilities.GetStringMoneyByLong(player.chip - betMoney);
            }
            else
            {
                BaiCaoEnemyPlayer enemy = p as BaiCaoEnemyPlayer;
                enemy.gold.text = Utilities.GetStringMoneyByLong(enemy.chip - betMoney);
            }
            StartCoroutine(AniCollectMoneyStartGame(betMoneyTable, p));
        }
    }
    private IEnumerator ShowTotalBetAfterAniCollectMoney(long totalBetMoney)
    {
        yield return new WaitForSeconds(0.5f);
        TxtTotalBet.text = TXUtil.ConvertMoneyToShortText(totalBetMoney);
        ChipTotal.gameObject.SetActive(true);
        ChipTotal.GetComponent<Animator>().Play("BetHorse", 0, 0.25f);
    }
    private IEnumerator AniCollectMoneyStartGame(long money, BaiCaoBasePlayer player)
    {
        Image aniChipWin = Instantiate(imgAnimationChip);
        aniChipWin.transform.GetChild(0).GetComponent<Text>().text = TXUtil.ConvertMoneyToShortText(money);
        aniChipWin.gameObject.SetActive(true);
        aniChipWin.transform.SetParent(player.transform);
        //imgAnimationChip.gameObject.SetActive(true);
        aniChipWin.transform.localPosition = Vector3.zero;
        aniChipWin.transform.localScale = new Vector3(1, 1, 1);
        aniChipWin.transform.DOMove(imgAnimationChip.transform.position, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        if (aniChipWin) Destroy(aniChipWin.gameObject);
        //imgAnimationChip.transform.position = new Vector3(0, 0, 0);
        //imgAnimationChip.gameObject.SetActive(false);
    }
    private int currCountNextTurn;
    private IEnumerator showTimeNextTurn(int countdownValue)
    {
        currCountNextTurn = countdownValue;
        while (currCountNextTurn > 1)
        {
            txtTimeGame.text = (currCountNextTurn - 1).ToString();
            yield return new WaitForSeconds(1.0f);
            currCountNextTurn--;
        }
        txtTimeGame.text = "";
    }

    private void EndGame(GamePacket param)
    {
        txtTimeGame.text = "";
        isStarting = false;
        isAnimationStarting = true;
        //"{\"data\":[{\"final_chip\":50070002,\"changed_chip\":0,\"card\":\"2_18_36\",\"sfsid\":3},
        //{\"final_chip\":100500000,\"changed_chip\":0,\"card\":\"46_21_24\",\"sfsid\":2}]}"
        ChipTotal.gameObject.SetActive(false);
        //imgAnimationChip.gameObject.SetActive(false);
        if (dataEndGame != null)
        {
            foreach (SFSObject item in dataEndGame)
            {
                int userId = item.GetInt("sfsid");
                long money = item.GetLong("changed_chip");
                string cards = "";
                if (item.ContainsKey("card") == false) continue;

                cards = item.GetUtfString("card");
                string[] cardList = cards.Split('_');
                BaiCaoBasePlayer player = GetPlayerBySFSId(userId);
                if (player == null)
                    continue;

                if (userId == MyInfo.SFS_ID)
                {
                    MyInfo.CHIP = item.GetLong("final_chip");
                    if (money > 0)
                        SoundManager.PlaySound(SoundManager.WIN);
                    else if (money < 0)
                        SoundManager.PlaySound(SoundManager.LOSE);
                }
                player.StopCountDownTime();
                player.setResult(money);
                player.showPoint(item.GetInt("point"));
                if (money > 0)
                {
                    StartCoroutine(animationChipWin(money, player));
                    //rank = 1;
                }
                //ShowPlayerRank(player, rank);
                StartCoroutine(ChangeMoney(player, money));

                if (player is BaiCaoEnemyPlayer)
                    ShowEnemyCards(player as BaiCaoEnemyPlayer, cardList, new string[0]);
            }
        }

        StartCoroutine("ResetGame");
    }

    private IEnumerator animationChipWin(long money, BaiCaoBasePlayer player)
    {
        Image aniChipWin = Instantiate(imgAnimationChip, new Vector3(0, 0, 0), Quaternion.identity);
        aniChipWin.transform.GetChild(0).GetComponent<Text>().text = TXUtil.ConvertMoneyToShortText(money);
        aniChipWin.gameObject.SetActive(true);
        aniChipWin.transform.SetParent(containerChipAni.transform);
        //imgAnimationChip.gameObject.SetActive(true);
        aniChipWin.transform.localScale = Vector3.one;
        aniChipWin.transform.DOMove(player.transform.GetChild(2).position, 0.5f, false);
        yield return new WaitForSeconds(0.5f);
        if (aniChipWin) Destroy(aniChipWin.gameObject);
        //imgAnimationChip.transform.position = new Vector3(0, 0, 0);
        //imgAnimationChip.gameObject.SetActive(false);
    }

    private IEnumerator ChangeMoney(BaiCaoBasePlayer player, long money)
    {
        if (player == null)
            yield return null;
        player.chip += money;
        if (player.chip < 0)
            player.chip = 0;
        player.gold.text = Utilities.GetStringMoneyByLong(player.chip);
        if (player.sfsId == MyInfo.SFS_ID)
            MyInfo.CHIP = player.chip;

        if (money >= 0)
        {
            player.moneyText.color = Color.yellow;
            player.moneyText.text = "+" + Utilities.GetStringMoneyByLong(money);
        }
        else
        {
            player.moneyText.color = Color.gray;
            player.moneyText.text = Utilities.GetStringMoneyByLong(money);
        }

        Vector3 origin = player.moneyText.gameObject.transform.localPosition;
        player.moneyText.gameObject.transform.DOLocalMove(new Vector3(origin.x, origin.y + 50), 5, false);
        yield return new WaitForSeconds(7);
        player.moneyText.text = "";
        player.moneyText.gameObject.transform.localPosition = origin;
    }

    private void ShowEnemyCards(BaiCaoEnemyPlayer _player, string[] cardList, string[] specialCards)
    {
        List<Card> playerCards = new List<Card>();
        Transform cardParent = _player.cardGroup.transform;
        float scaleRatio;
        float ratio = (float)Screen.width / (float)Screen.height;
        foreach (string id in cardList)
        {
            if (id.Equals(""))
                continue;
            Card card = deck.cards[int.Parse(id)];
            card.transform.SetParent(cardParent);
            card.transform.localPosition = Vector3.zero;
            if (ratio >= 1.5f)
                scaleRatio = 0.7f;
            else
                scaleRatio = 1f;


            card.transform.localScale = new Vector3(0.8f, 0.8f);
            playerCards.Add(card);
        }
        _player.SortCard();
    }

    private IEnumerator ResetGame()
    {
        txtTimeGame.text = "";
        foreach (BaiCaoEnemyPlayer enemy in enemies)
            enemy.hideSampleCard();
        yield return new WaitForSeconds(10);
        processReset();
    }

    private void processReset()
    {
        isStarting = false;
        player.cards.Clear();
        cardTypeImage.enabled = false;
        Vector3 deckPosition = new Vector3(-400, -400);
        player.Reset();
        foreach (BaiCaoEnemyPlayer enemy in enemies)
            enemy.Reset();

        foreach (Card card in deck.cards)
        {
            card.GetComponent<Image>().color = Color.white;
            if (card.Type == CardType.CO || card.Type == CardType.RO)
                card.transform.GetChild(0).GetComponent<Image>().color = Color.white;
            card.transform.GetChild(1).GetComponent<Image>().color = Color.white;
            card.transform.GetChild(2).GetComponent<Image>().color = Color.white;
            card.transform.localScale = Vector3.one;
        }
        isAnimationStarting = false;
        HandleNextWork();
    }

    private void HandleUserExit(GamePacket param)
    {
        SoundManager.PlaySound(SoundManager.EXIT_ROOM);
        print("------------------handle user exit");
        int userId = param.GetInt(ParamKey.USER_ID);

        if (userId == MyInfo.SFS_ID)
        {
            //Player exit
            if (MyInfo.tournamentStatus == TournamentStatus.None)//ko dang tham gia event
            {
                LoadingManager.Instance.ENABLE = true;
                GamePacket gp = new GamePacket(CommandKey.JOIN_GAME_LOBBY_ROOM);
                gp.Put(ParamKey.GAME_ID, GameHelper.currentGid);
                sfs.SendZoneRequest(gp);
            }
            else//dang tham gia tournament
            {
                //if (GameHelper.gameKind == GameKind.All)
                //{
                //    GameHelper.ChangeScene(GameScene.HomeScene);
                //}
                //else
                //{
                GameHelper.ChangeScene(GameScene.HomeSceneV2);
                //}
            }

        }
        else
        {
            //Another exit
            BaiCaoBasePlayer player = GetPlayerBySFSId(userId);
            if (player != null)
            {
                player.sfsId = -1;
                (player as BaiCaoEnemyPlayer).ExitRoom();
            }
            //            int hostId = param.GetInt(ParamKey.HOST_ID);
            //            this.hostId = hostId;            

            HandleNextWork();
            //if (!isStarting && MyInfo.SFS_ID == hostId & CountCurentPlayer() < 1) 
            //    startGameBtn.SetActive(false);

            txtHostname.text = "Chủ Phòng:" + param.GetString(ParamKey.HOST_NAME);
        }
    }

    private int CountCurentPlayer()
    {
        int count = 0;
        foreach (BaiCaoEnemyPlayer enemy in enemies)
        {
            //Debug.LogError("enemy.actived======================================" + enemy.actived);
            if (enemy.actived)
                count++;
        }
        return count;
    }

    private void ShowPlayerRank(BaiCaoBasePlayer player, int rank)
    {
        if (rank < 1)
            return;
        player.rankImage.sprite = rankSprite[rank - 1];
        //player.rankImage.SetNativeSize();
        player.rankImage.enabled = true;
    }

    public void ExitGame()
    {
        if (isStarting)
        {

            popupFullAlert.Show("Thoát", ConstText.NotifyExitBaiCao, () =>
            {
                popupFullAlert.Hide();

            }, () =>
            {
                GamePacket param = new GamePacket(CommandKey.USER_EXIT);
                sfs.SendRoomRequest(param);
                LoadingManager.Instance.ENABLE = false;
            });
        }
        else
        {
            LoadingManager.Instance.ENABLE = true;
            sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            //GamePacket param = new GamePacket(CommandKey.USER_EXIT);
            //sfs.SendRoomRequest(param);
            //LoadingManager.Instance.ENABLE = false;
        }
    }

    private void LoadWatingRoom(GamePacket param)
    {
        int gid = param.GetInt(ParamKey.GAME_ID);
        GameHelper.currentGid = gid;
        if (gid == 3)
            GameHelper.ChangeScene(GameScene.TaiXiuScene);
        else
            SceneManager.LoadScene(GameScene.WaitingRoom.ToString());
    }

    public void BtnChatOnClick()
    {
        PopupChatManager.instance.Show();
    }

    public void OnPublicMsg(string _msg)
    {
        int idUser = int.Parse(_msg.Split('#')[0]);
        string vip = "0";
        //Get Position by ID
        int position;
        if (idUser == MyInfo.SFS_ID)
        {
            position = 0;
            vip = MyInfo.MY_ID_VIP.ToString(); ;
        }
        else
        {
            BaiCaoBasePlayer player = GetPlayerBySFSId(idUser);
            if (player == null)
                return;
            position = GetPlayerBySFSId(idUser).transform.GetSiblingIndex() + 1;
            vip = player.Vip;
        }

        PopupChatManager.instance.RspChat(position, _msg, vip);

    }

    #region invite
    public void BtnInviteOnClick()
    {
        popupInvite.Show(RqInvite, CloseInviteOnClick);
        RqInvite();
    }
    public void ItemPlayerInviteOnClick(int _id)
    {
        GamePacket param = new GamePacket(CommandKey.INVITE);
        param.Put(ParamKey.USER_ID, _id);
        SFS.Instance.SendRoomRequest(param);
    }
    void RqInvite()
    {
        GamePacket param = new GamePacket(CommandKey.GET_PLAYERS_INVITE);
        SFS.Instance.SendRoomRequest(param);
    }
    void RspGetPlayersInvite(GamePacket _param)
    {
        //string userList = _param.GetString(ParamKey.USER_LIST);

        //if (string.IsNullOrEmpty(userList))
        //    return;

        //string[] infos = userList.Split('$');

        //popupInvite.ShowItems(infos);

        popupInvite.ShowItems(_param);
    }

    public void ShowSetting()
    {
        Module.Show();
    }

    public void CloseInviteOnClick()
    {
        popupInvite.Hide();
    }
    #endregion

    public void OnConnectionLost()
    {
        popupAlert.Show(ConstText.ErrorConnection, () =>
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
        });
    }
}
