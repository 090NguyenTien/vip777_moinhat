﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DG.Tweening;

public class BaiCaoPlayer : BaiCaoBasePlayer {

    private const int CARD_WIDTH = 100;

   // public GameObject buttonsGroup;
    public GameObject passBtn;
	//public GameObject quickUnselectBtn;

    [HideInInspector]
    public Dictionary<int, Card> cards;
    [HideInInspector]
    public List<Card> currentSelectCards;
    [HideInInspector]
    //public bool isHost = false;
    //[HideInInspector]
    public int serverPosition;
    [HideInInspector]
    public int currentRecomendIndex;

	private Vector3 moneyTextPos;
	private bool counting = false;

    //[SerializeField]
    //private Image imgYourTurn = null;
   
    void Awake()
    {
        cards = new Dictionary<int, Card>();
        currentSelectCards = new List<Card>();
		moneyTextPos = moneyText.transform.localPosition;
    }

    public void UpdateTime(float newValue)
    {
        timerImage.fillAmount = newValue;
		if (newValue > 1 - 0.3f && !counting)
			StartCoroutine ("PlayCountDownSound");
    }

	public void StopCountDownSound()
	{
		counting = false;
		SoundManager.StopSound (SoundManager.COUNT_DOWN);
		StopCoroutine ("PlayCountDownSound");
	}

	IEnumerator PlayCountDownSound()
	{
		counting = true;
		int count = 3;
		while (true) 
		{
			SoundManager.PlaySound (SoundManager.COUNT_DOWN);
			count--;
			if (count == 1)
				break;
			yield return new WaitForSeconds (1);
		}
		counting = false;
	}

    public override void StopCountDownTime()
    {
        StopCountDownSound ();
        //buttonsGroup.SetActive(false);
		timerBackground.gameObject.SetActive(false);
        //imgYourTurn.gameObject.SetActive(false);
        iTween.Stop (gameObject);
        //endOfMyTurn();
        //focusPlayer.SetActive(false);
    }

    public void SortCards(bool dontTween = false)
    {
        float distance = CARD_WIDTH;
        Vector3 center = cardGroup.transform.position;
        float x = center.x - cardGroup.transform.childCount / 2 * distance;

        if (dontTween == false)
        {
            Hashtable hash = new Hashtable();
            hash.Add("time", 0.1f);
            hash.Add("position", null);
            hash.Add("easetype", iTween.EaseType.linear);
            hash.Add("islocal", true);
            foreach (Transform t in cardGroup.transform)
            {
                hash["position"] = new Vector3(x, center.y);
                iTween.MoveTo(t.gameObject, hash);
                x += distance;
            }
        }
        else
        {
            foreach (Transform t in cardGroup.transform)
            {
                t.GetComponent<RectTransform>().anchoredPosition = new Vector3(x, center.y);
                x += distance; ;
            }
        }

        
    }

    public float getCardPosXByIndex(int cardIndex, int numChild)
    {
        float distance = CARD_WIDTH;
        Vector3 center = cardGroup.transform.position;
        float x = center.x - numChild / 2 * distance;

        return x + cardIndex * distance;
    }

    public override void SetInfo(string[] info, Hashtable userVariable)
    {

        int sfsId = int.Parse(info[0]);
        string userId = info[1];
        string userName = info[2];
        long chip = long.Parse(info[3]);
        string avatar = info[4];
		string vip_collect = info [5];
        this.level = int.Parse(info[6]);
        string bor_avatar = info[7];
        txtLevel.text = info[6];
        Debug.LogWarning("txtLevel.text=====" + info[6] + "  vip_collect === " + vip_collect + "   bor_avatar ==== " + bor_avatar);



        int id_Bor = int.Parse(bor_avatar);
        if (id_Bor != -1)
        {
            if (DataHelper.GetBoderAvatar(bor_avatar) != null)
            {
                avatarBoder.sprite = DataHelper.GetBoderAvatar(bor_avatar);
                avatarBoder.gameObject.SetActive(true);
                avatarBoder_Defaut.gameObject.SetActive(false);
            }
            else
            {
                avatarBoder.gameObject.SetActive(false);
                avatarBoder_Defaut.gameObject.SetActive(true);
            }
            
        }
        else
        {
            avatarBoder.gameObject.SetActive(false);
            avatarBoder_Defaut.gameObject.SetActive(true);
        }

        avatarBoder.transform.localScale = new Vector2(1.1f, 1.1f);


        this.sfsId = sfsId;
        this.userId = userId;
        this.chip = chip;

        displayName.text = userName;
        gold.text = Utilities.GetStringMoneyByLong(chip);
		this.avatar.gameObject.SetActive (true);
		LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
		// Debug.LogError("KhuongTest==========" + loginType);
		
        //load facebook avatar -- khuong
        if (MyInfo.SFS_ID == sfsId)
        {
            ChatController.RegistTextChip(gold);
            if (loginType != LoginType.Facebook)
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
            else
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
        }
        else
        {
            if (avatar.Contains("http") == true)
            {
                StartCoroutine(UpdateAvatarThread(avatar, this.avatar));
            }
            else
            {
                //this.avatar.sprite = DataHelper.GetAvatar(avatar);
                StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + avatar, this.avatar));
            }
        }

        if (inviteBtn != null)
			inviteBtn.gameObject.SetActive (false);		
	
    }

    

    public override void Reset()
    {
        txtPoint.text = "";
        moneyText.text = "";
		moneyText.gameObject.transform.localPosition = moneyTextPos;
        rankImage.enabled = false;
        sampleCard.SetActive(false);
        currentSelectCards.Clear();
        Vector3 deckPosition = new Vector3(-400, -400);
        List<Transform> cards = new List<Transform>();
        foreach (Transform tf in cardGroup.transform)
            cards.Add(tf);
        foreach (Transform tf in cards)
        {
            tf.SetParent(controller.deck.transform);
            tf.localPosition = deckPosition;
        }
    }

//    private void SetAvatar(string avatar)
//    {
////        Texture2D mainImage;
////        WWW www = new WWW(avatarUrl);
////        yield return www;
////
////        mainImage = www.texture;
////        Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
//
//		avatar.sprite = DataHelper.GetAvatar(avatar);
//    }

}
