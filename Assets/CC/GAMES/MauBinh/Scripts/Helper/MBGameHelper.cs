﻿using UnityEngine;
using System.Collections;

public class MBGameHelper {

	public static void LogCard(int _id)
	{
		BaseCardInfo card = BaseCardInfo.Get (_id);
		Debug.Log ("Card: " + card.Value + " - " + card.Type);
	}

	public static string GetCard(int _id)
	{
		BaseCardInfo card = BaseCardInfo.Get (_id);
		return card.Value.ToString() + " - " + card.Type.ToString();
	}

	public static int GoodID(int _badID)
	{
		return (_badID % 13 + 1) * 10 + _badID / 13;
	}
	public static int BadID(int _goodID)
	{
		int rank = _goodID / 10;
		int suite = _goodID % 10;

		int id = suite * 13 + (rank % 14) - 1;

		return id;
	}

}
