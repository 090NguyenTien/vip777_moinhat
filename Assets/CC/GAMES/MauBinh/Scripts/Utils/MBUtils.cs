﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum MBType{
	HIGH_CARD,
	PAIR,
	TWO_PAIRS,
	THREE_OF_A_KIND,
	STRAIGHT,
	FLUSH,
	FULL_HOUSE,
	FOUR_OF_A_KIND,
	STRAIGHT_FLUSH,
	STRAIGHT_FLUSH_LOWER,
	STRAIGHT_FLUSH_UPPER,
}

public enum MBTypeImmediate{
	NULL,
	TWELVE_OF_BLACK_AND_ONE_RED,
		TWELVE_OF_RED_AND_ONE_BLACK,
	THREE_FLUSH,
	THREE_STRAIGHT,
	SIX_PAIRS,
	FIVE_PAIRS_AND_ONE_THREE_OF_A_KIND,
	DRAGON_STRAIGHT,
	SAME_COLOR,
	FOUR_THREE_OF_A_KIND,
	DRAGON_ROYALE_FLUSH
}

public class MBUtils {

	/// <summary>
	/// Ktra chi thuong
	/// </summary>
	public static MBType getTypeRow(List<BaseCardInfo> cards){
		SortCardsList(cards);
				int pairNum = countPairNumber(cards);
				int threeOfAKindNum = countThreeOfAKindNumber(cards);
				int fourOfAKindNum = countFourOfAKindNumber(cards);
				
				if(pairNum == 0 && threeOfAKindNum == 0 && fourOfAKindNum == 0){
					if(isStraightFlushUpper(cards))
				return MBType.STRAIGHT_FLUSH_UPPER;
					if(isStraightFlushLower(cards))
				return MBType.STRAIGHT_FLUSH_LOWER;
					if(isStraightFlush(cards))
				return MBType.STRAIGHT_FLUSH;
					if(isStraight(cards))
				return MBType.STRAIGHT;
					if(isFlush(cards))
				return MBType.FLUSH;
			return MBType.HIGH_CARD;
				}
				
				if(fourOfAKindNum == 1)//Tu quy
			return MBType.FOUR_OF_A_KIND;
				if(pairNum == 1 && threeOfAKindNum == 1)//Cu lu
			return MBType.FULL_HOUSE;
				if(threeOfAKindNum == 1)//Xam chi
			return MBType.THREE_OF_A_KIND;
				if(pairNum == 2)
			return MBType.TWO_PAIRS;
				if(pairNum == 1)
			return MBType.PAIR;
		return MBType.HIGH_CARD;


//		List<MBCardInfo> originalCards = cards;
//		SortCardsList (_rowCards);
//
//		switch (getNumberOfSameCard (_rowCards)) {
//		case 0:
//			if (_rowCards.Count == 3)
//				return MBType.HIGH_CARD;
//			
//			if (isStraightFlushUpper (_rowCards))
//				return MBType.STRAIGHT_FLUSH_UPPER;
//			else if (isStraightFlushLower (_rowCards))
//				return MBType.STRAIGHT_FLUSH_LOWER;
//			else if (isStraightFlush (_rowCards))
//				return MBType.STRAIGHT_FLUSH;
//			else if (isFlush (_rowCards))
//				return MBType.FLUSH;
//			else if (isStraight (_rowCards))
//				return MBType.STRAIGHT;
//			break;
//		case 2:
//			return MBType.PAIR;
//		case 4:
//			return MBType.TWO_PAIRS;
//		case 6:
//			return MBType.THREE_OF_A_KIND;
//		case 8:
//			return MBType.FULL_HOUSE;
//		case 12:
////			foreach (BaseCardInfo card in _rowCards) {
////				if (card.Value == 14)
////					return MBType.FOUR_AS;
////			}
//			return MBType.FOUR_OF_A_KIND;
//		}
//		return MBType.HIGH_CARD;
	}

	public static MBTypeImmediate getTypeThreeSpecial(List<BaseCardInfo> _handCards)
	{
		if(isThreeStraight(_handCards))
			return MBTypeImmediate.THREE_STRAIGHT;
		else if(isThreeFlush(_handCards))
			return MBTypeImmediate.THREE_FLUSH;

		return MBTypeImmediate.NULL;
	}
	/// <summary>
	/// Ktra win special 
	/// </summary>
	public static MBTypeImmediate getTypeImmediateWin(List<BaseCardInfo> _handCards){

		List<BaseCardInfo> handCards = new List<BaseCardInfo> ();
		handCards.AddRange(_handCards);//(List<Card>) cards.clone();

		if (handCards.Count != 13)
			return MBTypeImmediate.NULL;
		
			SortCardsList(handCards);

		int pairCount = countPairNumber(handCards);
		int threeOfAKindCount = countThreeOfAKindNumber(handCards);
		int fourOfAKindCount = countFourOfAKindNumber(handCards);


		if (isDragonRoyaleFlush (handCards))
			return MBTypeImmediate.DRAGON_ROYALE_FLUSH;
		else if (isFourThreeOfAKind (pairCount, threeOfAKindCount, fourOfAKindCount))
			return MBTypeImmediate.FOUR_THREE_OF_A_KIND;
		else if (isSameColor (handCards))
			return MBTypeImmediate.SAME_COLOR;
//		else if (is12Red (handCards))
//			return MBTypeImmediate.TWELVE_OF_RED_AND_ONE_BLACK;
//		else if (is12Black (handCards))
//			return MBTypeImmediate.TWELVE_OF_BLACK_AND_ONE_RED;
		else if (isDragonStraight (handCards))
				return MBTypeImmediate.DRAGON_STRAIGHT;
		else if (isFivePairsAndOneThreeOfAKind (pairCount, threeOfAKindCount, fourOfAKindCount))
				return MBTypeImmediate.FIVE_PAIRS_AND_ONE_THREE_OF_A_KIND;
		else if (isSixPairs (pairCount, threeOfAKindCount, fourOfAKindCount))
				return MBTypeImmediate.SIX_PAIRS;
		else if (isThreeStraight (_handCards))
				return MBTypeImmediate.THREE_STRAIGHT;
		else if (isThreeFlush (_handCards))
				return MBTypeImmediate.THREE_FLUSH;
		else {
				
				return isTwelveOfAColorAndOneOther (handCards);
//						return MBTypeImmediate.NULL;
		}


	}

	private static bool isStraightFlushUpper(List<BaseCardInfo> cards){
		if (cards.Count < 5)
			return false;
        if (!isStraight(cards)) return false;

        BaseCardInfo previousCard = cards [0];
		for (int i = 1; i < 5; i++) {
			if (previousCard.Type != cards [i].Type)// || previousCard.Value + 1 != cards [i].Value)
				return false;
			else
				previousCard = cards [i];
		}
		if (cards [0].Value == 10 && cards [4].Value == 14)
			return true;
		return false;
	}

	private static bool isStraightFlushLower(List<BaseCardInfo> cards){
		if (cards.Count < 5)
			return false;
        if (!isStraight(cards)) return false;
        BaseCardInfo previousCard = cards [0];
		for (int i = 1; i < 5; i++) {
			if (previousCard.Type != cards [i].Type)// || previousCard.Value + 1 != cards [i].Value)
				return false;
			else
				previousCard = cards [i];
		}
		if (cards [4].Value == 14 && cards[0].Value == 2)
			return true;
		
		return false;
	}

	private static bool isStraightFlush(List<BaseCardInfo> cards){
		if (cards.Count < 5)
			return false;
        if (!isStraight(cards)) return false;
        BaseCardInfo previousCard = cards [0];
		for (int i = 1; i < 5; i++) {
			if (previousCard.Type != cards [i].Type)// || previousCard.Value + 1 != cards [i].Value)
				return false;
			previousCard = cards [i];
//			if (i == 4)
//				return true;
		}
		return true;
	}

	private static bool isFlush(List<BaseCardInfo> cards){
		if(cards.Count != 5)
			return false;
		foreach (BaseCardInfo card in cards) {
			if(card.Type != cards[0].Type)
				return false;
		}
		return true;
	}

	private static bool isStraight(List<BaseCardInfo> cards){
		if(cards.Count != 5)
			return false;
		BaseCardInfo previous = cards[0];
		for(int i = 1; i < 4; i++){
			if((previous.Value + 1 != cards[i].Value))
				return false;
			previous = cards[i];
		}
		if(previous.Value + 1 == cards[4].Value)
			return true;
		if(cards[0].Value == 2 && cards[4].Value == 14)
			return true;

		Debug.Log ("isStraight FALSE");
		return false;

	}
	private static bool isMiniStraight(List<BaseCardInfo> cards){
		if(cards.Count != 3)
			return false;
		if(cards[0].Value + 1 != cards[1].Value)
			return false;
		if(cards[1].Value + 1 != cards[2].Value){
			if(cards[0].Value == 2 && cards[2].Value == 14)
				return true;
			return false;
		}
		return true;
	}

	private static int getNumberOfSameCard(List<BaseCardInfo> cards){
		int count = 0;
		for (int i = 0; i < cards.Count; i++) {
			for (int j = 0; j < cards.Count; j++) {
				if (i != j) {
					if (cards [i].Value == cards [j].Value)
						count++;
				}
			}
		}
		return count;
	}

	private static bool isDragonRoyaleFlush(List<BaseCardInfo> cards){
		BaseCardInfo previous = cards[0];
		for (int i = 1; i < cards.Count; i++) {
			if (previous.Type != cards [i].Type || previous.Value + 1 != cards [i].Value)
				return false;
			previous = cards [i];
		}
		return true;
	}

	private static bool isFourThreeOfAKind(int _pair, int _three, int _four)
	{
		return _three == 4;
	}

	private static bool isSameColor(List<BaseCardInfo> cards){

		int countBlack = 0, countRed = 0;
		foreach (BaseCardInfo card in cards) {
			if ((int)card.Type == 0 || (int)card.Type == 1)
				countBlack++;
			else if ((int)card.Type == 2 || (int)card.Type == 3)
				countRed++;
		}
		if (countBlack == 13 || countRed == 13)
			return true;
//		Debug.Log (countBlack + " - " + countRed);
		return false;

	}
//	private static bool is12Black(List<BaseCardInfo> cards){
//
//		int countBlack = 0;
//		foreach (BaseCardInfo card in cards) {
//			if ((int)card.Type == 0 || (int)card.Type == 1)
//				countBlack++;
//		}
//		if (countBlack == 12)
//			return true;
//		
//		return false;
//
//	}
//	private static bool is12Red(List<BaseCardInfo> cards){
//
//		int countRed = 0;
//		foreach (BaseCardInfo card in cards) {
//			if ((int)card.Type == 2 || (int)card.Type == 3)
//				countRed++;
//		}
//		if (countRed == 12)
//			return true;
//		
//		return false;
//
//	}


	private static bool isDragonStraight(List<BaseCardInfo> cards){
		BaseCardInfo previous = cards [0];
		for (int i = 1; i < cards.Count; i++) {
			if (previous.Value + 1 != cards [i].Value)
				return false;
			previous = cards [i];
		}
		return true;
	}

	private static bool isFivePairsAndOneThreeOfAKind(int _pair, int _three, int _four){
		if(_pair == 5 && _three == 1)
			return true;
		if(_pair == 3 && _three == 1 && _four == 1)
			return true;
		if(_pair == 1 && _three == 1 && _four == 2)
			return true;
		return false;
	}

	private static bool isSixPairs(int _pair, int _three, int _four){
		if (_pair == 6)
			return true;
		if (_pair == 4 && _four == 1)
			return true;
		if (_pair == 2 && _four == 2)
			return true;
		return false;
	}

	public static bool isThreeStraight(List<BaseCardInfo> cards){
		if(cards.Count != 13)
				return false;
		List<BaseCardInfo> part1 = new List<BaseCardInfo>();
		List<BaseCardInfo> part2 = new List<BaseCardInfo>();
		List<BaseCardInfo> part3 = new List<BaseCardInfo>();

		for (int i = 0; i < cards.Count; i++) {
			if (i >= 0 && i < 5)
				part1.Add (cards [i]);
			else if (i >= 5 && i < 10)
				part2.Add (cards [i]);
			else if (i >= 10)
				part3.Add (cards [i]);
		}

		SortCardsList (part1);
		SortCardsList (part2);
		SortCardsList (part3);
//		Debug.Log (isStraight (part1) +" _ "+ isStraight (part2) +" _ "+ isMiniStraight (part3));
		if (isStraight (part1) && isStraight (part2) && isMiniStraight (part3))
			return true;
//		Debug.Log ("NOT 3 STRAGIGHT");
		return false;
	}
	private static bool isMiniFlush(List<BaseCardInfo> cards){
		foreach (BaseCardInfo card in cards) {
			if (card.Type != cards [0].Type)
				return false;
		}
		return true;
	}
	public static bool isThreeFlush(List<BaseCardInfo> cards){
		if(cards.Count != 13)
				return false;
		List<BaseCardInfo> part1 = new List<BaseCardInfo>();
		List<BaseCardInfo> part2 = new List<BaseCardInfo>();
		List<BaseCardInfo> part3 = new List<BaseCardInfo>();

		for(int i = 0; i < cards.Count; i++){
			if (i >= 0 && i < 5)
				part1.Add (cards [i]);
			else if (i >= 5 && i < 10)
				part2.Add (cards [i]);
			else if (i >= 10)
				part3.Add (cards [i]);
			}

			SortCardsList(part1);
			SortCardsList(part2);
			SortCardsList(part3);

		if(isFlush(part1) && isFlush(part2) && isMiniFlush(part3))
				return true;
			return false;
		}

		private static MBTypeImmediate isTwelveOfAColorAndOneOther(List<BaseCardInfo> cards){
				int countBlack = 0, countRed = 0;
				foreach (BaseCardInfo card in cards) {
						if ((int)card.Type == 0 || (int)card.Type == 1)
								countBlack++;
						else if ((int)card.Type == 2 || (int)card.Type == 3)
								countRed++;
				}
				if (countBlack == 12 && countRed == 1)
						return MBTypeImmediate.TWELVE_OF_BLACK_AND_ONE_RED;
				else if (countBlack == 1 && countRed == 12)
						return MBTypeImmediate.TWELVE_OF_RED_AND_ONE_BLACK;
				
				return MBTypeImmediate.NULL;
		}
//	private static bool isTwelveOfAColorAndOneOther(List<BaseCardInfo> cards){
//		int countBlack = 0, countRed = 0;
//		foreach (BaseCardInfo card in cards) {
//			if ((int)card.Type == 0 || (int)card.Type == 1)
//				countBlack++;
//			else if ((int)card.Type == 2 || (int)card.Type == 3)
//				countRed++;
//			}
//		if ((countBlack == 12 && countRed == 1) || (countBlack == 1 && countRed == 12))
//			return true;
//		return false;
//	}

	private static void SortCardsList(List<BaseCardInfo> cards){
		cards.Sort(delegate (BaseCardInfo x, BaseCardInfo y)
			{
				if (x.Value == y.Value)
					return ((int)x.Type).CompareTo((int)y.Type);
				return x.Value.CompareTo(y.Value);
			});
		}
	/// <summary>
	/// list1 luôn luôn >= lá bài của list2
	/// </summary>
	private static int compareHighCards(List<BaseCardInfo> _list1, List<BaseCardInfo> _list2){
		
		List<BaseCardInfo> list1 = new List<BaseCardInfo> ();
		List<BaseCardInfo> list2 = new List<BaseCardInfo> ();

		if (_list1.Count < _list2.Count) {
			list1 = _list2;
			list2 = _list1;
		}
		else {
			list1 = _list1;
			list2 = _list2;
		}

		for (int i = 1; i <= list2.Count; i++) {
			if (list1 [list1.Count - i].Value < list2 [list2.Count - i].Value)
				return -1;
			else if (list1 [list1.Count - i].Value > list2 [list2.Count - i].Value)
				return 1;
		}
		if (_list1.Count < _list2.Count) {
			Debug.Log ("<");
			return -1;
		} else if (_list1.Count > _list2.Count) {
			Debug.Log (">");
			return 1;
		}
		return 0;

	}

	private static int comparePairs(List<BaseCardInfo> _list1, List<BaseCardInfo> _list2){
		List<BaseCardInfo> list1 = new List<BaseCardInfo> ();
		List<BaseCardInfo> list2 = new List<BaseCardInfo> ();

		if (_list1.Count < _list2.Count) {
			list1 = _list2;
			list2 = _list1;
		}
		else {
			list1 = _list1;
			list2 = _list2;
		}

		int pair1Value = -1, pair2Value = -1;

		int count1 = 0;
		int count2 = 0;

		for(int i = 0; i < list1.Count; i++){
			
			for(int j = 0; j < list1.Count; j++){
				if (i != j) {
					if (list1 [i].Value == list1 [j].Value)
						pair1Value = list1 [i].Value;

					if (i < list2.Count && j < list2.Count) {
						if (list2 [i].Value == list2 [j].Value)
							pair2Value = list2 [i].Value;
					}
					if (pair1Value != -1 && pair2Value != -1)
						break;
				}
			}
		}

//		Debug.Log ("Pair Value : " + pair1Value + " vs " + pair2Value);

		if (pair1Value < pair2Value)
			return -1;
		else if (pair1Value > pair2Value)
			return 1;

		return compareHighCards (_list1, _list2);
//		for (int i = 0; i < 5; i++) {
//			if (list1 [i].Value < list2 [i].Value)
//				return -1;
//			else if (list1 [i].Value > list2 [i].Value)
//				return 1;
//		}
		return 0;
	}

	private static int compareTwoPairs(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
//		List<MBCardInfo> list1 = new List<MBCardInfo> ();
//		List<MBCardInfo> list2 = new List<MBCardInfo> ();
//
//		if (_list1.Count < _list2.Count)
//			list1 = _list2;
//		else {
//			list1 = _list1;
//			list2 = _list2;
//		}

		if (list1 [3].Value > list2 [3].Value)
			return 1;
		else if (list1 [3].Value < list2 [3].Value)
			return -1;

		if (list1 [1].Value > list2 [1].Value)
			return 1;
		else if (list1 [1].Value < list2 [1].Value)
			return -1;

		for (int i = 0; i < 5; i++) {
			if (i % 2 == 0) {
				if (list1 [i].Value > list2 [i].Value)
					return 1;
				else if (list1 [i].Value < list2 [i].Value)
					return -1;
			}
		}
		
		return 0;
	}

	private static int compareThreeOfAKind(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		if (list1 [2].Value > list2 [2].Value)
			return 1;
		else if (list1 [2].Value < list2 [2].Value)
			return -1;
		return 0;
	}

	private static int compareStraight(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		
		if (list1 [list1.Count - 1].Value < list2 [list2.Count - 1].Value)
			return -1;
		if (list1 [list1.Count - 1].Value > list2 [list2.Count - 1].Value)
			return 1;

		if (list1 [0].Value > list2 [0].Value)
			return 1;
		if (list1 [0].Value < list2 [0].Value)
			return -1;
		
		return 0;
	}

	private static int compareFlushs(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			return compareHighCards(list1, list2);
		}

	private static int compareFullHouse(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			return compareThreeOfAKind(list1, list2);
		}

	private static int compareFourOfAKinds(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			return compareThreeOfAKind(list1, list2);
		}

	private static int compareStraightFlushes(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
			return compareStraight(list1, list2);
		}

	public static int compare(List<BaseCardInfo> list1, List<BaseCardInfo> list2){
		SortCardsList (list1);
		SortCardsList (list2);
		MBType type1 = getTypeRow(list1);
		MBType type2 = getTypeRow(list2);

		if((int)type1 < (int)type2)
				return -1;
		else if((int)type1 > (int)type2)
				return 1;

		switch (type1) {
		case MBType.HIGH_CARD:
			return compareHighCards (list1, list2);
		case MBType.PAIR:
			return comparePairs (list1, list2);
		case MBType.TWO_PAIRS:
			return compareTwoPairs (list1, list2);
		case MBType.THREE_OF_A_KIND:
			return compareThreeOfAKind (list1, list2);
		case MBType.STRAIGHT:
			return compareStraight (list1, list2);
		case MBType.FLUSH:
			return compareFlushs (list1, list2);
		case MBType.FULL_HOUSE:
			return compareFullHouse (list1, list2);
		case MBType.FOUR_OF_A_KIND:
			return compareFourOfAKinds (list1, list2);
		default:
			return compareHighCards (list1, list2);
		}
	}
	private static int countSameNumber(List<BaseCardInfo> cards, int numberToSame){
		int result = 0;
		List<int> passElements = new List<int> ();

		for (int i = 0; i < cards.Count; i++) {
			int count = 0;
			for (int j = 0; j < cards.Count; j++) {
				if (i == j || passElements.Contains (i) || passElements.Contains (j))
					continue;
				if (cards [i].Value == cards [j].Value)
					count++;
			}
			if (count == numberToSame) {
				result++;
				passElements.Add (i);
			}
		}
		return result;
	}
	private static int countPairNumber(List<BaseCardInfo> cards){
		return countSameNumber (cards, 1);
	}
		
	private static int countThreeOfAKindNumber(List<BaseCardInfo> cards){
		return countSameNumber(cards, 2);
	}
	
	private static int countFourOfAKindNumber(List<BaseCardInfo> cards){
		return countSameNumber(cards, 3);
	}

}

