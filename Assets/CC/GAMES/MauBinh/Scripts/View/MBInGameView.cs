﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using ControllerMB;

namespace ViewMB {
		
	public class MBInGameView : MonoBehaviour {

		MBInGameController Controller;

		[SerializeField]
		private Text txtRoomId, txtBet, txtName, txtState, txtHostName;
		[SerializeField]
		private Button btnSortFinish, btnSortAgain, btnStartGame, 
			btnChat, btnExit;
		[SerializeField]
		private List<Button> lstBtnInvite;

		// Use this for initialization
	//	void Awake () {
	//		Controller = new MBInGameController ();
	//		Controller.Init (this);
	//	}

		public void Init(MBInGameController _controller)
		{
			Controller = _controller;

			btnSortFinish.onClick.AddListener (SortedOnClick);
			btnSortAgain.onClick.AddListener (SortAgainOnClick);


			for (int i = 0; i < lstBtnInvite.Count; i++) {
				lstBtnInvite[i].onClick.AddListener (InviteOnClick);
			}
			btnChat.onClick.AddListener (BtnChatOnClick);

			btnExit.onClick.AddListener (ExitOnClick);
		}

		void ExitOnClick()
		{
			Controller.ExitOnClick ();
		}
		void InviteOnClick ()
		{
						Controller.BtnInviteOnClick ();
		}
		void BtnChatOnClick()
		{
			Controller.BtnChatOnClick ();
		}

		public void ShowTableInfo(int _roomId, int _hostID, long _bet, string _hostnamme="")
		{
			txtRoomId.text = "Bàn số: " + _roomId.ToString();
			txtBet.text = "Mức cược: " + Utilities.GetStringMoneyByLong (_bet);
			txtName.text = ConstText.MauBinh;
            txtHostName.text = "Chủ Phòng:"+_hostnamme;

        }
        public void UpdateChuPhong(string _hostnamme)
        {
            txtHostName.text = "Chủ Phòng:" + _hostnamme;
        }
		#region TABLE STATE

		public void ShowTableState(string _msg)
		{
			txtState.gameObject.SetActive (true);
			txtState.text = _msg;
		}
		public void HideTableState()
		{
			txtState.gameObject.SetActive (false);
		}

		#endregion

		#region TABLE RESULT

		public void ShowTableResult()
		{

		}
		public void HideTableResult()
		{

		}

		#endregion

		#region Button Groups

		void SortedOnClick()
		{
			Controller.RqFinishMove ();
		}
		void SortAgainOnClick()
		{
			Controller.RqUnFinishMove ();
		}
		void StartGameOnClick()
		{
			Controller.RqStartGame ();
		}

		public void SetEnableBtnSorted(bool _enable)
		{
			btnSortFinish.gameObject.SetActive (_enable);
		}
		public void SetEnableBtnSortAgain(bool _enable)
		{
			btnSortAgain.gameObject.SetActive (_enable);
		}
		public void SetEnableBtnStartGame(bool _enable)
		{
//			btnStartGame.gameObject.SetActive (_enable);
		}

		#endregion

		public void FinishMove(int i)
		{
			Debug.Log("iii : " + i);
		}

	}
}