﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using SimpleJSON;

namespace ViewMB
{

    public class MBPlayerView : BasePlayerView
    {

        [SerializeField]
        private GameObject StateSorting, StateSorted, panelChip, BtnInfo, ViTriBom;
        [SerializeField]
        private Text txtGoldResult;
        [SerializeField]
        PopupInfoEnemy InfoEnemy;
        [SerializeField]
        public int Id_ViTriBom;

        const float TIME_GOLD_RESULT = 4; //Thgian kqua tien bay len

        public int SFS_ID;
        #region Extend

        public override void ShowPlayer(BaseUserInfo userInfo, Hashtable userVariable)
        {
            LogoBang.gameObject.SetActive(false);
            string logoBangUrl = userInfo.logoBangUrl;
            if (logoBangUrl != "")
            {
                LogoBang.gameObject.SetActive(true);
                StartCoroutine(UpdateAvatarThread(logoBangUrl, LogoBang));
            }
            Debug.Log("show player ne");
            imgAvatar.gameObject.SetActive(true);
            txtChip.gameObject.SetActive(true);
            txtDisName.gameObject.SetActive(true);

            sfsId = userInfo.IdSFS;
            userId = userInfo.IdUser;

            if (userVariable != null)
            {
                if (userVariable.ContainsKey("clanid")) this.IdBang = userVariable["clanid"].ToString();//edit khuong;
                if (userVariable.ContainsKey("ringid")) this.ringid = userVariable["ringid"].ToString();
                if (userVariable.ContainsKey("gender")) this.gender = userVariable["gender"].ToString();
                if (userVariable.ContainsKey("gender")) this.partner = userVariable["partner"].ToString();
            }

            if (ringid != "")
            {
                if (LogoBang.transform.Find("RingMarried") == null)
                {
                    LogoBang.gameObject.SetActive(true);
                    GameObject ring = Instantiate(LogoBang.gameObject, LogoBang.transform);
                    float posX = 0;
                    if (logoBangUrl != "")
                    {
                        if (userInfo.ClientPos == 1)
                        {
                            posX = -60;
                        }
                        else
                        {
                            posX = 60;
                        }

                    }
                    ring.transform.localPosition = new Vector3(posX, LogoBang.transform.position.y);
                    ring.name = "RingMarried";
                    ring.AddComponent<Image>();
                    ring.GetComponent<Image>().raycastTarget = false;
                    ring.SetActive(true);
                    StartCoroutine(UpdateAvatarThread(API.Instance.DOMAIN + "/files/assets/ring/" + ringid, ring.GetComponent<Image>()));
                }
            }
            else
            {
                if (LogoBang.transform.Find("RingMarried") != null)
                {
                    GameObject ringMar = LogoBang.transform.Find("RingMarried").gameObject;
                    if (ringMar != null)
                    {
                        Destroy(ringMar);
                    }
                }
            }


            Gem = userInfo.Gem;
            txtChip.text = Utilities.GetStringMoneyByLong(userInfo.Chip);
            txtDisName.text = userInfo.Name;
            txtLevel.text = userInfo.Level.ToString();
            Debug.LogWarning("txtLevel.text=====" + userInfo.Level + "   vip_collect = " + userInfo.Vip_collec + "    bor_avatar == " + userInfo.Bor_Avatar);
            this.Vip = DataHelper.GetStringVipByStringVipPoint(userInfo.Vip_collec);

            string My_bor = userInfo.Bor_Avatar;
            int id_Bor = int.Parse(My_bor);
            if (id_Bor != -1)
            {
                if (DataHelper.GetBoderAvatar(My_bor) != null)
                {
                    imgBorderAvt.sprite = DataHelper.GetBoderAvatar(My_bor);
                    imgBorderAvt.gameObject.SetActive(true);
                    imgBorderAvt_Defaut.gameObject.SetActive(false);
                }
                else
                {
                    imgBorderAvt.gameObject.SetActive(false);
                    imgBorderAvt_Defaut.gameObject.SetActive(true);
                }
                
            }
            else
            {
                imgBorderAvt.gameObject.SetActive(false);
                imgBorderAvt_Defaut.gameObject.SetActive(true);
            }
            imgBorderAvt.transform.localScale = new Vector2(1.1f, 1.1f);

            panelChip.SetActive(true);
            if (BtnInfo != null)
            {
                BtnInfo.SetActive(true);
            }
            if (ViTriBom != null)
            {
                ViTriBom.SetActive(true);
            }
            //imgAvatar.sprite = DataHelper.GetAvatar (userInfo.Avatar);

            LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            // Debug.LogError("KhuongTest==========" + loginType);
            //if (loginType != LoginType.Facebook) {
            //	imgAvatar.sprite = DataHelper.GetAvatar (userInfo.Avatar);
            //} else {
            //	StartCoroutine (UpdateAvatarThread (userInfo.Avatar, imgAvatar));
            //}
            //load avatar facebook -- khuong
            if (MyInfo.SFS_ID == sfsId)
            {
                ChatController.RegistTextChip(txtChip);
                if (loginType != LoginType.Facebook)
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
                else
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
            }
            else
            {
                if (userInfo.Avatar.Contains("http") == true)
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
                else
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
            }
            //end


            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(false);
            //	imgBorderAvt.gameObject.SetActive (true);
        }

        public override void HidePlayer()
        {
            LogoBang.gameObject.SetActive(false);
            imgAvatar.gameObject.SetActive(false);
            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(true);
            //imgBorderAvt.sprite = DataHelper.GetVip (0);
            imgBorderAvt.gameObject.SetActive(false);
            imgBorderAvt_Defaut.gameObject.SetActive(false);
            txtChip.gameObject.SetActive(false);
            txtDisName.gameObject.SetActive(false);
            panelChip.SetActive(false);
            if (BtnInfo != null)
            {
                BtnInfo.SetActive(false);
            }
            if (ViTriBom != null)
            {
                ViTriBom.SetActive(false);
            }
        }
        public override void UpdateChip(long _chip)
        {
            txtChip.text = Utilities.GetStringMoneyByLong(_chip);
        }


        public void LayThongTinBangUser(string idUser)
        {
            //  API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangHoi_user_1);
            API.Instance.RequestLayIdBangHoi(idUser, RspLayIdBangHoi);
        }


        void RspLayIdBangHoi(string _json)
        {

            Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

            JSONNode node = JSONNode.Parse(_json);

            string userBang = node["user_clan"]["$id"].Value;
            string BangMaster = node["master_clan"]["$id"].Value;

            //  Debug.LogWarning("usserBang------------- " + userBang);
            //  Debug.LogWarning("BangMaster------------- " + BangMaster);

            //IdBangMaster = BangMaster;
            //IdBangUserBiNem = userBang;
            this.IdBang = userBang;
        }



        #endregion

        public void SetEnableStateSorting(bool _enable)
        {
            StateSorting.SetActive(_enable);
        }
        public void SetEnableStateSorted(bool _enable)
        {
            StateSorted.SetActive(_enable);
        }

        public void ShowGoldResult(bool _isWin, long _gold)
        {
            if (_isWin)
            {

                txtGoldResult.text = "+" + Utilities.GetStringMoneyByLong(_gold);
                txtGoldResult.color = Color.yellow;


            }
            else
            {
                txtGoldResult.text = Utilities.GetStringMoneyByLong(_gold);
                txtGoldResult.color = Color.green;
            }

            //		txtGoldResult.transform.localPosition = new Vector3 (0, 20, 0);
            //		txtGoldResult.transform.localPosition = Vector3.zero;
            txtGoldResult.transform.DOLocalMoveY(40, TIME_GOLD_RESULT).From();

            txtGoldResult.DOFade(.25f, TIME_GOLD_RESULT);
        }

        bool isNem = false;
        IEnumerator DelayNemCoroutine()
        {
            yield return new WaitForSeconds(2);
            isNem = false;
        }
        public void NemBomVaoEnemy(int vitri)
        {
            if (isNem) return;
            StartCoroutine(DelayNemCoroutine());
            isNem = true;
            if (this.imgAvatar.gameObject.activeInHierarchy == true)
            {
                Sprite Ava = this.imgAvatar.sprite;
                Sprite Khung = this.imgBorderAvt.sprite;
                string ten = this.txtDisName.text;
                string vip = this.Vip;
                string level = this.txtLevel.text;
                string chip = this.txtChip.text;
                string gem = Utilities.GetStringMoneyByLong(this.Gem);
                string IDBANG = this.IdBang;
                string gioitinh = this.gender;
                int id_gioitinh = int.Parse(gioitinh);
                string ten_nguoi_tinh = this.partner;
                // this.sfsId;

                Debug.LogWarning("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem + " sfsId = " + this.sfsId);
                InfoEnemy.Init(userId, Ava, Khung, ten, vip, level, chip, gem, vitri, IDBANG, id_gioitinh, ten_nguoi_tinh, this.sfsId);
            }
        }

    }
}