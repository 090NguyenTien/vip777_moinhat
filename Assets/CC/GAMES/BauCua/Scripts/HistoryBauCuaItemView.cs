﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HistoryBauCuaItemView : MonoBehaviour {

    [SerializeField]
    Text TypeTxt, BetTxt, GetTxt;

    private HistoryItemBauCua item;

    public void Init(HistoryItemBauCua item)
    {
        this.item = item;
    }

    public void Show()
    {
        
        TypeTxt.text = item.Type;
        BetTxt.text = item.Bet;
        GetTxt.text = item.Get;
    }
}
