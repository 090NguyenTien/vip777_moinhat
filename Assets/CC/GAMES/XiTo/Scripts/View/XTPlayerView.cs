﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using BaseCallBack;
using HelperXT;
using ConstXT;
using SimpleJSON;

namespace ViewXT
{

    public class XTPlayerView : BasePlayerView
    {
        [SerializeField]
        RectTransform rectTxtChipResult;
        [SerializeField]
        private Text txtChipResult, txtAction;
        [SerializeField]
        Image imgAction;
        [SerializeField]
        private Image imgValueHandCards;
        [SerializeField]
        private GameObject panelChip, Win, Lose, BtnInfo, ViTriBom;

        [SerializeField]
        PopupInfoEnemy InfoEnemy;


        [SerializeField]
        public int Id_ViTriBom;

        const float TIME_GOLD_RESULT = 4; //Thgian kqua tien bay len
        [SerializeField]
        Text txtResultPoint;
        #region Extend

        public override void ShowPlayer(BaseUserInfo userInfo, Hashtable userVariable)
        {
            LogoBang.gameObject.SetActive(false);
            string logoBangUrl = userInfo.logoBangUrl;
            if (logoBangUrl != "")
            {
                LogoBang.gameObject.SetActive(true);
                StartCoroutine(UpdateAvatarThread(logoBangUrl, LogoBang));
            }

            panelChip.SetActive(true);
            if (BtnInfo != null)
            {
                BtnInfo.SetActive(true);
            }
            if (ViTriBom != null)
            {
                ViTriBom.SetActive(true);
            }
            txtChip.gameObject.SetActive(true);
            txtDisName.gameObject.SetActive(true);

            sfsId = userInfo.IdSFS;
            userId = userInfo.IdUser;

            if (userVariable != null)
            {
                if (userVariable.ContainsKey("clanid")) this.IdBang = userVariable["clanid"].ToString();//edit khuong;
                if (userVariable.ContainsKey("ringid")) this.ringid = userVariable["ringid"].ToString();
                if (userVariable.ContainsKey("gender")) this.gender = userVariable["gender"].ToString();
                if (userVariable.ContainsKey("gender")) this.partner = userVariable["partner"].ToString();
            }

            if (ringid != "")
            {
                if (LogoBang.transform.Find("RingMarried") == null)
                {
                    LogoBang.gameObject.SetActive(true);
                    GameObject ring = Instantiate(LogoBang.gameObject, LogoBang.transform);
                    float posX = 0;
                    if (logoBangUrl != "")
                    {
                        if(userInfo.ClientPos==1 || userInfo.ClientPos == 2)
                        {
                            posX = -60;
                        }
                        else
                        {
                            posX = 60;
                        }                        
                    }
                    ring.transform.localPosition = new Vector3(posX, LogoBang.transform.position.y);
                    ring.name = "RingMarried";
                    ring.AddComponent<Image>();
                    ring.GetComponent<Image>().raycastTarget = false;
                    ring.SetActive(true);
                    StartCoroutine(UpdateAvatarThread(API.Instance.DOMAIN + "/files/assets/ring/" + ringid, ring.GetComponent<Image>()));
                }
            }
            else
            {
                if (LogoBang.transform.Find("RingMarried") != null)
                {
                    GameObject ringMar = LogoBang.transform.Find("RingMarried").gameObject;
                    if (ringMar != null)
                    {
                        Destroy(ringMar);
                    }
                }
            }


            Gem = userInfo.Gem;
            txtChip.text = Utilities.GetStringMoneyByLong(userInfo.Chip);
            txtDisName.text = userInfo.Name;
            txtLevel.text = userInfo.Level.ToString();
            Debug.LogWarning("txtLevel.text=====" + userInfo.Level + "       Vip_collect======= " + userInfo.Vip_collec + "       BorAvatar========= " + userInfo.Bor_Avatar);
            imgAvatar.gameObject.SetActive(true);

            this.Vip = DataHelper.GetStringVipByStringVipPoint(userInfo.Vip_collec);

            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(false);

            int id_Bor = int.Parse(userInfo.Bor_Avatar);
            string my_id_Bor = userInfo.Bor_Avatar;

            if (id_Bor != -1)
            {
                if (DataHelper.GetBoderAvatar(my_id_Bor) != null)
                {
                    imgBorderAvt.sprite = DataHelper.GetBoderAvatar(my_id_Bor);

                    imgBorderAvt.gameObject.SetActive(true);
                    imgBorderAvt_Defaut.gameObject.SetActive(false);
                }
                else
                {
                    imgBorderAvt.gameObject.SetActive(false);
                    imgBorderAvt_Defaut.gameObject.SetActive(true);
                }
            }
            else
            {
                imgBorderAvt.gameObject.SetActive(false);
                imgBorderAvt_Defaut.gameObject.SetActive(true);
            }
            imgBorderAvt.transform.localScale = new Vector2(1.1f, 1.1f);
            // imgBorderAvt.gameObject.SetActive (true);
            LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            //if (loginType != LoginType.Facebook) {
            //	imgAvatar.sprite = DataHelper.GetAvatar (userInfo.Avatar);
            //} else {
            //	StartCoroutine (UpdateAvatarThread (userInfo.Avatar, this.imgAvatar));
            //}

            // load avatar facebook -- khuong
            if (MyInfo.SFS_ID == sfsId)
            {
                ChatController.RegistTextChip(txtChip);
                if (loginType != LoginType.Facebook)
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
                else
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
            }
            else
            {
                if (userInfo.Avatar.Contains("http") == true)
                {
                    StartCoroutine(UpdateAvatarThread(userInfo.Avatar, imgAvatar));
                }
                else
                {
                    //imgAvatar.sprite = DataHelper.GetAvatar(userInfo.Avatar);
                    StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + userInfo.Avatar, imgAvatar));
                }
            }



            //			imgBorderAvt.gameObject.SetActive (
            //				true);
            //			imgBorderAvt.sprite = DataHelper.GetVip (userInfo.VipPoint);

            //			StartCoroutine (UpdateAvatarThread (userInfo.Avatar));
        }
        //public override IEnumerator UpdateAvatarThread (string _url)
        //{
        //	Texture2D mainImage;
        //	WWW www = new WWW(_url);
        //	yield return www;

        //	mainImage = www.texture;
        //	Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

        //	imgAvatar.sprite = spr;
        //}

        public override void HidePlayer()
        {
            LogoBang.gameObject.SetActive(false);
            imgAvatar.gameObject.SetActive(false);
            //imgBorderAvt.sprite = DataHelper.GetVip (0);
            imgBorderAvt.gameObject.SetActive(false);
            imgBorderAvt_Defaut.gameObject.SetActive(false);
            panelChip.gameObject.SetActive(false);
            if (BtnInfo != null)
            {
                BtnInfo.SetActive(false);
            }
            if (ViTriBom != null)
            {
                ViTriBom.SetActive(false);
            }
            if (inviteBtn != null)
                inviteBtn.gameObject.SetActive(true);
            txtDisName.gameObject.SetActive(false);
        }
        public override void UpdateChip(long _chip)
        {
            txtChip.text = Utilities.GetStringMoneyByLong(_chip);
        }
        public void LayThongTinBangUser(string idUser)
        {
            //  API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangHoi_user_1);
            API.Instance.RequestLayIdBangHoi(idUser, RspLayIdBangHoi);
        }


        void RspLayIdBangHoi(string _json)
        {

            Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

            JSONNode node = JSONNode.Parse(_json);

            string userBang = node["user_clan"]["$id"].Value;
            string BangMaster = node["master_clan"]["$id"].Value;

            //  Debug.LogWarning("usserBang------------- " + userBang);
            //  Debug.LogWarning("BangMaster------------- " + BangMaster);

            //IdBangMaster = BangMaster;
            //IdBangUserBiNem = userBang;
            this.IdBang = userBang;
        }


        #endregion

        public void ShowChipResult(bool _isWin, long _gold)
        {
            txtChipResult.color = new Color(1, 1, 1, 1);
            txtChipResult.gameObject.SetActive(true);
            rectTxtChipResult.anchoredPosition = new Vector2(0, 50);
            //			txtChipResult.transform.localPosition = new Vector3 (0, 50, 0);

            if (_isWin)
            {
                //				Debug.Log ("ShowChipResult - " + _isWin + " - " + _gold);
                txtChipResult.text = "+" + Utilities.GetStringMoneyByLong(_gold);
                txtChipResult.color = Color.yellow;


            }
            else
            {
                txtChipResult.text = Utilities.GetStringMoneyByLong(_gold);
                txtChipResult.color = Color.gray;
            }
            rectTxtChipResult.DOAnchorPos(new Vector2(0, 100), TIME_GOLD_RESULT).OnComplete(() => {
                txtChipResult.gameObject.SetActive(false);
            });

            //			txtChipResult.DOFade (.25f, TIME_GOLD_RESULT);
        }

        public void ShowValueHandCards(XTType _type)
        {
            imgValueHandCards.sprite = XTDataHelper.instance.GetSprRow(_type);
            imgValueHandCards.gameObject.SetActive(true);
        }

        public void HideValueHandCards()
        {
            imgValueHandCards.gameObject.SetActive(false);
        }
        public void ShowResultPointLieng(string result)
        {
            txtResultPoint.gameObject.SetActive(true);
            txtResultPoint.text = GetResultLieng(result);
            txtResultPoint.color = GetColorResultLieng(result);
        }
        string GetResultLieng(string result)
        {

            switch (result.ToLower())
            {
                case "sap":
                    return "SÁP";
                case "lieng":
                    return "LIÊNG";
                case "anh":
                    return "ẢNH";
            }
            return result;
        }
        public void HideResultPointLieng()
        {
            txtResultPoint.gameObject.SetActive(false);
            txtResultPoint.text = "";
        }
        Color GetColorResultLieng(string result)
        {

            switch (result.ToLower())
            {
                case "sáp":
                    return Color.cyan;
                case "liêng":
                    return Color.green;
                case "ảnh":
                    return Color.red;
                
            }
            return Color.yellow;
        }
        public void SetEnableWin(bool _enable)
        {
            //			Win.SetActive (_enable);
        }
        public void SetEnableLose(bool _enable)
        {
            //			Lose.SetActive (_enable);
        }

        public void ShowAction(XTAction _action)
        {
            //			txtAction.text = _name;
            //			txtAction.gameObject.SetActive (true);

            imgAction.gameObject.SetActive(true);
            txtAction.text = XTDataHelper.instance.GetTextAction(_action);
            txtAction.color = GetColorAction(_action);
            //			imgAction.sprite = XTDataHelper.instance.GetSprRaise (_action);


        }
        public void ShowActionLieng(XTAction _action)
        {
            imgAction.gameObject.SetActive(true);
            txtAction.text = XTDataHelper.instance.GetTextActionLieng(_action);
            txtAction.color = GetColorAction(_action);
        }
        Color GetColorAction(XTAction _action)
        {

            switch (_action)
            {
                case XTAction.Call:
                    return Color.cyan;
                case XTAction.Check:
                    return Color.cyan;
                case XTAction.Fold:
                    return Color.red;
                case XTAction.Raise:
                    return Color.green;
                case XTAction.RaiseAll:
                    return Color.red;
                case XTAction.RaiseMulti2:
                    return Color.green;
                case XTAction.RaisePart4:
                    return Color.green;
                case XTAction.RaisePart2:
                    return Color.green;
            }
            return Color.white;
        }
        public void HideAction()
        {
            //			txtAction.gameObject.SetActive (false);
            imgAction.gameObject.SetActive(false);
        }

        bool isNem = false;
        IEnumerator DelayNemCoroutine()
        {
            yield return new WaitForSeconds(2);
            isNem = false;
        }
        public void NemBomVaoEnemy(int vitri)
        {
            if (isNem) return;
            StartCoroutine(DelayNemCoroutine());
            isNem = true;
            if (this.imgAvatar.gameObject.activeInHierarchy == true)
            {
                Sprite Ava = this.imgAvatar.sprite;
                Sprite Khung = this.imgBorderAvt.sprite;
                string ten = this.txtDisName.text;
                string vip = this.Vip;
                string level = this.txtLevel.text;
                string chip = this.txtChip.text;
                string gem = Utilities.GetStringMoneyByLong(this.Gem);
                string IDBANG = this.IdBang;

                string gioitinh = this.gender;
                int id_gioitinh = int.Parse(gioitinh);
                string ten_nguoi_tinh = this.partner;

                Debug.Log("Ava " + Ava + " Khung - " + Khung + " ten - " + ten + " vip - " + vip + " level -" + level + " chip - " + chip + " gem - " + gem + " sfsId = " + this.sfsId);


                InfoEnemy.Init(userId, Ava, Khung, ten, vip, level, chip, gem, vitri, IDBANG, id_gioitinh, ten_nguoi_tinh, sfsId);
            }
        }


        #region COUNTDOWN

        [SerializeField]
        GameObject panelCoutDown;
        [SerializeField]
        Image imgTime;

        onCallBack onCompleteCountDown;

        Coroutine countDownThread;

        float timeCurrent, timeTotal;
        int part;

        bool isStart = false;

        void Update()
        {
            if (isStart)
            {
                int _time = (int)timeCurrent;
                timeCurrent -= Time.deltaTime;

                if (_time < 5)
                {
                    if (_time > timeCurrent)
                    {
                        SoundManager.PlaySound(SoundManager.COUNT_DOWN);
                    }
                }

                imgTime.fillAmount = Mathf.Lerp(fillFrom, fillTo, timeCurrent / timeTotal);

                if (part != (int)timeCurrent)
                {
                    part = (int)timeCurrent;
                }

                if (timeCurrent < 0)
                {
                    OnCompleteCountDown();
                }
            }
        }
        Tween tweenColor;
        private const float fillFrom = 0.1f, fillTo = 0.9f;
        public void ShowCountDown(int _timeTotal, onCallBack _onComplete = null)
        {
            panelCoutDown.SetActive(true);
            imgTime.fillAmount = fillTo;
            //imgTime.color = Color.green;

            if (tweenColor != null)
                tweenColor.Pause();

            //tweenColor = imgTime.DOColor (Color.red, _timeTotal);
            //tweenColor.Play ();

            //			imgTime.color = Color.white;
            timeTotal = timeCurrent = _timeTotal + 1;

            onCompleteCountDown = _onComplete;

            isStart = true;
        }
        public void ShowCountDown(int _timeCurrent, int _timeTotal, onCallBack _onComplete = null)
        {
            panelCoutDown.SetActive(true);
            imgTime.fillAmount = Mathf.Lerp(fillFrom, fillTo, _timeCurrent / _timeTotal);
            timeTotal = _timeTotal;
            timeCurrent = _timeCurrent;

            onCompleteCountDown = _onComplete;

            isStart = true;
        }

        public void OnCompleteCountDown()
        {
            isStart = false;

            if (countDownThread != null)
            {
                StopCoroutine(countDownThread);
                countDownThread = null;
            }

            panelCoutDown.SetActive(false);
        }

        IEnumerator CountDownThread(int _second)
        {
            panelCoutDown.SetActive(true);

            timeCurrent = timeTotal = _second;

            //		for (int i = _second; i > 0; i--) {
            //
            //			txtTime.text = i.ToString ();
            //
            //			yield return new WaitForSeconds (1);
            //		}
            yield return new WaitForSeconds(_second);

            countDownThread = null;
            onCompleteCountDown();

            panelCoutDown.SetActive(false);

        }

        #endregion
    }
}