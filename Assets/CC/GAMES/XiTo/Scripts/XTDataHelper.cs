﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ConstXT;

namespace HelperXT {
		
public class XTDataHelper : MonoBehaviour {

		public static XTDataHelper instance;

		[SerializeField]
		List<Sprite> lstValueRow, lstNumber;


		[SerializeField]
		Sprite sprCall, sprCheck, sprFold, sprRaise, sprRaiseAll, sprRaiseMulti2, sprRaisePart4, sprRaisePart2;

		public void Init()
		{
				instance = this;
		}

		public Sprite GetSprRow(XTType _type)
		{
				return lstValueRow[(int)_type];
		}
		public Sprite GetSprNumber(int _number)
				{
						return lstNumber [_number];
				}
		public Sprite GetSprRaise(XTAction _action)
		{
			switch (_action) {
			case XTAction.Call:
				return sprCall;
			case XTAction.Check:
				return sprCheck;
			case XTAction.Fold:
				return sprFold;
			case XTAction.Raise:
				return sprRaise;
			case XTAction.RaiseAll:
				return sprRaiseAll;
			case XTAction.RaiseMulti2:
				return sprRaiseMulti2;
			case XTAction.RaisePart4:
				return sprRaisePart4;
			case XTAction.RaisePart2:
				return sprRaisePart2;
			}
			return null;
		}

	public string GetTextAction(XTAction _action)
	{
		switch (_action) {
		case XTAction.Call:
			return "Theo";
		case XTAction.Check:
			return "Nhường tố";
		case XTAction.Fold:
			return "Úp bỏ";
		case XTAction.Raise:
			return "Tố";
		case XTAction.RaiseAll:
			return "Tố tất cả";
		case XTAction.RaiseMulti2:
			return "Tố x2";
		case XTAction.RaisePart4:
			return "Tố 1/4";
		case XTAction.RaisePart2:
			return "Tố 1/2";
		}
		return null;
	}
    public string GetTextActionLieng(XTAction _action)
    {
        switch (_action)
        {
            case XTAction.Call:
                return "Theo";
            case XTAction.Check:
                return "Nhường tố";
            case XTAction.Fold:
                return "Úp bỏ";
            case XTAction.Raise:
                return "Tố";
            case XTAction.RaiseAll:
                return "Tố tất cả";
            case XTAction.RaiseMulti2:
                return "Tố x2";
            case XTAction.RaisePart4:
                return "Tố x4";
            case XTAction.RaisePart2:
                return "Tố x8";
        }
        return null;
    }
    }

}