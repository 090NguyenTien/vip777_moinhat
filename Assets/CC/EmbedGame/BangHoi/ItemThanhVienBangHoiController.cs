﻿using BaseCallBack;
using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemThanhVienBangHoiController : MonoBehaviour
{
    [SerializeField]
    Text txtChucvu, txtTen, txtDongGop, txtVip;
    string user_id, clan_id, username;
    int vip;
    long contribute;
    [SerializeField]
    Button BtnTuChoi;
    onCallBackString clickMenuAction;
    // Start is called before the first frame update
    void Start()
    {
       
    }

   public void InitInfo(ISFSObject infoMember, string owner, string deputy)
   {
        BtnTuChoi.gameObject.SetActive(false);
        user_id = infoMember.GetUtfString("user_id");
        contribute = infoMember.GetLong("contribute");
        clan_id = infoMember.GetUtfString("clan_id");
        username = infoMember.GetUtfString("username");
        vip = infoMember.GetInt("vip");
        if(user_id == owner)
        {
            txtChucvu.text = "Bang Chủ";
        }else if (user_id == deputy)
        {
            txtChucvu.text = "Bang Phó";
        }else
        {
            txtChucvu.text = "Thành Viên";
        }
        txtTen.text = username;
        if (contribute != 0)
        {
            txtDongGop.text = Utilities.GetStringMoneyByLongBigSmall(contribute);
        }
        else
        {
            txtDongGop.text = contribute.ToString();
        }
        txtVip.text = "Vip" + vip;
    }
    public void ShowBtnTuChoiCuuTro()
    {
        BtnTuChoi.gameObject.SetActive(true);
    }
    public void TuChoiCuuTro()
    {
        GamePacket gp = new GamePacket(CommandKey.REJECT_CUU_TRO);
        gp.Put("memberid", user_id);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void ShowAction()
    {
        clickMenuAction(user_id);
    }
    public void AddCallBack(onCallBackString _cb)
    {
        clickMenuAction = _cb;
    }

    public string GetUserId()
    {
        return user_id;
    }
    
}
