﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfoBangHoiController : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI TxtTenBang, TxtBangChu, TxtBangPho, TxtBangPho2;
    [SerializeField]
    Text TxtThanhVien, TxtChip, TxtGem;
    string idOwner, owner_name, nameBang, deputy, deputy_name, symbol_url, deputy2, deputy_name2;
    public static int is_public, min_vip, tax;
    public static int is_vip_clan;
    int max_member, total_member;
    public static long chip, gem, min_help, max_help;   
    [SerializeField]
    Toggle ToggleLeaveBang, ToggleChoDuyet, ToggleCauHinh, ToggleCuuTro;
    [SerializeField]
    Image AvatarBang, AvatarTrophy;
    [SerializeField]
    GameObject BANGPHO;
    [SerializeField]
    Button BtnXinCuuTro;
    string trophy_url = "";

    public static int is_deputyhelp;//(có cho bang phó cứu trợ hay ko)
    public static long max_deputyhelp;//ố cứu trợ tối đa 1 lần dành cho bang phó
    public static string clan_rule;//bang quy
    public static string clan_notice;//thông báo bang
    [SerializeField]
    GameObject noiquy, thongbao, lichsucuutro;
    [SerializeField]
    TMP_InputField InputNoiQuy, InputThongBao;
    [SerializeField]
    Text TxtThongBaoNgan;
    [SerializeField]
    ItemLichSuCuuTro ItemLichSu;
    [SerializeField]
    GameObject ContainerLichsu;
    [SerializeField]
    Button BtnLichSuCuuTro;
    [SerializeField]
    Button BtnCapNhatNoiQuy, BtnCapNhatThongBao;
    [SerializeField]
    Button tabCuuTro, tabDongGop;
    [SerializeField]
    Text txtTitle, txtDongGop,txtCuutro;
    int curTab=0;//0 cuu tro - 1 dong gop
    string curCommand = "requestClanHep";
    // Start is called before the first frame update
    void Start()
    {
        tabCuuTro.onClick.AddListener(OnclickTabCuutro);
        tabDongGop.onClick.AddListener(OnclickTabDonggop);
        txtDongGop.gameObject.SetActive(false);
        txtCuutro.gameObject.SetActive(true);
    }

    private void OnclickTabCuutro()
    {
        if (curTab == 0) return;
        curTab = 0;
        CurIndex = 0;
        curCommand = "requestClanHep";
        txtTitle.text = "LỊCH SỬ CỨU TRỢ";
        txtDongGop.gameObject.SetActive(false);
        txtCuutro.gameObject.SetActive(true);
        ShowLichSuCuuTro();
    }
    private void OnclickTabDonggop()
    {
        if (curTab == 1) return;
        curTab = 1;
        CurIndex = 0;
        curCommand = "requestClanContribute";
        txtTitle.text = "LỊCH SỬ ĐÓNG GÓP";
        txtDongGop.gameObject.SetActive(true);
        txtCuutro.gameObject.SetActive(false);
        ShowLichSuCuuTro();
    }
    public void CloseAllPanelConfig()
    {
        noiquy.SetActive(false);
        thongbao.SetActive(false);
        lichsucuutro.SetActive(false);
    }
    public void Show()
    {
        TxtThongBaoNgan.text = "...";
        InputNoiQuy.characterLimit = 1500;
        InputThongBao.characterLimit = 1500;
        BtnLichSuCuuTro.gameObject.SetActive(false);
        noiquy.SetActive(false);
        thongbao.SetActive(false);
        lichsucuutro.SetActive(false);
        InputNoiQuy.GetComponent<TMP_InputField>().interactable = false;
        InputThongBao.GetComponent<TMP_InputField>().interactable = false;
        BtnCapNhatNoiQuy.gameObject.SetActive(false);
        BtnCapNhatThongBao.gameObject.SetActive(false);
        BtnXinCuuTro.gameObject.SetActive(false);
        GamePacket gp = new GamePacket(CommandKey.GET_CLAN_INFO);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void CapNhatNoiQuyVaThongBao()
    {
        GamePacket gp = new GamePacket(CommandKey.SET_UP_CLAN);
        gp.Put("is_public", is_public);
        gp.Put("min_vip", min_vip);
        gp.Put("is_deputyhelp", is_deputyhelp);
        gp.Put("max_deputyhelp", max_deputyhelp);
        gp.Put("clan_rule", InputNoiQuy.text);
        gp.Put("clan_notice", InputThongBao.text);
        TxtThongBaoNgan.text = InputThongBao.text;
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    int CurIndex;
    public void NextDay()
    {
        if (CurIndex == 0) return;
        CurIndex++;
        GamePacket gp = new GamePacket(curCommand);
        gp.Put("index", CurIndex);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void BackDay()
    {
        if (CurIndex == -10) return;
        CurIndex--;
        GamePacket gp = new GamePacket(curCommand);
        gp.Put("index", CurIndex);
        SFS.Instance.SendRoomBangHoiRequest(gp);
    }
    public void ShowLichSuCuuTro()
    {
        CurIndex = 0;
        GamePacket gp = new GamePacket(curCommand);
        gp.Put("index", CurIndex);
        SFS.Instance.SendRoomBangHoiRequest(gp);

        noiquy.SetActive(false);
        thongbao.SetActive(false);
        lichsucuutro.SetActive(true);
    }
    public void ShowNoiQuy()
    {
        noiquy.SetActive(true);        
        thongbao.SetActive(false);
        lichsucuutro.SetActive(false);        
    }
    public void ShowThongBao()
    {
        noiquy.SetActive(false);
        lichsucuutro.SetActive(false);
        thongbao.SetActive(true);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case CommandKey.GET_CLAN_INFO:
                GetClanInfoRsp(param);
                break;
            case CommandKey.REQUEST_CUU_TRO:
                XinCuuTroRsp(param);
                break;
            case CommandKey.CONTRIBUTE_CLAN:
                ContributeClanRsp(param);
                break;
            case "requestClanHep":
            case "requestClanContribute":
                requestClanHepRsp(param);
                break;
        }
    }

    private void requestClanHepRsp(GamePacket param)
    {
        for (int z = 0; z < ContainerLichsu.transform.childCount; z++)
        {
            Destroy(ContainerLichsu.transform.GetChild(z).gameObject);
        }
        ISFSArray helplogs;
        if (curTab == 0)
        {
            helplogs = param.GetSFSArray("helplogs");
        }
        else
        {
            helplogs = param.GetSFSArray("contribute_logs");
        }
            
        
        foreach (ISFSObject member in helplogs)
        {
            ItemLichSuCuuTro item = Instantiate(ItemLichSu, ContainerLichsu.transform) as ItemLichSuCuuTro;
            if (curTab == 0) item.InitItem(member.GetUtfString("helpBy"), member.GetUtfString("user_name"), member.GetLong("chip"), member.GetUtfString("created_date"));
            else item.InitItem(member.GetUtfString("user_name"), "", member.GetLong("chip"), member.GetUtfString("created_date"));
        }
        if(helplogs==null || helplogs.Count == 0)
        {
            ItemLichSuCuuTro item = Instantiate(ItemLichSu, ContainerLichsu.transform) as ItemLichSuCuuTro;
            item.InitItem("Không Có Cứu Trợ", "Trong Ngày Này", 0, "");
        }
    }

    private void ContributeClanRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberId = param.GetString("memberid");
            long clan_chip = param.GetLong("clan_chip");
            chip = clan_chip;
            TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(chip);
            if (memberId == MyInfo.ID)
            {
                long chip = param.GetLong("chip");
                long user_chip = param.GetLong("user_chip");
                MyInfo.CHIP = user_chip;
                AlertController.api.showAlert("Quyên Góp Thành Công "+ Utilities.GetStringMoneyByLongBigSmall(chip) + " Vào Bang");
            }            
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void XinCuuTroRsp(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Đã Gửi Mong Muốn Xin Cứu Trợ Tới Bang Chủ");
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }

    private void GetClanInfoRsp(GamePacket param)
    {
        Debug.Log(param);
        ISFSObject info = param.GetSFSObject("info");
        MyInfo.CLAN_ID = info.GetUtfString("id");
        idOwner = info.GetUtfString("owner");
        chip = info.GetLong("chip");
        owner_name = info.GetUtfString("owner_name");
        max_member = info.GetInt("max_member");
        nameBang = info.GetUtfString("name");
        is_public = info.GetInt("is_public");
        min_vip = info.GetInt("min_vip");
        tax = info.GetInt("tax");
        total_member = info.GetInt("total_member");
        gem = info.GetLong("gem");
        min_help = info.GetLong("min_help");
        max_help = info.GetLong("max_help");
        symbol_url = info.GetUtfString("symbol_url");
        trophy_url = info.GetUtfString("trophy_url");
        is_vip_clan = info.GetInt("is_vip_clan");
        TxtBangPho.text = "";
        BANGPHO.SetActive(false);
        if (info.ContainsKey("deputy"))
        {
            deputy = info.GetUtfString("deputy");
            deputy_name = info.GetUtfString("deputy_name");
            TxtBangPho.text = deputy_name;
            BANGPHO.SetActive(true);
        }
        TxtBangPho2.text = "";
        if (info.ContainsKey("deputy2"))
        {
            deputy = info.GetUtfString("deputy2");
            deputy_name2 = info.GetUtfString("deputy_name2");
            TxtBangPho2.text = deputy_name2;
            BANGPHO.SetActive(true);
        }

        is_deputyhelp = info.GetInt("is_deputyhelp");
        max_deputyhelp = info.GetLong("max_deputyhelp");
        clan_rule = info.GetUtfString("rule");
        clan_notice = info.GetUtfString("notice");
        InputNoiQuy.text = clan_rule;
        TxtThongBaoNgan.text = InputThongBao.text = clan_notice;

        TxtTenBang.text = nameBang;
        TxtBangChu.text = owner_name;
        TxtThanhVien.text = total_member + "/" + max_member;
        TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(chip);
        TxtGem.text = gem.ToString();

        if(idOwner == MyInfo.ID)
        {
            ToggleLeaveBang.gameObject.SetActive(false);
            ToggleChoDuyet.gameObject.SetActive(true);
            ToggleCauHinh.gameObject.SetActive(true);
            ToggleCuuTro.gameObject.SetActive(true);

            BtnLichSuCuuTro.gameObject.SetActive(true);
            BtnCapNhatNoiQuy.gameObject.SetActive(true);
            BtnCapNhatThongBao.gameObject.SetActive(true);
            InputNoiQuy.GetComponent<TMP_InputField>().interactable = true;
            InputThongBao.GetComponent<TMP_InputField>().interactable = true;
        }
        else if(deputy == MyInfo.ID || deputy2 == MyInfo.ID)
        {
            ToggleLeaveBang.gameObject.SetActive(true);
            ToggleChoDuyet.gameObject.SetActive(true);
            ToggleCauHinh.gameObject.SetActive(false);
            ToggleCuuTro.gameObject.SetActive(true);
            BtnXinCuuTro.gameObject.SetActive(true);

            BtnLichSuCuuTro.gameObject.SetActive(true);
            BtnCapNhatNoiQuy.gameObject.SetActive(true);
            BtnCapNhatThongBao.gameObject.SetActive(true);
            InputNoiQuy.GetComponent<TMP_InputField>().interactable = true;
            InputThongBao.GetComponent<TMP_InputField>().interactable = true;
        }
        else
        {
            ToggleLeaveBang.gameObject.SetActive(true);
            ToggleChoDuyet.gameObject.SetActive(false);
            ToggleCauHinh.gameObject.SetActive(false);
            ToggleCuuTro.gameObject.SetActive(false);
            BtnXinCuuTro.gameObject.SetActive(true);

            BtnLichSuCuuTro.gameObject.SetActive(false);
            BtnCapNhatNoiQuy.gameObject.SetActive(false);
            BtnCapNhatThongBao.gameObject.SetActive(false);
            InputNoiQuy.GetComponent<TMP_InputField>().interactable = false;
            InputThongBao.GetComponent<TMP_InputField>().interactable = false;
        }
        StartCoroutine(UpdateAvatarThread(symbol_url, AvatarBang));
        if(trophy_url != "")
        {
            StartCoroutine(UpdateAvatarThread(trophy_url, AvatarTrophy));
            AvatarTrophy.gameObject.SetActive(true);
        }
        else
        {
            AvatarTrophy.gameObject.SetActive(false);
        }        
    }
   
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        if (mainImage != null && avatar != null)
            avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }

}
