﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDuyetController : MonoBehaviour
{
    [SerializeField]
    Text txtTen, txtVip;
    string user_id, username;
    int vip;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void InitInfo(ISFSObject infoMember)
    {
        user_id = infoMember.GetUtfString("user_id");
        username = infoMember.GetUtfString("username");
        vip = infoMember.GetInt("vip");

        txtTen.text = username;
        txtVip.text = "Vip" + vip.ToString();
    }
    public void DongY()
    {
        GamePacket gp = new GamePacket(CommandKey.REQUEST_REQUIRE_JOIN);
        gp.Put("memberid", user_id);
        gp.Put("response", 1);
        SFS.Instance.SendRoomBangHoiRequest(gp);
        DestroyItem();
    }
    public void TuChoi()
    {
        GamePacket gp = new GamePacket(CommandKey.REQUEST_REQUIRE_JOIN);
        gp.Put("memberid", user_id);
        gp.Put("response", -1);
        SFS.Instance.SendRoomBangHoiRequest(gp);
        DestroyItem();
    }
    public void DestroyItem()
    {
        Destroy(gameObject);
    }
}
