﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelNotification : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtMsg;
    [SerializeField]
    Button BtnClosePanel;
    // Start is called before the first frame update
    void Start()
    {
        BtnClosePanel.onClick.AddListener(OnClosePanel);
    }

    private void OnClosePanel()
    {
        gameObject.SetActive(false);
    }

    public void ShowMsg(string msg)
    {
        txtMsg.text = msg;
    }
    
}
