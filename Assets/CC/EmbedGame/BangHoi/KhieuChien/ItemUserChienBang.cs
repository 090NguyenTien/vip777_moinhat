﻿using BaseCallBack;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUserChienBang : MonoBehaviour
{
    [SerializeField]
    Text TxtName, TxtChip;
    [SerializeField]
    Button BtnChon;
    string idUser;
    onCallBackString callback;
    // Start is called before the first frame update
    void Start()
    {
        BtnChon.onClick.AddListener(OnChonUserThiDau);
    }

    private void OnChonUserThiDau()
    {
        AlertController.api.showAlert("Bạn muốn chọn "+TxtName.text + " làm người đại diện để thách đấu?",()=> {
            if (callback!=null) callback(idUser);
        });
        
    }

    public void InitData(ISFSObject data, onCallBackString cb)
    {
        callback = cb;
        idUser = data.GetUtfString("user_id");        
        string clandId = data.GetUtfString("clan_id");
        TxtName.text = data.GetUtfString("username");        
        TxtChip.text = Utilities.GetStringMoneyByLongBigSmall(data.GetLong("contribute"));        
    }
    
}
