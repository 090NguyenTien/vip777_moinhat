﻿using BaseCallBack;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemBangHoiChien : MonoBehaviour
{
    //[SerializeField]
    //Text TxtTenBang, TxtThanhvien, TxtNganKho;
    [SerializeField]
    TMPro.TextMeshProUGUI TxtBangChu, TxtTenBang, TxtThanhvien, TxtNganKho;
    [SerializeField]
    Button BtnThachDau;
    [SerializeField]
    Image AvatarBang, AvatarTrophy;
    int numMember, maxMember, is_public, total_member;
    string bangName, ownerIdBangChu, ownerName, idBang, deputyId, deputy_name, symbol_url;
    long ngankho, gem;
    string trophy_url;
    onCallBackString callBack;
    // Start is called before the first frame update
    void Start()
    {
        BtnThachDau.onClick.AddListener(ThachDau);
    }
    public void InitInfo(SFSObject clan, onCallBackString _cb)
    {
        callBack = _cb;
        ownerIdBangChu = clan.GetUtfString("owner");
        ngankho = clan.GetLong("chip");
        ownerName = clan.GetUtfString("owner_name");
        maxMember = clan.GetInt("max_member");
        total_member = clan.GetInt("total_member");
        bangName = clan.GetUtfString("name");
        is_public = clan.GetInt("is_public");
        idBang = clan.GetUtfString("id");
        gem = clan.GetLong("gem");
        symbol_url = clan.GetUtfString("symbol_url");
        trophy_url = clan.GetUtfString("trophy_url");
        if (clan.ContainsKey("deputy"))
        {
            deputyId = clan.GetUtfString("deputy");
            deputy_name = clan.GetUtfString("deputy_name");
        }

        TxtTenBang.text = bangName;
        TxtBangChu.text = ownerName;
        TxtNganKho.text = Utilities.GetStringMoneyByLongBigSmall(ngankho);
        TxtThanhvien.text = total_member.ToString() +"/"+ maxMember.ToString();
        
        //StartCoroutine(UpdateAvatarThread(symbol_url, AvatarBang));
        //if (trophy_url != "")
        //{
        //    StartCoroutine(UpdateAvatarThread(trophy_url, AvatarTrophy));
        //    AvatarTrophy.gameObject.SetActive(true);
        //}
        //else
        //{
        //    AvatarTrophy.gameObject.SetActive(false);
        //}        
    }
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        if (mainImage != null && avatar != null)
            avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
    }
    public void ThachDau()
    {
        callBack(idBang);       
    }
    
}
