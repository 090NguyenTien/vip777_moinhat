﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThanBaiDoanController : MonoBehaviour
{
    [SerializeField]
    ItemUserThanBai itemUserThanbai;
    [SerializeField]
    ItemLichSuDoan itemLichSuDoan;
    [SerializeField]
    GameObject panelLichsuCuoc, panelCuoc;
    [SerializeField]
    GameObject contentUserThanBai, contentLichSu;
    [SerializeField]
    Button btnCloseDuDoan, btnCloseLichsu, btnShowHistory, btnCloseCuoc, btnCuoc;
    List<ItemUserThanBai> lstItemUserThanbai;
    [SerializeField]
    Text txtCuoc, TitleStatusGiai;
    long Mincuoc=0;
    int tile = 0;

    [SerializeField]
    Toggle TabDanhSach, TabCapDau;
    [SerializeField]
    GameObject contentUserGiaiDau, scrollUser, scrollCapDau, PanelLichsuCuocCapDau, contentLichSuCuoc;
    [SerializeField]
    ItemUserCuocGiaiDau itemUserCuocGiaiDau;
    [SerializeField]
    ItemLichSuCuocCapDau itemLichsuCuocCapDau;
    // Start is called before the first frame update
    void Start()
    {
        btnCloseDuDoan.onClick.AddListener(CloseDuDoanThanBai);
        btnCloseLichsu.onClick.AddListener(CloseLichSu);
        btnShowHistory.onClick.AddListener(ShowLichSu);
        btnCloseCuoc.onClick.AddListener(CloseCuocPanel);
        btnCuoc.onClick.AddListener(CuocThanBai);

        panelCuoc.SetActive(false);
        panelLichsuCuoc.SetActive(false);

        scrollUser.SetActive(true);
        scrollCapDau.SetActive(false);

        TabDanhSach.onValueChanged.AddListener(OnChangeTab);
        TabCapDau.onValueChanged.AddListener(OnChangeTab);
        PanelLichsuCuocCapDau.SetActive(false);
        for (int z = 0; z < contentUserThanBai.transform.childCount; z++)
        {
            Destroy(contentUserThanBai.transform.GetChild(z).gameObject);
        }
        for (int z = 0; z < contentLichSu.transform.childCount; z++)
        {
            Destroy(contentLichSu.transform.GetChild(z).gameObject);
        }
        GamePacket gp = new GamePacket("godOfGamblerInit");
        SFS.Instance.SendZoneRequest(gp);

        
    }

    int curTab=0;
    private void OnChangeTab(bool val)
    {
        if (val == true)
        {
            if (TabDanhSach.isOn&&curTab!=0)
            {
                curTab = 0;
                GamePacket gp = new GamePacket("godOfGamblerInit");
                SFS.Instance.SendZoneRequest(gp);
                scrollUser.SetActive(true);
                scrollCapDau.SetActive(false);
            }
            else if (TabCapDau.isOn && curTab != 1)
            {
                curTab = 1;
                GamePacket gp = new GamePacket("getMatchCompetitor");
                SFS.Instance.SendZoneRequest(gp);
                scrollUser.SetActive(false);
                scrollCapDau.SetActive(true);
            }
        }
    }

    
    private void ShowLichSu()
    {
        panelLichsuCuoc.SetActive(true);
        GamePacket gp = new GamePacket("guessGodGamblerHistory");
        SFS.Instance.SendZoneRequest(gp);
    }
    private void CloseLichSu()
    {
        panelLichsuCuoc.SetActive(false);
    }
    public void OnChatResponse(GamePacket param)
    {
        switch (param.cmd)
        {
            case "godOfGamblerInit":
                ShowListUserThamGia(param);
                break;
            case "guessGodOfGambler":
                RspCuocUser(param);
                break;
            case "guessGodGamblerHistory":
                RspGetHistory(param);
                break;
            case "getMatchCompetitor":
                GetMatchCompetitor(param);
                break;
            case "bettingEventMatch":
                BettingEventMatch(param);
                break;
            case "getBettingMatchHistory":
                GetBettingMatchHistory(param);
                break;
        }
    }

    private void BettingEventMatch(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Cược "+param.GetLong("betmoney")+" Thành Công");
            GamePacket gp = new GamePacket("getMatchCompetitor");
            SFS.Instance.SendZoneRequest(gp);
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void GetBettingMatchHistory(GamePacket param)
    {
        PanelLichsuCuocCapDau.SetActive(true);
        for (int z = 0; z < contentLichSuCuoc.transform.childCount; z++)
        {
            Destroy(contentLichSuCuoc.transform.GetChild(z).gameObject);
        }
        ISFSArray lstUser = param.GetSFSArray("betting");
        foreach (ISFSObject usercuoc in lstUser)
        {
            ItemLichSuCuocCapDau item = Instantiate(itemLichsuCuocCapDau, contentLichSuCuoc.transform) as ItemLichSuCuocCapDau;
            item.InitClass(usercuoc);
        }
    }
    public void ClosePanelLichsuCuocCapDau()
    {
        PanelLichsuCuocCapDau.SetActive(false);
    }
    private void GetMatchCompetitor(GamePacket param)
    {       
        for (int z = 0; z < contentUserGiaiDau.transform.childCount; z++)
        {
            Destroy(contentUserGiaiDau.transform.GetChild(z).gameObject);
        }
        ISFSArray lstUser = param.GetSFSArray("matchcompetitors");
        foreach (ISFSObject capdau in lstUser)
        {
            ShowUserThamGia(capdau);
        }
    }
    void ShowUserThamGia(ISFSObject user)
    {
        ItemUserCuocGiaiDau item = Instantiate(itemUserCuocGiaiDau, contentUserGiaiDau.transform) as ItemUserCuocGiaiDau;
        item.InitClass(user);
    }
    private void RspGetHistory(GamePacket param)
    {
        //clear old danh sach
        for (int z = 0; z < contentLichSu.transform.childCount; z++)
        {
            Destroy(contentLichSu.transform.GetChild(z).gameObject);
        }        

        ISFSArray listUser = param.GetSFSArray("history");
        foreach (SFSObject user in listUser)
        {
            ItemLichSuDoan item = Instantiate(itemLichSuDoan, contentLichSu.transform) as ItemLichSuDoan;
            item.InitClass(user.GetUtfString("competitorname"), user.GetLong("betmoney"), user.GetUtfString("created_date"), user.GetInt("win_ratio"));
               
        }
        
    }
    private void RspCuocUser(GamePacket param)
    {
        int status = param.GetInt("status");

        if (status == 1)
        {
            AlertController.api.showAlert("DỰ ĐOÁN THÀNH CÔNG");
            MyInfo.CHIP = param.GetLong("chip");
            CloseCuocPanel();
            GamePacket gp = new GamePacket("godOfGamblerInit");
            SFS.Instance.SendZoneRequest(gp);
        }
        else
        {
            AlertController.api.showAlert(param.GetString("msg"));
        }
    }
    private void ShowListUserThamGia(GamePacket param)
    {
        //clear old danh sach
        for (int z = 0; z < contentUserThanBai.transform.childCount; z++)
        {
            Destroy(contentUserThanBai.transform.GetChild(z).gameObject);
        }
        int status = param.GetInt("status");
        if (status == 1)//đang trong thời gian cược
        {
            TitleStatusGiai.text = "GIẢI ĐANG BẮT ĐẦU TỈ LỆ DỰ ĐOÁN LÀ x" + param.GetInt("win_ratio");
            Mincuoc = param.GetLong("min_betting");
            ShowUserThamGia(param.GetSFSArray("competitors"),true);            
        }
        else if (status == 2)//ngung nhan cuoc
        {
            TitleStatusGiai.text = "GIẢI ĐÃ BẮT ĐẦU NGƯNG DỰ ĐOÁN";
            //AlertController.api.showAlert(param.GetString("msg"));
            ShowUserThamGia(param.GetSFSArray("competitors"), false);
        }
        else if (status == 3)//chua toi thoi gian nhan cuoc (không cho click lịch sử
        {
            TitleStatusGiai.text = "CHƯA TỚI THỜI GIAN DỰ ĐOÁN";
            ShowUserThamGia(param.GetSFSArray("competitors"), false);
        }
        else if(status == -1)//Mùa giải đã kết thúc, chờ admin thông báo thời gian mùa giải kế tiếp nhé.
        {
            AlertController.api.showAlert(param.GetString("msg"), ()=> {
                CloseDuDoanThanBai();
            });
        }
        else//Liên hệ admin để biết thông tin giải đấu nhé
        {
            AlertController.api.showAlert(param.GetString("msg"), () => {
                CloseDuDoanThanBai();
            });
        }

    }
    void ShowUserThamGia(ISFSArray listUser, bool isShowCuoc)
    {
        foreach (SFSObject user in listUser)
        {
            ItemUserThanBai item = Instantiate(itemUserThanbai, contentUserThanBai.transform) as ItemUserThanBai;
            item.InitClass(ShowPanelCuoc, user.GetUtfString("id"), user.GetUtfString("username"), user.GetLong("totalBetMoney"), user.GetInt("totalBetTimes"), user.GetInt("status"), isShowCuoc);
            //lstItemUserThanbai.Add(item);
        }
    }
    string curIdUserChose;
    void ShowPanelCuoc(string iduser)
    {
        chipDuDoan = 0;
        txtCuoc.text = "0";
        curIdUserChose = iduser;
        panelCuoc.SetActive(true);
    }
    void CloseCuocPanel()
    {
        panelCuoc.SetActive(false);
    }
    void CuocThanBai()
    {
        if (chipDuDoan < Mincuoc)
        {
            AlertController.api.showAlert("BẠN PHẢI DỰ ĐOÁN ÍT NHẤT LÀ "+ Utilities.GetStringMoneyByLongBigSmall(Mincuoc) + "CHIP");
            return;
        }
        GamePacket gp = new GamePacket("guessGodOfGambler");
        gp.Put("competitor_id", curIdUserChose);
        gp.Put("betmoney", chipDuDoan);
        SFS.Instance.SendZoneRequest(gp);
    }
    long chipDuDoan;
    public void AddChipQuyenGop(int val = 0)
    {
        chipDuDoan += val;
        txtCuoc.text = Utilities.GetStringMoneyByLongBigSmall(chipDuDoan);
    }
    private void CloseDuDoanThanBai()
    {
        Destroy(gameObject);
    }

    
}
