﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
public class ItemUserCuocGiaiDau : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtName1, txtName2, txtTongCuoc1, txtTongCuoc2, txtStatus1, txtStatus2;
    
    string IdCapDau, IDUser1, IDUser2,  Uname1, Uname2, time;
    [SerializeField]
    Button btnCuoc1, btnCuoc2, btnLichsu1, btnLichsu2;
    [SerializeField]
    Text txtBet, txtBetTime;
    long totalBetPlayer1, totalBetPlayer2;
    void Start()
    {
        btnCuoc1.onClick.AddListener(OnClickCuoc1);
        btnCuoc2.onClick.AddListener(OnClickCuoc2);
        btnLichsu1.onClick.AddListener(OnClickLichsu1);
        btnLichsu2.onClick.AddListener(OnClickLichsu2);       
    }
    public void InitClass(ISFSObject user)
    {        
        //user.GetUtfString("id"), user.GetUtfString("player_1_id"), user.GetUtfString("player_1_name"), user.GetUtfString("winner")
        IdCapDau = user.GetUtfString("id");
        IDUser1 = user.GetUtfString("player_1_id");
        Uname1 = user.GetUtfString("player_1_name");
        totalBetPlayer1 = user.GetLong("totalBetPlayer1");
        IDUser2 = user.GetUtfString("player_2_id");
        Uname2 = user.GetUtfString("player_2_name");
        totalBetPlayer2 = user.GetLong("totalBetPlayer2");
        time = user.GetUtfString("time");
        txtBetTime.text = time;
        txtTongCuoc1.text = Utilities.GetStringMoneyByLongBigSmall(totalBetPlayer1);
        txtTongCuoc2.text = Utilities.GetStringMoneyByLongBigSmall(totalBetPlayer2);
        string winner = user.GetUtfString("winner");
        txtName1.text = Uname1;
        txtName2.text = Uname2;
        txtStatus1.text = "";
        txtStatus2.text = "";
        if (winner == IDUser1)
        {
            txtStatus1.text = "thắng";
            txtStatus2.text = "thua";
        }
        else if (winner == IDUser2)
        {
            txtStatus2.text = "thắng";
            txtStatus1.text = "thua";
        }
        ResetChip();
    }
    private void OnClickCuoc1()
    {
        OnCuocUser(IDUser1);
    }
    private void OnClickCuoc2()
    {
        OnCuocUser(IDUser2);
    }
    void OnCuocUser(string idUser)
    {
        GamePacket gp = new GamePacket("bettingEventMatch");
        gp.Put("id", IdCapDau);
        gp.Put("player_id", idUser);
        gp.Put("betmoney", chipCuoc);
        SFS.Instance.SendZoneRequest(gp);
    }
    private void OnClickLichsu1()
    {
        GamePacket gp = new GamePacket("getBettingMatchHistory");
        gp.Put("id", IdCapDau);
        SFS.Instance.SendZoneRequest(gp);
    }
    private void OnClickLichsu2()
    {
        GamePacket gp = new GamePacket("getBettingMatchHistory");
        gp.Put("id", IdCapDau);
        SFS.Instance.SendZoneRequest(gp);
    }
    
    
    long chipCuoc = 0;
    public void AddChipQuyenGop(int val = 0)
    {
        chipCuoc += val;
        //if (chipCuoc > 1000000000)
        //{
        //    chipCuoc = 1000000000;
        //}
        if (MyInfo.CHIP < chipCuoc)
        {
            chipCuoc = MyInfo.CHIP;
        }
        txtBet.text = Utilities.GetStringMoneyByLongBigSmall(chipCuoc);
    }
    public void ResetChip()
    {
        chipCuoc = 0;
        txtBet.text = Utilities.GetStringMoneyByLongBigSmall(chipCuoc);
    }
}
