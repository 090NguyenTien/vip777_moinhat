﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLichSuDoan : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtName, txttongcuoc, txtDate, txtRatio;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitClass(string uname, long tongcuoc, string datetime, int ratio)
    {        
        txtName.text = uname;       
        txttongcuoc.text = Utilities.GetStringMoneyByLongBigSmall(tongcuoc);
        txtDate.text = datetime;
        txtRatio.text = "x" + ratio;
    }
}
