﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLichSuCuocCapDau : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI txtNameCuoc, TxtResultPlayername, txttongcuoc, txtThuve, txtHoanlai, txtDate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void InitClass(ISFSObject user)
    {
        txtNameCuoc.text = user.GetUtfString("playername");
        TxtResultPlayername.text = user.GetUtfString("resultPlayername");
        if(TxtResultPlayername.text == "")
        {
            TxtResultPlayername.text = "Chờ Kết Quả";
        }
        txttongcuoc.text = Utilities.GetStringMoneyByLongBigSmall(user.GetLong("betmoney"));
        txtHoanlai.text = user.GetLong("returnMoney").ToString();
        txtThuve.text = user.GetLong("finalMoney").ToString();
        txtDate.text = user.GetUtfString("created_date").ToString();
    }
}
