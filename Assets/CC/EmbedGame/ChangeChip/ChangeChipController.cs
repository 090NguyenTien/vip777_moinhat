﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeChipController : MonoBehaviour
{
    [SerializeField]
    InputField chipReceive, userReceive, MsgContent;
    [SerializeField]
    Text chipUser, vipUser, Description, txtPercent, txtUser;
    long minChip = 10000000;
    long limitChip=0;
    List<long> lstChipLimit;
    int transfer_fee;
    long min_transfer;
    // Start is called before the first frame update
    void Start()
    {
        if (!GameHelper.IS_LOAD_LOGIN)
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
            return;
        }
        txtUser.text = MyInfo.NAME;
        API.Instance.RequestInitChangeChip(InitChangeChipComplete);
        
    }
    public void BackToHome()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.HomeSceneV2);
    }
    void InitChangeChipComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        minChip = long.Parse(node["min_chip"].Value);
        transfer_fee = int.Parse(node["transfer_fee"]);
        min_transfer = long.Parse(node["min_transfer"]);
        txtPercent.text = transfer_fee + "%";
        LoadingManager.Instance.ENABLE = false;
        chipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        vipUser.text = "Vip"+ MyInfo.MY_ID_VIP.ToString();
        HanMucChuyenTheoVip();
    }
    void HanMucChuyenTheoVip()
    {
        int vip = MyInfo.MY_ID_VIP;
        GoiVip gv;
        Description.text="";
        for (int i=0; i < VIP_Controll.DicVipData.Count;i++)
        {
            gv = VIP_Controll.DicVipData[i];
            if (gv.HanMucTangChip != "")
            {
                Description.text += "Vip" + i + " : " + Utilities.GetStringMoneyByLong(long.Parse(gv.HanMucTangChip)) + "/lần \n";
            }                
        }
        gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
        if (gv.HanMucTangChip == "")
        {
            limitChip = 0;
        }
        else
        {
            limitChip = long.Parse(gv.HanMucTangChip);
        }
        
    }
    public void ChangeChipToUser()
    {
        string uReceive = userReceive.text;
        string strChipIsReceive = chipReceive.text;
        long chipIsReceive = 0;
        if (strChipIsReceive != "")
        {
            chipIsReceive = long.Parse(strChipIsReceive);
        }
        if (uReceive == "")
        {
            AlertController.api.showAlert("Vui Lòng Nhập Chính Xác Tên Người Được Tặng!");
            return;
        }else if (MyInfo.CHIP < minChip)
        {
            AlertController.api.showAlert("Số Chip Của Bạn Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(minChip) + " Mới Có Thể Chuyển Được!");
            return;
        }else if (limitChip==0)
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip 1 Trở Lên Mới Có Thể Tặng Chip Được!");
            return;
        }else if (chipIsReceive < min_transfer)
        {
            AlertController.api.showAlert("Số Chip Tặng Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(min_transfer) + " !");
            return;
        }
        else if (chipIsReceive>limitChip)
        {
            AlertController.api.showAlert("Vượt Hạn Mức Cho Phép. Số Chip Tặng Của Bạn Phải Thấp Hơn " + Utilities.GetStringMoneyByLong(limitChip) + " !");
            return;
        }
        else if (MyInfo.CHIP- chipIsReceive < minChip)
        {
            AlertController.api.showAlert("Vượt Hạn Mức Cho Phép. Số Chip Còn Lại Của Bạn Sau Khi Chuyển Phải Lớn Hơn "+ Utilities.GetStringMoneyByLong(minChip) + " !");
            return;
        }else if (MsgContent.text =="")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Nội Dung Tặng!");
            return;
        }
        //API.Instance.RequestChangeChip(MsgContent.text, uReceive, chipIsReceive, ChangeChipComplete);
        GamePacket gp = new GamePacket(CommandKey.TRANSFER_ITEM);
        gp.Put("type", "chip");
        gp.Put("amount", chipIsReceive);
        gp.Put("to_user_name", uReceive);
        SFS.Instance.SendZoneRequest(gp);        
    }
    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.TRANSFER_ITEM:
                ChangeChipFromServer(gp);
                break;
        }
    }

    private void ChangeChipFromServer(GamePacket gp)
    {
        int status = gp.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Tặng Chip Thành Công, Chip Tặng Sẽ Được Gửi Vào Hộp Thư Người Được Tặng!");
            MyInfo.CHIP = gp.GetLong("chip");
            chipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        }
        else
        {
            AlertController.api.showAlert(gp.GetString("msg"));
        }
    }
    void ChangeChipComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        if (int.Parse(node["status"].Value) == -1)
        {
            AlertController.api.showAlert(node["msg"].Value);
            return;
        }
        else if(int.Parse(node["status"].Value) == 1)
        {
            AlertController.api.showAlert("Tặng Chip Thành Công, Chip Tặng Sẽ Được Gửi Vào Hộp Thư Người Được Tặng!");
            MyInfo.CHIP = long.Parse(node["chip"].Value);
            chipUser.text = Utilities.GetStringMoneyByLong(MyInfo.CHIP);
        }
        else
        {
            AlertController.api.showAlert("Lỗi Hệ Thống! Vui Lòng Thử Lại");
        }
    }
}
