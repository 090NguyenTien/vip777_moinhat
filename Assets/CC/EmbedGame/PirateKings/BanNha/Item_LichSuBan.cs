﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item_LichSuBan : MonoBehaviour
{
    [SerializeField]
    Text TxtName, TxtTime, TxtItem;

    public void InIt(string ten, string thoigian, string tensanpham )
    {
        TxtName.text = ten;
        TxtTime.text = "Thời gian: " + thoigian;
        TxtItem.text = "Bắn Phá: " + tensanpham;
    }
}
