﻿using Com.TheFallenGames.OSA.Util.IO;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemColor : MonoBehaviour
{
    public Image image;
    public Text price, vipRequired;
    //public ColorItem item;
    public int index;
    private BaseCallBack.onCallBackInt callBack;
    public void init(int index, BaseCallBack.onCallBackInt callBack)
    {
        this.index = index;
        string color = DataHelper.ShopColorItems.color[index].ToString();
        long price = DataHelper.ShopColorItems.cost;
        int minVip = DataHelper.ShopColorItems.minvip;
        this.price.text = Utilities.GetStringMoneyByLong(price);//.ToString();
        this.vipRequired.text = "Vip." + minVip.ToString();
        string[] colorPoint = color.Split(',');
        image.color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
        //item.index = index;
        //item.sColor = color;
        //item.price = price;
        this.callBack = callBack;
        image.GetComponent<Button>().onClick.AddListener(onClick);
    }

    private void onClick()
    {
        callBack(index);
    }
}