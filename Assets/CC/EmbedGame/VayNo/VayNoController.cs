﻿using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VayNoController : MonoBehaviour
{
    [SerializeField]
	Text txtVip, txtVipTheChap, txtHanMucVay, txtLaiPhaiTra, txtNo;
    [SerializeField]
	Button btnVay, btnTra, btnCloseModule, btnOk, btnClosePopPlus, btnLichsu, btnCloseLichsu;
    [SerializeField]
	Text TxtChipPlus;
	[SerializeField]
	GameObject PopupPlusChip, PopupLichsu, containerHis;
	[SerializeField]
	ItemLichSuVay itemLichsu;
	int statusVayTra=0; // 0 là vay 1 là tra.
    // Start is called before the first frame update
    void Start()
	{
		
        btnVay.onClick.AddListener(OnClickVay);
		btnTra.onClick.AddListener(OnClickTra);
		btnLichsu.onClick.AddListener(OnClickLichSu);
		btnCloseLichsu.onClick.AddListener(OnClickCloseLichSu);
		btnCloseModule.onClick.AddListener(OnClickCloseModule);
		btnOk.onClick.AddListener(OnClickOK);
		btnClosePopPlus.onClick.AddListener(OnClickClosePopPlus);
		Init();
        
	}
	private void OnClickCloseLichSu()
	{
		PopupLichsu.SetActive(false);
	}
    private void OnClickLichSu()
    {
	    PopupLichsu.SetActive(true);
	    GamePacket gp = new GamePacket("credit_History");
	    SFS.Instance.SendZoneRequest(gp);
    }

    void Init(){
		TxtChipPlus.text = "0";
        chipCuuTro = 0;
        PopupPlusChip.SetActive(false);
		PopupLichsu.SetActive(false);
		GamePacket gp = new GamePacket("credit_Info");
		SFS.Instance.SendZoneRequest(gp);
	}
	void OnClickCloseModule(){
        GamePacket gp = new GamePacket(CommandKey.REFRESH);
        SFS.Instance.SendZoneRequest(gp);
        Destroy(gameObject);
	}
	void OnClickClosePopPlus(){
		PopupPlusChip.SetActive(false);
	}
	void OnClickOK(){
		if(statusVayTra==0){
			GamePacket gp = new GamePacket("credit_Borrow");
			gp.Put("chipborrow",chipCuuTro);
			SFS.Instance.SendZoneRequest(gp);
		}else{
			GamePacket gp = new GamePacket("credit_Pay");
			gp.Put("paidchip",chipCuuTro);
			SFS.Instance.SendZoneRequest(gp);
		}
	}
    private void OnClickTra()
	{
		statusVayTra=1;
		PopupPlusChip.SetActive(true);
		TxtChipPlus.text = "0";
        chipCuuTro = 0;
    }

    private void OnClickVay()
	{
		statusVayTra=0;
		PopupPlusChip.SetActive(true);
		TxtChipPlus.text = "0";
        chipCuuTro = 0;
    }

    public void OnSFSResponse(GamePacket param)
    {       
        switch (param.cmd)
        {
            case "credit_Info":
                CreditInfoRes(param);
                break;
            case "credit_Pay":
                CreditPayRes(param);
                break;
            case "credit_Borrow":
                CreditBorrowRes(param);
                break;
            case "credit_History":
	            CreditHistoryRes(param);
                break;
        }
    }
	private void CreditHistoryRes(GamePacket param)
	{
        for (int z = 0; z < containerHis.transform.childCount; z++)
        {
            Destroy(containerHis.transform.GetChild(z).gameObject);
        }
        ISFSArray history = param.GetSFSArray("history");
        foreach (SFSObject his in history)
        {
            ItemLichSuVay item = Instantiate(itemLichsu, containerHis.transform) as ItemLichSuVay;
            item.INIT(his.GetInt("type"), his.GetLong("chip"), his.GetUtfString("created_date"));            
        }
    }
    private void CreditBorrowRes(GamePacket param)
	{
		int status = 1;
		if(param.ContainKey("status")){
			status = param.GetInt("status");
		}
		if(status==-1){
			AlertController.api.showAlert(param.GetString("message"),()=>{
				Init();
			});
		}else{
            AlertController.api.showAlert("Vay Thành Công");
            Init();
		}
    }

    private void CreditPayRes(GamePacket param)
    {
	    int status = 1;
	    if(param.ContainKey("status")){
		    status = param.GetInt("status");
	    }
	    if(status==-1){
		    AlertController.api.showAlert(param.GetString("message"),()=>{
			    Init();
		    });
	    }else{
            AlertController.api.showAlert("Trả Thành Công");
            Init();
	    }
    }

    private void CreditInfoRes(GamePacket param)
    {
	    long uservip=0;
	    long maxcredit = 0;
	    long debt = 0;
	    long mortgageVip=0;
	    long interest=0;
	    if(param.ContainKey("uservip")) uservip = param.GetLong("uservip");
	    if(param.ContainKey("maxcredit")) maxcredit = param.GetLong("maxcredit");
	    if(param.ContainKey("debt")) debt = param.GetLong("debt");
	    if(param.ContainKey("mortgageVip")) mortgageVip = param.GetLong("mortgageVip");
	    if(param.ContainKey("interest")) interest = param.GetLong("interest");
	    
	    //Text txtVip, txtVipTheChap, txtHanMucVay, txtLaiPhaiTra;
	    txtVip.text = uservip.ToString();
        MyInfo.MY_ID_VIP = (int)uservip;
	    txtHanMucVay.text = Utilities.GetStringMoneyByLongBigSmall(maxcredit);
	    txtNo.text = Utilities.GetStringMoneyByLongBigSmall(debt);
	    txtVipTheChap.text = mortgageVip.ToString();
	    txtLaiPhaiTra.text = Utilities.GetStringMoneyByLongBigSmall(interest);
    }

    long chipCuuTro;
    public void AddChip(string val)
    {
        chipCuuTro += long.Parse(val);
        if (chipCuuTro > 100000000000)
        {
            AlertController.api.showAlert("Số Chip Vượt Mức Quy Định");
            chipCuuTro = 100000000000;
        }
        TxtChipPlus.text = Utilities.GetStringMoneyByLongBigSmall(chipCuuTro);
    }
}