﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeGemController : MonoBehaviour
{
    [SerializeField]
    InputField gemReceive, userReceive, MsgContent;
    [SerializeField]
    Text gemUser, vipUser, Description, txtPercent, txtUser;
    long minGem = 10000000;
    long limitGem = 0;
    //List<long> lstChipLimit;
    int transfer_fee;
    long min_transfer;
    // Start is called before the first frame update
    void Start()
    {
        if (!GameHelper.IS_LOAD_LOGIN)
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
            return;
        }
        txtUser.text = MyInfo.NAME;
        //API.Instance.RequestInitChangeChip(InitChangeChipComplete);

        WWWForm form = new WWWForm();
        form.AddField("", "");
        API.Instance.BaseCallService("/transfer/init", form, InitChangeChipComplete);

    }
    public void BackToHome()
    {        
        Destroy(gameObject);
    }
    void InitChangeChipComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        minGem = long.Parse(node["min_gem"].Value);
        transfer_fee = int.Parse(node["transfer_gem_fee"]);
        min_transfer = long.Parse(node["min_gem_transfer"]);
        txtPercent.text = transfer_fee + "%";
        LoadingManager.Instance.ENABLE = false;
        gemUser.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        vipUser.text = "Vip" + MyInfo.MY_ID_VIP.ToString();
        HanMucChuyenTheoVip();
    }
    void HanMucChuyenTheoVip()
    {
        int vip = MyInfo.MY_ID_VIP;
        GoiVip gv;
        Description.text = "";
        for (int i = 0; i < VIP_Controll.DicVipData.Count; i++)
        {
            gv = VIP_Controll.DicVipData[i];
            if (gv.HanMucTangGem != "")
            {
                Description.text += "Vip" + i + " : " + Utilities.GetStringMoneyByLong(long.Parse(gv.HanMucTangGem)) + "/lần \n";
            }
        }
        gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
        if (gv.HanMucTangGem == "")
        {
            limitGem = 0;
        }
        else
        {
            limitGem = long.Parse(gv.HanMucTangGem);
        }

    }
    public void ChangeChipToUser()
    {
        string uReceive = userReceive.text;
        string strGemIsReceive = gemReceive.text;
        long gemIsReceive = 0;
        if (strGemIsReceive != "")
        {
            gemIsReceive = long.Parse(strGemIsReceive);
        }
        if (uReceive == "")
        {
            AlertController.api.showAlert("Vui Lòng Nhập Chính Xác Tên Người Được Tặng!");
            return;
        }
        else if (MyInfo.GEM < minGem)
        {
            AlertController.api.showAlert("Số Gem Của Bạn Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(minGem) + " Mới Có Thể Chuyển Được!");
            return;
        }
        else if (limitGem == 0)
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip 1 Trở Lên Mới Có Thể Tặng Gem Được!");
            return;
        }
        else if (gemIsReceive < min_transfer)
        {
            AlertController.api.showAlert("Số Gem Tặng Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(min_transfer) + " !");
            return;
        }
        else if (gemIsReceive > limitGem)
        {
            AlertController.api.showAlert("Vượt Hạn Mức Cho Phép. Số Gem Tặng Của Bạn Phải Thấp Hơn " + Utilities.GetStringMoneyByLong(limitGem) + " !");
            return;
        }
        else if (MyInfo.GEM - gemIsReceive < minGem)
        {
            AlertController.api.showAlert("Số Gem Còn Lại Của Bạn Sau Khi Chuyển Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(minGem) + " !");
            return;
        }
        else if (MsgContent.text == "")
        {
            AlertController.api.showAlert("Bạn Chưa Nhập Nội Dung Tặng!");
            return;
        }
        //API.Instance.RequestChangeChip(MsgContent.text, uReceive, chipIsReceive, ChangeChipComplete);
        //WWWForm form = new WWWForm();
        //form.AddField("to_user_name", uReceive);
        //form.AddField("money", gemIsReceive.ToString());        
        //form.AddField("msg", MsgContent.text);
        //API.Instance.BaseCallService("/transfer/transfergem", form, ChangeChipComplete);

        GamePacket gp = new GamePacket(CommandKey.TRANSFER_ITEM);
        gp.Put("type", "gem");
        gp.Put("amount", gemIsReceive);
        gp.Put("to_user_name", uReceive);
        SFS.Instance.SendZoneRequest(gp);
    }
    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.TRANSFER_ITEM:
                ChangeGemFromServer(gp);
                break;
        }
    }
    private void ChangeGemFromServer(GamePacket gp)
    {
        int status = gp.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Tặng Chip Thành Công, Chip Tặng Sẽ Được Gửi Vào Hộp Thư Người Được Tặng!");
            MyInfo.GEM = gp.GetLong("gem");
            gemUser.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        }
        else
        {
            AlertController.api.showAlert(gp.GetString("msg"));
        }
    }
    void ChangeChipComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        if (int.Parse(node["status"].Value) == -1)
        {
            AlertController.api.showAlert(node["msg"].Value);
            return;
        }
        else if (int.Parse(node["status"].Value) == 1)
        {
            AlertController.api.showAlert("Tặng Gem Thành Công, Gem Tặng Sẽ Được Gửi Vào Hộp Thư Người Được Tặng!");
            //MyInfo.CHIP = long.Parse(node["chip"].Value);
            MyInfo.GEM = long.Parse(node["gem"].Value);
            gemUser.text = Utilities.GetStringMoneyByLong(MyInfo.GEM);
        }
        else
        {
            AlertController.api.showAlert("Lỗi Hệ Thống! Vui Lòng Thử Lại");
        }
    }
}
