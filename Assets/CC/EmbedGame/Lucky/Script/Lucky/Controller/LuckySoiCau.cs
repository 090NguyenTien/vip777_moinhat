﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuckySoiCau : MonoBehaviour {

    // Use this for initialization
    public GameObject historyContainer;
    public Sprite whitePoint;
    public Sprite blackPoint;
    void Start () {
		
	}
    public void Open()
    {
        gameObject.SetActive(true);
        GamePacket gp = new GamePacket(LuckyMiniCommandKey.GetSicBoLog100);
        SFS.Instance.SendRoomRequest(gp);
    }
    public void onExtensionResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case LuckyMiniCommandKey.GetSicBoLog100:
                GetSicBoLog100(gp);
                break;
        }
    }

    private void GetSicBoLog100(GamePacket gp)
    {
        var logTX = gp.param.GetUtfString(LuckyMiniParamKey.LogTx);
        var log = logTX.ToCharArray();
        //1: xiu; 0:tai
        for (var i = log.Length-1; i >= 0; i--)
        {
            var image = historyContainer.transform.GetChild(i).GetComponent<UnityEngine.UI.Image>();
            image.sprite = log[i].Equals('1') ? whitePoint : blackPoint;
        }
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }
}
