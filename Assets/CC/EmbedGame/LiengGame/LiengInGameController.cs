﻿using UnityEngine;
using System.Collections;
using HelperXT;
using ViewXT;
using System.Collections.Generic;
using BaseCallBack;
using UnityEngine.UI;
using ConstXT;
using System;
using SimpleJSON;

namespace ControllerXT
{
    public class LiengInGameController : MonoBehaviour
    {

        [SerializeField]
        XTDataHelper lieng_DataHelper;
        [SerializeField]
        LiengInGameView View;
        [SerializeField]
        LiengCardView CardView;
        [SerializeField]
        XTPlayerView[] InfoAllView;

        [SerializeField]
        XTPlayerView[] InfoAllView_Enemy;

        [SerializeField]
        XTCountDownPickCard CountDownPickCard;
        //	[SerializeField]

        [SerializeField]
        PopupInviteManager popupInvite;
        [SerializeField]
        PopupAlertManager popupAlert;
        [SerializeField]
        CountDownManager CountDown;

        public static LiengInGameController Instance { get; set; }
        //int[4 players] [5 cards]
        private int[][] HandCardsAll;   //HandCards 5 player, moi player 5 la bai, vi tri player nao ko co = null
        private BaseUserInfo[] InfoAll;

        private int sfsid_NemBomPha = -1;
        private int sfsid_BiNemBomPha = -1;

        string IdBangMaster = "";
        string IdBangUser_Nem = "";
        string IdBangUser_BiNem = "";
        string IdBangChinhMinh = "";

        [SerializeField]
        GameObject ViTriBoomUser, ViTriBoom_1, ViTriBoom_2, ViTriBoom_3, ViTriBoom_4, ViTriBoom_5;

        [SerializeField]
        GameObject ViTri_CANON_0, ViTri_CANON_1, ViTri_CANON_2, ViTri_CANON_3, ViTri_CANON_4, ViTri_CANON_5;

        [SerializeField]
        GameObject ItemBoom_1, ItemBoom_2, ItemBoom_3, ItemBoom_4, ItemBoom_5, ItemBoom_6, PhaBom, Canon, BaiVang, ThanBai, Item_quyphi, QuyPhi, Item_SoaiCa, SoaiCa, Item_HaiPhuong, HaiPhuong;
        [SerializeField]
        GameObject Item_ThietPhien, ThietPhien;

        [SerializeField]
        GameObject ObjAvata_1, ObjAvata_2, ObjAvata_3, ObjAvata_4, ObjAvata_5;
        [SerializeField]
        GameObject ObjBtnInfo_1, ObjBtnInfo_2, ObjBtnInfo_3, ObjBtnInfo_4, ObjBtnInfo_5;

        private int UserPosSV = 0;
        private int sfsid_BiNem = -1;
        private int SfsLoaiBoom = -1;

        bool isHost;

        int TurnRound;

        bool IsFirstRaise = false;

        /// <summary>
        /// Tien cuoc toi thieu 1 lan to
        /// </summary>
        long CASH_IN = 1000;

        const int MAX_HANDCARDS=3;
        const int MAX_USER = 6;

        enum TableState
        {
            Waiting,
            CountDown,
            Started,
            Result,
        }
        TableState State;
        /// <summary>
        /// < idSFS , positionClient >
        /// </summary>
        Dictionary<int, int> dictIdPos;

        int CurrentHandCardsCount = 0; //Luot to trong van (0-5)
        long BetCurrent, BetTotal;  //So tien cuoc hien tai va tong cong

        //		int checkCount = 0;	//Bao nhieu lan Check, de biet nguoi cuoi cung, ko dc Check nua
        //		int playerCount = 0;	//So luong nguoi tham gia van choi

        [SerializeField]
        PopupFullManager popupFull;

        public delegate void CallBack();
        CallBack resultComplete;

        SFS sfs;

        bool isRaised = false; //Vong to moi co nguoi to hay chua
        #region RESULT
        // idAllGroup # handCardsGroup # idFailGroup # idWinGroup # idNormalGroup # idDieGroup #
        //	kqChi1Group # kqChi2Group # kqChi3Group #
        //	kqChiAtGroup # kqTotalChiGroup # kqTotalChip
        int[] userIdAll;
        int[] userIdFailGroup;
        int[] userIdWinGroup;
        int[] userIdNormalGroup;
        int[] userIdDieGroup;
        int[][] handCardsGroup;
        int[] resultRow1Group, resultRow2Group, resultRow3Group;
        int[] resultChiAtGroup, resultRowTotalAll;
        long[] resultChipTotalAll;

        #endregion




        private bool WorkDone = true;
        private bool RunningSubCoroutine = false;
        private Queue<GamePacket> GamePacketQueue = new Queue<GamePacket>();
        private readonly object SyncLock = new object();
        private bool IsGettedGameRoomInfo = false;

        string[] SoundAllMoney = new string[]{"Sounds/XiTo/AllMoney/ChoiToiBenLuonNe",
            "Sounds/XiTo/AllMoney/ChoiToiBenLuonNeCon",
            "Sounds/XiTo/AllMoney/ToHetLuonNeMay"
        };
        string[] SoundAllIn = new string[] {"Sounds/XiTo/AllIn/BacNaoDamTheo",
            "Sounds/XiTo/AllIn/BaiNhoThiUpNhanh",
            "Sounds/XiTo/AllIn/CoGioiThiToTatCaLuonDi",
            "Sounds/XiTo/AllIn/CoNgonThiTheoDi",
            "Sounds/XiTo/AllIn/ToTatCaLuonNe"
        };
        string[] SoundCall = new string[] {"Sounds/XiTo/Call/Theo",
            "Sounds/XiTo/Call/TheoLuon",
            "Sounds/XiTo/Call/TheoLuonNeMay",
            "Sounds/XiTo/Call/TheoNe"
        };
        string[] SoundCheck = new string[] { "Sounds/XiTo/Check/BacToDiHaha",
            "Sounds/XiTo/Check/NhuongToDo"
        };
        string[] SoundFold = new string[] {"Sounds/XiTo/Fold/UpNhe",
            "Sounds/XiTo/Fold/UpThoiBaiXauQua"
        };
        string[] SoundRaise = new string[] {"Sounds/XiTo/Raise/To",
            "Sounds/XiTo/Raise/ToNe"
        };
        string[] SoundRaiseDouble = new string[] {"Sounds/XiTo/RaiseDouble/GapDoiLuonNe",
            "Sounds/XiTo/RaiseDouble/GapDoiNeCon",
            "Sounds/XiTo/RaiseDouble/ToGapDoiNe"
        };
        string[] SoundRaisePart2 = new string[] {"Sounds/XiTo/RaisePart2/MotPhanHaiLuonNe",
            "Sounds/XiTo/RaisePart2/ToMotPhanHaiNe"
        };
        string[] SoundRaisePart4 = new string[] {"Sounds/XiTo/RaisePart4/MotPhanTuNhaCacBac",
            "Sounds/XiTo/RaisePart4/ToMotPhanTuNe"
        };

        void PlaySound(string _sound = SoundManager.BUTTON_CLICK)
        {
            SoundManager.PlaySound(_sound);
        }
        void PlaySoundAllIn()
        {
            int index = UnityEngine.Random.Range(0, SoundAllIn.Length);
            PlaySound(SoundAllIn[index]);
        }
        void PlaySoundAllMoney()
        {
            int index = UnityEngine.Random.Range(0, SoundAllMoney.Length);
            PlaySound(SoundAllMoney[index]);
        }
        void PlaySoundCall()
        {
            int index = UnityEngine.Random.Range(0, SoundCall.Length);
            PlaySound(SoundCall[index]);
        }
        void PlaySoundFold()
        {
            int index = UnityEngine.Random.Range(0, SoundFold.Length);
            PlaySound(SoundFold[index]);
        }
        void PlaySoundCheck()
        {
            int index = UnityEngine.Random.Range(0, SoundCheck.Length);
            PlaySound(SoundCheck[index]);
        }
        void PlaySoundRaise()
        {
            int index = UnityEngine.Random.Range(0, SoundRaise.Length);
            PlaySound(SoundRaise[index]);
        }
        void PlaySoundRaiseDouble()
        {
            int index = UnityEngine.Random.Range(0, SoundRaiseDouble.Length);
            PlaySound(SoundRaiseDouble[index]);
        }
        void PlaySoundRaisePart2()
        {
            //int index = UnityEngine.Random.Range(0, SoundRaisePart2.Length);
            //PlaySound(SoundRaisePart2[index]);
            SoundManager.PlaySound(SoundManager.TL_NGON_VO_DAY);
        }
        void PlaySoundRaisePart4()
        {
            //int index = UnityEngine.Random.Range(0, SoundRaisePart4.Length);
            //PlaySound(SoundRaisePart4[index]);
            SoundManager.PlaySound(SoundManager.TL_NGON_VO_DAY);
        }

        void Awake()
        {
            Instance = this;
            LoadingManager.Instance.ENABLE = false;
            sfs = SFS.Instance;

            Init();
            IdBangMaster = MyInfo.ID_BANG_MASTER;
            IdBangChinhMinh = MyInfo.ID_BANG;
            popupFull.Init();
            LayThongTinThanBai();
        }

        float timePing = 10;
        void Update()
        {
            timePing -= Time.deltaTime;

            if (timePing < 0)
            {
                timePing = 10;
                sfs.Ping();
            }

        }






        #region NÉM ITEM

        #region GỬI REQUEST

        public void SendBomb()
        {
            GamePacket param = new GamePacket("bomb");
            param.Put("sfsid_nem", MyInfo.SFS_ID);
            param.Put("sfsid_binem", sfsid_BiNem);
            param.Put("bomb_type", SfsLoaiBoom);
            SFS.Instance.SendBoomRequest(param);
        }

        public void SendBomPha()
        {
            GamePacket param = new GamePacket("bomb");
            param.Put("sfsid_nem", sfsid_NemBomPha);
            param.Put("sfsid_binem", sfsid_BiNemBomPha);
            param.Put("bomb_type", SfsLoaiBoom);

            if (sfsid_NemBomPha != sfsid_BiNemBomPha)
            {
                SFS.Instance.SendBoomRequest(param);
            }
            sfsid_NemBomPha = -1;
            sfsid_BiNemBomPha = -1;
        }

        #endregion

        #region NHẬN RESPONSE


        public void ResponseNemBoom(GamePacket param)
        {

            int sfsid_Nem = param.GetInt("sfsid_nem");
            int sfsid_BiNem = param.GetInt("sfsid_binem");
            int LoaiBoom = param.GetInt("bomb_type");
            XTPlayerView Enemy_Nem = new XTPlayerView();
            XTPlayerView Enemy_BiNem = new XTPlayerView();
            bool NemVaoUser = false;
            //  Debug.Log("Vô ResponseNemBoom--------------sfsid_Nem- " + sfsid_Nem + " sfsid_BiNem = " + sfsid_BiNem + " LoaiBoom = " + LoaiBoom);
            if (sfsid_Nem != MyInfo.SFS_ID)
            {
                foreach (var item in InfoAllView_Enemy)
                {
                    if (item.sfsId == sfsid_Nem)
                    {
                        Enemy_Nem = item;
                    }
                }

                if (sfsid_BiNem == MyInfo.SFS_ID)
                {
                    NemVaoUser = true;
                }
                else
                {
                    foreach (var item in InfoAllView_Enemy)
                    {
                        if (item.sfsId == sfsid_BiNem)
                        {
                            Enemy_BiNem = item;
                        }
                    }
                }
                int ViTriSinh = Enemy_Nem.Id_ViTriBom;

                if (NemVaoUser == false)  // TRƯỜNG HỢP NGƯỜI THỨ 3
                {
                    int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                    IdBangUser_Nem = Enemy_Nem.IdBang;
                    IdBangUser_BiNem = Enemy_BiNem.IdBang;
                    string idUser_Nem = Enemy_Nem.userId;

                    NguoiThuBa(LoaiBoom, ViTriSinh, ViTriNem, IdBangUser_Nem, IdBangUser_BiNem, idUser_Nem);
                }
                else     // TRƯỜNG HỢP NGƯỜI BỊ NÉM
                {
                    NguoiBiNem(LoaiBoom, ViTriSinh, Enemy_Nem.IdBang, MyInfo.ID);
                }
            }
            else      // TRƯỜNG HỢP NGƯỜI NÉM
            {
                if (LoaiBoom == 6)
                {
                    foreach (var item in InfoAllView_Enemy)
                    {
                        if (item.sfsId == sfsid_BiNem)
                        {
                            Enemy_BiNem = item;
                        }
                    }
                    int ViTriNem = Enemy_BiNem.Id_ViTriBom;
                    IdBangUser_BiNem = Enemy_BiNem.IdBang;

                    NguoiNem(ViTriNem, MyInfo.ID);
                }
            }
        }




        void NguoiNem(int ViTriNem, string idUser_ChinhMinh)
        {

            bool check = CheckPhanDame(IdBangUser_BiNem, IdBangChinhMinh);
            //   check = false;
            if (check == false)
            {
                int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

                if (loaiThanBai == -1)
                {
                    return;
                }
                else
                {
                    GoiThanBai(ViTriNem, 0, 0, loaiThanBai);
                }
            }
            else
            {
                SinhBoom_DiChuyenToiViTri(ViTriNem, 0, 0);
                StartCoroutine(GoiPhanDame(0, ViTriNem));
            }
        }

        void NguoiBiNem(int LoaiBoom, int ViTriSinh, string IdBangNem, string idUser_ChinhMinh)
        {
            if (LoaiBoom != 6)
            {
                IdBangUser_Nem = IdBangNem;
                bool check = CheckPhanDame(IdBangUser_Nem, IdBangChinhMinh);
                //   check = false;
                if (check == false)
                {
                    int loaiThanBai = CheckDicThanBai(idUser_ChinhMinh);

                    if (loaiThanBai == -1)
                    {
                        SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                    }
                    else
                    {
                        GoiThanBai(ViTriSinh, 0, LoaiBoom, loaiThanBai);
                    }
                }
                else
                {
                    SinhBoom_DiChuyenToiViTri(ViTriSinh, 0, LoaiBoom);
                    StartCoroutine(GoiPhanDame(0, ViTriSinh));
                }
            }
        }

        void NguoiThuBa(int LoaiBoom, int ViTriSinh, int ViTriNem, string IdBangUser_Nem, string IdBangUser_BiNem, string idUser_Nem)
        {
            if (LoaiBoom != 6)
            {
                bool check = CheckPhanDame(IdBangUser_Nem, IdBangUser_BiNem);

                if (check == false)
                {
                    SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
                }
                else
                {
                    SinhBoom_DiChuyenToiViTri(ViTriSinh, ViTriNem, LoaiBoom);
                    StartCoroutine(GoiPhanDame(ViTriNem, ViTriSinh));
                }

            }
            else
            {
                bool check = CheckPhanDame(IdBangUser_BiNem, IdBangUser_Nem);

                if (check == true)
                {
                    SinhBoom_DiChuyenToiViTri(ViTriNem, ViTriSinh, 0);
                    StartCoroutine(GoiPhanDame(ViTriSinh, ViTriNem));
                }
                else
                {
                    int loaiThanBai = CheckDicThanBai(idUser_Nem);

                    if (loaiThanBai == -1)
                    {

                    }
                    else
                    {
                        GoiThanBai(ViTriNem, ViTriSinh, 1, loaiThanBai);
                    }
                }
            }
        }


        #endregion

        #region SINH ITEM BOOM





        public void SinhBoom_DiChuyenToiViTri(int ViTriSinh, int ViTriNem, int loaiBoom, bool Send = false, int LoaiBoom_PhanDame = 0)
        {
            Transform targetMove = null;
            GameObject MyBoom = null;
            GameObject MyCanon = null;

            // Debug.LogWarning("ViTriSinh - " + ViTriSinh + " ViTriNem - " + ViTriNem + " loaiBoom - " + loaiBoom);

            #region Tao Boom

            if (loaiBoom != 6) // TRƯỜNG HỢP THƯỜNG
            {
                MyBoom = TaoBoomTaiViTriSinh(loaiBoom, ViTriSinh);
            }
            else // TRƯỜNG HỢP PHAN DAME
            {
                MyCanon = TAO_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
                MyBoom = TAO_BOOM_PHANDAME(ViTriSinh, LoaiBoom_PhanDame);
            }

            MyBoom.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            // XÁC ĐỊNH VỊ TRÍ NÉM
            targetMove = XacDinhViTriNem(ViTriNem);

            #endregion

            ItemBoomControll MyItem = MyBoom.GetComponent<ItemBoomControll>();

            MyItem.Init(targetMove);

            if (Send == false)  // TRƯỜNG HỢP CHỈ DIỂN HOẠT KHÔNG GỬI REQUEST
            {
                MyItem.NemBoom(NemThuong, loaiBoom);
            }
            else   // TRƯỜNG HỢP GỬI REQUEST
            {
                if (loaiBoom != 6) // Choi Thuong
                {
                    sfsid_BiNem = GetSfsIdEnemy(ViTriNem);
                    if (sfsid_BiNem != -1)
                    {
                        SfsLoaiBoom = loaiBoom;
                        MyItem.NemBoom(SendBomb, loaiBoom);
                    }
                }
                else // Choi Nhung Bi Phan Dame
                {
                    sfsid_BiNemBomPha = MyInfo.SFS_ID;
                    sfsid_NemBomPha = GetSfsIdEnemy(ViTriSinh);
                    if (sfsid_BiNemBomPha != -1)
                    {
                        SfsLoaiBoom = loaiBoom;
                        MyItem.NemBoom(SendBomPha, loaiBoom);
                    }
                }

            }

        }





        GameObject TaoBoomTaiViTriSinh(int loaiboom, int ViTriSinh)
        {
            GameObject MyBoom = null;
            GameObject ObjBoom = null;
            ObjBoom = XacDinhLoaiBoom(loaiboom);

            if (ViTriSinh == 0)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoomUser.transform) as GameObject;
            }
            else if (ViTriSinh == 1)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoom_1.transform) as GameObject;
            }
            else if (ViTriSinh == 2)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoom_2.transform) as GameObject;
            }
            else if (ViTriSinh == 3)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoom_3.transform) as GameObject;
            }
            else if (ViTriSinh == 4)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoom_4.transform) as GameObject;
            }
            else if (ViTriSinh == 5)
            {
                MyBoom = Instantiate(ObjBoom, ViTriBoom_5.transform) as GameObject;
            }

            return MyBoom;
        }


        GameObject XacDinhLoaiBoom(int loaiboom)
        {
            GameObject ObjBoom = null;

            if (loaiboom == 0)
            {
                ObjBoom = ItemBoom_1;
            }
            else if (loaiboom == 1)
            {
                ObjBoom = ItemBoom_2;
            }
            else if (loaiboom == 2)
            {
                ObjBoom = ItemBoom_3;
            }
            else if (loaiboom == 3)
            {
                ObjBoom = ItemBoom_4;
            }
            else if (loaiboom == 4)
            {
                ObjBoom = ItemBoom_5;
            }
            else if (loaiboom == 5)
            {
                ObjBoom = ItemBoom_6;
            }

            return ObjBoom;
        }




        GameObject TAO_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
        {
            GameObject Canon = null;
            GameObject Obj_Canon = XacDinhLoai_PHANDAME(LoaiBoom_PhanDame);


            if (ViTriSinh == 0)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_0.transform) as GameObject;

            }
            else if (ViTriSinh == 1)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_1.transform) as GameObject;

            }
            else if (ViTriSinh == 2)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_2.transform) as GameObject;

            }
            else if (ViTriSinh == 3)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_3.transform) as GameObject;

            }
            else if (ViTriSinh == 4)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_4.transform) as GameObject;

            }
            else if (ViTriSinh == 5)
            {
                Canon = Instantiate(Obj_Canon, ViTri_CANON_5.transform) as GameObject;

            }

            return Canon;
        }


        GameObject TAO_BOOM_PHANDAME(int ViTriSinh, int LoaiBoom_PhanDame)
        {
            GameObject itemboom = null;
            GameObject Obj_itemboom = XacDinhLoai_BOOM_PHANDAME(LoaiBoom_PhanDame);

            if (ViTriSinh == 0)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoomUser.transform) as GameObject;
            }
            else if (ViTriSinh == 1)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoom_1.transform) as GameObject;
            }
            else if (ViTriSinh == 2)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoom_2.transform) as GameObject;
            }
            else if (ViTriSinh == 3)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoom_3.transform) as GameObject;
            }
            else if (ViTriSinh == 4)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoom_4.transform) as GameObject;
            }
            else if (ViTriSinh == 5)
            {
                itemboom = Instantiate(Obj_itemboom, ViTriBoom_5.transform) as GameObject;
            }

            return itemboom;

        }


        GameObject XacDinhLoai_BOOM_PHANDAME(int loaiboom)
        {
            GameObject ObjBoom = null;

            String name = loaiboom.ToString() + "_efect";
            ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
            LoadBundleLogin.api.UnloadBundle();

            if (ObjBoom == null)
            {
                ObjBoom = Item_SoaiCa;
            }
            return ObjBoom;
        }

        GameObject XacDinhLoai_PHANDAME(int loaiboom)
        {
            GameObject ObjBoom = null;

            String name = loaiboom.ToString() + "_item";
            ObjBoom = LoadBundleLogin.api.GetAssetBundleByName(name);
            LoadBundleLogin.api.UnloadBundle();

            if (ObjBoom == null)
            {
                ObjBoom = SoaiCa;
            }
            return ObjBoom;

        }


        Transform XacDinhViTriNem(int ViTriNem)
        {
            Transform vitri = null;
            if (ViTriNem == 0)
            {
                vitri = ViTriBoomUser.transform;
            }
            else if (ViTriNem == 1)
            {
                vitri = ViTriBoom_1.transform;
            }
            else if (ViTriNem == 2)
            {
                vitri = ViTriBoom_2.transform;
            }
            else if (ViTriNem == 3)
            {
                vitri = ViTriBoom_3.transform;
            }
            else if (ViTriNem == 4)
            {
                vitri = ViTriBoom_4.transform;
            }
            else if (ViTriNem == 5)
            {
                vitri = ViTriBoom_5.transform;
            }
            return vitri;
        }


        void NemThuong()
        {

        }


        public int GetSfsIdEnemy(int id_ViTriBom)
        {
            int SfsId = -1;
            foreach (var item in InfoAllView_Enemy)
            {
                if (item.Id_ViTriBom == id_ViTriBom)
                {
                    SfsId = item.sfsId;
                    return SfsId;
                }
            }
            return SfsId;
        }



        #endregion

        #region GOI PHẢN DAME


        bool CheckPhanDame(string idBangUser_Nem, string idBangUser_BiNem)
        {
            if (idBangUser_BiNem == "")
            {
                return false;
            }

            if (idBangUser_BiNem == idBangUser_Nem)
            {
                return false;
            }

            if (idBangUser_BiNem != IdBangMaster)
            {
                return false;
            }

            return true;
        }



        public void PhanDame(int Enemy, int LoaiBoom)
        {
            SinhBoom_DiChuyenToiViTri(0, Enemy, LoaiBoom);
            StartCoroutine(PrintfAfter(Enemy, LoaiBoom));
        }

        IEnumerator PrintfAfter(int Enemy, int LoaiBoom)
        {
            yield return new WaitForSeconds(0.3f);
            SinhBoom_DiChuyenToiViTri(Enemy, 0, 6, true);
        }


        IEnumerator GoiPhanDame(int vitri_nem, int vitri_binem)
        {
            yield return new WaitForSeconds(0.3f);
            SinhBoom_DiChuyenToiViTri(vitri_nem, vitri_binem, 6);
        }



        int CheckDicThanBai(string idUser)
        {
            int kq = -1;
            if (DataHelper.DicThanBai.ContainsKey(idUser))
            {
                kq = DataHelper.DicThanBai[idUser];
            }
            return kq;
        }



        public void LayThongTinThanBai()
        {
            API.Instance.RequestGetData_userAvoidItem(RspLayThongTinThanBai);
        }


        void RspLayThongTinThanBai(string _json)
        {
            JSONNode node = JSONNode.Parse(_json);
            DataHelper.DicThanBai.Clear();

            int cou = node.Count;

            for (int i = 0; i < cou; i++)
            {
                string id = node[i]["user_id"].Value;
                int id_item = int.Parse(node[i]["item_id"].Value);
                DataHelper.DicThanBai.Add(id, id_item);
            }
        }

        public void GoiThanBai(int ViTri_Nem, int Vitri_ThanBai, int loaiboom, int loaiThanBai, bool sentRequest = false)
        {
            SinhBoom_DiChuyenToiViTri(ViTri_Nem, Vitri_ThanBai, loaiboom);

            StartCoroutine(WaitTest(ViTri_Nem, Vitri_ThanBai, loaiThanBai, sentRequest));

        }


        IEnumerator WaitTest(int ViTri_Nem, int Vitri_ThanBai, int loaiThanBai, bool sentRequest = false)
        {
            yield return new WaitForSeconds(0.3f);
            SinhBoom_DiChuyenToiViTri(Vitri_ThanBai, ViTri_Nem, 6, sentRequest, loaiThanBai);
        }


        public void TestGoiThanBai(int loaiThanBai)
        {
            GoiThanBai(3, 1, 2, loaiThanBai);
        }





        #endregion


        #endregion



        public void LayThongTinBangChinhMinh()
        {
            API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangChinhMinh);
        }


        void RspLayIdBangChinhMinh(string _json)
        {

            Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

            JSONNode node = JSONNode.Parse(_json);
            if (node != null)
            {
                string userBang = node["user_clan"]["$id"].Value;
                string BangMaster = node["master_clan"]["$id"].Value;

                //  Debug.LogWarning("usserBang------------- " + userBang);
                // Debug.LogWarning("BangMaster------------- " + BangMaster);

                IdBangMaster = BangMaster;
                IdBangChinhMinh = userBang;
            }
            
        }






        void Init()
        {
            sfs = SFS.Instance;
            lieng_DataHelper.Init();

            View.Init(this);
            CardView.Init(this);

            isHost = false;
            State = TableState.Waiting;
            HandCardsAll = new int[MAX_USER][];

            InfoAll = new BaseUserInfo[MAX_USER];
            dictIdPos = new Dictionary<int, int>();

            View.SetEnableBtnStartGame(false);
            CardView.SetEnablePickCard(false);

            for (int i = 0; i < MAX_USER; i++)
            {
                CardView.SetEnableHandCards(i, false);

                InfoAll[i] = new BaseUserInfo();
                InfoAll[i].IsJoined = false;
                InfoAllView[i].HideResultPointLieng();
                HandCardsAll[i] = null;

            }

            RemoveAllPlayer();

            RqGetRoomInfo();

            popupAlert.Init();

            popupInvite.Init();

            LoadingManager.Instance.ENABLE = false;
        }


        #region PRIVATE - Table Info - Content

        void ShowTableInfo(GamePacket _param)
        {
            View.ShowTableInfo(_param.GetInt(ParamKey.ROOM_ID),
            _param.GetInt(ParamKey.HOST_ID),
            _param.GetLong(ParamKey.BET_MONEY),
            _param.GetString(ParamKey.HOST_NAME));
        }


        #endregion

        #region Actions OnClick

        public void FoldOnClick()
        {
            PlaySound();
            RqActionFold();
        }
        public void CheckOnClick()
        {
            PlaySound();
            RqActionCheck();
        }
        public void RaiseOnClick()
        {
            PlaySound();
            RqActionRaise(1);
        }
        public void CallOnClick()
        {
            PlaySound();
            RqActionRaise(1);
        }
        public void RaiseMulti2OnClick()
        {
            PlaySound();
            RqActionRaise(2);
        }
        public void RaisePart4OnClick()
        {
            PlaySound();
            RqActionRaise(3);
        }
        public void RaisePart2OnClick()
        {
            PlaySound();
            RqActionRaise(4);
        }
        public void RaiseAllOnClick()
        {
            PlaySound();
            RqActionRaise(5);
        }

        #endregion

        #region INVITE

        public void BtnInviteOnClick()
        {
            PlaySound();
            popupInvite.Show(RqInvite, CloseInviteOnClick);
            RqInvite();
        }
        public void ItemPlayerInviteOnClick(int _id)
        {
            GamePacket param = new GamePacket(CommandKey.INVITE);
            param.Put(ParamKey.USER_ID, _id);
            SFS.Instance.SendRoomRequest(param);
        }
        void RqInvite()
        {
            GamePacket param = new GamePacket(CommandKey.GET_PLAYERS_INVITE);
            SFS.Instance.SendRoomRequest(param);
        }
        void RspGetPlayersInvite(GamePacket _param)
        {
            //			string userList = _param.GetString (ParamKey.USER_LIST);
            //
            //			if (string.IsNullOrEmpty (userList)) {
            //				popupInvite.ClearAllitem ();
            //				return;
            //			}
            //			string[] infos = userList.Split ('$');
            //
            //			popupInvite.ShowItems (infos);

            popupInvite.ShowItems(_param);
        }

        public void CloseInviteOnClick()
        {
            popupInvite.Hide();
        }

        #endregion

        #region CHAT

        public void BtnChatOnClick()
        {
            PlaySound();
            PopupChatManager.instance.Show();
        }

        public void OnPublicMsg(string _msg)
        {
            int idUser = int.Parse(_msg.Split('#')[0]);

            //Get Position by ID
            int position = dictIdPos[idUser];
            string vip = "0";
            for (int i = 0; i < 4; i++)
            {
                if (InfoAllView[i].sfsId == idUser)
                {
                    vip = InfoAllView[i].Vip;
                    break;
                }

            }
            PopupChatManager.instance.RspChat(position, _msg, vip);
        }

        #endregion

        #region KICK

        GamePacket packetKick;
        bool isKick;

        private void RspKickUser(GamePacket param)
        {
            int[] userList = param.GetIntArray(ParamKey.USER_LIST);
            foreach (int id in userList)
            {
                Debug.Log("id KICKED: " + id);
                if (id == MyInfo.SFS_ID)
                {
                    popupAlert.Show("Thông báo", "Bạn không đủ tiền tham gia ván chơi. Vui lòng nạp thêm!", () => {
                        sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
                    });

                }
                else
                {
                    PlayerExit(dictIdPos[id]);
                }
            }
        }

        #endregion

        #region ===== REQUEST =====

        void RqGetRoomInfo()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(CommandKey.GET_GAME_ROOM_INFO));
        }
        public void RqStartGame()
        {
            SFS.Instance.SendRoomRequest(new GamePacket(CommandKey.START_GAME));
        }

        private void RqPickCard(int _idCard)
        {
            GamePacket param = new GamePacket(XTCommandKey.PICK_CARD);
            param.Put(ParamKey.CARD_ID, _idCard);
            SFS.Instance.SendRoomRequest(param);
        }

        private void RqActionRaise(int _action)
        {
            GamePacket param = new GamePacket(XTCommandKey.RAISE);
            param.Put(ParamKey.ACTION_RAISE_XT, _action);

            SFS.Instance.SendRoomRequest(param);
        }
        private void RqActionFold()
        {
            GamePacket param = new GamePacket(XTCommandKey.FOLD);

            SFS.Instance.SendRoomRequest(param);
        }
        private void RqActionCheck()
        {
            GamePacket param = new GamePacket(XTCommandKey.CHECK);

            SFS.Instance.SendRoomRequest(param);
        }

        public void RqMoveCard(int _indexCard1, int _indexCard2)
        {
            //			GamePacket param = new GamePacket (MBCommandKey.MOVE_CARD);
            //			param.Put (MBParamKey.CARD_1, _indexCard1);
            //			param.Put (MBParamKey.CARD_2, _indexCard2);
            //
            //			SFS.Instance.SendRoomRequest (param);
        }
        public void RqFinishMove()
        {
            //			SFS.Instance.SendRoomRequest (new GamePacket (MBCommandKey.FINISH_MOVE));
        }
        public void RqUnFinishMove()
        {
            //			SFS.Instance.SendRoomRequest (new GamePacket (MBCommandKey.UNFINISH_MOVE));
        }

        #endregion

        #region ======== RESPONSE PUSH ========

        public void OnSFSResponse(GamePacket gp)
        {
            Debug.Log("XT - " + gp.cmd + " - " + gp.param.ToJson());

            switch (gp.cmd)// những command ngoài game.
            {
                case CommandKey.ADD_FRIEND_CONFIRM:
                case CommandKey.ADD_FRIEND_IGNORE:
                case CommandKey.ADD_FRIEND_NOTICE:
                case CommandKey.ADD_FRIEND_REQUEST:
                case CommandKey.ADD_FRIEND_RESULT:
                case CommandKey.ADD_FRIEND_SUCCESS:
                case CommandKey.GET_LIST_FRIEND:

                    return;
            }


            //Thuc hien lien ngay va lap tuc


            switch (gp.cmd)
            {
                case "bomb":
                    ResponseNemBoom(gp);
                    break;
                case CommandKey.GET_GAME_ROOM_INFO:
                    PlaySound(SoundManager.ENTER_ROOM);
                    RspRoomInfo(gp);
                    break;
                case CommandKey.USER_EXIT:
                    int userId = gp.GetInt(ParamKey.USER_ID);
                    if (userId == MyInfo.SFS_ID)
                    {
                        GamePacketQueue = new Queue<GamePacket>();
                        WorkDone = false;
                        RspExitGame(gp);
                        PlaySound(SoundManager.EXIT_ROOM);
                    }
                    else
                        GamePacketQueue.Enqueue(gp);
                    break;
                case CommandKey.CountDownAtStartGame:
                    RspCountDown(gp);
                    break;
                case CommandKey.START_GAME:
                    RspStartGame(gp);
                    break;

                case CommandKey.JOIN_GAME_LOBBY_ROOM:
                    GameHelper.ChangeScene(GameScene.WaitingRoom);
                    break;
                case CommandKey.GET_PLAYERS_INVITE:
                    RspGetPlayersInvite(gp);
                    break;

                case CommandKey.KickUser:
                    packetKick = gp;
                    isKick = true;
                    break;
                case CommandKey.KICK_USER_BY_HOST:
                    KickUserByHost(gp);
                    return;
                case CommandKey.KICK_USER_ERROR:
                    KickUserError(gp);
                    return;
                default:
                    GamePacketQueue.Enqueue(gp);
                    break;

            }

            if (WorkDone)
                HandleNextWorkInQueue();


        }
        private void HandleNextWorkInQueue()
        {
            WorkDone = true;
            if (GamePacketQueue.Count > 0)
            {
                HandleGamePacketQueue(GamePacketQueue.Dequeue());
            }

        }
        private void HandleGamePacketQueue(GamePacket gp)
        {
            if (!IsGettedGameRoomInfo)
            {
                if (gp.cmd != CommandKey.WatcherGetGameRoomInfoOK && gp.cmd != CommandKey.GET_GAME_ROOM_INFO)
                {
                    Debug.Log("Chua lay thong tin room!");
                    return;
                }
            }
            if (!WorkDone)
                return;

            WorkDone = false;

            //			Debug.Log ("HandleQueue: " + gp.cmd);
            switch (gp.cmd)
            {
                case CommandKey.NOTICE_JOIN_GAME_ROOM:
                    PlaySound(SoundManager.ENTER_ROOM);
                    PushJoinGame(gp);
                    break;
                case XTCommandKey.PICK_CARD:
                    PlaySound(SoundManager.SELECT_CARD);
                    RspPickCard(gp);
                    break;
                case XTCommandKey.DEAL_A_CARD:
                    PlaySound(SoundManager.DEAL_ONE_CARD);
                    RspDealACard(gp);
                    break;
                case XTCommandKey.DEAL_FINAL_CARD:
                    PlaySound(SoundManager.DEAL_ONE_CARD);
                    RspDealFinalCard(gp);
                    break;
                case XTCommandKey.FOLD:
                    PlaySoundFold();
                    RspFold(gp);
                    break;
                case XTCommandKey.CHECK:
                    PlaySoundCheck();
                    RspCheck(gp);
                    break;
                case XTCommandKey.RAISE:
                    RspRaise(gp);
                    break;
                case XTCommandKey.FINISH_GAME:
                    PushFinishGame(gp);
                    break;
                case CommandKey.USER_EXIT:
                    PlaySound(SoundManager.EXIT_ROOM);
                    RspExitGame(gp);
                    break;
            }
        }
        private void KickUserError(GamePacket param)
        {
            AlertController.api.showAlert(param.GetString("Message"));
        }
        private void KickUserByHost(GamePacket param)
        {
            AlertController.api.showAlert("Bạn đã bị chủ bàn mời ra khỏi phòng!");
            SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);
        }
        void RspCountDown(GamePacket param)
        {
            int second = param.GetInt(ParamKey.SecondsUntilStartGame);
            CountDown.InitNew((float)second);

            if (State != TableState.Result)
                CountDown.Show();
        }
        public void OnModeratorMessage(string _msg)
        {
            JSONNode node = JSONNode.Parse(_msg);
            if (node["is_level_up"].AsInt == 1)
            {
                InfoAllView[0].txtLevel.text = node["cur_level"];
            }

        }
        List<Hashtable> lstUserVariable;
        public void OnVariableResponse(List<Hashtable> lstDataRes)
        {
            lstUserVariable = lstDataRes;

        }
        private Hashtable GetUserVariableById(string uId)
        {
            //string uid = data["uid"].ToString();
            //string clan_id = data["clanid"].ToString();
            //string nhan_id = data["ringid"].ToString();
            //string gioitinh_id = data["gender"].ToString();
            //string tennguoitinh = data["partner"].ToString();
            foreach (Hashtable uVariable in lstUserVariable)
            {
                if (uVariable["uid"].ToString() == uId)
                {
                    return uVariable;
                }
            }
            return null;
        }
        void RspRoomInfo(GamePacket param)
        {

            Debug.Log("rsp room info ne");
            WorkDone = false;

            ShowTableInfo(param);

            CASH_IN = param.GetLong(ParamKey.BET_MONEY);

            string[] infoPlayers = param.GetString(ParamKey.USER_INFO).Split('$');

            //		InfoAll = new BaseUserInfo[MAX_USER];

            for (int pos = 0; pos < MAX_USER; pos++)
            {
                if (int.Parse(infoPlayers[pos].Split('#')[0]) == MyInfo.SFS_ID)
                {
                    UserPosSV = pos;
                    //Debug.Log ("UserPosSV: " + pos);
                    break;
                }
            }

            bool isPlaying = false;
            //SV pos => client Pos
            for (int i = 0; i < MAX_USER; i++)
            {
                if (infoPlayers.Length <= i) break;
                if (!string.IsNullOrEmpty(infoPlayers[i]))
                {

                    BaseUserInfo user = CastUserInfoRsp(infoPlayers[i], i);
                    user.IsReady = infoPlayers[i].Split('#')[5].Equals("1");
                    dictIdPos[user.IdSFS] = user.ClientPos;
                    //Thong tin la User
                    if (user.IdSFS == MyInfo.SFS_ID)
                    {
                        AddPlayer(0, user);
                    }
                    //Thong tin la Player
                    else
                    {
                        AddPlayer(user.ClientPos, user);
                    }

                    if (user.IsReady)
                        isPlaying = user.IsReady;
                }
            }

            int hostID = param.GetInt(ParamKey.HOST_ID);
            isHost = hostID == InfoAll[0].IdSFS;
            View.SetEnableBtnStartGame(isHost);

            if (isPlaying)
            {
                State = TableState.Started;
                View.ShowTableState(GetMsgState(State));
            }
            else
            {
                State = TableState.Waiting;
                View.ShowTableState(GetMsgState(State));
            }


            IsGettedGameRoomInfo = true;
            WorkDone = true;
            HandleNextWorkInQueue();
        }
        string GetMsgState(TableState _state)
        {
            switch (_state)
            {
                case TableState.Waiting:
                    return "Ðang chờ ván mới bắt đầu.";
                case TableState.Started:
                    return "Bàn đang chơi, vui lòng chờ giây lát.";
                case TableState.Result:
                    return "Ðang hiển thị kết quả, sắp bắt đầu ván mới.";
            }
            return "Noname";
        }
        void PushJoinGame(GamePacket _param)
        {
            string userInfo = _param.GetString(ParamKey.USER_INFO);
            int posSV = _param.GetInt(ParamKey.POSITION);


            BaseUserInfo newUser = CastUserInfoPush(userInfo, posSV);
            if (State == TableState.Started || State == TableState.Result)
                newUser.IsReady = true;

            newUser.IsReady = false;
            newUser.IsJoined = true;

            idAnother = newUser.IdSFS;
            Debug.Log("add user join game " + newUser.IdSFS);
            dictIdPos[newUser.IdSFS] = newUser.ClientPos;

            AddPlayer(newUser.ClientPos, newUser);
            //		InfoAll [newUser.ClientPos] = newUser;
            //		InfoAllView [newUser.ClientPos].ShowPlayer (InfoAll [newUser.ClientPos]);

            //		ShowCountDown ();

            if (isHost && State == TableState.Waiting)
                View.SetEnableBtnStartGame(true);

            HandleNextWorkInQueue();
        }
        void RspStartGame(GamePacket _param)
        {
            isRaised = false;
            CountDown.Stop();
            CurrentHandCardsCount = 0;

            State = TableState.Started;

            View.HideTableState();

            int[] ids = CastInt(_param.GetString(ParamKey.USER_LIST).Split('#'));

            int[] idsJoinGame = new int[ids.Length];
            //			Debug.Log ("Length users: " + ids.Length);

            for (int j = 0; j < ids.Length; j++)
            {
                if (!dictIdPos.ContainsKey(ids[j]))
                    continue;
                int pos = dictIdPos[ids[j]];

                InfoAll[pos].IsJoined = true;
                InfoAll[pos].Chip -= CASH_IN;
                InfoAllView[pos].UpdateChip(InfoAll[pos].Chip);
                Hashtable uVariable = GetUserVariableById(InfoAll[pos].IdUser);
                InfoAllView[pos].ShowPlayer(InfoAll[pos], uVariable);
                //InfoAllView[pos].ShowPlayer(InfoAll[pos]);
                idsJoinGame[j] = pos;

                HandCardsAll[pos] = new int[MAX_HANDCARDS];


                if (pos == 0)
                {

                    string cardList = _param.GetString(ParamKey.CARD_LIST);
                    int[] handCards = CastInt(cardList.Split(','));
                    for (int i = 0; i < handCards.Length; i++)
                    {
                        HandCardsAll[0][i] = handCards[i];
                    }
                }
                
                //				Debug.Log ("Pos in game: " + pos);
                InfoAll[pos].IsReady = true;
            }



            BetCurrent = 0;
            BetTotal = CASH_IN * ids.Length;

            View.ShowBetGameCurrent(BetTotal, BetCurrent);
            int auid = _param.GetInt(ParamKey.USER_FIRST_RAISE);
            DealCard(idsJoinGame, auid);

            View.SetEnableBtnStartGame(false);
            IsFirstRaise = true;
            ShowBtnActions(false);
        }
        void ShowThreeCardUser(int userId)
        {
            if (!dictIdPos.ContainsKey(userId))
                return;
            int clientPos = dictIdPos[userId];

            if (clientPos == 0)
            {
                CardView.SetEnablePickCard(false);

                int card1 = HandCardsAll[0][0];
                int card2 = HandCardsAll[0][1];
                int card3 = HandCardsAll[0][2];

                
                CardView.ShowHandCardPlayer(clientPos, 0, HandCardsAll[0][0]);
                CardView.ShowHandCardPlayer(clientPos, 1, HandCardsAll[0][1]);
                CardView.ShowHandCardPlayer(clientPos, 2, HandCardsAll[0][2]);


                //CardView.ShowDarkLightCard(clientPos, 0, true);

                //CardView.SetEnableBackCard(clientPos, 0, false);
                //CardView.SetEnableBackCard(clientPos, 1, false);
                //CardView.SetEnableBackCard(clientPos, 2, false);
            }
            
        }
        #region Response Action

        void RspPickCard(GamePacket param)
        {
            int userId = param.GetInt(ParamKey.USER_ID);
            int cardId = param.GetInt(ParamKey.CARD_ID);

            if (!dictIdPos.ContainsKey(userId))
                return;
            int clientPos = dictIdPos[userId];

            if (clientPos == 0)
            {
                CardView.SetEnablePickCard(false);

                int card1 = HandCardsAll[0][0];
                int card2 = HandCardsAll[0][1];

                if (cardId == card1)
                {
                    HandCardsAll[0][0] = card2;
                    HandCardsAll[0][1] = card1;
                }
                CardView.ShowHandCardPlayer(clientPos, 0, HandCardsAll[0][0]);
                CardView.ShowHandCardPlayer(clientPos, 1, HandCardsAll[0][1]);


                CardView.ShowDarkLightCard(clientPos, 0, true);

                CardView.SetEnableBackCard(clientPos, 0, false);
                CardView.SetEnableBackCard(clientPos, 1, false);
            }
            else
            {
                CardView.SetEnableBackCard(clientPos, 1, false);
                HandCardsAll[clientPos][1] = cardId;

                CardView.ShowHandCardPlayer(clientPos, 1, cardId);
            }
            HandleNextWorkInQueue();
        }
        void RspDealACard(GamePacket _param)
        {
            isRaised = false;

            HideActionAll();

            CurrentHandCardsCount++;

            BetCurrent = 0;

            string ul = _param.GetString(ParamKey.USER_LIST);
            string cl = _param.GetString(ParamKey.CARD_LIST);
            int auid = _param.GetInt(ParamKey.USER_FIRST_RAISE);

            int[] ids = CastInt(ul.Split('#'));
            int[] cards = CastInt(cl.Split('#'));

            int[] posClients = new int[ids.Length];
            for (int i = 0; i < ids.Length; i++)
            {

                if (!dictIdPos.ContainsKey(ids[i]))
                    continue;

                posClients[i] = dictIdPos[ids[i]];

                HandCardsAll[posClients[i]][CurrentHandCardsCount - 1] = cards[i];
            }
            CardView.StartCoroutine(CardView.DealACard(posClients, CurrentHandCardsCount,
                () => {
                    DealACardComplete(posClients, auid);

                    ShowBtnActions(auid == MyInfo.SFS_ID);
                }));

            View.ShowBetGameCurrent(BetTotal, BetCurrent);
        }
        void RspDealFinalCard(GamePacket _param)
        {
            isRaised = false;

            HideActionAll();

            string users = _param.GetString(ParamKey.USER_LIST);
            int userFirstRaise = _param.GetInt(ParamKey.USER_FIRST_RAISE);

            CurrentHandCardsCount = 5;

            BetCurrent = 0;

            int[] ids = CastInt(users.Split('#'));

            int[] posClients = new int[ids.Length];

            for (int i = 0; i < ids.Length; i++)
            {
                if (!dictIdPos.ContainsKey(ids[i]))
                {
                    continue;
                }
                posClients[i] = dictIdPos[ids[i]];
            }

            bool isJoinGame = _param.param.ContainsKey(ParamKey.CARD_ID);
            if (isJoinGame)
            {
                int card = _param.GetInt(ParamKey.CARD_ID);
                HandCardsAll[0][CurrentHandCardsCount - 1] = card;
            }

            CardView.StartCoroutine(CardView.DealACard(posClients, CurrentHandCardsCount,
                () =>
                {
                    DealACardComplete(posClients, -1, true);
                    if (userFirstRaise != -1)
                    {
                        ShowBtnActions(userFirstRaise == MyInfo.SFS_ID);
                        ShowCountDown(dictIdPos[userFirstRaise]);
                    }
                    if (isJoinGame)
                    {
                        CardView.handleDragOpenCard(0, CurrentHandCardsCount - 1, true);
                    }

                }));

            View.ShowBetGameCurrent(BetTotal, BetCurrent);
        }

        void DealACardComplete(int[] _poss, int _idNextTurn = -1, bool isFinalCard = false)
        {
            for (int i = 0; i < _poss.Length; i++)
            {

                int pos = _poss[i];
                if (HandCardsAll[pos] != null)
                {
                    //				Debug.Log ("Hide CardDeal pos: " + pos);
                    int index = CurrentHandCardsCount - 1;
                    CardView.ShowHandCardPlayer(pos, index, HandCardsAll[pos][index]);

                    if (State == TableState.Started)
                        CardView.SetEnableBackCard(pos, index, (index == 4 && pos != 0) || isFinalCard == true);

                    CardView.SetEnableCardDeal(pos, false);
                }
                else
                    Debug.Log("Player NULL ___ " + pos);
            }

            if (_idNextTurn != -1)
                ShowCountDown(dictIdPos[_idNextTurn]);

            TurnRound += 1;
            IsFirstRaise = true;

            HandleNextWorkInQueue();
        }


        void RspFold(GamePacket _param)
        {
            int uid = _param.GetInt(ParamKey.USER_ID);
            int auid = _param.GetInt(ParamKey.USER_FIRST_RAISE);

            View.HideRaiseParent();


            if (!dictIdPos.ContainsKey(uid))
                return;


            int posFold = dictIdPos[uid];

            InfoAll[posFold].IsReady = false;

            HideCountDown(posFold);

            if (auid != -1)
            {
                ShowBtnActions(auid == MyInfo.SFS_ID, false, !isRaised);
                ShowCountDown(dictIdPos[auid]);
            }

            HandCardsAll[posFold] = null;
            InfoAllView[posFold].ShowActionLieng(XTAction.Fold);
            CardView.SetEnableBackCard(posFold, true);

            HandleNextWorkInQueue();
        }
        void RspCheck(GamePacket _param)
        {
            int uid = _param.GetInt(ParamKey.USER_ID);
            int auid = _param.GetInt(ParamKey.USER_FIRST_RAISE);

            if (dictIdPos.ContainsKey(uid))
            {
                int posCheck = dictIdPos[uid];

                HideCountDown(posCheck);
                InfoAllView[posCheck].ShowActionLieng(XTAction.Check);
            }

            if (dictIdPos.ContainsKey(auid))
            {
                int posRaise = dictIdPos[auid];
                ShowCountDown(posRaise);
            }


            ShowBtnActions(auid == MyInfo.SFS_ID, false, true);
            View.HideRaiseParent();//edit khuong


            HandleNextWorkInQueue();
        }
        void RspRaise(GamePacket _param)
        {

            int uid = _param.GetInt(ParamKey.USER_ID);
            int auid = _param.GetInt(ParamKey.USER_FIRST_RAISE);
            int action = _param.GetInt(ParamKey.ACTION_RAISE_XT);
            long bet = _param.GetLong(ParamKey.BET_MONEY);
            long betTotal = _param.GetLong(ParamKey.BET_MONEY_TOTAL);

            BetCurrent = bet;
            BetTotal = betTotal;

            View.ShowBetGameCurrent(BetTotal, BetCurrent);

            if (dictIdPos.ContainsKey(uid))
            {
                Debug.LogError("KHUONGTEST==============uid:=====" + uid);
                int pos = dictIdPos[uid];

                if (pos == 0)
                    IsFirstRaise = false;

                InfoAll[pos].Chip -= BetCurrent;


                if (action == 1)
                {
                    if (isRaised)
                    {

                        if (InfoAll[pos].Chip == 0)
                        {
                            PlaySoundAllMoney();
                        }
                        else
                        {
                            PlaySoundCall();
                        }
                        InfoAllView[pos].ShowActionLieng(XTAction.Call);
                        //InfoAllView[pos].ShowActionLieng(XTAction.Raise);
                    }
                    else
                    {

                        if (InfoAll[pos].Chip == 0)
                        {
                            PlaySoundAllMoney();
                        }
                        else
                            PlaySoundRaise();
                        InfoAllView[pos].ShowActionLieng(XTAction.Raise);
                    }
                }
                else
                {
                    XTAction _action = (XTAction)action;
                    InfoAllView[pos].ShowActionLieng(_action);

                    if (InfoAll[pos].Chip == 0)
                    {
                        PlaySoundAllMoney();
                    }
                    else
                    {
                        switch (_action)
                        {
                            case XTAction.RaiseAll:
                                PlaySoundAllIn();
                                break;
                            case XTAction.RaiseMulti2:
                                PlaySoundRaiseDouble();
                                break;
                            case XTAction.RaisePart2:
                                PlaySoundRaisePart2();
                                break;
                            case XTAction.RaisePart4:
                                PlaySoundRaisePart4();
                                break;
                        }
                    }
                }


                Debug.LogError("LogChipAfterBet=====KHUONGTEST==========" + InfoAll[pos].Chip);
                InfoAllView[pos].UpdateChip(InfoAll[pos].Chip);

                HideCountDown(pos);


            }

            isRaised = true;

            if (auid != -1)
            {
                ShowBtnActions(auid == MyInfo.SFS_ID, false);
                //ShowBtnActions(auid == MyInfo.SFS_ID, true);

                ShowCountDown(dictIdPos[auid]);
            }
            else
                ShowBtnActions(false, false);

            View.HideRaiseParent();

            HandleNextWorkInQueue();
        }


        #endregion

        void HideActionAll()
        {
            for (int i = 0; i < MAX_USER; i++)
                InfoAllView[i].HideAction();
        }
        int idAnother;
        void PushFinishGame(GamePacket _param)
        {
            View.SetEnableBtnActions(false);
            CardView.SetEnablePickCard(false);
            CardView.SetEnableCardDeal(false);

            HideActionAll();

            string ul = _param.GetString(ParamKey.USER_LIST);   //listUser van con choi

            int[] ids = CastInt(ul.Split('#'));


            string[] result = _param.GetString(ParamKey.RESULT_GAME).Split('#');
            int[] idAll = CastInt(result[0].Split('$'));
            long[] chipAll = CastLong(result[1].Split('$'));
            long[] chipAllFinal = CastLong(result[2].Split('$'));

            //Chi co 1 nguoi thang
            if (ids.Length == 1)
            {
                InfoAllView[0].OnCompleteCountDown();
                ShowResultState(idAll, chipAll, chipAllFinal);

                Wait(true, OnFinishResult, 5);
            }
            else
            {
                //Co >=2 nguoi => so bai
                string cl = _param.GetString(ParamKey.CARD_LIST);   //Lat bai cac listUser tren
                string[] cards = cl.Split('#');
                string[] cardTypeUser = _param.GetString("usercardtype").Split('$');
                
                ShowResultState(idAll, chipAll, chipAllFinal);

                int[] posClients = new int[ids.Length];
                for (int i = 0; i < ids.Length; i++)
                {
                    if (!dictIdPos.ContainsKey(ids[i]))
                        continue;
                    int pos = dictIdPos[ids[i]];

                    InfoAllView[pos].OnCompleteCountDown();

                    posClients[i] = pos;

                    for (int k = 0; k < MAX_HANDCARDS; k++)
                    {
                        HandCardsAll[pos][k] = int.Parse(cards[i].Split('$')[k]);
                    }
                    CardView.ShowHandCardsPlayer(HandCardsAll[pos], pos);
                    CardView.SetEnableBackCard(pos, false);
                    CardView.SetEnableBackCard(pos, false);

                    //show điểm của mỗi người chơi ở đây
                    //InfoAllView[pos].ShowResultPointLieng("Test thoi");
                    for(int index=0;index< cardTypeUser.Length; index++)
                    {
                        string[] objUserCardType = cardTypeUser[i].Split('#');
                        if(int.Parse(objUserCardType[0])== ids[i])
                        {
                            string point = "";
                            switch (objUserCardType[1])
                            {
                                case "0":
                                case "1":
                                case "2":
                                case "3":
                                case "4":
                                case "5":
                                case "6":
                                case "7":
                                case "8":
                                case "9":
                                    point = "ĐIỂM";
                                    break;
                            }
                            if (point == "")
                            {
                                InfoAllView[pos].ShowResultPointLieng(objUserCardType[1]);
                            }
                            else
                            {
                                InfoAllView[pos].ShowResultPointLieng(objUserCardType[1] + " " + point);
                            }
                            
                            break;
                        }
                    }


                    //string s = "Pos " + pos + ": ";
                    //for (int j = 0; j < MAX_HANDCARDS; j++)
                    //{
                    //    s += MBGameHelper.GoodID(HandCardsAll[pos][j]) + " _";
                    //}
                    //Debug.Log("HandCard pos " + pos + " - " + s);

                    //XTType type = XTUtils.getCardsType(GetHandCards(HandCardsAll[pos]));

                    //InfoAllView[pos].ShowValueHandCards(type);



                    //bool[] indexTrue = XTUtils.GetIndexSuccess(type, GetHandCards(HandCardsAll[pos]));
                    //CardView.ShowDarkLightCard(pos, indexTrue);

                    //string s22 = "POS " + pos.ToString() + " TYPE - " + type.ToString() + " ___ ";
                    //for (int n = 0; n < indexTrue.Length; n++)
                    //{
                    //    if (indexTrue[n])
                    //        s22 += " - " + n;
                    //}
                    //Debug.Log(s22);
                }

                Wait(true, OnFinishResult, 10);
            }

            State = TableState.Result;



        }
        void ShowResultState(int[] _ids, long[] _chips, long[] _chipsFinal)
        {
            for (int i = 0; i < _ids.Length; i++)
            {
                if (!dictIdPos.ContainsKey(_ids[i]))
                    continue;
                int pos = dictIdPos[_ids[i]];
                bool isWin = _chips[i] >= 0;

                InfoAllView[pos].ShowChipResult(isWin, _chips[i]);
                InfoAllView[pos].SetEnableWin(isWin);
                InfoAllView[pos].SetEnableLose(!isWin);

                if (pos == 0)
                {

                    PlaySound(isWin ? SoundManager.WIN : SoundManager.LOSE);
                }
                //				Debug.Log ("=== Pos " + pos + " - " + InfoAll [pos].Gold + " + " + _chips [i]);
                //				if (_chips [i] > 0)
                if (InfoAll[pos].IsJoined)
                {
                    InfoAll[pos].Chip = _chipsFinal[i];

                    InfoAllView[pos].UpdateChip(InfoAll[pos].Chip);
                }

                //				Debug.Log ("=== POS " + pos + " - " + InfoAll [pos].Gold);
            }
            MyInfo.CHIP = InfoAll[0].Chip;
        }

        void OnFinishResult()
        {
            State = TableState.Waiting;

            if (isKick)
            {
                RspKickUser(packetKick);
                isKick = false;
            }

            int count = 0;
            for (int i = 0; i < MAX_USER; i++)
            {
                if (i != 0)
                {
                    if (InfoAll[i] == null)
                    {
                        //						Debug.Log ("Info == NULL - " + i);
                    }
                    else
                    {
                        //						Debug.Log ("Info != NULL - " + i);

                        InfoAll[i].IsReady = false;

                        if (!InfoAll[i].IsJoined)
                        {

                            //							Debug.Log ("NOT JOINED! " + i);
                            //						if (!InfoAll [i].IsReady) {
                            InfoAll[i].IsJoined = false;
                            HandCardsAll[i] = null;

                            InfoAllView[i].HidePlayer();

                            //							count++;
                            //						}
                        }
                        else
                            count++;
                    }
                }
                CardView.SetEnableHandCards(i, false);

                InfoAllView[i].HideResultPointLieng();
                InfoAllView[i].SetEnableWin(false);
                InfoAllView[i].SetEnableLose(false);

                CardView.ShowDarkLightCard(i, new bool[] { true, true, true});
            }

            //		if (count < MAX_USER - 1)
            if (count > 0)
                CountDown.Show();

            View.SetEnableBtnStartGame(isHost);
            View.HideBetGameCurrent();

            HandleNextWorkInQueue();
        }

        #endregion


        #region DEAL CARD

        void DealCard(int[] _poss, int auid=-1)
        {
            CurrentHandCardsCount = 3;

            //		CardView.SetEnableCardDeal (true);
            CardView.StartCoroutine(CardView.DealCard(_poss, DealCardComplete, auid));
        }
        void DealCardComplete(int auid)
        {
            //CountDownPickCard.Show(TIME_COUNT_DOWN);

            CardView.SetEnableCardDeal(false);
            CardView.SetEnableHandCards(0, false);

            for (int i = 0; i < InfoAll.Length; i++)
            {

                if (HandCardsAll[i] != null)
                {
                    if (InfoAll[i].IsReady)
                    {
                        int clientPos = InfoAll[i].ClientPos;

                        for (int j = 0; j < 3; j++)
                        {
                            if (clientPos == 0)
                            {
                                CardView.SetEnableBackCard(clientPos, j, false);
                            }
                            else
                            {
                                CardView.SetEnableBackCard(clientPos, j, true);
                            }                            
                            CardView.ShowHandCardPlayer(clientPos, j, HandCardsAll[clientPos][j]);
                        }
                    }
                }
            }
            ShowThreeCardUser(MyInfo.SFS_ID);

            if (auid != -1)
            {
                ShowBtnActions(auid == MyInfo.SFS_ID, true);
                ShowCountDown(dictIdPos[auid]);
            }

            TurnRound = 2;
            IsFirstRaise = true;

            HandleNextWorkInQueue();
        }

        #endregion

        #region EXIT

        public void ExitOnClick()
        {
            PlaySound();
            if (State == TableState.Started && InfoAll[0].IsReady)
            {
                popupFull.Show(ConstText.NotifyExitXT, popupFull.Hide, RqExitRoom);
            }
            else
            {
                LoadingManager.Instance.ENABLE = true;
                sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
                //RqExitRoom();
            }
        }
        void RqExitRoom()
        {
            sfs.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));
            LoadingManager.Instance.ENABLE = true;
        }
        void RspExitGame(GamePacket _param)
        {

            int hostID = _param.GetInt(ParamKey.HOST_ID);
            isHost = hostID == MyInfo.SFS_ID;

            if (State == TableState.Waiting)
            {

                View.SetEnableBtnStartGame(isHost);
            }
            int userId = _param.GetInt(ParamKey.USER_ID);
            if (dictIdPos.ContainsKey(userId))
            {
                int pos = dictIdPos[userId];
                PlayerExit(pos);
            }
            else
            {
                PlayerExit(0);
            }

            HandleNextWorkInQueue();
            View.UpdateHostName(_param.GetString(ParamKey.HOST_NAME));
            for (int i = 1; i < MAX_USER; i++)
                if (InfoAll[i].IsJoined)
                    return;

            CountDown.Stop();


        }
        void PlayerExit(int _position)
        {
            if (_position == 0)
            {
                sfs.RequestJoinGameLobbyRoom((int)GameHelper.currentGid);
            }
            else
            {
                InfoAll[_position].IsReady = false;
                InfoAll[_position].IsJoined = false;

                if (State != TableState.Result)
                {
                    if (State == TableState.Started && InfoAll[_position].IsReady)
                        InfoAllView[_position].ShowActionLieng(XTAction.Fold);

                    InfoAll[_position].IsJoined = false;
                    HandCardsAll[_position] = null;

                    InfoAllView[_position].HidePlayer();
                    View.HideRaiseParent();

                    CardView.SetEnableBackCard(_position, true);
                }
            }
        }

        #endregion

        void ShowBtnActions(bool _enable, bool _firstRaise = true, bool _secondRaise = false)
        {
            //		Debug.Log ("Action - " + _enable + " - " + _firstRaise);

            View.SetEnableBtnActions(_enable);

            if (_enable)
            {
                View.SetActiveBtnCall(!_firstRaise && !_secondRaise);// Btncall là theo
                View.SetActiveBtnCheck(_firstRaise || _secondRaise, _firstRaise && !_secondRaise);//btnCheck là nhuong to
                View.SetActiveBtnRaise(_enable, _firstRaise || _secondRaise);
                View.SetActiveBtnPart4(!_firstRaise && !_secondRaise || TurnRound > 3);


                //View.SetActiveRaiseUp(IsFirstRaise);
                View.SetActiveRaiseUp(true);
            }
        }

        /// <summary>
        /// Shows the value hand cards.
        /// </summary>
        /// <param name="_position">Position.</param>
        /// <param name="_status">-1 : LUNG ___ 0 : Normal ___ 1 : WIN</param>
        public void ShowValueHandCards(int _position, int _status)
        {

            CardView.SetEnablePickCard(false);
            CardView.SetEnableHandCards(_position, true);
            CardView.ShowHandCardsPlayer(HandCardsAll[_position], _position);

        }
        public void ShowPickCards()
        {
            CardView.SetEnablePickCard(true);
            CardView.SetEnableHandCards(0, false);

            CardView.ShowPickCards(HandCardsAll[0]);
        }

        #region PICK CARDS

        public void PickCard(int _index)
        {
            RqPickCard(HandCardsAll[0][_index]);
        }

        #endregion

        #region PRIVATE - Players Info

        void AddPlayer(int _position, BaseUserInfo _user)
        {
            InfoAll[_position] = _user;
            InfoAll[_position].IsJoined = true;
            Hashtable uVariable = GetUserVariableById(_user.IdUser);
            InfoAllView[_position].ShowPlayer(_user, uVariable);
            //InfoAllView[_position].ShowPlayer(_user);
            HandCardsAll[_position] = new int[MAX_HANDCARDS];
        }
        void RemovePlayer(int _position)
        {
            InfoAll[_position].IsJoined = false;
            HandCardsAll[_position] = null;
            InfoAllView[_position].HidePlayer();
        }
        void RemoveAllPlayer()
        {
            for (int i = 0; i < MAX_USER; i++)
            {
                InfoAll[i].IsJoined = false;
                InfoAllView[i].HidePlayer();
                HandCardsAll[i] = null;
            }
        }

        #endregion

        #region COUNTDOWN
        int TIME_COUNT_DOWN = 10;
        void ShowCountDown(int _pos)
        {
            InfoAllView[_pos].ShowCountDown(TIME_COUNT_DOWN);
        }
        void HideCountDown(int _pos)
        {
            InfoAllView[_pos].OnCompleteCountDown();
        }

        #endregion

        #region Utils

        int[] CastInt(string[] _arr)
        {
            try
            {
                return Array.ConvertAll<string, int>(_arr, int.Parse);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        long[] CastLong(string[] _arr)
        {
            try
            {
                return Array.ConvertAll<string, long>(_arr, long.Parse);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        void ExchangeCardUser(int _indexFirst, int _indexSecond)
        {
            int card1 = HandCardsAll[0][_indexFirst];

            HandCardsAll[0][_indexFirst] = HandCardsAll[0][_indexSecond];
            HandCardsAll[0][_indexSecond] = card1;

            BaseCardInfo mbCard1 = BaseCardInfo.Get(HandCardsAll[0][_indexSecond]);
            BaseCardInfo mbCard2 = BaseCardInfo.Get(HandCardsAll[0][_indexFirst]);

            Debug.Log("Card1: " + mbCard2.Value + " - " + mbCard2.Type);
            Debug.Log("Card2: " + mbCard1.Value + " - " + mbCard1.Type);

        }
        /// <summary>
        /// Phai cap nhat posUserSV truoc
        ///   - \n  - input _posSV de cap nhat _posClient
        /// </summary>
        BaseUserInfo CastUserInfoRsp(string _param, int _posSV)
        {
            BaseUserInfo info = new BaseUserInfo();
            Debug.Log("Chuoi===  " + _param);
            string[] infoUser = _param.Split('#');

            info.IdSFS = int.Parse(infoUser[0]);
            info.IdUser = infoUser[1];
            info.Name = infoUser[2];
            info.Chip = long.Parse(infoUser[3]);
            info.Avatar = infoUser[4];
            info.ClientPos = PosClient(_posSV);
            //	if (info.ClientPos==0)
            //		info.VipPoint = int.Parse (infoUser[6]);


            info.Vip_collec = infoUser[6];
            info.Level = int.Parse(infoUser[7]);

            info.Bor_Avatar = infoUser[8];

            info.logoBangUrl = infoUser[10];

            //   info.Gem = long.Parse(infoUser[9]);

            //Debug.Log ("VIP: " + info.VipPoint);
            return info;
        }
        BaseUserInfo CastUserInfoPush(string _param, int _posSV)
        {
            BaseUserInfo info = new BaseUserInfo();

            string[] infoUser = _param.Split('#');

            info.IdSFS = int.Parse(infoUser[0]);
            info.IdUser = infoUser[1];
            info.Name = infoUser[2];
            info.Chip = long.Parse(infoUser[3]);
            info.Avatar = infoUser[4];

            info.ClientPos = PosClient(_posSV);

            //	info.VipPoint = int.Parse (infoUser[5]);



            info.Vip_collec = infoUser[5];
            info.Level = int.Parse(infoUser[6]);
            info.Bor_Avatar = infoUser[7];

            info.logoBangUrl = infoUser[9];
            //    info.Gem = long.Parse(infoUser[8]);


            //			Debug.Log ("VIP: " + info.VipPoint);

            return info;
        }

        /// <summary>
        /// Phai co UserPosSV truoc
        /// </summary>
        int PosClient(int _posSV)
        {
            return (_posSV + MAX_USER - UserPosSV) % MAX_USER;
        }
        List<BaseCardInfo> GetHandCards(int[] _handCards)
        {
            List<BaseCardInfo> result = new List<BaseCardInfo>();
            for (int i = 0; i < MAX_HANDCARDS; i++)
            {
                result.Add(BaseCardInfo.Get(_handCards[i]));
            }

            return result;
        }

        #endregion
        onCallBack onFinishPhase;
        void Wait(bool _isDelay, onCallBack _onFinish = null, float _time = 3)
        {
            StartCoroutine(ActionDelay(_isDelay, _onFinish, _time));
        }
        IEnumerator ActionDelay(bool _isDelay, onCallBack _onFinish = null, float _time = 3)
        {
            if (!_isDelay)
                _time = 0;
            onFinishPhase = _onFinish;
            yield return new WaitForSeconds(_time);
            if (onFinishPhase != null)
                onFinishPhase();
        }

        int Card(int _newID)
        {
            int rank = _newID / 10;
            int suite = _newID % 10;

            int id = suite * 13 + (rank % 14) - 1;

            return id;
        }
        void LogValue(List<BaseCardInfo> _cards)
        {
            string s = "Value : " + MBUtils.getTypeRow(_cards).ToString();
            for (int i = 0; i < _cards.Count; i++)
            {
                s += " - " + _cards[i].Id;
            }

            Debug.Log(s);
        }
        public void OnConnectionLost()
        {
            popupAlert.Show(ConstText.Notify, ConstText.ErrorConnection, () => {
                GameHelper.ChangeScene(GameScene.LoginScene);
            });
        }
    }
}