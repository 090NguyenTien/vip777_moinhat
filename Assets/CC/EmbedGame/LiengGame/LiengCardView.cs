﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System;
using ControllerXT;
using ConstXT;
using BaseCallBack;

namespace ViewXT
{

    public class LiengCardView : MonoBehaviour
    {
        LiengInGameController Controller;

        public delegate void CallBack();
        CallBack dealOneCardsDone, resultRow;
        onCallBackInt dealCardsDone;
        //[SerializeField]
        //private GameObject BigCard;
        //[SerializeField]
        //private List<XTPickCardItem> lstPickCards;
        [SerializeField]
        List<GameObject> lstBackCards;  //Bai up khi dealcards
        [SerializeField]
        private List<XTCardItem> lstCardUser,
        lstCardP1, lstCardP2, lstCardP3, lstCardP4, lstCardP5;

        //PICK CARDS
        [SerializeField]
        private Text txtValueRow1, txtValueRow2, txtValueRow3,
        txtRowsSpecial, //3 thung, 3 sanh
        txtBigWin,  //sanh rong, luc phe bon, ...
        txtFail;    //Binh lung


        [SerializeField]
        private Sprite[] arrSprSmallSuite,
        arrSprBigSuite,
        arrSprValue,
        arrSprJQK;  // 11_0 12_0 13_0 ... 11_3 12_3 13_3


        List<RectTransform> lstRectTrsfPickCards;   //Tham chieu toi RectTrsf BigCards
        List<Vector2> lstRectTrsfLocalBigCards; //Giu lai localPosition dau tien

        Dictionary<int, List<XTCardItem>> dictPosHandCards; //<posClient, cards>

        const float TIME_MOTION = .3f;
        const float ALPHA_CARD = .5f;


        /// <summary>
        /// Khoi tao duy nhat 1 lan khi Scene Active
        /// </summary>
        public void Init(LiengInGameController _controller)
        {
            Controller = _controller;

            dictPosHandCards = new Dictionary<int, List<XTCardItem>>();
            dictPosHandCards[0] = lstCardUser;
            dictPosHandCards[1] = lstCardP1;
            dictPosHandCards[2] = lstCardP2;
            dictPosHandCards[3] = lstCardP3;
            dictPosHandCards[4] = lstCardP4;
            dictPosHandCards[5] = lstCardP5;


            lstRectTrsfPickCards = new List<RectTransform>();
            lstRectTrsfLocalBigCards = new List<Vector2>();

            //for (int i = 0; i < lstPickCards.Count; i++)
            //{
            //    lstPickCards[i].Init(i);
            //    lstRectTrsfPickCards.Add(lstPickCards[i].GetComponent<RectTransform>());
            //    lstRectTrsfLocalBigCards.Add((Vector2)lstRectTrsfPickCards[i].localPosition);
            //}

        }

        #region PICK CARD

        public void ShowPickCard(int _idCard1, int _idCard2)
        {
            //ShowCard(lstPickCards[_idCard1], _idCard2);
        }
        /// <summary>
        /// Hien thi 2 la bai lon cua User 
        /// </summary>
        public void ShowPickCards(int[] _handCards)
        {
            //_handCard length = 5, nen chi chay 2 phan tu dau
            //for (int i = 0; i < 2; i++)
            //{
            //    //				Debug.Log ("PickCard: " + MBGameHelper.GoodID (_handCards [i]));
            //    ShowCard(lstPickCards[i], _handCards[i]);
            //}
        }
        public void SetEnablePickCard(bool _enable)
        {
            //BigCard.SetActive(_enable);
        }

        public void ChangeBigCards(int _indexFirst, int _indexSecond)
        {
            //Bay tu vi tri la bai thu 2 || Bay tu vi tri hien tai
            //		lstRectTrsfBigCards [_indexFirst].localPosition = lstRectTrsfLocalBigCards [_indexSecond];
            lstRectTrsfPickCards[_indexFirst].transform.parent.SetAsFirstSibling();
            lstRectTrsfPickCards[_indexFirst].SetAsFirstSibling();

            lstRectTrsfPickCards[_indexSecond].localPosition = lstRectTrsfPickCards[_indexFirst].localPosition;
            lstRectTrsfPickCards[_indexSecond].DOLocalMove(lstRectTrsfLocalBigCards[_indexSecond], TIME_MOTION);

            lstRectTrsfPickCards[_indexFirst].localPosition = lstRectTrsfLocalBigCards[_indexSecond];
            lstRectTrsfPickCards[_indexFirst].DOLocalMove(lstRectTrsfLocalBigCards[_indexFirst], TIME_MOTION);
        }

        #region Value Rows - Big Cards

        public void SetEnableValueRows(bool _enable)
        {
            txtValueRow1.gameObject.SetActive(_enable);
            txtValueRow2.gameObject.SetActive(_enable);
            txtValueRow3.gameObject.SetActive(_enable);
        }
        public void ShowValueBigCards(MBType _row1, MBType _row2, MBType _row3)
        {
            txtValueRow1.text = _row1.ToString();
            txtValueRow2.text = _row2.ToString();
            txtValueRow3.text = _row3.ToString();
        }

        #endregion

        #region Rows Special - 3 thung, 3 sanh

        public void SetEnableRowsSpecial(bool _enable)
        {
            txtRowsSpecial.gameObject.SetActive(_enable);
        }
        public void ShowRowsSpecial(MBTypeImmediate _type)
        {
            txtRowsSpecial.text = _type.ToString();
        }

        #endregion

        #region Win special - Sanh rong, Dong hoa, ...

        public void SetEnableBigWin(bool _enable)
        {
            txtBigWin.gameObject.SetActive(_enable);
        }
        public void ShowBigWin(MBTypeImmediate _type)
        {
            txtBigWin.text = _type.ToString();
        }

        #endregion

        #region Binh lung

        public void SetEnableValueFail(bool _enable)
        {
            txtFail.gameObject.SetActive(_enable);
        }

        #endregion

        #endregion

        #region HandCards

        public void SetEnableHandCards(int _position, bool _enable)
        {
            for (int i = 0; i < 3; i++)
                dictPosHandCards[_position][i].gameObject.SetActive(_enable);
        }

        public void ShowHandCardPlayer(int _position, int _indexCard, int _idCard)
        {
            ShowCard(dictPosHandCards[_position][_indexCard], _idCard);
        }
        public void ShowHandCardsPlayer(int[] _idCards, int _position)
        {
            for (int i = 0; i < _idCards.Length; i++)
            {
                ShowCard(dictPosHandCards[_position][i], _idCards[i]);
            }
        }

        /// <summary>
        /// Hien/An tat ca la bai up cua Players
        /// </summary>
        public void SetEnableBackCard(int _position, bool _enable)
        {
            for (int i = 0; i < 3; i++)
            {
                dictPosHandCards[_position][i].BackCard = _enable;
            }
        }
        /// <summary>
        /// Hien/An la bai up theo chi, cua Players
        /// </summary>
        /// <param name="_position">Position.</param>
        public void SetEnableBackCard(int _position, int _indexCard, bool _enable)
        {
            dictPosHandCards[_position][_indexCard].BackCard = _enable;
        }

        public void ShowAlphaCard(int _position, int _index, bool _isBlur)
        {
            //		Debug.Log(_position + " - " + _index + " _ " + _isBlur);
            dictPosHandCards[_position][_index].GetComponent<CanvasGroup>().alpha =
                _isBlur ? ALPHA_CARD : 1;
        }
        public void ShowDarkLightCard(int _position, int _index, bool _isDark)
        {
            dictPosHandCards[_position][_index].DarkLight = _isDark;
        }

        public void handleDragOpenCard(int _position, int _index, bool _isDark)
        {
            dictPosHandCards[_position][_index].setActiveDrag();
        }

        public void ShowDarkLightCard(int _position, bool[] _indexHighlight)
        {
            for (int i = 0; i < _indexHighlight.Length; i++)
            {
                dictPosHandCards[_position][i].DarkLight = !_indexHighlight[i];
            }
        }
        #region So chi

        /// <summary>
        /// Show chi MauBinh (an trang)
        /// </summary>
        public void ShowValueWinAll(string[] _handCardsPlayers, CallBack _onComplete, float _timeComplete = 0)
        {
            //Duyet xem co player nao co MauBinh hay ko?
            for (int i = 0; i < _handCardsPlayers.Length; i++)
            {
                if (!string.IsNullOrEmpty(_handCardsPlayers[i]))
                {

                    string[] _handCards = _handCardsPlayers[i].Split('#');

                    ShowHandCardsPlayer(ConvertArray(_handCardsPlayers), i);
                }
            }

            StartCoroutine(CallBackThread(_timeComplete, _onComplete));
        }
        /// <summary>
        /// Show 1 chi cua 1 player
        /// </summary>
        public void ShowValueRowAll(int _position, int _row, int[] _handCards)
        {
            if (_handCards == null)
            {
                Debug.Log("HANDCARDS null");
                return;
            }
        }

        IEnumerator CallBackThread(float _timeDelay, CallBack _callBack)
        {
            yield return new WaitForSeconds(_timeDelay);
            _callBack();
        }

        #endregion


        #endregion

        #region Utils

        /// <summary>
        /// Hien thi la bai theo id
        /// </summary>
        private void ShowCard(BaseCardItem _cardItem, int _id)
        {
            _cardItem.gameObject.SetActive(true);
            _cardItem.card = BaseCardInfo.Get(_id);

            if (_cardItem.Value == null)
            {
                _cardItem.Value = _cardItem.transform.Find("Card Value").GetComponent<Image>();
                _cardItem.SmallSuite = _cardItem.transform.Find("Upper Type").GetComponent<Image>();
                _cardItem.BigSuite = _cardItem.transform.Find("Main Type").GetComponent<Image>();
            }

            _cardItem.Value.sprite = arrSprValue[(_cardItem.card.Value - 1) % 13];

            _cardItem.Value.color = (int)_cardItem.card.Type < 2 ? Color.black : Color.red;

            //		Debug.Log ("TYPE _ " + (int)_cardItem.card.Type);
            _cardItem.SmallSuite.sprite = arrSprSmallSuite[(int)_cardItem.card.Type];

            if (_cardItem.card.Value < 11 || _cardItem.card.Value > 13)
            {
                _cardItem.BigSuite.sprite = arrSprBigSuite[(int)_cardItem.card.Type];
            }
            else
            {
                _cardItem.BigSuite.sprite = arrSprJQK[(_id / 13) * 3 + (_cardItem.card.Value % 11)];
            }
        }

        #endregion

        #region DEAL CARD

        public void SetEnableCardDeal(int _index, bool _enable)
        {
            lstBackCards[_index].SetActive(_enable);

            if (_enable)
                lstBackCards[_index].transform.localPosition = Vector3.zero;

        }
        public void SetEnableCardDeal(bool _enable)
        {
            for (int i = 0; i < lstBackCards.Count; i++)
            {
                lstBackCards[i].SetActive(_enable);

                if (_enable)
                    lstBackCards[i].transform.localPosition = Vector3.zero;
            }
        }

        /// <summary>
        /// DealCards
        /// </summary>
        /// <returns>The card.</returns>
        /// <param name="players">vi tri cac player tham gia</param>
        /// <param name="playerCards">ID bai tren tay User</param>
        /// <param name="_onComplete">On complete.</param>
        public IEnumerator DealCard(int[] players, onCallBackInt _onComplete, int auid)
        {
            //Create deal cards animation for all players
            dealCardsDone = _onComplete;

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < players.Length; j++)
                {
                    GameObject temp = lstBackCards[j + i * players.Length];
                    temp.SetActive(true);
                    temp.transform.localPosition = Vector3.zero;
                    temp.transform.DOMove(dictPosHandCards[players[j]][i].transform.position, .4f).SetEase(Ease.Linear);

                    SoundManager.PlaySound(SoundManager.DEAL_ONE_CARD);

                    yield return new WaitForSeconds(0.1f);
                    //					}
                }
            }
            yield return new WaitForSeconds(1f);

            dealCardsDone(auid);
        }

        public IEnumerator DealACard(int[] poss, int _handCardsCount, CallBack _onComplete)
        {
            //Create deal a card animation for all players
            dealOneCardsDone = _onComplete;


            for (int i = 0; i < poss.Length; i++)
            {
                int indexCard = poss[i];

                XTCardItem card = dictPosHandCards[poss[i]][_handCardsCount - 1];
                card.gameObject.SetActive(true);
                card.BackCard = true;
                card.transform.DOMove(Vector3.zero, .4f).From();

                SetEnableCardDeal(indexCard, true);

            }

            yield return new WaitForSeconds(1f);

            dealOneCardsDone();
        }

        #endregion

        int[] ConvertArray(string[] _arr)
        {
            return Array.ConvertAll<string, int>(_arr, int.Parse);
        }
    }

}