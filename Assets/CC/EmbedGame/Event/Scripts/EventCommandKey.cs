﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCommandKey 
{
    public const string JOIN_EVENT_GAME = "jelt";
    public const string END_EVENT = "eev";
    public const string GET_EVENT_TOP_HEAD = "geth";
    public const string GET_EVENT_INFO = "gei";
    public const string GET_EVENT_TIME = "geti";
    public const string JOIN_EVENT_TAIXIU_GAME = "jge";
    public const string END_EVENT_NOTICE = "eevn";
}
