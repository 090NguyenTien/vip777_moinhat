﻿using UnityEngine;
using System.Collections;
using System;
using Sfs2X.Entities.Data;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LuckyControllerEvent : MonoBehaviour {
    //private SFS sfs;
    [SerializeField]
    GameObject Waiting;
    [SerializeField]
    EventController eventController;
    public string gametype = "taixiu";
    private double betMoney;
    public LuckyHistory history;
    public LuckyCup cup;
    public Text timeCountDown;
    public Text waitTimeCountDown;
    public Text chooseBetMoney;
    public Text totalTai;
    public Text totalXiu;
    public Text totalUserTai;
    public Text totalUserXiu;
    public Text totalUserBetTai;
    public Text totalUserBetXiu;
    public Text Alert;
    public Text taiWinMoney;
    public Text xiuWinMoney;
    public Text txtChipUser;
    public Text txtPointUser;
    public Text txtUserName;
    public LuckyDice diceContainer;
    public GameObject waitTimeObj;
    public GameObject historyBar;
    public Sprite blackPoint;
    public Sprite whitePoint;
    private int remainTime;
    private int waitTime;
    private LuckyBet totalBet;
    private LuckyBet userBet;
    private LuckyBet totalUser;
    public GameObject SicEffect;
    public GameObject BoEffect;
    public GameObject SicBorder;
    public GameObject BoBorder;
    public GameObject hand;
    public GameObject maskBtn;
   
    public bool isMask = false;
    public bool isTaiXiu = true;
    public int bettype = -1;
    public Text taibet;
    public Text xiubet;
    public GameObject guide;
    public LuckySoiCau soicau;
	public GameObject betAnimator;
	public Image eye;
	public GameObject cankeo;
    public GameObject timeAnimation;
	
    public AudioSource startSound, stopSound;
    public AudioSource[] diceResultSound;

    bool isEnd;
    bool isAnimationDicing;

    void Start() {
        //sfs = SFS.Instance;
    }
    public void ShowTaiXiuMiniEvent()
    {
        isEnd = false;
        timeCountDown.text = "Chờ...";
        SicBorder.SetActive(false);
        BoBorder.SetActive(false);
        //timeCountDown.gameObject.SetActive(true);
        totalBet = new LuckyBet();
        userBet = new LuckyBet();
        totalUser = new LuckyBet();
        diceContainer.gameObject.SetActive(false);
        bettype = -1;
        taibet.text = "...";
        xiubet.text = "...";
        txtUserName.text = MyInfo.NAME;
        txtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP_EVENT);
        //gameObject.SetActive(false);
        ShowAlert("Chờ Tí Nhé...");
        //RequestJoinTaiXiuMiniRoom();
        GetGameInfo();
    }
    public void RequestJoinTaiXiuMiniRoom()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        GameHelper.currentGid = (int)GAMEID.TaiXiuMini;
        GamePacket gp = new GamePacket(EventCommandKey.JOIN_EVENT_TAIXIU_GAME);
        gp.Put(ParamKey.GAME_ID, GameHelper.currentGid);
        SFS.Instance.SendRoomRequest(gp);
        

        LoadingManager.Instance.ENABLE = true;
    }
    void OnEnable() {
        //sfs = SFS.Instance;
        //GetGameInfo();
        timeCountDown.text = "Chờ...";
        totalBet = new LuckyBet();
        userBet = new LuckyBet();
        totalUser = new LuckyBet();
        diceContainer.gameObject.SetActive(false);
        bettype = -1;
        taibet.text = "...";
    }

    private void ResetInfo()
    {
		Debug.Log ("reset info ne");
        totalBet.Reset();
        userBet.Reset();
        totalUser.Reset();
        ShowBetData();
    }
    private void GetGameInfo()
    {
        //GameController.instance.SendRoomRequest(LuckyMiniCommandKey.GetLuckyGameInfo, null, MyInfo.luckyRoom);
        GamePacket gp = new GamePacket(LuckyMiniCommandKey.GetLuckyGameInfo);
        SFS.Instance.SendRoomRequest(gp);
    }
    public void ChooseType(int type) {
        bettype = type;
        if (bettype == 0)
        {
            taibet.text = "0";
            xiubet.text = "...";
			totalUserBetTai.color = new Color((float)250/255, (float)142 /255, (float)22 /255);
			totalUserBetXiu.color = Color.white;
            SicBorder.SetActive(true);
            BoBorder.SetActive(false);
        }
        else {
            taibet.text = "...";
            xiubet.text = "0";
			totalUserBetTai.color = Color.white;
			totalUserBetXiu.color = new Color((float)250 / 255, (float)142 / 255, (float)22 / 255);
            SicBorder.SetActive(false);
            BoBorder.SetActive(true);
        }
        betAnimator.GetComponent<Animator>().Play("BetHorse", 0, 0.25f);
        //betAnimator.transform.localPosition = new Vector3 (-200, - 172, 0);
        //iTween.MoveTo(betAnimator, iTween.Hash("position", new Vector3(-200, -362, 0), "islocal", true, "onupdatetarget", gameObject, "onupdate", "onUpdateDice", "time", 0.5f));
    }

    public void ChooseMoney(string money) {
		if (bettype == -1) {
			if (gametype == "taixiu") {
				ShowAlert("Bạn chưa chọn Tài hay Xỉu");
			} else {
				ShowAlert("Bạn chưa chọn Chẵn hay Lẻ");
			}

			return;
		}
        if (money.Equals("all"))
        {
            betMoney = MyInfo.CHIP_EVENT;
        } else
        {
            betMoney += Convert.ToDouble(money);
        }
		if (betMoney > MyInfo.CHIP_EVENT) {
			betMoney = MyInfo.CHIP_EVENT;
		}
		if (bettype == 0) {
			taibet.text = betMoney.ToString("N0");
		} else {
			xiubet.text = betMoney.ToString("N0");
		}
        
        
        //chooseBetMoney.text = betMoney.ToString("N0");
		//hand.gameObject.SetActive (false);
    }

	public void Bet(){
		Betting(bettype);
	}

    public void ClearBet()
    {
        betMoney = 0;
        //chooseBetMoney.text = "0";
		if (bettype == 0) {
			taibet.text = "0";
		} else {
			xiubet.text = "0";
		}
        
        //hand.gameObject.SetActive (true);
    }

    public void Betting(int type)
    {
        if (isEnd)
        {
            ShowAlert("Event đã kết thúc bạn không thể cược tiếp!");
            return;
        }
        if (betMoney < 1000)
        {
            ShowAlert("Tối thiểu phải cược 1000 chip");
            return;
        }
        if (waitTime > 0)
        {
            ShowAlert("Chưa bắt đầu ván cược mới");
            return;
        }
        var param = new SFSObject();
        param.PutUtfString(LuckyMiniParamKey.betMoney, type + "#" + betMoney);
       
        GamePacket gp = new GamePacket(LuckyMiniCommandKey.LuckyBetChip);
        gp.param.PutUtfString(LuckyMiniParamKey.betMoney, type + "#" + betMoney);
        SFS.Instance.SendRoomRequest(gp);
		betMoney = 0;
		//chooseBetMoney.text = "0";
		taibet.text = "...";
		xiubet.text = "...";

    }
    public void OnPublicMsg(string _sms)
    {

    }
    public void OnSFSResponse(GamePacket gp) { 

        switch (gp.cmd)
        {            
            case LuckyMiniCommandKey.JOIN_LUCKY_MINi_GAME:
                LoadingManager.Instance.ENABLE = false;
                break;
            case LuckyMiniCommandKey.LuckyBet:
                BetResponse(gp);
                break;
            case LuckyMiniCommandKey.GetLuckyGameInfo:
                GetGameInfo(gp);
                break;
            case LuckyMiniCommandKey.LuckyBetFail:
                LuckyBetFail(gp);
                break;
            case LuckyMiniCommandKey.LuckyResult:
                LuckyResult(gp);
                break;
            case LuckyMiniCommandKey.LuckyStartEvent:
                LuckyStartEvent(gp);
                break;
            case LuckyMiniCommandKey.LuckyResultDetail:
                LuckyResultDetail(gp);
                break;
            case LuckyMiniCommandKey.UpdateBetInfo:
                UpdateBetInfo(gp);
                break;
            case LuckyMiniCommandKey.USER_EXIT:
                //SceneManager.LoadScene(GameScene.HomeSceneV2.ToString());
                EndEvent(false);
                break;
            case LuckyMiniCommandKey.LuckyMaxBetting:
                ShowAlert("Mức cược tối đa cho mỗi bên là 10.000M");
                break;
            case EventCommandKey.END_EVENT:
                EndEvent(true);
                break;
            case EventCommandKey.GET_EVENT_TOP_HEAD:
                eventController.GetEventTopHead(gp);
                break;
            case EventCommandKey.END_EVENT_NOTICE:
                isEnd = true;
                break;
        }
        if (history.gameObject.activeSelf)
        {
            history.onExtensionResponse(gp);
        }
        if (cup.gameObject.activeSelf)
        {
            cup.onExtensionResponse(gp);
        }
        if (soicau.gameObject.activeSelf)
        {
            soicau.onExtensionResponse(gp);
        }
        
    }
    IEnumerator WaitAnimatingEndGame(bool isEndEvent)
    {
        yield return new WaitForSeconds(2);
        EndEvent(isEndEvent);
    }
    private void EndEvent(bool isEndEvent)
    {
        if (isAnimationDicing)
        {
            StartCoroutine(WaitAnimatingEndGame(isEndEvent));
            return;
        }
        ResetInfo();
        gameObject.SetActive(false);
        Waiting.SetActive(true);
        eventController.InitEvent(isEndEvent);       
        
    }

    private void UpdateBetInfo(GamePacket gp)
    {
        //Debug.LogError("UpdateBetInfo " + gp.param.ToJson());
        var taiMoney = 0d;
        var xiuMoney = 0d;

        var taidata = gp.param.GetData(LuckyMiniParamKey.TaiMoney);
        
        if (taidata.Type == (int)SFSDataType.INT) {
            taiMoney = gp.param.GetInt (LuckyMiniParamKey.TaiMoney);
        }else if(taidata.Type == (int)SFSDataType.DOUBLE)
        {
            taiMoney = gp.param.GetDouble(LuckyMiniParamKey.TaiMoney);
        }
        var xiudata = gp.param.GetData(LuckyMiniParamKey.XiuMoney);
        if (xiudata.Type == (int)SFSDataType.INT)
        {
            xiuMoney = gp.param.GetInt(LuckyMiniParamKey.XiuMoney);
        }
        else if (xiudata.Type == (int)SFSDataType.DOUBLE)
        {
            xiuMoney = gp.param.GetDouble(LuckyMiniParamKey.XiuMoney);
        }

        Debug.Log ("taiMoney "+ taiMoney);
        Debug.Log ("xiuMoney "+ xiuMoney);
        var userBetNo = gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalUser).Split('#');
        totalBet.taiChip = taiMoney;
        totalBet.xiuChip = xiuMoney;
        totalUser.setData(userBetNo);
        ShowBetData();
    
    }

    private void GetGameInfo(GamePacket gp)
    {
        var timeType = gp.param.GetInt(LuckyMiniParamKey.timeType);
        var time = gp.param.GetInt(LuckyMiniParamKey.time);
        UpdateCountDownTime(timeType, time);
        var logTX = gp.param.GetUtfString(LuckyMiniParamKey.LogTx);
        ParseTxHistory(logTX);
        if (gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalBet) != null)
        {
            var totalBetMoney = gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalBet).Split('#');
            var totalBetUser = gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalUser).Split('#');
            totalBet.setData(totalBetMoney);
            totalUser.setData(totalBetUser);
            ShowBetData();
        }
        if (timeType == 1) {
            diceContainer.mask.SetActive(false);
        }
        if (resultEffect != null)
            resultEffect.SetActive(false);
    }

    private void ParseTxHistory(string logTX)
    {
        //Debug.LogError("log TX "+ logTX);
        if (logTX == null) {
            return;
        }
		var log = logTX.ToCharArray ();
        //1: xiu; 0:tai
		for (var i = 0; i < log.Length; i++)
        {
            var image = historyBar.transform.GetChild(i).GetComponent<Image>();
			image.sprite = log [i].Equals ('1') ? whitePoint : blackPoint;
        }
    }

    private void LuckyResultDetail(GamePacket gp)
    {
        isAnimationDicing = true;
        StartCoroutine(DelayShowResultDetail(6f, gp));
    }
	private GameObject resultTextAnimation;
    IEnumerator DelayShowResultDetail(float v, GamePacket gp)
    {
        yield return new WaitForSeconds(v);
        var chipResult = gp.param.GetUtfString(LuckyMiniParamKey.ResultChip).Split('#');                
		print("DelayShowResultDetail "+ gp.param.GetUtfString(LuckyMiniParamKey.ResultChip));
        MyInfo.CHIP_EVENT = (long)Convert.ToDouble(chipResult[3]);
        MyInfo.POINT_EVENT = (long)Convert.ToDouble(chipResult[4]);
        txtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP_EVENT);
        txtPointUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.POINT_EVENT);
        //HomeViewV2.api.ShowInfoUser(
        //    MyInfo.CHIP_EVENT.ToString("N0"), "0");
        var taiWin = Convert.ToDouble(chipResult[0]);
		if (taiWin > 0) {
			taiWinMoney.text = "+$" + taiWin.ToString ("N0");
			resultTextAnimation = taiWinMoney.gameObject;
		}

        var xiuWin = Convert.ToDouble(chipResult[1]);
		if (xiuWin > 0) {
			xiuWinMoney.text = "+$" + xiuWin.ToString ("N0");
			resultTextAnimation = xiuWinMoney.gameObject;
		}
		if (taiWin > 0 || xiuWin > 0) {
            //SoundController.instance.CollectCoinSound();
		}
		AnimationWinMoney ();
        isAnimationDicing = false;
    }

	

	void AnimationWinMoney(){
		if (resultTextAnimation != null) {
            resultTextAnimation.transform.localPosition = new Vector3(resultTextAnimation.transform.localPosition.x, -250, 0);
            iTween.MoveTo(resultTextAnimation, iTween.Hash("position", new Vector3(resultTextAnimation.transform.localPosition.x, -100, 0), "islocal", true, "oncompletetarget", gameObject,
                "oncomplete", "onCompleteAnimationWinMoney", "time", 2.5f, "easetype", "easeOutExpo"));

            //resultTextAnimation.transform.localPosition = new Vector3 (0, 62, 0);            
            //iTween. (resultTextAnimation, iTween.Hash ("alpha", 0, "time", 5));
            //iTween.FadeTo (resultTextAnimation, 1, 1);
            //iTween.MoveTo (resultTextAnimation, iTween.Hash ("position", new Vector3 (0, 110, 0), "islocal", true, "oncompletetarget", gameObject,
            //"oncomplete", "onCompleteAnimationWinMoney", "time", 0.5f, "easetype", "easeOutSine"));
        }
	}

	void onCompleteAnimationWinMoney(){
        iTween.MoveTo(resultTextAnimation, iTween.Hash("position", new Vector3(resultTextAnimation.transform.localPosition.x, -50, 0), "islocal", true, "oncompletetarget", gameObject,
            "oncomplete", "onEndingAnimationMoney", "time", 0.5, "easetype", "linear"));

        //iTween.FadeTo(resultTextAnimation, 0, 2);
        //iTween.MoveTo(resultTextAnimation, iTween.Hash("position", new Vector3(0, 150, 0), "islocal", true, "oncompletetarget", gameObject,
        //	"oncomplete", "onEndingAnimationMoney", "time", 1, "easetype", "linear" ));

    }
    
	void onEndingAnimationMoney(){
		taiWinMoney.text = "";
		xiuWinMoney.text = "";
        
    }
    IEnumerator DelayHideWinMoney(float v)
    {
        yield return new WaitForSeconds(v);
        taiWinMoney.text = "";
        xiuWinMoney.text = "";
    }

    private void LuckyBetFail(GamePacket gp)
    {
        var reason = gp.param.GetInt(LuckyMiniParamKey.LuckyBetFailReason);
        if (reason == 1)
			ShowAlert("Not enough money!");
        if (reason == 2)
			ShowAlert("New session not start yet");
        StartCoroutine(DelayHideAlert(3f));
    }

    private void ShowAlert(string msg) {
        Alert.text = msg;
        StartCoroutine(DelayHideAlert(3f));
    }

    private IEnumerator DelayHideAlert(float v)
    {
        yield return new WaitForSeconds(v);
        Alert.text = "";
    }

    private void UpdateCountDownTime(int timeType, int time)
    {
        Debug.LogError("update countdown " + timeType + " : " + time);

        if (timeType == 1)
        {
			ResetInfo();
            waitTimeObj.SetActive(false);
            diceContainer.gameObject.SetActive(false);
            //if (remainTime > 0)
            //    return;
            remainTime = time;
            if (remainTime > 0)
            {
                timeAnimation.SetActive(true);
                timeCountDown.gameObject.SetActive(true);
                timeCountDown.text = formatCountDown(remainTime);
                StartCoroutine(CountDownTime());
            }else
                timeAnimation.SetActive(false);
        }
        else
        {
            timeAnimation.SetActive(false);
            //if (waitTime > 0)
            //    return;
            waitTime = time;
            if (waitTime > 0)
            {
                waitTimeCountDown.text = waitTime.ToString();
                waitTimeObj.SetActive(true);
                StartCoroutine(CountDownWaitTime());
            }
        }
    }

    private void LuckyStartEvent(GamePacket gp)
    {
        //Debug.LogError("lucky start event ne");
        
		if (resultEffect != null)
			resultEffect.SetActive (false);		
        var timeType = gp.param.GetInt(LuckyMiniParamKey.timeType);
		if (timeType == 1) {
			ResetInfo ();
            diceContainer.mask.SetActive(false);
            startSound.Play();
        }
        var time = gp.param.GetInt(LuckyMiniParamKey.time);
        UpdateCountDownTime(timeType, time);
    }
	private string logTX;
	private string[] numdice;
    private void LuckyResult(GamePacket gp)
    {
        isAnimationDicing = true;
        //betAnimator.transform.localPosition = new Vector3 (-200, - 172, 0);
        var numdice = gp.param.GetUtfString(LuckyMiniParamKey.NumDice).Split('#');
        Debug.LogError("LuckyResult "+ numdice);
        timeCountDown.gameObject.SetActive(false);
        diceContainer.ShowDice(Convert.ToInt32(numdice[0]), Convert.ToInt32(numdice[1]), Convert.ToInt32(numdice[2]), isMask);
        var logTX = gp.param.GetUtfString(LuckyMiniParamKey.LogTx);
		this.numdice = numdice;
		this.logTX = logTX;

        if (!isMask)
        {
            StartCoroutine(DelayShowHistory(6f, logTX, numdice));
            
        }
        else
        {
            StartCoroutine(DelayShowHistory(11f, logTX, numdice));
        }
		
    }

	public void ShowHistoryNow(){
		StartCoroutine(DelayShowHistory(0f, logTX, numdice));
	}

	private GameObject resultEffect;
    private IEnumerator DelayShowHistory(float v, string logTX, string[] numdice)
    {
        yield return new WaitForSeconds(v);
        int totalDice = Convert.ToInt32(numdice[0]) + Convert.ToInt32(numdice[1]) + Convert.ToInt32(numdice[2]);
        Debug.Log("totalDice " + totalDice);
        Debug.Log("diceResultSound " + diceResultSound.Length);
        diceResultSound[totalDice - 1].Play();
        //Debug.Log ("logTX "+logTX);
        ParseTxHistory(logTX);
        if (gametype == "taixiu")
        {
			if (Convert.ToInt32 (numdice [0]) + Convert.ToInt32 (numdice [1]) + Convert.ToInt32 (numdice [2]) > 10) {
				resultEffect = SicEffect;
				eye.sprite = blackPoint;
			} else {
				resultEffect = BoEffect;
				eye.sprite = whitePoint;
			}
        }
        else {
            if ((Convert.ToInt32(numdice[0]) + Convert.ToInt32(numdice[1]) + Convert.ToInt32(numdice[2])) % 2 == 0)
                resultEffect = SicEffect;
            else
                resultEffect = BoEffect;
        }
		resultEffect.SetActive (true);
        isAnimationDicing = false;
    }
    
    IEnumerator CountDownWaitTime()
    {
        yield return new WaitForSeconds(1f);
        if (waitTime > 0)
        {
            waitTime -= 1;
            waitTimeCountDown.text = waitTime.ToString();
            StartCoroutine(CountDownWaitTime());
        }
        else {
            diceContainer.gameObject.SetActive(false);
        }
    }

    private void BetResponse(GamePacket gp)
    {
        Debug.LogError("BetResponse ");
        var betMoneytype = gp.param.GetInt(LuckyMiniParamKey.betMoneyType);
		var bettingMoney = gp.param.GetLong(LuckyMiniParamKey.betMoney);
		var betType = gp.param.GetInt(LuckyMiniParamKey.betType);
        var totalBetMoney = gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalBet).Split('#');
        var totalBetUser = gp.param.GetUtfString(LuckyMiniParamKey.luckyTotalUser).Split('#');
        var userBetDetail = gp.param.GetUtfString(LuckyMiniParamKey.luckyUserBet).Split('#');
		var currentChip = gp.param.GetLong (LuckyMiniParamKey.NowMoney);
        totalBet.setData(totalBetMoney);
        totalUser.setData(totalBetUser);
        userBet.setData(userBetDetail);
		Debug.Log (userBet.xiuChip  + " - " + userBet.taiChip);
		MyInfo.CHIP_EVENT = currentChip;
        txtChipUser.text = Utilities.GetStringMoneyByLongBigSmall(MyInfo.CHIP_EVENT);
        //HomeViewV2.api.ShowInfoUser(MyInfo.CHIP_EVENT.ToString("N0"), "0");
        ShowBetData();
    }

    private void ShowBetData()
    {
        //totalTai.text = Utilities.GetStringMoneyByLong((long)totalBet.taiChip);
        //totalXiu.text = Utilities.GetStringMoneyByLong((long)totalBet.xiuChip);
        //Debug.LogError("userBet.taiChip " + userBet.taiChip);
        totalTai.text = totalBet.taiChip.ToString("N0");
        totalXiu.text = totalBet.xiuChip.ToString("N0");
        totalUserBetTai.text = "$"+userBet.taiChip.ToString("N0");
		totalUserBetXiu.text = "$"+userBet.xiuChip.ToString("N0");
		totalUserTai.text = totalUser.taiChip.ToString("N0");
		totalUserXiu.text = totalUser.xiuChip.ToString("N0");
    }       

    IEnumerator CountDownTime()
    {
        yield return new WaitForSeconds(1f);
        if(remainTime > 0)
        {
            remainTime -= 1;
			if (remainTime == 1) {
                stopSound.Play();
				cankeo.transform.localPosition = new Vector3 (-Screen.width / 2, 0, 0);
		
				iTween.MoveTo(cankeo, iTween.Hash("position", new Vector3(600, 0, 0), "islocal", true, "oncompletetarget", gameObject, "oncomplete", "oncompletecankeo", "time", 1));
			}
            timeCountDown.text = formatCountDown(remainTime);
            StartCoroutine(CountDownTime());
        }else
            timeAnimation.SetActive(false);
    }

	public void oncompletecankeo(){
		iTween.MoveTo(cankeo, iTween.Hash("position", new Vector3(Screen.width + 1274, 0, 0), "islocal", true, "time", 3));
	}

    private string formatCountDown(int time)
    {
        //int minute = time / 60;
        //int second = time - (minute * 60);
        //return formatTime(minute) + ":" + formatTime(second);
		return formatTime(time);
    }

    private string formatTime(int time) {
        if (time < 10)
            return "0" + time;
        return time.ToString();
    }

    private void ShowHistoryBar(GamePacket gp)
    {
        var history = gp.param.GetUtfString(LuckyMiniParamKey.HistoryResult).Split('#');
        for(var i = 0; i < historyBar.transform.childCount; i++)
        {
            var perResult = history[i];
            var perHistory = historyBar.transform.GetChild(i).GetComponent<Image>();
            perHistory.sprite = perResult.Equals("0") ? whitePoint : blackPoint;
        }
    }
		
    public void Cup()
    {
        cup.Open();
    }

    public void History()
    {
        history.Open();
    }

    public void SoiCau() {
        soicau.Open();
    }

    public void ShowMask() {
        isMask = !isMask;
        maskBtn.transform.GetChild(0).gameObject.SetActive(!isMask);
    }

    public void Close() {
        //gameObject.SetActive(false);
        GamePacket gp = new GamePacket(LuckyMiniCommandKey.USER_EXIT);
        SFS.Instance.SendRoomRequest(gp);
        LoadingManager.Instance.ENABLE = true;
    }
	void OnApplicationPause(bool status){
		if (status) {
			OnEnable ();
		}
	}

    public void OpenGuide(bool isOpen) {
        guide.SetActive(isOpen);
    }
}
