﻿using Sfs2X.Entities.Data;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventController : MonoBehaviour
{
    [SerializeField]
    GameObject Waiting, Shop, PanelThele;
    [SerializeField]
    LuckyControllerEvent luckControllerEvent;
    [SerializeField]
    TimeCountDownControllerEvent timeCountDownEvent;
    [SerializeField]
    Text txtPointEvent, txtChipEvent, txtUserName, txtTimeStatus;
    private SFS sfs;
    private bool isCallServiceShop;

    private bool isStartEvent, isJoinEvent;

    public static bool isBuyShop;

    [SerializeField]
    List<ItemShopEvent> LstItemShopEvent;
    [SerializeField]
    TopEvent topEvent;
    // Start is called before the first frame update
    void Start()
    {
        InitEvent();
        isBuyShop = false;
    }
    public void InitEvent(bool isEndEvent=false)
    {
        if (!GameHelper.IS_LOAD_LOGIN)
        {
            GameHelper.ChangeScene(GameScene.LoginScene);
            return;
        }
        isStartEvent = false;
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.CallServiceGetProductInAppId();
        luckControllerEvent.gameObject.SetActive(false);
        Waiting.SetActive(true);
        Shop.SetActive(false);
        PanelThele.SetActive(false);
        topEvent.gameObject.SetActive(false);
        for (int i = 0; i < LstItemShopEvent.Count; i++)
        {
            LstItemShopEvent[i].gameObject.SetActive(false);
        }
        sfs = SFS.Instance;

        GameHelper.currentGid = (int)GAMEID.Event;
        GamePacket gp = new GamePacket(EventCommandKey.JOIN_EVENT_GAME);
        sfs.SendZoneRequest(gp);
        LoadingManager.Instance.ENABLE = true;
        txtPointEvent.text = "0";
        txtChipEvent.text = "0";
        txtUserName.text = MyInfo.NAME;
        if (isEndEvent)
        {
            AlertController.api.showAlert("Đã kết thúc Event! Giải thưởng sẽ được gửi vào hộp thư lúc 10h00 giờ sáng hôm sau!");
        }
    }

    public void BackHome()
    {
        LoadingManager.Instance.ENABLE = true;
        GamePacket gp = new GamePacket(CommandKey.USER_EXIT);
        SFS.Instance.SendRoomRequest(gp);
        GameHelper.ChangeScene(GameScene.HomeSceneV2);
    }
    public void ShowTheLe()
    {
        PanelThele.SetActive(true);
    }
    public void CloseTheLe()
    {
        PanelThele.SetActive(false);
    }
    public void ShowTopEvent()
    {
        GamePacket gp = new GamePacket(EventCommandKey.GET_EVENT_TOP_HEAD);
        sfs.SendRoomRequest(gp);
    }
    public void CloseTopEvent()
    {
        topEvent.Close();
    }
    public void ShowShopEvent()
    {
        if (!isCallServiceShop)
        {
            API.Instance.GetShopInfoEvent(ShowShopInfo);
        }
    }
    public void CloseShopEvent()
    {
        Shop.SetActive(false);
        if (isBuyShop)
        {
            isBuyShop = false;
            GetEventInfo();
        }        
    }
    private void ShowShopInfo(string _json)
    {
        Debug.LogWarning(" ShowShopInfo==== " + _json);
        JSONNode node = JSONNode.Parse(_json);
        JSONArray data = node.AsArray;
        for(int i = 0; i < data.Count; i++)
        {
            var productid = data[i]["inapp_id"];
            var amount = int.Parse(data[i]["amount"]);
            var inapp = Double.Parse(data[i]["inapp"]);
            var card = int.Parse(data[i]["card"]);
            LstItemShopEvent[i].Init(productid, amount, inapp, card);
        }
        string thoigian = node["data"]["created_date"];
        Shop.SetActive(true);
    }

    public void RequestJoinTaiXiuMiniRoom()
    {
        luckControllerEvent.RequestJoinTaiXiuMiniRoom();
        //SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        //GameHelper.currentGid = (int)GAMEID.TaiXiuMini;
        //GamePacket gp = new GamePacket(EventCommandKey.JOIN_EVENT_TAIXIU_GAME);
        //gp.Put(ParamKey.GAME_ID, GameHelper.currentGid);
        //sfs.SendRoomRequest(gp);
        //LoadingManager.Instance.ENABLE = true;
    }
    public void OnClickJoinEvent()
    {
        if (isStartEvent)
        {
            if (isJoinEvent)
            {
                Waiting.SetActive(false);
                
                RequestJoinTaiXiuMiniRoom();
            }
            else
            {
                AlertController.api.showAlert("Bạn Vui Lòng Mua Điểm Để Chơi Event!");
            }            
        }
        else
        {
            AlertController.api.showAlert("Event Chưa Tới Giờ Bắt Đầu!");
        }
        
    }
    

    public void GetEventTopHead(GamePacket gp)
    {
        Debug.LogWarning("GPPPP" + gp.param.ToJson());
        //SoundManager.PlaySound(SoundManager.ENTER_ROOM);
        ISFSArray aTop = gp.GetSFSArray("top");
        foreach(SFSObject item in aTop)
        {
            Debug.Log("Point====" + item.GetLong("point"));
            Debug.Log("username====" + item.GetUtfString("username"));
            Debug.Log("===========================");
        }
        topEvent.Open(gp);
    }





    public void OnSFSResponse(GamePacket gp)
    {
        Debug.LogError("=============OnSFSResponse Event cmd - " + gp.cmd);
        Debug.Log("OnSFSResponse Event- " + gp.ToString());       
        switch (gp.cmd)
        {
            case EventCommandKey.JOIN_EVENT_GAME:
                LoadingManager.Instance.ENABLE = false;
                GetEventInfo();
                break;
            case EventCommandKey.END_EVENT:
                EndEvent();
                break;
            case EventCommandKey.GET_EVENT_TOP_HEAD:
                GetEventTopHead(gp);
                break;
            case EventCommandKey.GET_EVENT_INFO:
                ResGetEventInfo(gp);
                break;
            case "241":
                Waiting.SetActive(false);
                LoadingManager.Instance.ENABLE = false;
                luckControllerEvent.gameObject.SetActive(true);
                break;
            case EventCommandKey.JOIN_EVENT_TAIXIU_GAME:
                luckControllerEvent.ShowTaiXiuMiniEvent();
                break;
           
        }
    }

    private void EndEvent()
    {
        //Waiting.SetActive(true);
        //LoadingManager.Instance.ENABLE = false;
        //luckControllerEvent.gameObject.SetActive(false);
        AlertController.api.showAlert("Đã kết thúc Event! Giải thưởng sẽ được gửi vào hộp thư lúc 10h00 giờ sáng hôm sau!");
    }

    
    private void ResGetEventInfo(GamePacket gp)
    {
        var isStart = gp.param.GetBool("isstart");
        var isJoin = gp.param.GetBool("is_join");
        var difftime = gp.param.GetLong("diftime");
        isJoinEvent = isJoin;
        if (isJoin)
        {
            var event_money = gp.param.GetLong("event_money");
            MyInfo.CHIP_EVENT = event_money;
            var event_point = gp.param.GetLong("event_point");
            MyInfo.POINT_EVENT = event_point;
            txtPointEvent.text = Utilities.GetStringMoneyByLongBigSmall(event_point);
            txtChipEvent.text = Utilities.GetStringMoneyByLongBigSmall(event_money);
        }
        if (isStart)//nếu isstart = true thì diftime là thời gian đếm ngươc đến kết thúc
        {
            txtTimeStatus.text = "Đang Mở Sẽ Kết Thúc Trong";
            timeCountDownEvent.initTimeCountDown(0,int.Parse(difftime.ToString()),0,CompleteCountDownEndEvent);
            timeCountDownEvent.isStartEvent = true;
            isStartEvent = true;
        }
        else//nếu isstart = false thì thời gian đếm ngược đến sự kiện tiếp theo
        {
            txtTimeStatus.text = "Bắt Đầu Trong";
            timeCountDownEvent.initTimeCountDown(0, int.Parse(difftime.ToString()), 0, CompleteCountDownNextEvent);
            timeCountDownEvent.isStartEvent = false;
        }

    }

    private void CompleteCountDownNextEvent()
    {
        isStartEvent = true;
        timeCountDownEvent.isStartEvent = true;
        txtTimeStatus.text = "Đang Bắt Đầu!";
    }

    private void CompleteCountDownEndEvent()
    {
        isStartEvent = false;
        timeCountDownEvent.isStartEvent = false;
        txtTimeStatus.text = "Event Đã Kết Thúc!";
    }

    private void GetEventInfo()
    {
        GamePacket gp = new GamePacket(EventCommandKey.GET_EVENT_INFO);
        sfs.SendRoomRequest(gp);
    }
}
