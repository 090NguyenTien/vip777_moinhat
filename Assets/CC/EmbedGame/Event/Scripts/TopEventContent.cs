﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class TopEventContent : MonoBehaviour {
    public Text Rank;
    public Text Username;
    public Text Money;
    public Image RankIcon;
    public GameObject RowBg;
    public Sprite Rank1;
    public Sprite Rank2;
    public Sprite Rank3;
    public void Show(int index, SFSObject data)
    {
//      "data": [ { "user_id": "57961ab8f5867005baeae7a6", "username": "thanhnm", "money": 10000 }
        Rank.text = index.ToString();
        Username.text = data.GetUtfString("username").ToString();
        Money.text = data.GetLong("point").ToString("N0");        
    }

    public void ShowPokerTop(int index, SFSObject data) {
        if (index > 3)
        {
            RankIcon.gameObject.SetActive(false);
            Rank.gameObject.SetActive(true);
            Rank.text = index.ToString();
        }
        else
        {
            Rank.gameObject.SetActive(false);
            RankIcon.gameObject.SetActive(true);
            switch (index) {
                case 1:
                    RankIcon.sprite = Rank1;
                    break;
                case 2:
                    RankIcon.sprite = Rank2;
                    break;
                case 3:
                    RankIcon.sprite = Rank3;
                    break;
            }
        }
        RowBg.SetActive(index % 2 == 0);
        Username.text = data.GetUtfString("username").ToString();
        Money.text = data.GetLong("point").ToString("N0");
    }
}
