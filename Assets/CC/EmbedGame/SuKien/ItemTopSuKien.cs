﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTopSuKien : MonoBehaviour
{
    [SerializeField]
    Text txtIndex, txtUserName;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void Init(string index, string username)
    {
        txtIndex.text = index;
        txtUserName.text = username;
    }
}
