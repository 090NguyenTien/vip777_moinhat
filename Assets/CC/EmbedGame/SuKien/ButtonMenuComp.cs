﻿using BaseCallBack;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonMenuComp : MonoBehaviour
{
    //[SerializeField]
    //Button BtnMenu;
    [SerializeField]
    Text txtTitle;
    string id, title, description, chip, gem, startTime, endTime;
    onCallBackJSONNode callback;
    JSONNode data;
    // Start is called before the first frame update
    void Start()
    {
        Toggle t = GetComponent<Toggle>();
        t.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(t);
        });
    }
    void ToggleValueChanged(Toggle tg)
    {
        if (tg.isOn)
        {
            callback(data);
        }
    }
    public void Init(JSONNode _data, onCallBackJSONNode _callback)
    {
        callback = _callback;
        this.data = _data;
        id = data["id"].Value;
        title = data["title"].Value;
        description = data["description"].Value;
        chip = data["id"].Value;
        gem = data["chip"].Value;
        startTime = data["startTime"].Value;
        endTime = data["endTime"].Value;

        SetTitle(title);
    }
    public void SetTitle(string title)
    {
        txtTitle.text = title;
    }
    public void ChangeStatus()
    {
        GetComponent<Toggle>().isOn = true;
    }
}
