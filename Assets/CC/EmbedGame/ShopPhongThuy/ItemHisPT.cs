﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemHisPT : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    string id, name, status, address, quantity, created_date;
    [SerializeField]
    Text txtName, txtStatus, txtAddress, txtQuatity, txtCreateDate;
    public void init(JSONNode item)
    {
        txtName.text = name = item["item_name"];
        id = item["_id"]["$id"];
        txtQuatity.text = quantity = "số lượng:" + item["qty"];
        txtStatus.text = status = item["status"];
        txtAddress.text = address = "địa chỉ:"+ item["address"];
        txtCreateDate.text = created_date = item["created_date"];
    }
}
