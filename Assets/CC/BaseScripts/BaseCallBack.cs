﻿using UnityEngine;
using System.Collections;
using UnityEngine.Purchasing;
using SimpleJSON;

namespace BaseCallBack
{
	public delegate void onCallBack();
	public delegate void onCallBackBool(bool _enable);
		public delegate void onCallBackInt(int i);
		public delegate void onCallBackString(string s);
	public delegate void onCallBackStringString(string s1, string s2);
    public delegate void onCallBackIntBool(int i, bool _enalbe);
    public delegate void onCallBackStringInt(string s, int i);
	public delegate void onCallBackIntString(int i, string s = "");
		public delegate void onCallBackSprite(Sprite _spr);
	public delegate void onCallBackIntSprite(int i, Sprite spr);
	public delegate void onCallBackStringSprite(string _url, Sprite _spr);
    public delegate void onCallBackStringStringBool(string s, string s2, bool val);
    public delegate void onCallBackGameObject(GameObject val); 
    public delegate void onCallBackJSONNode(JSONNode val); 

}
