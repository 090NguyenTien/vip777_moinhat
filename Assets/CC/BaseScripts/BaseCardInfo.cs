﻿using UnityEngine;
using System.Collections;

public class BaseCardInfo {

	public int Id { get; set; }
	public int Value { get; set; }
	internal CardType Type { get; set; }

	public static BaseCardInfo Get(int _id)
	{
		BaseCardInfo _card = new BaseCardInfo ();

		int value = (_id % 13) + 1;

		if (value == 1)
			value = 14;

		_card.Id = _id;
		_card.Value = value;
		_card.Type = (CardType)(_id / 13);

//		Debug.Log ("ID - " + _id + " ___ " + "Type: " + _card.Type);
		return _card;
	}
}
