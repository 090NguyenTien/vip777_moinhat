﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public abstract class BasePlayerView : MonoBehaviour, IBasePlayerView
{
    [Header("QUAN LY THONG TIN")]
    [HideInInspector]
    public int sfsId;
    [HideInInspector]
    public string userId;
    [HideInInspector]
    public string IdBang;
    [HideInInspector]
    public string ringid;
    [HideInInspector]
    public string gender;
    [HideInInspector]
    public string partner;





    [HideInInspector]
    public long Gem;
    [SerializeField]
    protected Text txtDisName;
    [SerializeField]
    protected Image imgAvatar, imgBorderAvt, imgBorderAvt_Defaut, inviteBtn;
    [SerializeField]
    protected Text txtChip;
    public Text txtLevel, txtExp;
    public GameObject Level;
    public string Vip;

    [SerializeField]
    public Image LogoBang;

    //	[SerializeField]
    //	private Image imgTimer;
    //	[SerializeField]
    //	private GameObject SampleCard;


    //	public abstract void CountDownTime(float _time);
    //	public abstract void CountDownTime(float _timeRemain, float _time);
    //	public abstract void StopCountDownTime ();

    public abstract void ShowPlayer(BaseUserInfo userInfo, Hashtable userVariable);
    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
        
    }
    public abstract void HidePlayer();
    public abstract void UpdateChip(long _gold);

}
