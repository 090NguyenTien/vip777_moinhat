﻿using System.Collections;
using UnityEngine;
using GoogleMobileAds.Api;
using System;
using BaseCallBack;
using System.Collections.Generic;

public class AdmobAdsManager : MonoBehaviour
{
    //public RewardBasedVideoAd rewardBasedVideo;
    public RewardedAd rewardedAd;
    public bool isLoadcomplete = false;
    public onCallBackInt CbCompleteVideo;
    public void Init(onCallBackInt cb)
    {
        CbCompleteVideo = cb;
    }
    // Start is called before the first frame update
    public void Start()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-2522337678847260/2105803914";
#elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-2522337678847260/2105803914";
#else
        string appId = "unexpected_platform";
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(adUnitId);

        this.rewardedAd = new RewardedAd(adUnitId);

        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        RequestRewardBasedVideo();

    }

    void RequestRewardBasedVideo()
    {
        // Create an empty ad request.        
        AdRequest request = this.CreateAdRequest();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
    }

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: "
                             + args.Message);
        result.Add(3);
    }

    private List<int> result = new List<int>();
    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdClosed event received");
        if (isLoadcomplete)
        {
            result.Add(1);
        }
        RequestRewardBasedVideo();
    }
    private void OnApplicationFocus(bool focus)
    {
        if (focus == false)
        {
            return;
        }
        StartCoroutine(delayAfterAds());
    }
    IEnumerator delayAfterAds()
    {
        yield return new WaitForSeconds(0.25f);
        if (result.Count > 0)
        {
            if (result.Contains(1) == true)
            {
                CbCompleteVideo(1);
            }
            else
            {
                CbCompleteVideo(3);
            }
        }
    }
    private void onWatchAdsCompleted()
    {
        CbCompleteVideo(1);
    }
    private IEnumerator CountDownCloseAdmob()
    {
        yield return new WaitForSeconds(2f);
        CbCompleteVideo(1);

    }
    public void HandleUserEarnedReward(object sender, Reward args)
    {
        isLoadcomplete = true;
        MonoBehaviour.print("HandleUserEarnedReward");
        //string type = args.Type;
        //double amount = args.Amount;
        //MonoBehaviour.print(
        //    "HandleRewardedAdRewarded event received for "
        //                + amount.ToString() + " " + type);

    }


    private AdRequest CreateAdRequest()
    {

        return new AdRequest.Builder()
            .AddTestDevice(AdRequest.TestDeviceSimulator)
            .AddTestDevice("40DA6565AC8B570A158724531BFFACD1")
            .AddKeyword("game")
            .SetGender(Gender.Male)
            .SetBirthday(new DateTime(1985, 1, 1))
            .TagForChildDirectedTreatment(false)
            .AddExtra("color_bg", "9B30FF")
            .Build();
    }
    public void UserChoseToWatchAd()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
    }



}
