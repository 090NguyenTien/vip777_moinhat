﻿using BaseCallBack;
using SimpleJSON;
using UnityEngine;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif
public class VideoAdsManager : MonoBehaviour
{
    public static VideoAdsManager Instance = null;
    private string vungleRewardId = "";
    private string adsSecretkey = "";
    private bool isCallService = false;
    bool adInited = false;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            //string gameId = "1066689";
            //bool testMode = true;
            //Advertisement.Initialize(gameId, testMode);
        }

    }

    private void OnEnable()
    {
        string appId = "";
        string rewardId = "";

        
    }
    onCallBackInt callbackCompleteVideo;
    public void ShowAds(onCallBackInt callBack = null)
    {
        callbackCompleteVideo = callBack;
        API.Instance.RequestGlobalAds(RspRequestAds);
    }

    void RspRequestAds(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" ========================RspRequestAds=============== " + _json);
        if (node["status"].AsInt == 1)
        {
            adsSecretkey = node[ParamKey.ADS_SECRET_KEY].Value;
#if UNITY_IOS || UNITY_ANDROID
            ShowUnityVideoRewardAd();
#endif
        }
    }
    private void callReceiveGiftWatched()
    {
        if (adsSecretkey != "")
        {
            API.Instance.RewardGlobalAds(adsSecretkey, RspRewardCallback);
        }
    }
#if UNITY_IOS || UNITY_ANDROID
    public string placementId = "rewardedVideo";
    void ShowUnityVideoRewardAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
    }



    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched();
        }
        else if (result == ShowResult.Skipped)
        {
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            ShowOptions optionsFull = new ShowOptions();
            optionsFull.resultCallback = HandleShowResultFull;
            Advertisement.Show("video",optionsFull);
        }
    }
    void HandleShowResultFull(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched();
        }
        else if (result == ShowResult.Skipped)
        {
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            //AlertController.api.showAlert("Chưa tải được Video phù hợp, Vui lòng thử lại!");
        }
    }
#endif
    void RspRewardCallback(string _json)
    {
        Debug.Log("=============RspRewardCallback==========" + _json);
        JSONNode node = JSONNode.Parse(_json);
        if (node["status"].AsInt == 1)
        {
        
            double mychip = double.Parse(node[ParamKey.CHIP].Value);
            int chipReward = (int)(mychip - MyInfo.CHIP);            

            if (HomeViewV2.api != null)
            {
                AlertController.api.showAlert("Bạn nhận được " + chipReward + " chip");
                callbackCompleteVideo(chipReward);
                //MyInfo.CHIP = long.Parse(mychip.ToString());//ở trang home thì khong set vi animation da~ cong chip roi 
            }
            else
            {
                MyInfo.CHIP = long.Parse(mychip.ToString());
                callbackCompleteVideo(chipReward);
            }
        }
        else if (node["status"].AsInt != 1)
        {
            AlertController.api.showAlert(node["msg"]);
        }

    }
    
}
