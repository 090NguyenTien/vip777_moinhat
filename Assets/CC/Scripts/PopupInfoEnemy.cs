﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;
using UnityEngine.SceneManagement;
using SimpleJSON;
using ControllerXT;

public class PopupInfoEnemy : MonoBehaviour
{
    [SerializeField]
    GameObject BtnKetBan, TxtStatusFriend;
    [SerializeField]
    Image Avatar, KhungAvatar;
    [SerializeField]
    Text TxtTen, TxtLoaiVip, TxtLevel, TxtChip, TxtGem;
    [SerializeField]
    List<Sprite> ListSprBoom;
    [SerializeField]
    Transform ChuaItemsBoom;
    int Enemy;
    onCallBackInt onClick;
    [SerializeField]
    GameObject Popup;
    [SerializeField]
    ControllerMB.MBInGameController MauBinhControll;
    [SerializeField]
    ControllerXT.XTInGameController XiToControll;

    [SerializeField]
    TLMNRoomController TienLenControll;
    public static PopupInfoEnemy Instance { get; set; }

    int sfsId_MB;
    string userId;

    string IdBangUserBiNem = "";
    string IdBangMaster = "";
    string IdBangCuaMinh = "";

    public string MadUser = "1,2,3,4,5,6,3,2";

    [SerializeField]
    MadControll Mad;
    [SerializeField]
    Image imgUser_fa, imgUser, img_NguoiTinh;
    [SerializeField]
    Text TxtTenNguoiTinh;
    [SerializeField]
    Sprite SprMacDinh, SprNam, SprNu;
    [SerializeField]
    GameObject Obj_InGame, Obj_InChat;
    int VipUser;
    [SerializeField]
    Button BtnKetBan_InChat, BtnTangChip_InChat;
    [SerializeField]
    GameObject TxtStatusFriend_InChat;

    private void Awake()
    {
        //LayThongTinBang_CuaMinh();
    }

    // Use this for initialization
    public void Init(string _userId, Sprite _ava, Sprite _KhungAva, string _ten, string _vip, string _level, string _chip, string _gem, int _enemy, string idBang, int id_gioitinh, string TenNguoiTinh, int sfsid = -2)
    {
        Debug.Log("Vo Toi Init");
        Debug.LogError("id_gioitinh---------- " + id_gioitinh + "  ------------TenNguoiTinh------- " + TenNguoiTinh);
        XuLy_DamCuoi(id_gioitinh, TenNguoiTinh);

        if (MyInfo.CHAT_STATUS == 0)
        {
            BtnKetBan.SetActive(false);
        }
        else
        {
            BtnKetBan.SetActive(true);
        }
        this.userId = _userId;
        if (_ava != null)
        {
            Avatar.sprite = _ava;
        }

        if (_KhungAva != null)
        {
            KhungAvatar.sprite = _KhungAva;
        }
        TxtTen.text = _ten;
        TxtLoaiVip.text = "Vip " + _vip;
        TxtLevel.text = "Level " + _level;
        TxtChip.text = _chip;
        TxtGem.text = _gem;

        Enemy = _enemy;

        sfsId_MB = sfsid;
        // LayThongTinBang_CuaMinh();

        IdBangCuaMinh = MyInfo.ID_BANG;
        IdBangMaster = MyInfo.ID_BANG_MASTER;
        IdBangUserBiNem = idBang;

        int MyVip = MyInfo.MY_ID_VIP;
        VipUser = int.Parse(_vip);
        for (int i = 0; i < ChuaItemsBoom.childCount; i++)
        {

            // ChuaItemsBoom.GetChild(i).GetComponent<Button>().onClick.AddListener(ItemBoomOnClick);
            int VipUnClock = DataHelper.DicItemBoom[i];
            GameObject Boom = ChuaItemsBoom.GetChild(i).gameObject;
            if (MyVip >= VipUnClock)
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
            }
            else
            {
                Boom.GetComponent<ItemIdView>().Init(i, ItemBoomClockOnClick);
                Boom.GetComponent<Image>().sprite = GetSprite(i);
                Boom.GetComponent<Image>().color = Color.gray;
                Text TxtVip = Boom.transform.GetChild(0).gameObject.GetComponent<Text>();
                TxtVip.text = "Vip " + VipUnClock.ToString();
                TxtVip.gameObject.SetActive(true);
            }


        }

        // LayThongTinBangUser_BiNem(_userId);

        Popup.SetActive(true);
        //Check đã là bạn chưa
        CheckIsFriend();
        API.Instance.RequestVatPham_SoHuu_Enemy(_userId, RspLayDuLieuVatPham_SoHuu);

    }



    public void InitInChat(string _userId, string _ten, string _vip, string idBang, int id_gioitinh, string TenNguoiTinh, int sfsid = -2)
    {

        



            Debug.Log("Vo Toi Init");
        Debug.LogError("id_gioitinh---------- " + id_gioitinh + "  ------------TenNguoiTinh------- " + TenNguoiTinh);
        XuLy_DamCuoi(id_gioitinh, TenNguoiTinh);

        if (MyInfo.CHAT_STATUS == 0)
        {
            TxtStatusFriend_InChat.GetComponent<Text>().text = "...";
            BtnKetBan_InChat.gameObject.SetActive(false);
        }
        else
        {
            TxtStatusFriend_InChat.GetComponent<Text>().text = "Người lạ";
            BtnKetBan_InChat.gameObject.SetActive(true);
        }
        this.userId = _userId;
      
        TxtTen.text = _ten;
        TxtLoaiVip.text = "Vip " + _vip;
       

        sfsId_MB = sfsid;
        // LayThongTinBang_CuaMinh();


        Obj_InGame.SetActive(false);
        Obj_InChat.SetActive(true);



        IdBangCuaMinh = MyInfo.ID_BANG;
        IdBangMaster = MyInfo.ID_BANG_MASTER;
        IdBangUserBiNem = idBang;

        int MyVip = MyInfo.MY_ID_VIP;
        VipUser = int.Parse(_vip);
       
        // LayThongTinBangUser_BiNem(_userId);

        Popup.SetActive(true);
        //Check đã là bạn chưa
        CheckIsFriend();
        API.Instance.RequestVatPham_SoHuu_Enemy(_userId, RspLayDuLieuVatPham_SoHuu);

    }











    void XuLy_DamCuoi(int Id_GioiTinh, string Ten_nguoitinh)
    {
        if (Ten_nguoitinh == "") // Doc than
        {
            imgUser_fa.sprite = SprMacDinh;
            if (Id_GioiTinh == 1)
            {
                imgUser_fa.sprite = SprNam;
            }
            else if (Id_GioiTinh == 2)
            {
                imgUser_fa.sprite = SprNu;
            }
            imgUser_fa.gameObject.SetActive(true);
            imgUser.gameObject.SetActive(false);
            img_NguoiTinh.gameObject.SetActive(false);
            TxtTenNguoiTinh.gameObject.SetActive(false);
        }
        else
        {
            if (Id_GioiTinh == 1) // la nam
            {
                imgUser.sprite = SprNam;
                img_NguoiTinh.sprite = SprNu;
                TxtTenNguoiTinh.text = Ten_nguoitinh;
            }
            else // la nu
            {
                imgUser.sprite = SprNu;
                img_NguoiTinh.sprite = SprNam;
                TxtTenNguoiTinh.text = Ten_nguoitinh;
            }
            imgUser_fa.gameObject.SetActive(false);
            imgUser.gameObject.SetActive(true);
            img_NguoiTinh.gameObject.SetActive(true);
            TxtTenNguoiTinh.gameObject.SetActive(true);

        }
    }



    public void LayThongTinBangUser_BiNem(string idUser_BiNem)
    {
        API.Instance.RequestLayIdBangHoi(idUser_BiNem, RspLayIdBangHoi_BiNem);
    }


    void RspLayIdBangHoi_BiNem(string _json)
    {

        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

        JSONNode node = JSONNode.Parse(_json);

        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

        Debug.LogWarning("usserBang------------- " + userBang);
        Debug.LogWarning("BangMaster------------- " + BangMaster);

        IdBangMaster = BangMaster;
        IdBangUserBiNem = userBang;

    }

    public void LayThongTinBang_CuaMinh()
    {
        //  API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangHoi_user_1);
        API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangChinhMinh);
    }


    void RspLayIdBangChinhMinh(string _json)
    {

        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

        JSONNode node = JSONNode.Parse(_json);

        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

        Debug.LogWarning("usserBangChinhMinh------------- " + userBang);
        Debug.LogWarning("BangMaster------------- " + BangMaster);

        IdBangMaster = BangMaster;
        IdBangCuaMinh = userBang;
    }



    bool CheckPhanDame()
    {

        if (IdBangUserBiNem == "")
        {
            return false;
        }

        if (IdBangUserBiNem == IdBangCuaMinh)
        {
            return false;
        }

        if (IdBangUserBiNem != IdBangMaster)
        {
            return false;
        }

        return true;
    }

    int CheckDicThanBai(string idUser)
    {
        int kq = -1;
        if (DataHelper.DicThanBai.ContainsKey(idUser))
        {
            kq = DataHelper.DicThanBai[idUser];
        }
        return kq;
    }




    public void KickUser()
    {
        Debug.LogError("KickUser===" + MyInfo.SFS_ID + " =====sfsidBekick= " + sfsId_MB);
        if (MyInfo.MY_ID_VIP <= VipUser)
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip Lớn Hơn Người Chơi Bạn Muốn Kick!");
        }
        else
        {
            GamePacket param = new GamePacket(CommandKey.KICK_USER_BY_HOST);
            param.Put(ParamKey.USER_ID, sfsId_MB);
            SFS.Instance.SendRoomRequest(param);
        }
        ClosePopup();
    }
    void CheckIsFriend()
    {

    }
    public void AddFriend()
    {
        BtnKetBan.SetActive(false);
        TxtStatusFriend.GetComponent<Text>().text = "Đang Chờ Đồng Ý";
        SFS.Instance.RequestAddFriend(userId);
    }

    public void AddFriend_InChat()
    {
        BtnKetBan_InChat.gameObject.SetActive(false);
        TxtStatusFriend_InChat.GetComponent<Text>().text = "Đang Chờ Đồng Ý";
        SFS.Instance.RequestAddFriend(userId);
    }



    Sprite GetSprite(int _index)
    {
        return ListSprBoom[_index];
    }


    public void ItemBoomClockOnClick(int id)
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        // Popup.SetActive(false);
    }

    public void ItemBoomOnClick(int id)
    {

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);


        if (SceneManager.GetActiveScene().name.Equals(GameScene.TLMNScene.ToString()))
        {
            bool check = CheckPhanDame();
            if (check == true)
            {
                TienLenControll.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    TienLenControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    TienLenControll.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }


            }

        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.PhomScene.ToString()))
        {
            bool check = CheckPhanDame();
            if (check == true)
            {
                PhomRoomController.Instance.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    PhomRoomController.Instance.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    PhomRoomController.Instance.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }
            }


        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.MBScene.ToString()))
        {
            bool check = CheckPhanDame();
            if (check == true)
            {
                MauBinhControll.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    MauBinhControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    MauBinhControll.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }
            }
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.XTScene.ToString()))
        {
            bool check = CheckPhanDame();
            if (check == true)
            {
                XiToControll.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    XiToControll.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    XiToControll.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }
            }
        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.BaiCaoScence.ToString()))
        {
            // BaiCaoRoomController.Instance.NemBoomControll(0, Enemy, id);

            //IdBangUserBiNem = IdBangMaster;
            bool check = CheckPhanDame();
            if (check == true)
            {
                BaiCaoRoomController.Instance.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    BaiCaoRoomController.Instance.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    BaiCaoRoomController.Instance.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }
            }



        }
        else if (SceneManager.GetActiveScene().name.Equals(GameScene.LiengScene.ToString()))
        {
            bool check = CheckPhanDame();
            if (check == true)
            {
                LiengInGameController.Instance.PhanDame(Enemy, id);
            }
            else
            {
                int loaiThanBai = CheckDicThanBai(this.userId);
                if (loaiThanBai == -1)
                {
                    LiengInGameController.Instance.SinhBoom_DiChuyenToiViTri(0, Enemy, id, true);
                }
                else
                {
                    LiengInGameController.Instance.GoiThanBai(0, Enemy, id, loaiThanBai, true);
                }
            }
        }

        IdBangUserBiNem = "";

    }

    public void ClosePopup()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        Popup.SetActive(false);
    }




    public void RequestLayDuLieuVatPham_SoHuu()
    {


    }

    long GetDoanhThuByIDTong(string idTong, long level, string type)
    {
        long giaban = 0;
        if (type == "house")
        {
            giaban = DataHelper.DuLieuNha[idTong].chip;
        }
        else if (type == "shop")
        {
            giaban = DataHelper.DuLieuShop[idTong].chip;
        }
        else if (type == "car")
        {
            long giagem = DataHelper.DuLieuXe[idTong].gem;
            giaban = giagem * 5000000;
        }
        long thunhap = GetDoanhThuByGiaBan(giaban, level);
        return thunhap;
    }


    long GetDoanhThuByGiaBan(long GiaBan, long level)
    {
        long doanhthu = (GiaBan / 1000) * level;
        return doanhthu;
    }



    void HienThiMadMacDinh_Enemy()
    {
        Item_InMad itInMad_Home = new Item_InMad();
        itInMad_Home.id = "5dc03420e52b8e2d238b6045";
        itInMad_Home.idTong = "5dc03420e52b8e2d238b6045";
        itInMad_Home.Level = 0;
        itInMad_Home.DoanhThu = 0;

        DataHelper.DuLieuMap_Enemy.Add(0, itInMad_Home);

        for (int i = 1; i < 8; i++)
        {

            Item_InMad itInMad = new Item_InMad();
            itInMad.id = "";
            itInMad.idTong = "";
            itInMad.Level = 0;
            itInMad.DoanhThu = 0;


            DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
        }
    }




    void RspLayDuLieuVatPham_SoHuu(string _json)
    {
        Debug.LogWarning("DU LIEU SAN PHAM so huu: " + _json);
        DataHelper.DuLieuMap_Enemy.Clear();
        if (_json == "false")
        {           
            HienThiMadMacDinh_Enemy();
        }
        else
        {
            JSONNode node = JSONNode.Parse(_json);
            if (node["placed_assets"].Value == "null")
            {
                HienThiMadMacDinh_Enemy();
            }
            else
            {
                int cou = node["placed_assets"].Count;

                for (int i = 0; i < cou; i++)
                {
                    int pos = int.Parse(node["placed_assets"][i]["pos"].Value);
                    string a_id = node["placed_assets"][i]["id"].Value;
                    string a_id_tong = node["placed_assets"][i]["id_asset"]["$id"].Value;
                    long a_level = long.Parse(node["placed_assets"][i]["level"].Value);

                    Item_InMad itInMad = new Item_InMad();
                    itInMad.id = a_id;
                    itInMad.idTong = a_id_tong;
                    itInMad.Level = a_level;

                    string type = "house";

                    if (pos > 0 && pos < 6)
                    {
                        type = "shop";
                    }
                    else if (pos > 5 && pos < 8)
                    {
                        type = "car";
                    }

                    long doanhthu = GetDoanhThuByIDTong(a_id_tong, a_level, type);
                    itInMad.DoanhThu = doanhthu;
                    DataHelper.DuLieuMap_Enemy[pos] = itInMad;
                }

                for (int i = 0; i < 8; i++)
                {
                    if (DataHelper.DuLieuMap_Enemy.ContainsKey(i) == false)
                    {
                        Item_InMad itInMad = new Item_InMad();
                        itInMad.id = "";
                        itInMad.idTong = "";

                        itInMad.Level = 0;
                        itInMad.DoanhThu = 0;

                        DataHelper.DuLieuMap_Enemy.Add(i, itInMad);
                    }

                }

            }
        }


        Mad.XemNha();

    }


}
