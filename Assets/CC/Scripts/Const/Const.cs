﻿using UnityEngine;
using System.Collections;

public class Const {
    public const string CHAT_CHAR_COMPONENT = "chatcomponent$^!@_$*";//biến dùng phân biệt chat với component chat và chat thường
    #if UNITY_ANDROID
    public const string PLATFORM = "android";
	#elif UNITY_IOS
	public const string PLATFORM = "ios";
	#else
	public const string PLATFORM = "android";
	#endif
}
public enum GameKind
{
    Baucua = 0,
    TLMN = 1,
    MB = 2,
    XT =3,
    Phom = 4,
    TaiXiu = 5,
    TLDL = 6
}
