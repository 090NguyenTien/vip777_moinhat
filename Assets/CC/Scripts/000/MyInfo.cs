﻿using BaseCallBack;
using Facebook.Unity;
using Sfs2X.Entities;
using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyInfo
{
    public static int GEM_PIRATE=1;
    public static int KHIEN_PIRATE=0;
    public static string PAYMENT_RANDOM="";
    public static string CLAN_ID = "";
    public static bool IS_APK_PURE = false;
    public static int CUR_LEVEL = 0;
    public static int CUR_EXP = 0;
    public static int MIN_EXP = 0;
    public static int MAX_EXP = 0;
    public static int FIRST_PAYMENT = 0;
    public static int MAX_FRIEND = 50;
    public static int CHAT_STATUS = 0;
    public static int CLAN_STATUS = 0;
    public static int CLAN_WAR_STATUS = 0;
    public static int SHOP_PHONGTHUY_STATUS = 0;
    public static int CHANGE_TLMN = 0;
    public static int EVENT_STATUS = 0;
    public static int MARKET_STATUS = 0;
    public static int POST_NEWS_STATUS = 0;
    public static int BUNDLE_VERSION = 0;

    public static string MY_VIP = "VIP 0";
    public static string MY_NEXT_VIP = "VIP 1";
    public static int MY_ID_VIP = 0;
    public static int VIP_CHAT_ALLOW = 3;
    public static float MY_VIP_COLLECT = 0;
    public static int MY_BOR_AVATAR = -1;
    public static List<int> MY_BORDERS_AVATAR = new List<int>();
    public static string MY_NEW_AVARTAR_BORDER = "";
    public static string ID_BANG = "";
    public static string ID_BANG_MASTER = "";

    public static bool AVATAR_FACEBOOK_LOAD = false;
    public static string ID = "";
    public static int SFS_ID = 0;
    public static int SPEC_WHEEL = 0;
    public static long SPEC_WHEEL_CHIP_REQUIRED = 0;
    public static int LUCKY_SPEC_WHEEL = 0;
    public static string NAME = "";
    public static bool CALL_SERVICE_INIT = false;
    public static bool SHOW_SHOP_FROM_SERVICE = false;
    public static bool PC_VERSION;
    public static bool IS_WHEEL_LUCKY = false;
    public static int XAM_BOI_TOAN = 0;
    
    public static int ID_GioiTinh = 0;
    //Bar Xeng Hoa Qua
    public static int BAR_POINT = 0;

    public static string TEN_NGUOITINH = "";
    
    public static string ID_QUANHE = "";


    public static long CHIP
    {
        get
        {
            return _chip;
        }
        set
        {
            if (value < 500000)
            {
                string sceneName = SceneManager.GetActiveScene().name;
                Debug.Log("KHUONG================sceneName===========" + sceneName);               
                if(sceneName == GameScene.WaitingRoom.ToString())
                {
                    AlertController.api.showAlertWatchAds("bạn sắp hết tiền. bạn có thể nhận <color=#00FF02FF>Free Chip</color> miễn phí nếu click xem video quảng cáo bên dưới", LobbyView.api.getTxtChipUser().gameObject);
                }else if(sceneName == GameScene.HomeSceneV2.ToString() || sceneName == GameScene.LoginScene.ToString())
                {
                    //AlertController.api.showAlertWatchAds("bạn sắp hết tiền. bạn có thể nhận <color=#00FF02FF>Free Chip</color> miễn phí nếu click xem video quảng cáo bên dưới", null);
                } 
            }
            _chip = value;
        }
    }


    public static long CHIPBANK
    {
        get
        {
            return _chipBank;
        }
        set
        {

            _chipBank = value;
        }
    }



    public static long GEM
    {
        get
        {
            return _gem;
        }
        set
        {
            if (value < 100000)
            {
                /*
                string sceneName = SceneManager.GetActiveScene().name;
                Debug.Log("KHUONG================sceneName===========" + sceneName);
                if (sceneName == GameScene.WaitingRoom.ToString())
                {
                    AlertController.api.showAlertWatchAds("bạn sắp hết tiền. bạn có thể nhận <color=#00FF02FF>Free Chip</color> miễn phí nếu click xem video quảng cáo bên dưới", LobbyView.api.getTxtChipUser().gameObject);
                }
                else if (sceneName == GameScene.HomeSceneV2.ToString() || sceneName == GameScene.LoginScene.ToString())
                {
                    AlertController.api.showAlertWatchAds("bạn sắp hết tiền. bạn có thể nhận <color=#00FF02FF>Free Chip</color> miễn phí nếu click xem video quảng cáo bên dưới", null);
                }
                */
            }
            _gem = value;
        }
    }

    public static long CHIP_EVENT;
    public static long POINT_EVENT;


    public static long DOANHTHU
    {
        get
        {
            return _doanhthu;
        }
        set
        {          
            _doanhthu = value;
        }
    }





    public static string GetVipName()
    {
        if (MY_ID_VIP == 1)
        {
            MY_VIP = "VIP 1";
            MY_NEXT_VIP = "VIP 2";
        }
        else if (MY_ID_VIP == 2)
        {
            MY_VIP = "VIP 2";
            MY_NEXT_VIP = "VIP 3";
        }
        else if (MY_ID_VIP == 3)
        {
            MY_VIP = "VIP 3";
            MY_NEXT_VIP = "VIP 4";
        }
        else if (MY_ID_VIP == 4)
        {
            MY_VIP = "VIP 4";
            MY_NEXT_VIP = "VIP 5";
        }
        else if (MY_ID_VIP == 5)
        {
            MY_VIP = "VIP 5";
            MY_NEXT_VIP = "VIP 6";
        }
        else if (MY_ID_VIP == 6)
        {
            MY_VIP = "VIP 6";
            MY_NEXT_VIP = "VIP 7";
        }
        else if (MY_ID_VIP == 7)
        {
            MY_VIP = "VIP 7";
            MY_NEXT_VIP = "VIP 8";
        }
        else if (MY_ID_VIP == 8)
        {
            MY_VIP = "VIP 8";
            MY_NEXT_VIP = "VIP 9";
        }
        else if (MY_ID_VIP == 9)
        {
            MY_VIP = "VIP 9";
            MY_NEXT_VIP = "VIP 10";
        }
        else if (MY_ID_VIP == 10)
        {
            MY_VIP = "VIP 10";
            MY_NEXT_VIP = "VIP 11";
        }
        else if (MY_ID_VIP == 11)
        {
            MY_VIP = "VIP 11";
            MY_NEXT_VIP = "VIP 12";
        }
        else if (MY_ID_VIP == 12)
        {
            MY_VIP = "VIP 12";
            MY_NEXT_VIP = "VIP 13";
        }
        else if (MY_ID_VIP == 13)
        {
            MY_VIP = "VIP 13";
            MY_NEXT_VIP = "VIP 14";
        }
        else if (MY_ID_VIP == 14)
        {
            MY_VIP = "VIP 14";
            MY_NEXT_VIP = "VIP 15";
        }
        else if (MY_ID_VIP == 15)
        {
            MY_VIP = "VIP 15";
            MY_NEXT_VIP = "VIP 16";
        }

        return MY_VIP;
    }



    public static void CheckMoney(GameObject target, System.Action cb =null)
    {
        if (_chip < 500000) AlertController.api.showAlertWatchAds("bạn sắp hết tiền. bạn có thể nhận <color=#00FF02FF>Free Chip</color> miễn phí nếu click xem video quảng cáo bên dưới Hoặc nạp tiền", target, cb);
    }
    
    public static long _chip = 0;
    public static long _chipBank = 0;
    public static long _gem = 0;
    public static long _doanhthu = 0;





    public static long GOLD = 0;
	public static int VIPPOINT = 0;
	private static Sprite _sprAvatar = null;
    private static Sprite _sprBorAvatar = null;
    public static string FB_TOKEN = "";
    private static string _AvatarName = "https://scontent-hkg3-1.xx.fbcdn.net/t31.0-8/q86/p960x960/10896312_778970498842797_3943371920215508704_o.jpg";
    private static string _BorderAvatarName = "";

    public static string TOKEN = "";
	public static string EXPIRED = "";
	public static string FULLNAME = "";
	public static string PHONE = "";
	public static string EMAIL = "";
	public static string CMND = "";

	public static int WIN = 0;
	public static int LOSE = 0;
	public static int DRAW = 0;

	public static int GENDER = -1; //-1 - None ___ 0 - Male ___ 1 - Female 

    public static List<Inbox> INBOXES = new List<Inbox>();
    public static int UNREAD_INBOX = 0;

    //event toutnament
    private static float _timeCountDownToActive;
    private static float _timeCountDownToActiveInit;
    public static float tournamentCountDownToActive
    {
        get
        {
            return _timeCountDownToActive - (Time.realtimeSinceStartup - _timeCountDownToActiveInit);
        }
        set
        {
            if (isInitTournament == false)
            {
                _timeCountDownToActiveInit = Time.realtimeSinceStartup;
                _timeCountDownToActive = value;
            }
        }
    }
    public static string AvatarName
    {
        get
        {
            return _AvatarName;
        }
        set
        {
            //LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
           // Debug.LogError("SET-AVATAR==========" + AccessToken.CurrentAccessToken.TokenString);
           // if (loginType == LoginType.Facebook)
            //    _AvatarName = "https://graph.facebook.com/me/picture?access_token=" + AccessToken.CurrentAccessToken.TokenString;
           // else
                _AvatarName = value;
        }
    }


    public static string BorderAvatarName
    {
        get
        {
            return _BorderAvatarName;
        }
        set
        {
            //LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            // Debug.LogError("SET-AVATAR==========" + AccessToken.CurrentAccessToken.TokenString);
            // if (loginType == LoginType.Facebook)
            //    _AvatarName = "https://graph.facebook.com/me/picture?access_token=" + AccessToken.CurrentAccessToken.TokenString;
            // else
            _BorderAvatarName = value;
        }
    }




    public static Sprite sprAvatar
    {
        get
        {
            return _sprAvatar;
        }
        set
        {
            // LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            //Debug.LogError("SET-AVATAR==========" + AccessToken.CurrentAccessToken.TokenString);
            //if (loginType == LoginType.Facebook)
            //{
            //StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=large&access_token=" + AccessToken.CurrentAccessToken.TokenString));
            // }

            //else
            _sprAvatar = value;
        }
    }



    public static Sprite sprBorAvatar
    {
        get
        {
            return _sprBorAvatar;
        }
        set
        {
            // LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            //Debug.LogError("SET-AVATAR==========" + AccessToken.CurrentAccessToken.TokenString);
            //if (loginType == LoginType.Facebook)
            //{
            //StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=large&access_token=" + AccessToken.CurrentAccessToken.TokenString));
            // }

            //else
            _sprBorAvatar = value;
        }
    }





    public IEnumerator UpdateAvatarThread(string _url)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        _sprAvatar = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

    }
    private static float _timeCountDownToClose;
    private static float _timeCountDownToCloseInit;
    public static float tournamenToClose
    {
        get
        {
            return _timeCountDownToClose - (Time.realtimeSinceStartup - _timeCountDownToCloseInit);
        }
        set
        {
            if (isInitTournament == false)
            {
                isInitTournament = true;
                _timeCountDownToCloseInit = Time.realtimeSinceStartup;
                _timeCountDownToClose = value;
            }
           
        }
    }

    public static Room ChatRoom { get; internal set; }
    public static Room BangChatRoom { get; internal set; }
    public static string BangChatRoomName = "";
    public static bool isInitTournament = false;
    public static TournamentStatus tournamentStatus = TournamentStatus.None;
    public static TournamentType tournamentType = TournamentType.MoneyWin;
    public static long[] tournamentBettingArr;
    internal static JSONNode PROMOTION_IN_APP;
}
public enum TournamentStatus
{
    None = -1,
    XT_Tournament = 1,
    MB_Tournament = 2,
    TaiXiu_Tournament = 3,
    Phom_Tournament = 4,
    TLMN_Tournament = 5,
    TLDL_Tournament = 6
}
