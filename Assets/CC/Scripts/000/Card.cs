﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Card : MonoBehaviour
{

    public int Id { get; set; }
    public int Value { get; set; } //la cay gi
    public CardType Type { get; set; }//la bich chuon ro co

    public Image ValueImage;
    public Image UpperTypeImage;
    public Image MainTypeImage;
    public Image BackgroundImage;

    [SerializeField]
    private Image ImgCantUse;

    public static bool isPointerDowning = false;
    public static Card cardClickDown = null;

    public void OnPointerDown()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;

        if (currentSceneName.Equals(GameScene.TLMNScene.ToString()))
        {
            TLMNRoomController.Instance.player.PickCard(this);
        }
        if (currentSceneName.Equals(GameScene.PhomScene.ToString()))
            PhomRoomController.Instance.PlayerSlot.gameObject.SendMessage("PickCard", this, SendMessageOptions.DontRequireReceiver);
        cardClickDown = this;
        isPointerDowning = true;
    }
    public void setBackCardActive(bool isActive)
    {
        BackgroundImage.gameObject.SetActive(false);
    }
    public void OnPointerUp()
    {
        isPointerDowning = false;
        cardClickDown = null;
    }

    public void OnPointerEnter()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        if (currentSceneName.Equals(GameScene.TLMNScene.ToString()) == true)
        {
            if (isPointerDowning == true && TLMNRoomController.Instance.isOnMyTurn == true && TLMNRoomController.Instance.isNewRound == true && cardClickDown != this)
            {
                TLMNRoomController.Instance.player.PickCard(this);
            }
        }
    }

    public void setCanUse(bool isSet)
    {
        if (ImgCantUse != null)
        {
            //ImgCantUse.gameObject.SetActive(!isSet);
        }
    }

}
