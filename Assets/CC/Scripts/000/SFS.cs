﻿using System;
using Sfs2X;
using Sfs2X.Core;
using UnityEngine;
using Sfs2X.Requests;
using UnityEngine.SceneManagement;
using Sfs2X.Entities.Data;
using System.Collections.Generic;
using System.Collections;
using Facebook.Unity;
using Sfs2X.Entities;

public class SFS : MonoBehaviour {
//	[SerializeField]
//	private string HOST = "sfs.winplay.vn";

//	private string HOST = "125.212.225.89";
//	private string HOST = "192.168.0.13";
//	private string HOST = "127.0.0.1";

	private string HOST{
		get{ return API.DomainSFS; }
	}
	private int PORT
	{
		get{ return API.PortSFS; }
	}
    [SerializeField]
	private const string ZONE_NAME = "GameTet";

    private static SFS instance;

    private SmartFox sfs;

	public static SFS Instance
	{
		get{
			if (instance == null) {
				GameObject obj = new GameObject ("SFS");
				instance = obj.AddComponent<SFS> ();
			}
			return instance;
		}
	}


	void Awake()
    {
		Application.runInBackground = true;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;

				if (instance != null && instance != this)
						Destroy (gameObject);

				instance = this;

				DontDestroyOnLoad (gameObject);
#if UNITY_IOS
		Application.targetFrameRate = 60;
#endif

    }

	void Update () {
        if (sfs != null)
            sfs.ProcessEvents();

	}

    void OnApplicationQuit()
    {
        if (sfs != null && sfs.IsConnected)
        {
            sfs.RemoveAllEventListeners();
            sfs.Disconnect();
        }
    }
	public void Ping()
	{
//		Debug.Log ("PING!!!");
		sfs.Send (new PingPongRequest ());

	}
    public void KEEP_CONNECT()
    {
        GamePacket gp = new GamePacket("9999");
        gp.Put("a", "");
        SendZoneRequest(gp);
    }
	public void LogOut()
	{
		sfs.Send (new LogoutRequest ());
	}
    public void UpdateUserName(string uName)
    {
        GamePacket gp = new GamePacket("uun");
        gp.Put("uN", uName);
        SendZoneRequest(gp);
    }
    internal void RequestHuyKetBan(string uId)
    {
        GamePacket gp = new GamePacket(CommandKey.UNFRIEND_FRIEND_REQUEST);
        gp.Put(ParamKey.USER_ID, uId);
        SendZoneRequest(gp);
    }
    public void RequestAddFriend(string userId)
    {
        GamePacket gp = new GamePacket(CommandKey.ADD_FRIEND_REQUEST);
        gp.Put(ParamKey.USER_ID, userId);
        SendZoneRequest(gp);
    }
    public void GetListFriend()
    {
        GamePacket gp = new GamePacket(CommandKey.GET_LIST_FRIEND);
        SendZoneRequest(gp);
    }
    public void AddFriendConfirm(string uId,int status)
    {
        GamePacket gp = new GamePacket(CommandKey.ADD_FRIEND_CONFIRM);
        gp.Put(ParamKey.USER_ID, uId);
        gp.Put("grs", status);
        SendZoneRequest(gp);
    }
    public void RequestJoinChatRoom()
    {        
        GamePacket gp = new GamePacket(CommandKey.JOIN_CHAT_ROOM);
        SendZoneRequest(gp);
    }
	public void RequestJoinGameLobbyRoom(int gid)
	{
		GameHelper.currentGid = gid;

		if (gid == (int)GAMEID.TaiXiu)
        {
			//GamePacket gp = new GamePacket(CommandKey.JOIN_LUCKY_GAME);
			GamePacket gp = new GamePacket("jlkv2");
			SendZoneRequest(gp);
		}else if(gid == (int)GAMEID.BauCua)
        {
            //GamePacket gp = new GamePacket(CommandKey.JOIN_BAU_CUA_GAME);
            GamePacket gp = new GamePacket("jbcv2");
            SendZoneRequest(gp);
        }
        else if (gid == (int)GAMEID.Lottery)
        {
            GamePacket gp = new GamePacket(CommandKey.JOIN_LOTTERY_GAME);
            SendZoneRequest(gp);
        }
        else if (gid == (int)GAMEID.TaiXiuMini)
        {
            GamePacket gp = new GamePacket(LuckyMiniCommandKey.JOIN_LUCKY_MINi_GAME);
            SendZoneRequest(gp);
        }
        else if(gid == (int)GAMEID.DuaNgua)
        {
            GamePacket gp = new GamePacket(CommandKey.JOIN_RACING_HORSE_GAME);
            SendZoneRequest(gp);
        }      
        else
        {
            if (MyInfo.tournamentStatus == TournamentStatus.None)
            {
                GamePacket param = new GamePacket(CommandKey.JOIN_GAME_LOBBY_ROOM);
                param.Put(ParamKey.GAME_ID, gid);
                SendZoneRequest(param);
            }
            else//dang tham gia tournament
            {
                //if (GameHelper.gameKind == GameKind.All)
                //{
                //    GameHelper.ChangeScene(GameScene.HomeScene);
                //}
                //else
                //{
                    GameHelper.ChangeScene(GameScene.HomeSceneV2);
                //}
            }
    }
	}
    void OnDestroy()
    {
				if (sfs == null)
						return;
        sfs.RemoveAllEventListeners();
        sfs.Disconnect();
    }

	public void InitSmartfox(bool _isOnline = true)
    {
        sfs = new SmartFox(true);

        GameHelper.bugState = "Server";
        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLoggin);
        sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLogginError);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoomResponse);
        sfs.AddEventListener(SFSEvent.USER_ENTER_ROOM, OnEnterRoomResponse);
        sfs.AddEventListener(SFSEvent.USER_EXIT_ROOM, OnExitRoomResponse);
        sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomErrorResponse);
        sfs.AddEventListener (SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);
        sfs.AddEventListener(SFSEvent.PRIVATE_MESSAGE, OnPrivateMessage);
        sfs.AddEventListener(SFSEvent.MODERATOR_MESSAGE, OnModeratorMessage);
        sfs.AddEventListener (SFSEvent.ADMIN_MESSAGE, OnAdminMessage);
		sfs.AddEventListener (SFSEvent.LOGOUT, OnLogOut);
		sfs.AddEventListener (SFSEvent.PING_PONG, OnPingPong);  

		if (_isOnline)
        {
			Debug.Log (HOST + " - " + PORT);
//			sfs.Connect (HOST, PORT);
			sfs.Connect (HOST, PORT);
		}
        else
        {
//			sfs.Connect ("192.168.0.101", 8989);
			Debug.Log("*****************************");
            //sfs.Connect("192.168.0.100", 8989);
            //sfs.Connect("192.168.1.4", 9933);
            sfs.Connect("127.0.0.1", 9933);
            //sfs.Connect("35.198.197.44", 9933);
        }
    }

    

    private void OnExitRoomResponse(BaseEvent evt)
    {
        Debug.Log("OnExitRoomResponse");
        Room room = (Room)evt.Params["room"];
        if (room.Name.Equals(chatRoomName))
        {
            User user = (User)evt.Params["user"];
            string uName = user.Name;
            GameObject chatcontroller = GameObject.FindGameObjectWithTag("ChatController");
            if(chatcontroller!=null) chatcontroller.SendMessage("OnExitChatRoom", uName);
        }else if (room.Name.Equals(MyInfo.BangChatRoomName))
        {
            GamePacket gp = new GamePacket("requestJoinClanRoom");
            SendZoneRequest(gp);
        }
    }

    private void OnEnterRoomResponse(BaseEvent evt)
    {
        Debug.Log("OnEnterRoomResponse");
        Room room = (Room)evt.Params["room"];
        if (room.Name.Equals(chatRoomName))
        {
            User user = (User)evt.Params["user"];
            string uName = user.Name;
            string avatar = user.GetVariable("avatar").GetStringValue();
            int avatarBorder = user.GetVariable("avatar_border").GetIntValue();
            Hashtable dataRes = new Hashtable();
            dataRes.Add("uN", uName);
            dataRes.Add("avatar", avatar);
            dataRes.Add("avatarborder", avatarBorder);
            dataRes.Add("uid", user.GetVariable("uid").GetStringValue());
            dataRes.Add("sfsid", user.Id);
            GameObject chatcontroller = GameObject.FindGameObjectWithTag("ChatController");
            if (chatcontroller != null) chatcontroller.SendMessage("OnEnterChatRoom", dataRes);
            //GameObject.FindGameObjectWithTag("ChatController").SendMessage("OnEnterChatRoom", dataRes);
        }
    }

    private void OnJoinRoomResponse(BaseEvent evt)
    {
        Room room = (Room) evt.Params["room"];
        string groupId = room.GroupId;
        if (groupId == "Clan")
        {
            MyInfo.BangChatRoom = room;
            MyInfo.BangChatRoomName = room.Name;
        }
        else
        {
            if (room.Name.Equals(chatRoomName))
            {
                MyInfo.ChatRoom = room;
            }
        }
    }

    private void OnJoinRoomErrorResponse(BaseEvent evt)
    {
        
    }

    GameObject objModule;
	void OnPingPong (BaseEvent evt)
	{
		if (objModule == null)
			objModule = GameObject.FindGameObjectWithTag ("Module");

		if (objModule != null)
			objModule.SendMessage ("ShowPing", evt.Params ["lagValue"].ToString ());
//		Debug.Log ("OnPingPong");
//		foreach (string key in evt.Params.Keys)
//			Debug.Log (key);
//		Debug.Log (evt.Type);
//		Debug.Log (evt.Params ["lagValue"].ToString ());
	}

    public bool isRelogin = false;
	void OnLogOut (BaseEvent evt)
	{
        isRelogin = true;
        GameHelper.ChangeScene (GameScene.LoginScene);
	}

    private void OnConnection(BaseEvent evt)
    {
        Debug.Log("OnConnection");
		if ((bool)evt.Params ["success"])
        {
            SFSObject loginParam = SFSObject.NewInstance();
            LoginType selectLoginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
            if (selectLoginType == LoginType.Facebook)
            {
                loginParam.PutUtfString("fbtoken",AccessToken.CurrentAccessToken.TokenString);
                sfs.Send(new LoginRequest(MyInfo.ID, MyInfo.TOKEN, ZONE_NAME, loginParam));
            }
            else
            {
                sfs.Send(new LoginRequest(MyInfo.ID, MyInfo.TOKEN, ZONE_NAME));
            }
		}
		else
        {
			LoadingManager.Instance.ENABLE = false;
			Debug.Log ("Connect error, reason: " + evt.Params ["errorMessage"]);
		}


    }

    private void OnConnectionLost(BaseEvent evt)
    {
        foreach (string key in evt.Params.Keys)
            Debug.Log(key + " : " + evt.Params[key]);

        print("Connect error, reason: " + evt.Params["reason"]);
        //GameObject.FindGameObjectWithTag ("GameController").SendMessage ("OnConnectionLost");
        isCheckDisconnect = false;
        GamePacket param = new GamePacket("recheckDisconnect");
        SendRoomRequest(param);
        StartCoroutine(CheckDisConnect());
              

    }

    IEnumerator CheckDisConnect()
    {
        yield return new WaitForSeconds(3);
        if (!isCheckDisconnect)
        {
            Debug.LogError("checkDisconnect=1");
            GamePacket param = new GamePacket("recheckDisconnect");
            SendRoomRequest(param);
            yield return new WaitForSeconds(3);
            if (!isCheckDisconnect)
            {
                Debug.LogError("checkDisconnect=2");
                GameObject[] GameControllers = GameObject.FindGameObjectsWithTag("GameController");
                for (int i = 0; i < GameControllers.Length; i++)
                    if (GameControllers[i] != null) GameControllers[i].SendMessage("OnConnectionLost");
                LoadingManager.Instance.ENABLE = false;
            }
        }

    }

    private void OnLoggin(BaseEvent evt)
    {
		Debug.Log ("OnLogin");
		sfs.EnableLagMonitor (true);

        MyInfo.SFS_ID = ((Sfs2X.Entities.User)evt.Params["user"]).Id;

        GameObject.FindGameObjectWithTag("GameController").SendMessage("OnLoginSFS");

        print("Login Success with sfs id: " + MyInfo.SFS_ID);
    }

    private void OnLogginError(BaseEvent evt)
    {
		Debug.Log ("Loggin fail");
        print("Connect error, reason: " + evt.Params["errorMessage"]);
    }

    public void SendBoomRequest(GamePacket packet)
    {
        Debug.Log("RequestBoom: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }

    bool isCheckDisconnect = false;
    private void OnExtensionResponse(BaseEvent evt)
    {
        string cmd = (string)evt.Params["cmd"];
        SFSObject param = (SFSObject)evt.Params["params"];
		Debug.Log ("cmd_" + cmd + " - " + param.ToJson ());
        GamePacket packet = new GamePacket(cmd, param);
        isCheckDisconnect = true;// có command trả về là còn có connect với server
        switch (cmd)// command ngoài game.
        {
            case CommandKey.ADD_FRIEND_CONFIRM:
            case CommandKey.ADD_FRIEND_IGNORE:
            case CommandKey.ADD_FRIEND_NOTICE:
            case CommandKey.ADD_FRIEND_REQUEST:
            case CommandKey.ADD_FRIEND_RESULT:
            case CommandKey.ADD_FRIEND_SUCCESS:
            case CommandKey.GET_LIST_FRIEND:
            case CommandKey.JOIN_CHAT_ROOM:
            case CommandKey.UNFRIEND_FRIEND_REQUEST:
            case CommandKey.POST_NEW:
            case CommandKey.GET_POST_NEW:
            case CommandKey.GET_CLAN_LIST:
            case CommandKey.REQUEST_JOIN_CLAN:
            case CommandKey.REQUEST_LEAVE_CLAN:
            case CommandKey.GET_LIST_THANH_VIEN:
            case CommandKey.GET_LIST_CHO_DUYET:
            case CommandKey.GET_CLAN_INFO:
            case CommandKey.SET_UP_CLAN:
            case CommandKey.REQUEST_REQUIRE_JOIN:
            case CommandKey.UPGRADE_DEPUTY:
            case CommandKey.DOWN_GRADE_DEPUTY:
            case CommandKey.KICK_MEMBER_BANG_HOI:
            case CommandKey.HELP_MEMBER_BANGHOI:
            case CommandKey.RECEIVE_HELP_BANGHOI:
            case CommandKey.GET_LIST_THANH_VIEN_CUU_TRO:
            case CommandKey.REJECT_CUU_TRO:
            case CommandKey.REQUEST_CUU_TRO:
            case CommandKey.CONTRIBUTE_CLAN:
            case "requestClanHep":
            case "requestClanContribute":
            case "godOfGamblerInit":
            case "guessGodOfGambler":
            case "guessGodGamblerHistory":
            case "getMatchCompetitor":
            case "getPlayerToPlayboard":
            case "setPlayerWin":
            case "bettingEventMatch":
            case "getBettingMatchHistory":
            case "getBackUpChat":
            case "getBackUpClanChat":
            case "withdrawBank":
            case "mainWithdrawBank":
            case "mainDepositBank":
            case CommandKey.REFRESH:
            case "getClanWarInfo":
            case "sendClanDuel":
            case "receiveClanDue":
            case "responseClanDue":
            case "cancelClanDue":
            case "getClanWarTime":
            case "noticeClanWarMatch":
            case "endClanWarMatch":
            case "fightClanDue":
            case "startClanWarMatch":
            case "cancelClanDueCauseNotFight":
            case "getClanWarHistory":
            case "clanWarMatchPause":
            case "matchPause":
            case "noticeUserRejoin":
                GameObject[] ChatController = GameObject.FindGameObjectsWithTag("ChatController");
                for (int j = 0; j < ChatController.Length; j++)
                    if (ChatController[j] != null) ChatController[j].SendMessage("OnChatResponse", packet);
                return;
        }
        
        GameObject[] GameControllers = GameObject.FindGameObjectsWithTag("GameController");
        if (cmd == CommandKey.GET_GAME_ROOM_INFO||cmd == CommandKey.NOTICE_JOIN_GAME_ROOM)
        {
            Room roomgame = (Room)evt.Params["room"];
            List<User> lstUser = roomgame.UserList;
            //= (User)evt.Params["user"];
            List<Hashtable> lstDataRes = new List<Hashtable>();
            int index = 0;
            foreach (User user in lstUser)
            {
                Hashtable dataRes = new Hashtable();
                dataRes.Add("uid", user.GetVariable("uid").GetStringValue());
                if(user.ContainsVariable("clan_id")) dataRes.Add("clanid", user.GetVariable("clan_id").GetStringValue());
                else dataRes.Add("clanid", "");
                if (user.ContainsVariable("ringid")) dataRes.Add("ringid", user.GetVariable("ringid").GetStringValue());
                else dataRes.Add("ringid", "");
                if (user.ContainsVariable("gender")) dataRes.Add("gender", user.GetVariable("gender").GetIntValue());
                else dataRes.Add("gender", -1);
                if (user.ContainsVariable("partner")) dataRes.Add("partner", user.GetVariable("partner").GetStringValue());
                else dataRes.Add("partner", "");
                lstDataRes.Add(dataRes);
            }            
            for (int i = 0; i < GameControllers.Length; i++)
                if (GameControllers[i] != null) GameControllers[i].SendMessage("OnVariableResponse", lstDataRes);
        }

        //GameObject[] GameControllers = GameObject.FindGameObjectsWithTag("GameController");
        for(int i = 0; i < GameControllers.Length; i++)
            if(GameControllers[i] !=null) GameControllers[i].SendMessage("OnSFSResponse", packet);
        GameObject Lottery = GameObject.FindGameObjectWithTag("Lottery");
        if (Lottery != null) Lottery.SendMessage("OnSFSResponse", packet);

        

        
    }
    public void GetUserBetHorse(GamePacket packet)
    {
        Debug.Log("RequestZone: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }
    public void SendZoneRequest(GamePacket packet)
    {
        Debug.Log("RequestZone: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }

    public void SendPirateAttackHistoryRequest(GamePacket packet)
    {
        Debug.Log("RequestZone: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }

    public void SendRoomBangHoiRequest(GamePacket packet)
    {
        Debug.LogError("RequestRoom: " + packet.cmd + " _ " + packet.param.ToJson() + " - " + MyInfo.BangChatRoom);
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param, MyInfo.BangChatRoom));
    }
    public void SendRoomChatRequest(GamePacket packet)
    {
        Debug.LogError("RequestRoom: " + packet.cmd + " _ " + packet.param.ToJson() + " - " + MyInfo.ChatRoom);
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param, MyInfo.ChatRoom));
    }
    public void SendRoomRequest(GamePacket packet)
    {
        Debug.LogError("RequestRoom: " + packet.cmd + " _ " + packet.param.ToJson() + " - "+ sfs.LastJoinedRoom);
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param, sfs.LastJoinedRoom));
    }
    public void GetMissionRequest(GamePacket packet)
    {
        Debug.Log("GetMissionRequest: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }
    public void ClaimMissionRequest(GamePacket packet)
    {
        Debug.Log("ClaimMissionRequest: " + packet.cmd + " _ " + packet.param.ToJson());
        sfs.Send(new ExtensionRequest(packet.cmd, packet.param));
    }
	public void SendZoneRefreshData()
	{
		sfs.Send(new ExtensionRequest(CommandKey.REFRESH, new SFSObject()));
	}
   
    const string chatRoomName = "chatroom";
    public void SendPrivateMessageChatRoom(string _msg, int idSFS, string uName, int tab, int idReceive, string IdUser)
    {

        SFSObject param = new SFSObject();
        param.PutInt("idSFS", idSFS);
        param.PutInt("tab", tab);
        param.PutUtfString("uName", uName);
        param.PutUtfString("userId", IdUser);
        param.PutInt("idReceive", idReceive);
        sfs.Send(new PrivateMessageRequest(_msg, idReceive, param));

    }
    public void SendPublicChatBang(int vip, string _msg, int idSFS, string uName, int tab, string IdUser)
    {
        Debug.LogError("=SendChatBangToServer=========" + _msg);
        SFSObject param = new SFSObject();
        param.PutInt("idSFS", idSFS);
        param.PutInt("tab", tab);
        param.PutUtfString("uName", uName);
        param.PutUtfString("userId", IdUser);
        param.PutInt("vip", vip);
        sfs.Send(new PublicMessageRequest(_msg, param, MyInfo.BangChatRoom));
    }
    public void SendPublicChatRoom(int vip, string _msg, int idSFS, string uName, int tab, string IdUser)
    {
        Debug.LogError("=sendchattoserver========="+_msg);
        SFSObject param = new SFSObject();
        param.PutInt("idSFS", idSFS);
        param.PutInt("tab", tab);
        param.PutUtfString("uName", uName);
        param.PutUtfString("userId", IdUser);
        param.PutInt("vip", vip);       
        sfs.Send(new PublicMessageRequest(_msg, param, MyInfo.ChatRoom));  
    }
	public void SendPublicMsg(string _msg, int type = 0)//0 default ; 1 là chat tự nhập rào trường hợp ko cho vip < 3 thấy
	{
        if (type == 0)
        {
            sfs.Send(new PublicMessageRequest(_msg, new SFSObject(), sfs.LastJoinedRoom));
        }else
        {
            SFSObject sfsObj = new SFSObject();
            sfsObj.PutInt("vip", 1);
            sfs.Send(new PublicMessageRequest(_msg, sfsObj, sfs.LastJoinedRoom));
        }
		
	}
    void OnModeratorMessage(BaseEvent evt)
    {
        string msg = (string)evt.Params["message"];
        GameObject.FindGameObjectWithTag("GameLevelUp").SendMessage("OnModeratorMessage", msg);
        GameObject.FindGameObjectWithTag("GameController").SendMessage("OnModeratorMessage", msg);
    }    
    void OnPrivateMessage(BaseEvent evt)
    {       
        GameObject.FindGameObjectWithTag("ChatController").SendMessage("OnChatFriendResponse", evt.Params);       
    }
    void OnPublicMessage (BaseEvent evt)
	{
		string msg = (string)evt.Params["message"];
        Room room = (Room)evt.Params["room"];
        //User sender = (User)evt.Params["sender"];
        if (room.Name.Equals(chatRoomName))
        {
            GameObject chatcontroller = GameObject.FindGameObjectWithTag("ChatController");
            if (chatcontroller != null)
            {
                chatcontroller.SendMessage("ResponseChatFromServer", evt.Params);
            }
            
        }else if (room.Name.Equals(MyInfo.BangChatRoomName))
        {
            GameObject chatcontroller = GameObject.FindGameObjectWithTag("ChatController");
            if (chatcontroller != null)
            {
                chatcontroller.SendMessage("ResponseChatBangFromServer", evt.Params);
            }
        }
        else
        {
            SFSObject data = (SFSObject)evt.Params["data"];
            int vipchat = data.GetInt("vip");
            if (vipchat == 1)
            {
                if (MyInfo.MY_ID_VIP>=MyInfo.VIP_CHAT_ALLOW)
                {
                    GameObject[] GameControllers = GameObject.FindGameObjectsWithTag("GameController");
                    for (int i = 0; i < GameControllers.Length; i++)
                        if (GameControllers[i] != null) GameControllers[i].SendMessage("OnPublicMsg", msg);
                }
            }else
            {
                GameObject[] GameControllers = GameObject.FindGameObjectsWithTag("GameController");
                for (int i = 0; i < GameControllers.Length; i++)
                    if (GameControllers[i] != null) GameControllers[i].SendMessage("OnPublicMsg", msg);
            }

        }
	}
    
	void OnAdminMessage (BaseEvent evt)
	{
		string msg = (string)evt.Params ["message"];
		string[] s = msg.Split ('#');

		int loop = int.Parse (s [0]);


		PopupNotifyManager.Instance.Show (loop, s[1]);
	}

    public void ClientOnAdminMessage(int loopCount, string content)
    {
        PopupNotifyManager.Instance.Show(loopCount, content);
    }
}
