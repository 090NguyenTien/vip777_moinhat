﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using SimpleJSON;
using System.Linq;
using Sfs2X.Entities.Data;
using BaseCallBack;

public class LobbyController : MonoBehaviour {

    [SerializeField]
    LobbyView View;
    [SerializeField]
    PopupCreateRoom popupCreateRoom;
    [SerializeField]
    PopupAlertManager popupAlert;
    [SerializeField]
    PopupInvited popupInvited;
    [SerializeField]
    PopupAchivementManager Achivement;
    [SerializeField]
    PopupJoinGameManager popupJoinGame;
    [SerializeField]
	LobbyDataHelper lobbyDataHelper;
    [SerializeField]
    PopupModuleManager Module;
    [SerializeField]
    PopupShopManager Shop;
    [SerializeField]
	PopupInfomationManager popupInfoUser;



   

    private SFS sfs{
		get{return SFS.Instance;}
	}

    int currentLevel; //1_2_3

    Dictionary<int, ItemRoomInfo> dictIdRoom;

    int currentGid;

    enum ScreenLobby
    {
        Lobby,
        Achivement,
    }
    ScreenLobby _screen;

	void PlaySound(string _sound){
		SoundManager.PlaySound (_sound);
	}

    void Awake()
    {
        Init();
    }

    bool isClickBackHome;
    void Init()
    {
        isClickBackHome = false;
        View.LOADING = true;

        currentLevel = 1;
        currentGid = GameHelper.currentGid;

        _screen = ScreenLobby.Lobby;

        dictIdRoom = new Dictionary<int, ItemRoomInfo>();

        popupCreateRoom.Init(CancelCreateRoom, OkCreateRoom, OnValueBetChange);
        View.Init(this);
        View.ClearEmptyRooms(1, 0);
        View.ClearEmptyRooms(2, 0);
        View.ClearEmptyRooms(3, 0);

        SetEnablePanelLevel(1);

        ShowNameLobby();

        //RqRoomInfo(3);
        RqRoomInfo(2);
        RqRoomInfo(1);

		View.ShowInfoUser (MyInfo.NAME,
			Utilities.GetStringMoneyByLong (MyInfo.CHIP),
			MyInfo.sprAvatar);

        popupJoinGame.Init();
        popupAlert.Init();
        popupInvited.Init(BtnCancelInvitedOnClick, BtnOkInvitedOnClick);
//        Achivement.Init(BackAchivementOnClick);
        Shop.Init(UpdateInfoUser, BtnBackShopOnClick);     
		popupInfoUser.Init (BtnSaveInfoOnClick, BtnCloseInfoOnClick);
        Module.InitLobby(null);
        //a
        View.SetEnablePanelFresher(true);

        //		API.Instance.RequestAchivementUser (GameHelper.currentGid, RspAchivementUser);
        LayThongTinBangChinhMinh();


        LoadingManager.Instance.ENABLE = false;
        MyInfo.CheckMoney(View.getTxtChipUser().gameObject, CheckMoneyAndOpenShop);

        if (MyInfo.CHIP < 1000000 && MyInfo.FIRST_PAYMENT == 0)
        {
            ShowFirstPurchasePanel();
        }

        //		sfs.SendZoneRefreshData ();
    }
    public void ShowFirstPurchasePanel()
    {
        AlertController.api.ShowFirstPurchase(() => {
            ShopOnClick();
        });
    }
    void CheckMoneyAndOpenShop()
    {
        ShopOnClick();
    }



    public void LayThongTinBangChinhMinh()
    {
        API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangChinhMinh);
    }


    void RspLayIdBangChinhMinh(string _json)
    {

        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);

        JSONNode node = JSONNode.Parse(_json);

        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

          Debug.LogWarning("usserBang------------- " + userBang);
         Debug.LogWarning("BangMaster------------- " + BangMaster);

        MyInfo.ID_BANG_MASTER = BangMaster;
        MyInfo.ID_BANG = userBang;
    }





    #region Info User

    public void BtnAvatarOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		popupInfoUser.Show ();
	}
	void BtnSaveInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		popupInfoUser.Hide ();
		View.UpdateAvatar ();
	}
	void BtnCloseInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		popupInfoUser.Hide ();
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        //Debug.LogError("KhuongTest=========="+ loginType);
        if (loginType != LoginType.Facebook)
        {
            View.UpdateAvatar();
        }
	}

	#endregion

    void BtnBackShopOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        View.SetEnablePanelUI(true);
        Shop.Hide();
        MyInfo.CheckMoney(View.getTxtChipUser().gameObject, CheckMoneyAndOpenShop);
    }

    void UpdateInfoUser()
    {
        View.UpdateChipUser(MyInfo.CHIP, MyInfo.GOLD);

		View.ShowInfoUser (MyInfo.NAME,
			Utilities.GetStringMoneyByLong (MyInfo.CHIP),
			MyInfo.sprAvatar);

    }

    #region MODULE

    public void ModuleOnClick()
    {
		//PlaySound (SoundManager.BUTTON_CLICK);
        Module.Show();
    }

    #endregion


    public void ExitOnClick()
    {
        isClickBackHome = true;
		PlaySound (SoundManager.BUTTON_CLICK);
        LoadingManager.Instance.ENABLE = true;
        sfs.SendRoomRequest(new GamePacket(CommandKey.USER_EXIT));
    }

    #region SHOP

    public void ShopOnClick(bool ByGem = false)
    {
		PlaySound (SoundManager.BUTTON_CLICK);    
        Shop.Show(ByGem);
        View.SetEnablePanelUI(false);
    }

    #endregion

    #region Achivement

    public void BtnAchivementOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        Achivement.SetEnable(true);
        SetEnableContent(false);
        _screen = ScreenLobby.Achivement;
    }
    void BackAchivementOnClick()
    {
        View.SetEnablePanelUI(true);
        Achivement.SetEnable(false);
    }
    void SetEnableContent(bool _enable)
    {
        View.SetEnablePanelUI(_enable);
    }

    #endregion

    #region INVITED

    void BtnCancelInvitedOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        popupInvited.Hide();
    }

    void BtnOkInvitedOnClick(int _roomID, string _pass)
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        if (_roomID == -1) {
            popupAlert.Show(ConstText.ErrorNoMoneyToPlay, popupAlert.Hide);
            popupInvited.Hide();
        } else {
            RqJoinGameRoom(_roomID, _pass);

            LoadingManager.Instance.ENABLE = true;
        }
    }

    void RspInvited(GamePacket _param)
    {
        string s = _param.GetString("ivn");
        string[] info = s.Split('#');

        string name = info[0];
        int roomID = int.Parse(info[1]);
        int gameID = int.Parse(info[2]);
        int roomLv = int.Parse(info[3]);
        long bet = long.Parse(info[4]);
        long betRequire = long.Parse(info[5]);
        string pass = info[6];
        popupInvited.Show(name, roomID, gameID, roomLv, bet, betRequire, pass);
    }

    #endregion

    void ShowNameLobby()
    {
        View.ShowName(lobbyDataHelper.GetSpriteNameGame((GAMEID)GameHelper.currentGid));
    }

    private void GetListRoom()
    {
        GamePacket param = new GamePacket(CommandKey.GET_PLAY_BOARD_LIST);
        param.Put(ParamKey.ROOM_LIST_LEVEL, currentLevel);
        sfs.SendRoomRequest(param);
    }

    #region Nap

    public void NapOnClick()
    {
        PlaySound(SoundManager.BUTTON_CLICK);
        ShopOnClick();
    }

    #endregion

    #region Request

    void RqRoomInfo(int _level)
    {
        GamePacket param = new GamePacket(CommandKey.GET_PLAY_BOARD_LIST);
        param.Put(ParamKey.ROOM_LIST_LEVEL, _level);

        sfs.SendRoomRequest(param);
    }

#endregion

#region Panel Fresher Junior Senior

    public void ToggleFresher(bool _enable)
    {
        SetEnablePanelLevel(_enable ? 1 : 0);
    }
    public void ToggleJunior(bool _enable)
    {
        SetEnablePanelLevel(_enable ? 2 : 0);
    }
    public void ToggleSenior(bool _enable)
    {
        SetEnablePanelLevel(_enable ? 3 : 0);
    }

    void SetEnablePanelLevel(int _level)
    {
        View.SetEnablePanelFresher(_level == 1);
        View.SetEnablePanelJunior(_level == 2);
       
        currentLevel = _level;
        //if(currentLevel==2) View
    }

#endregion

#region Room List

    void AddRoom(ItemRoomInfo _room)
    {
        if (dictIdRoom.ContainsKey(_room.ID))
            dictIdRoom[_room.ID] = _room;
        else
            dictIdRoom.Add(_room.ID, _room);
    }

    private void LoadRoomsList(GamePacket param)
    {
        //Debug.LogError("LoadRoomsList======param" + param.ToString());        
        //set level
        ISFSObject LevelData = param.GetSFSObject("level");
        //Debug.LogError("sLevelData" + LevelData);
        JSONNode node = JSONNode.Parse(LevelData.ToJson());
        MyInfo.CUR_LEVEL = node["cur_level"].AsInt;
        MyInfo.CUR_EXP = node["exp"].AsInt;
        MyInfo.MIN_EXP = node["min_exp"].AsInt;
        MyInfo.MAX_EXP = node["max_exp"].AsInt;

        Debug.Log("------ node[cur_leve];     " + View.GetTxtLevel());

        View.GetTxtLevel().text = node["cur_level"];
        //end
        //long chip = param.GetLong(ParamKey.CHIP);
        //MyInfo.CHIP = chip;
        View.ShowChip(MyInfo.CHIP);

        int level = param.GetInt(ParamKey.ROOM_LIST_LEVEL);
        string roomsListInfo = param.GetString(ParamKey.PLAY_BOARD_LIST);
      
        string[] listInfo = roomsListInfo.Split('$');

        View.ClearEmptyRooms(level, 0);

        List<RoomInfo> lstRoom = new List<RoomInfo>();
        for (int i = 0; i < listInfo.Length; i++)
        {
            
            RoomInfo roomInfo = new RoomInfo(listInfo[i]);

            lstRoom.Add(roomInfo);
            //int roomId = int.Parse(roomInfo[0]);
            //long bettingMoney = long.Parse(roomInfo[1]);
            //long require = long.Parse(roomInfo[2]);
            //int playerNumber = int.Parse(roomInfo[3]);
            //bool isPass = roomInfo.Equals("1");
            //string host = roomInfo[5];

        }
        lstRoom = lstRoom.OrderByDescending(x => x.playerNumber).ToList();
        for (int i = 0; i < lstRoom.Count; i++)
        {

            RoomInfo roomInfo = lstRoom[i];

            ItemRoomInfo room = new ItemRoomInfo(currentLevel, roomInfo.roomId, roomInfo.bettingMoney, roomInfo.require, roomInfo.playerNumber, GameHelper.currentGid, roomInfo.isPass, roomInfo.host,roomInfo.isPlaying);

            AddRoom(room);

            if (level == 1)
                View.AddItemRoomFresherView(i, room, roomInfo.isPass, ItemRoomOnClick);
            else if (level == 2)
                View.AddItemRoomJuniorView(i, room, roomInfo.isPass, ItemRoomOnClick);
            
        }
    }

    void ItemRoomOnClick(int _roomID, bool _isPlaying=false) 
    {
        if (_isPlaying)
        {
            popupAlert.Show(ConstText.RoomIsPlaying, () => {
                HidePopupAlert();
                popupJoinGame.Hide();
                popupInvited.Hide();
                RqRoomInfo(currentLevel);
            });
        }
        else
        {
            if (dictIdRoom.ContainsKey(_roomID))
            {
                if (MyInfo.CHIP < dictIdRoom[_roomID].ChipRequire)
                    popupAlert.Show(ConstText.ErrorNoMoneyToPlay, popupAlert.Hide);
                else
                {
                    if (dictIdRoom[_roomID].IsPass)
                        popupJoinGame.Show(dictIdRoom[_roomID].ID, dictIdRoom[_roomID].Bet, dictIdRoom[_roomID].Host, RqJoinGameRoom);
                    else
                    {
                        RqJoinGameRoom(_roomID);

                    }
                }
            }
            else
                RqJoinGameRoom(_roomID);
        }

        
    }

#endregion

#region CreateRoom

    void OnValueBetChange(int _indexValue)
    {
        //long betCurrent = ValueBet(currentLevel, (int)_indexValue);
        long betCurrent = ValueBet(4, (int)_indexValue);
        popupCreateRoom.ShowBetCurrent(Utilities.GetStringMoneyByLong(betCurrent));
    }
    public void CreateRoomOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        popupCreateRoom.Show();

        //long betCurrent = ValueBet(currentLevel, 0); 
        long betCurrent = ValueBet(4, 0);
        popupCreateRoom.ShowBetCurrent(Utilities.GetStringMoneyByLong(betCurrent));
    }
    void CancelCreateRoom()
    {
        popupCreateRoom.Hide();
    }
    void OkCreateRoom(string _password, int _indexValueBet)
    {
        //RqCreateRoom(currentLevel, _password, ValueBet(currentLevel, _indexValueBet));
        RqCreateRoom(currentLevel, _password, ValueBet(4, _indexValueBet));
    }

    void RqCreateRoom(int _level, string _password, long _bet)
    {
        GamePacket param = new GamePacket(CommandKey.CREATE_ROOM);
        param.Put(ParamKey.ROOM_LIST_LEVEL, _level);
        param.Put(ParamKey.BET_MONEY, _bet);
        param.Put(ParamKey.ROOM_PASSWORD, _password);

        sfs.SendRoomRequest(param);

        LoadingManager.Instance.ENABLE = true;
    }

#endregion

#region JoinRoom

    public void QuickGameOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        RqQuickGame();
    }
    void RqQuickGame()
    {
        LoadingManager.Instance.ENABLE = true;
        GamePacket param = new GamePacket(CommandKey.JOIN_QUICK_GAME);
        param.Put(ParamKey.ROOM_LIST_LEVEL, currentLevel);
        sfs.SendRoomRequest(param);

    }

    private void RqJoinGameRoom(int roomId, string _pass = "")
    {
        Debug.Log("PASS: " + _pass);
        GamePacket param = new GamePacket(CommandKey.JOIN_GAME_ROOM);

        param.Put(ParamKey.ROOM_ID, roomId);

        if (!string.IsNullOrEmpty(_pass))
            param.Put(ParamKey.ROOM_PASSWORD, _pass);

        sfs.SendRoomRequest(param);

        LoadingManager.Instance.ENABLE = true;
    }

    //		private void JoinGameRoom(int _gameID)
    //		{
    //				switch (_gameID) {
    //				case 5:
    //						GameHelper.ChangeScene (GameScene.TLMNScene);
    //						break;
    //				case 1:
    //						GameHelper.ChangeScene (GameScene.XTScene);
    //						break;
    //				case 2:
    //						GameHelper.ChangeScene (GameScene.MBScene);
    //						break;
    //				case 4:
    //						GameHelper.ChangeScene (GameScene.PhomScene);
    //						break;
    //				}
    //		}


#endregion

#region Join Room by RoomID

    public void BtnEnterOnClick(string _roomID)
    {
        if (string.IsNullOrEmpty(_roomID))
            return;

        _roomID = _roomID.Replace(" ", "");

        Debug.Log("Text: " + _roomID);

        int idRoom;
        bool isInt = int.TryParse(_roomID, out idRoom);

        if (isInt)
            ItemRoomOnClick(idRoom);
        else
            Debug.Log("RoomID must be number");
    }

#endregion

    public void BtnRefreshOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        RqRoomInfo(currentLevel);
        View.LOADING = true;
    }

    public void OnSFSResponse(GamePacket param)
    {
        Debug.Log("RSP - Lobby - CMD _ " + param.cmd + " - " + param.param.ToJson());

        switch (param.cmd) {
            case CommandKey.GET_PLAY_BOARD_LIST:
                LoadRoomsList(param);
                View.LOADING = false;
                break;
            case CommandKey.CREATE_ROOM:
                GameHelper.ChangeScene(currentGid);
                break;
            case CommandKey.JOIN_GAME_ROOM:
                GameHelper.ChangeScene(currentGid);
                break;
            case CommandKey.JOIN_QUICK_GAME:
                break;
            case CommandKey.INVITE:
                RspInvited(param);
                break;
            case CommandKey.ERROR:
                RspError(param);
                LoadingManager.Instance.ENABLE = false;
                break;
		    case CommandKey.USER_EXIT:
				    if(isClickBackHome) GameHelper.ChangeScene (GameScene.HomeSceneV2);
			    break;
		    case CommandKey.REFRESH:
			    RspRefreshUserInfo (param);
                break;
        }
    }

	void RspRefreshUserInfo (GamePacket param)
	{
//		Debug.LogError ("Rsp Refresh");
		MyInfo.CHIP = param.GetLong (ParamKey.CHIP);
		UpdateInfoUser ();

	}

    void RspError(GamePacket _param)
    {
        int reason = _param.GetInt(ParamKey.REASON);

        switch (reason) {
            case 1:
                popupAlert.Show(ConstText.ErrorRoomFull, () => {
                    HidePopupAlert();
                    popupJoinGame.Hide();
                    popupInvited.Hide();
                    RqRoomInfo(currentLevel);
                });

                popupJoinGame.Hide();
                break;
            case 2:
                popupAlert.Show(ConstText.ErrorRoomNotExist, () => {
                    HidePopupAlert();
                    popupJoinGame.Hide();
                    popupInvited.Hide();
                    RqRoomInfo(currentLevel);
                });
                popupJoinGame.Hide();
                break;
            case 3:
                if (popupCreateRoom.IsEnable)
                    popupCreateRoom.ShowError(ConstText.ErrorNoMoneyToPlay);
                else {
                    popupAlert.Show(ConstText.ErrorNoMoneyToPlay, () => {
                        HidePopupAlert();
                        popupJoinGame.Hide();
                        popupInvited.Hide();
                        RqRoomInfo(currentLevel);
                    });
                }
                popupJoinGame.Hide();
                break;
            case 4:
                popupAlert.Show(ConstText.ErrorCantFindRoom, () => {
                    HidePopupAlert();
                    popupJoinGame.Hide();
                    popupInvited.Hide();
                    RqRoomInfo(currentLevel);
                });
                popupJoinGame.Hide();
                break;
            case 5:
                popupJoinGame.ShowError(ConstText.PasswordNotRight);
                break;
            case 6:
                popupAlert.Show(ConstText.RoomIsPlaying, () => {
                    HidePopupAlert();
                    popupJoinGame.Hide();
                    popupInvited.Hide();
                    RqRoomInfo(currentLevel);
                });
                break;
        }
    }
    void HidePopupAlert()
    {
        popupAlert.Hide();
    }

    public void OnConnectionLost()
    {
        popupAlert.Show(ConstText.ErrorConnection, () => {
            GameHelper.ChangeScene(GameScene.LoginScene);
        });
    }

	void OnApplicationPause(bool _pause){
		if (!_pause) {
			sfs.SendZoneRefreshData ();
		}
	}

#region Utils

    long ValueBet(int _level, int _valueIndex)
    {
        int minLevel1 = 1000000;
        int minLevel2 = 1000000;

        int minLevel3 = 11000000;

		int minLevel4 = 100000000;

        if (_level == 1)
        {
            if (_valueIndex == 0)
            {
                return minLevel1;
            }
            else if (_valueIndex <= 99)
            {
                int part = 1000000;//10.000
                return minLevel1 + part * _valueIndex;//keo 1 cai 5000
            }
            else
            {
                return minLevel2;
            }
        }
        else if (_level == 2)
        {
            if (_valueIndex == 0)
            {
                return minLevel2;
            }
            else if (_valueIndex <= 99)
            {
                int part = 1000000;//100.000
                return minLevel2 + part * _valueIndex;//keo 1 cai 10.000
            }
            else
            {
                return minLevel3;
            }

        }
        else if (_level == 3)
        {
            if (_valueIndex == 0)
            {
                return 200000;
            }
            else
            {
                int part = (minLevel4 - minLevel3) / 100;
                return minLevel3 + part * _valueIndex;
            }
        }
        else if (_level == 4)
        {
            if (_valueIndex == 0)
            {
                return 100000;
            }else if(_valueIndex == 1)
            {
                return 500000;
            }
            else if (_valueIndex == 2)
            {
                return 1000000;
            }
            else if (_valueIndex == 3)
            {
                return 5000000;
            }
            else if (_valueIndex == 4)
            {
                return 10000000;
            }
            else if (_valueIndex>=98)
            {
                return 100000000;
            }
            else
            {
                int part = (minLevel4 - minLevel3) / 100;
                return minLevel3 + part * _valueIndex;
            }
        }
        return 0;
    }

#endregion
}
