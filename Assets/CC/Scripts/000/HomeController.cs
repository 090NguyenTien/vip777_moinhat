﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BaseCallBack;
using SimpleJSON;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using Sfs2X.Entities.Data;

public class HomeController : MonoBehaviour
{
    /*
		[SerializeField]
		HomeView View;

    [SerializeField]
    GameObject PanelPopup;

    [SerializeField]
	PopupAlertManager popupAlert;
	[SerializeField]
	PopupFullManager popupFull;
		[SerializeField]
		PopupInvited popupInvited;
	[SerializeField]
	PopupAchivementManager Achivement;
//	[SerializeField]
//	PopupNotifyManager Notify;
	[SerializeField]
	PopupInfomationManager InfoUser;
	private SFS sfs{
		get{ return SFS.Instance; }
	}
	[SerializeField]
	PopupModuleManager Module;
	[SerializeField]
	PopupShopManager Shop;
    [SerializeField]
    PopupLotteryManager Lottery;
    [SerializeField]
	PopupEventManager Event;
    [SerializeField]
    public PopupInboxManager Inbox;
    [SerializeField]
    PopupVipManager Vip;
    [SerializeField]
	PopupCardWheelManager CardWheel;
	[SerializeField]
	PopupExchangeManager Exchange;

	[SerializeField]
	PopupArenaManager Arena;

    [SerializeField]
    private PopupSpecialWheelManager specWheel = null;
    [SerializeField]
    private PopupVideoReward videoReward = null;
    [SerializeField]
    Button BtnLottery;

    [SerializeField]
    private GiftCode giftCode = null;

    [SerializeField]
    private PopupMissionManager PanelMission = null;

    public static bool FistClickBtnLotteryAfterHaveResult = false;
    public static bool FistClickBtnResultAfterHaveResult = false;

    enum ScreenHome
	{
		Home,
		Achivement,
	}
	ScreenHome _screen;

    void Awake()
    {        
		Init ();
        //Enable Popup Panel
        PanelPopup.SetActive(true);
        popupAlert.gameObject.SetActive(true);

        //Kiem Tra mở Promotion x2 thẻ nạp cho user
        PromotionPopup();
    }
    void PromotionPopup()
    {
        
        if (MyInfo.PROMOTION_IN_APP["value"].AsInt > 1)
        {
            popupAlert.showAlertPromotion();
        }
    }
    #if UNITY_EDITOR

    void Update()
    {
		if (Input.GetKeyDown (KeyCode.Space))
        {
			string token = "fdfjfeeofkmkhahpofdmfiba.AO-J1Ozhgv2rwlFOyFb0TSL6w58otdFVn5GnCUAjjpcJqAbqcQFrxLKmYR3_FfCmgMWN5cD7JghZlJg5hldtHXqTkgTWlv-ImB9Rj92qKATMm6nTfc2NrRkuaK6p18bWHcDgZgWnO9UK";
			API.Instance.RequestPaymentInAppAndroid (
				"googleplay", 
				"com.b3b.zinplay.1", 
				token, 
				RspPaymentInApp);
		}

        if (Input.GetKeyUp(KeyCode.Return))
        {
            AlertController.api.showAlert("Bạn có muốn thoát game?", Application.Quit, true);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            MyInfo.CHIP = 9000;
        }
        

    }
	#endif

	void RspPaymentInApp (string s)
	{
		Debug.Log (s);
	}

	#region Properties

	long chip;
	long CHIP {
		get {
			chip = MyInfo.CHIP;
			return chip;
		}
		set {
			chip = MyInfo.CHIP = value;
		}
	}
	long gold;
	long GOLD {
		get {
			gold = MyInfo.GOLD;
			return gold;
		}
		set {
			gold = MyInfo.GOLD = value;
		}
	}

	int vippoint;
	int VIPPOINT{
		get{
			vippoint = MyInfo.VIPPOINT;
			return vippoint;
		}
		set{
			vippoint = MyInfo.VIPPOINT = value;
		}
	}

	#endregion

	string FormatCash(long _value)
	{
		return Utilities.GetStringMoneyByLong (_value);
	}
	void PlaySound(string _sound){
		SoundManager.PlaySound (_sound);
	}

    [SerializeField]
    private GameObject showFillInfo = null;

    [SerializeField]
    private PopupSpecialWheelManager specWheelPopup;

    [SerializeField]
    private TournamentEvent tournamentEvent = null;

    
	void Init()
	{
//		txtTest.text = "AAA";
		_screen = ScreenHome.Home;

		View.Init (this);
        View.ShowOrHideUnreadInbox();
		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP),
			FormatCash(GOLD), MyInfo.AvatarName,
			MyInfo.VIPPOINT
		);
		
		popupInvited.Init (BtnCancelInvitedOnClick, BtnOkInvitedOnClick);
		Achivement.Init (BackAchivementOnClick);
		popupAlert.Init ();
		popupFull.Init ();
		Module.InitHome (LogOutOnClick);
		Shop.Init (UpdateInfoUser, BtnBackShopOnClick);
		Event.Init (BackEventOnClick);
//		Exchange.Init (OnExChanged, OnBackExchange);
        //Inbox.Init(InboxExitOnClick);

//		Arena.Init (Arena.Hide, BtnRegisterArenaOnClick, BtnStartArenaOnClick, BtnFinishArenaOnClick);

		InfoUser.Init (BtnSaveInfoOnClick, BtnCloseInfoOnClick);

//		CardWheel.Init (BtnBackCardWheelOnClick);


		if (PlayerPrefs.GetInt ("First", 0) == 0)
        {
			//popupAlert.Show ("Hãy cập nhật thông tin để bảo mật tài khoản của bạn.", ()=>{
			//	InfoUser.Show();
			//	popupAlert.Hide();
			//});
            showFillInfo.gameObject.SetActive(true);

            PlayerPrefs.SetInt ("First", 1);
		}

        int rating = PlayerPrefs.GetInt("rating", 0);

        if (rating == 2)//login lan thu 2 => hien thi luon
        {
            AlertController.api.showAlert("Bạn có thích Vip777 không? hãy cho chúng tôi biết ý kiến của bạn về game nhé!", gotoStorePage, true);
        }
        PlayerPrefs.SetInt("rating", rating + 1);

        int specWheel = PlayerPrefs.GetInt("SpecWheel", 0);

        if (rating == 1)//login lan thu 2 => hien thi luon
        {
            specWheelPopup.showPopupSpecWhell();
        }
        PlayerPrefs.SetInt("SpecWheel", specWheel + 1);

        //View.SetEnableExchange (GameHelper.EnableExchange);

        LoadingManager.Instance.ENABLE = false;

		View.SetEnableArena (GameHelper.IsHaveArena);

		if (GameHelper.IsChangeVippoint)
			Exchange.ResponseAPI ();

		if (GameHelper.IsHaveBonusDaily > 0) {

			popupAlert.Show ("Quà tặng VIP mỗi ngày\n" + GameHelper.IsHaveBonusDaily
			+ " zin", popupAlert.Hide);

			GameHelper.IsHaveBonusDaily = 0;
		}

		View.SetEnableExchange (GameHelper.EnableExchange);
		View.SetEnableCardWheel (GameHelper.EnableCardWheel);

		InitEnableGame (GameHelper.dictEnableGame);
        tournamentEvent.initTournament();
        //set enable panel Mission
        PanelMission.PanelPopupMissionManager.SetActive(false);
    }

    private void gotoStorePage()
    {
#if !UNITY_IOS
        Application.OpenURL(GameHelper.getAppStoreURL());
#endif
    }
    void InitEnableGame(Dictionary<string, bool> _dictGame){

        Debug.Log("InitEnableGame");
        foreach (string _key in _dictGame.Keys) {
            Debug.Log("_key "+ _key + " : " + _dictGame[_key]);
            switch (_key) {
			case "tienlen":
				View.SetEnableGameTLMN (_dictGame [_key]);
				break;
			case "maubinh":
				View.SetEnableGameMB (_dictGame [_key]);
				break;
			case "xito":
				View.SetEnableGameXT (_dictGame [_key]);
				break;
			case "phom":
				View.SetEnableGameP (_dictGame [_key]);
				break;
			case "taixiu":
				View.SetEnableGameTX (_dictGame [_key]);
				break;

			}
		}
		View.SetEnableGameXT (true);
	}

	public void BtnExchangeOnClick(){
		Exchange.Show ();
	}
	void OnExChanged (string _msg)
	{
		popupAlert.Show (_msg, popupAlert.Hide);
		View.ShowInfoUser (
			Utilities.GetStringMoneyByLong (CHIP),
			Utilities.GetStringMoneyByLong (GOLD));
	}

	void OnBackExchange ()
	{
		Exchange.Hide ();

	}

	void BtnBackShopOnClick ()
	{


//		sfs.SendZoneRefreshData ();
		PlaySound (SoundManager.BUTTON_CLICK);
		Shop.Hide ();
		View.SetEnablePanelUI (true);
	}

	void BtnShopInfoOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
		Shop.Show ();
		View.SetEnablePanelUI (false);
	}

	void BtnBackCardWheelOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		UpdateInfoUser ();
		CardWheel.Hide ();
	}

	public void RequestJoinGameLobbyRoom(int gid)
	{
		//SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		GameHelper.currentGid = gid;

		SFS.Instance.RequestJoinGameLobbyRoom (gid);
        Debug.Log("RequestJoinGameLobbyRoom => gid = " + gid);

		LoadingManager.Instance.ENABLE = true;
	}


    void ShowNotificationLottery()
    {
        if (FistClickBtnLotteryAfterHaveResult == true)
        {
            Lottery.ShowNotificationLottery();
        }
        else if (FistClickBtnResultAfterHaveResult == true)
        {
            Lottery.OnNotificationResult();
        }
        else
        {
            Lottery.NotShowNotificationLottery();
        }        
    }





    public void BtnAchivementOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		Achivement.SetEnable (true);
		SetEnableContent (false);
		_screen = ScreenHome.Achivement;
	}
	void BackAchivementOnClick()
	{
		
		View.SetEnablePanelUI (true);
		Achivement.SetEnable (false);
	}
	void SetEnableContent(bool _enable)
	{
		View.SetEnablePanelUI (_enable);
	}

	void UpdateInfoUser(){
		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP), 
			FormatCash(GOLD), 
			MyInfo.AvatarName, 
			VIPPOINT);
	}
	void UpdateInfoUser(long _chip, long _gold, int _vippoint){
		CHIP = _chip;
		GOLD = _gold;
		VIPPOINT = _vippoint;

		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP), 
			FormatCash(GOLD), 
			MyInfo.AvatarName, 
			VIPPOINT);
	}

	#region Log Out

	public void LogOutOnClick()
	{
		//PlaySound (SoundManager.BUTTON_CLICK);
		SFS.Instance.LogOut ();
	}

	#endregion

	#region SHOP

	public void ShopOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
        Debug.Log("TRUOC KHI SHOW");
		Shop.Show ();
        Debug.Log("SAU KHI SHOW");
        View.SetEnablePanelUI (false);
	}

	#endregion

	#region MODULE

	public void ModuleOnClick()
	{
		//PlaySound (SoundManager.BUTTON_CLICK);
		Module.Show ();
	}

	#endregion

	#region EVENT

	public void EventOnClick()
	{
		//PlaySound (SoundManager.BUTTON_CLICK);
		Event.Show ();
		View.SetEnablePanelUI (false);
	}

	void BackEventOnClick ()
	{
		//Event.Hide ();
		View.SetEnablePanelUI (true);

		UpdateInfoUser ();
	}

    #endregion

    #region Inbox

    public void InboxOnClick()
    {
		PlaySound (SoundManager.BUTTON_CLICK);
        Inbox.Init(InboxExitOnClick);
        Inbox.Show();
    }

    void InboxExitOnClick()
    {
        Inbox.Hide();
    }

    #endregion

    #region Vip Info

    public void VipInfoOnClick()
    {
        PlaySound(SoundManager.BUTTON_CLICK);
        Vip.Init(VipExitOnClick);
        Vip.Show();
    }

    void VipExitOnClick()
    {
        Vip.Hide();
    }

    #endregion

    #region Nap

    public void NapOnClick()
    {
        //PlaySound(SoundManager.BUTTON_CLICK);
        VipExitOnClick();
        ShopOnClick();
    }

    #endregion

    #region Uu Dai

    public void UuDaiOnClick()
    {
        print("UuDaiOnClick");
        PlaySound(SoundManager.BUTTON_CLICK);

#if UNITY_ANDROID || UNITY_IOS
			InAppBrowser.DisplayOptions options = new InAppBrowser.DisplayOptions();
			options.displayURLAsPageTitle = false;
			options.pageTitle = "WinPlay";
			InAppBrowser.OpenURL(DataHelper.LinkVipPoint, options);
#endif
    }

    #endregion


    #region Lucky Wheel

    public void BtnCardWheelOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		CardWheel.Show ();
	}

	#endregion

	#region Invite

	void BtnCancelInvitedOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
			popupInvited.Hide ();
	}
	void BtnOkInvitedOnClick (int _roomID, string _pass)
	{
		//PlaySound (SoundManager.BUTTON_CLICK);
		if (_roomID == -1) {
			popupAlert.Show (ConstText.ErrorNoMoneyToPlay, popupAlert.Hide);
			popupInvited.Hide ();
		} else {
			RqJoinGameRoom (_roomID, _pass);
			LoadingManager.Instance.ENABLE = true;
		}
	}
	private void RqJoinGameRoom(int roomId, string _pass)
	{
		GamePacket param = new GamePacket (CommandKey.JOIN_GAME_ROOM);

		if (!string.IsNullOrEmpty (_pass))
			param.Put (ParamKey.ROOM_PASSWORD, _pass);
	
		param.Put (ParamKey.ROOM_ID, roomId);
		param.Put (ParamKey.GAME_ID, GameHelper.currentGid);
		sfs.SendZoneRequest (param);
	}
	void RspInvited(GamePacket _param)
	{
			string s = _param.GetString ("ivn");
			string[] info = s.Split ('#');

			string name = info [0];
			int roomID = int.Parse (info [1]);
			int gameID = int.Parse (info [2]);
			int roomLv = int.Parse (info [3]);
	long bet = long.Parse (info [4]);
	long betRequire = long.Parse (info [5]);
		string pass = info [6];

	GameHelper.currentGid = gameID;
		popupInvited.Show (name, roomID, gameID, roomLv, bet, betRequire, pass);
	}

	#endregion

	#region Info User

	public void BtnAvatarOnClick()
	{
		//PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Show ();
	}
	void BtnSaveInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
		View.UpdateAvatar ();
	}
	void BtnCloseInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
        //Debug.LogError("KhuongTest==========" + loginType);
        if (loginType != LoginType.Facebook)
        {
            View.UpdateAvatar();
        }
        
	}

	#endregion

	#region Lucky Wheel

	private void OnResultLuckyWheel ()
    {
		View.UpdateChip (Utilities.GetStringMoneyByLong(CHIP));
	}

	#endregion

	#region ARENA

	public void BtnArenaOnClick(){

		LoadingManager.Instance.ENABLE = true;

		RequestArenaData ();
	}

	void RequestArenaData(){
		API.Instance.RequestArenaData (RspArenaData);
	}

	void BtnRegisterArenaOnClick(){

		if (MyInfo.CHIP < Arena.COST) {
			popupAlert.Show ("Không đủ CHIP đăng ký đấu trường.",
				popupAlert.Hide);
		} else {
			popupFull.Show ("Xác nhận phí đăng ký tham gia thi đấu\n"
								+ Utilities.GetStringMoneyByLong (Arena.COST),

				popupFull.Hide , 

				() => {

				API.Instance.RequestArenaRegister (RspArenaRegister);

			});
		}

	}

	void RspArenaRegister (string _json)
	{
		popupFull.Hide ();

		JSONNode node = JSONNode.Parse (_json);

		long chip = long.Parse (node ["items"] ["chip"].Value);
		long gold = long.Parse(node["items"]["gold"].Value);;
		int vippoint = node ["items"] ["vippoint"].AsInt;

		UpdateInfoUser (
			chip,gold,vippoint
		);

		Arena.SetEnableBtnRegister (false);
		Arena.SetEnableBtnStart (true, false);

		RequestArenaData ();

	}

	void BtnStartArenaOnClick(){
		
		GamePacket pack = new GamePacket (CommandKey.START_ARENA);

		sfs.SendZoneRequest (pack);

	}
	void RspStartArena(GamePacket _packet){
		int result = _packet.GetInt (ParamKey.RESULT);

		switch (result) {
		case 1:
			Arena.ShowPopupMatching ();
			break;
		case 2:
			popupAlert.Show ("Không đủ chip tối thiểu tham gia thi đấu.", popupAlert.Hide);
			break;
		case 3:
			popupAlert.Show ("Chưa tới giờ thi đấu.", popupAlert.Hide);
			break;
		case 4:
			popupAlert.Show ("Bạn chưa đăng ký tham gia.", popupAlert.Hide);
			break;
		case 5:
			GameHelper.ChangeScene(_packet.GetInt(ParamKey.GAME_ID));
			break;

		}
	}

	void BtnFinishArenaOnClick(){

	}

	void RequestFinishArena(){

	}

	void RspArenaData (string _json)
	{
		Arena.Show ();

		LoadingManager.Instance.ENABLE = false;

		JSONNode node = JSONNode.Parse (_json);	

		Debug.Log ("Rsp ARENA:\n" + node.Value);

		Arena.ShowStatus (node ["status"].AsInt);

		bool isStarted = node ["status"].AsInt != 0;

//		Arena.ShowName(GameHelper.nam

		//Tran dau dang dien ra
		if (isStarted) {

			//Neu chua dang ky
			if (node ["is_regis"].AsInt == 0) {
				
				Arena.SetEnableBtnRegister (false);
				Arena.SetEnableBtnStart (false);

			} 
			//Neu da dang ky
			else {
				
				Arena.SetEnableBtnRegister (false);
				Arena.SetEnableBtnStart (true);

			}

		} 
		//Tran dau chua dien ra
		else {

			//Neu chua dang ky
			if (node ["is_regis"].AsInt == 0) {

				Arena.SetEnableBtnRegister (true);
				Arena.SetEnableBtnStart (false);

			} 
			//Neu da dang ky
			else {

				Arena.SetEnableBtnRegister (false);
				Arena.SetEnableBtnStart (true, false);

			}

		}

		Arena.InitTopAchivement (
			node ["top_day"],
			node ["top_week"]);

		Arena.ShowAchivement ();
		Arena.ShowUserCount (node ["user_regis"].AsInt);

		Arena.ShowRemainTime (node ["remain_time"].AsInt);

		Arena.COST = long.Parse (node ["chip_regis"].Value);
	}


//	void RspStartArena (GamePacket param)
//	{
//		
//	}

	void RspFinishArena (GamePacket param)
	{
		
	}

	#endregion

    
    public void OnSFSResponse(GamePacket param)
    {
        Debug.Log("Home rsp - " + param.param.ToJson());
        Debug.Log("Home cmd - " + param.cmd);

        switch (param.cmd)
        {

            case CommandKey.JOIN_GAME:
                bool isRoomEvent = param.GetBool("is_room_event");
                if (isRoomEvent == false)
                {
                    if (GameHelper.currentGid != (int)GAMEID.Lottery)
                        GameHelper.ChangeScene(GameHelper.currentGid);
                    else
                    {
                        LoadingManager.Instance.ENABLE = false;
                        Lottery.Init();
                    }
                }
                else
                {
                    GameHelper.currentGid = (int)MyInfo.tournamentStatus;
                    GameHelper.ChangeScene(GameHelper.currentGid);
                }
                
                break;
            case CommandKey.INVITE:
                RspInvited(param);
                break;
            case CommandKey.JOIN_GAME_LOBBY_ROOM:
                LoadWatingRoom(param);
                break;
            case CommandKey.ERROR:
                RspError(param);
                LoadingManager.Instance.ENABLE = false;
                break;
            case CommandKey.REFRESH:
                //			txtTest.text = " RSP Refresh";
                RspRefreshUserInfo(param);
                break;
            case CommandKey.START_ARENA:
                RspStartArena(param);
                break;
            case CommandKey.FINISH_ARENA:
                RspFinishArena(param);
                break;
            //case CommandKey.JOIN_ARENA_GAME:
            //    print("JOIN_ARENA_GAME ID: " + param.GetInt(ParamKey.GAME_ID));
            //    GameHelper.ChangeScene(param.GetInt(ParamKey.GAME_ID));
            //    break;
            case CommandKey.POPUP_PRIZE:
                print("Message: " + param.GetString(ParamKey.Message));
                //GameHelper.ChangeScene(param.GetInt(ParamKey.GAME_ID));

                popupAlert.Show(param.GetString(ParamKey.Message), popupFull.Hide);

                UpdateInfoUser(param.GetLong(ParamKey.CHIP), param.GetLong(ParamKey.GOLD), param.GetInt(ParamKey.VIPPOINT));
                break;
            case CommandKey.REQUEST_ADS:
                videoReward.onResponseRequestAds(param);
                break;
            case CommandKey.REWARD_ADS:
                videoReward.onResponseRewardAds(param);
                break;
            case CommandKey.GET_SPEC_WHEEL:
                specWheel.resGetSpecWheel(param);
                break;
            case CommandKey.START_ROLL_SPEC_WHEEL:
                specWheel.onStartWheelResponse(param);
                break;
           case CommandKey.GET_EVENT_STATUS:
                tournamentEvent.responseEventStatus(param);
                break;
            case CommandKey.JOIN_EVENT_REQUEST:

                List<int> lstEventType1 = param.GetIntArray("event1").ToList();
                List< int > lstEventType2 = param.GetIntArray("event2").ToList();

                tournamentEvent.showEvent(lstEventType1, lstEventType2);
                break;
            case CommandKey.JOIN_GAME_EVENT:
                tournamentEvent.showEventWaitingRoom(param);
                break;
            case CommandKey.LEADERBOARD:
                tournamentEvent.onLeaderBoardResponse(param);
                break;
            case CommandKey.EXIT_GAME_EVENT:
                tournamentEvent.closeEventWaitingRoom();
                break;
            case CommandKey.USE_GIFTCODE:
                giftCode.onGiftCodeResponse(param);
                break;
            case CommandKey.MISSION:
                PanelMission.OnSFSResponse(param);
                break;
            case CommandKey.CLAIM_MISSION:
                PanelMission.OnSFSResponse(param);
                break;
        }
    }

	void RspRefreshUserInfo (GamePacket param)
	{


		CHIP = param.GetLong (ParamKey.CHIP);	

		UpdateInfoUser ();


	}
//	[SerializeField]
//	UnityEngine.UI.Text txtTest;
//	string test1 = "";
	void OnApplicationPause(bool _pause){
		if (!_pause) {
//			txtTest.text = "Waiting!";
//			StartCoroutine (GameHelper.Thread (2, () => {
//				txtTest.text = "Requesting!";
				sfs.SendZoneRefreshData ();
//			}));
		}
	}
	void RspError(GamePacket _param)
	{
		int reason = _param.GetInt (ParamKey.REASON);

		switch (reason) {
		case 1:
			popupAlert.Show (ConstText.ErrorRoomFull, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 2:
			popupAlert.Show (ConstText.ErrorRoomNotExist, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 3:
			popupAlert.Show (ConstText.ErrorNoMoneyToPlay, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 4:
			popupAlert.Show (ConstText.ErrorCantFindRoom, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		}
	}
	void HidePopupAlert()
	{
		popupAlert.Hide ();
	}
	void HidePopupInvited()
	{
		popupInvited.Hide ();
	}

    private void LoadWatingRoom(GamePacket param)
    {
        int gid = param.GetInt(ParamKey.GAME_ID);
		GameHelper.currentGid = gid;

				if (gid == 3)
						GameHelper.ChangeScene (GameScene.TaiXiuScene);
				else
        	SceneManager.LoadScene(GameScene.WaitingRoom.ToString());

    }

	public void OnConnectionLost()
	{
		popupAlert.Show (ConstText.ErrorConnection, () => {
			GameHelper.ChangeScene(GameScene.LoginScene);
		});
	}*/
}
