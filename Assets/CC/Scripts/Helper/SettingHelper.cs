using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KEYSETTING{
	public const string ENABLE_SOUND = "enableSound";
	public const string ENABLE_BACKGROUND_MUSIC = "enable_BackgroundMusic_Sound";
    public const string ENABLE_INVITED = "enableInvited";
}
public class SettingHelper : MonoBehaviour {

	static SettingHelper instance;


	void Awake()
	{
		if (instance)
			Destroy (gameObject);
		else {
			instance = this;
			DontDestroyOnLoad (gameObject);
		}
	}

	public static SettingHelper Instance{
		get{
			if (instance == null) {
				GameObject container = new GameObject ("SettingHelper");
				instance = container.AddComponent<SettingHelper> ();

			}
				return instance;			
		}
	}

	public void Init()
	{
		
	}


	public bool INVITED
    {
		get
        {
			return PlayerPrefs.GetInt (KEYSETTING.ENABLE_INVITED, 1) == 1;
		}
		set
        {
			PlayerPrefs.SetInt (KEYSETTING.ENABLE_INVITED, value ? 1 : 0);
		}
	}

	public class SOUND
	{
		public static bool ENABLE
		{
			get{
				AudioListener.volume = PlayerPrefs.GetInt (KEYSETTING.ENABLE_SOUND, 1);
				return PlayerPrefs.GetInt (KEYSETTING.ENABLE_SOUND, 1) != 0;
			}
			set{
				PlayerPrefs.SetInt (KEYSETTING.ENABLE_SOUND, value ? 1 : 0);
				AudioListener.volume = value ? 1 : 0;
			}
		}
	}
    public class BACKGROUND_MUSIC
    {
        public static bool ENABLE
        {
            get
            {
                return PlayerPrefs.GetInt(KEYSETTING.ENABLE_BACKGROUND_MUSIC, 1) != 0;
            }
            set
            {
                PlayerPrefs.SetInt(KEYSETTING.ENABLE_BACKGROUND_MUSIC, value ? 1 : 0);
                //Debug.LogError("set int " + (value ? 1 : 0).ToString());
                //AudioListener.volume = value ? 1 : 0;
                if (LoadingManager.Instance != null)
                {
                    LoadingManager.Instance.setPlayMusic(value);
                }
            }
        }
    }
}

public static class SoundManager
{
	public const string BUTTON_CLICK = "Sounds/Common/Click";
    public const string DAP_HEO = "Sounds/Common/ColorBomb";
    public const string ENTER_ROOM = "Sounds/Common/VaoPhong";
	public const string EXIT_ROOM = "Sounds/Common/RoiPhong";
	public const string DEAL_MANY_CARD = "Sounds/Common/ChiaBaiNhieu";
	public const string DEAL_ONE_CARD = "Sounds/Common/Chia1La";
	public const string SELECT_CARD = "Sounds/Common/ChonBai";
    public const string DANH_1_LA = "Sounds/Common/Danh1La";
	public const string COUNT_DOWN = "Sounds/Common/CountDown";
	public const string LOSE = "Sounds/Common/Lose";
	public const string WIN = "Sounds/Common/Win";

	public const string TL_HEO_NE = "Sounds/TienLen/HeoNe";
	public const string TL_DOI_HEO = "Sounds/TienLen/DoiHeo";
	public const string TL_3_DOI_THONG = "Sounds/TienLen/3DoiThong";
	public const string TL_4_DOI_THONG = "Sounds/TienLen/4DoiThong";
	public const string TL_CHET_MAY_NE = "Sounds/TienLen/ChetMayNe";
	public const string TL_MAY_HA_BUOI = "Sounds/TienLen/MayHaBuoi";
	public const string TL_NGON_VO_DAY = "Sounds/TienLen/DoDi";
	public const string TL_TU_QUY = "Sounds/TienLen/TuQuy";

	public const string MB_XepBai = "Sounds/MauBinh/XepBai";
	public const string MB_FINISH = "Sounds/MauBinh/ketthuc";
	public const string MB_SoChi = "Sounds/MauBinh/sochi";


    public const string NUOCDA = "Sounds/Common/TiengNuocDa";
    public const string COITAU = "Sounds/Common/CoiTau";
    public const string NOBOM = "Sounds/BarXengHoaQua/LuckyShoot";


    public const string PHOM_HA_PHOM = "Sounds/Phom/haphom";
    public const string PHOM_TAO_PHOM = "Sounds/Phom/TaoPhom";

    //sound dua ngua
    public const string DUANGUA_RACE = "Sounds/DuaNgua/start_race";

    //sound Bar Xeng Hoa Qua
    public const string BAR_BET0 = "Sounds/BarXengHoaQua/Bet0";
    public const string BAR_BET1 = "Sounds/BarXengHoaQua/Bet1";
    public const string BAR_BET2 = "Sounds/BarXengHoaQua/Bet2";
    public const string BAR_BET3 = "Sounds/BarXengHoaQua/Bet3";
    public const string BAR_BET4 = "Sounds/BarXengHoaQua/Bet4";
    public const string BAR_BET5 = "Sounds/BarXengHoaQua/Bet5";
    public const string BAR_BET6 = "Sounds/BarXengHoaQua/Bet6";
    public const string BAR_BET7 = "Sounds/BarXengHoaQua/Bet7";
    public const string BAR_BgXeng = "Sounds/BarXengHoaQua/BgXeng";
    public const string BAR_EndRun = "Sounds/BarXengHoaQua/EndRun";
    public const string BAR_HitButton = "Sounds/BarXengHoaQua/HitButton";
    public const string BAR_Nuot = "Sounds/BarXengHoaQua/Nuot";
    public const string BAR_OverRun = "Sounds/BarXengHoaQua/OverRun";
    public const string BAR_StarRun = "Sounds/BarXengHoaQua/StarRun";
    public const string BAR_TaiXiu = "Sounds/BarXengHoaQua/TaiXiu";
    public const string BAR_WinSicBo = "Sounds/BarXengHoaQua/WinSicBo";
    public const string BAR_PrepairEndRun = "Sounds/BarXengHoaQua/PrepairEndRun";
    public const string BAR_HitResult = "Sounds/BarXengHoaQua/HitResult";
    public const string BAR_PushCoin = "Sounds/BarXengHoaQua/push_coin";
    public const string BAR_LuckyShoot = "Sounds/BarXengHoaQua/LuckyShoot";
    public const string BAR_WaitLuckyShoot = "Sounds/BarXengHoaQua/WaitLuckyShoot";
    public const string NEM_BOM = "Sounds/Bom/Nem";


    public const string WHEEL_STAR = "Sounds/PirateKings/WheelLoop";
    public const string WHEEL_END = "Sounds/PirateKings/WheelEnd";

    static Dictionary<string, AudioSource> soundSources;

	static SoundManager()
	{
		soundSources = new Dictionary<string, AudioSource>();
	}

	public static void PlaySound(string sound, bool loop=false)
	{
		if(!soundSources.ContainsKey(sound))
        {
			AudioSource source = SettingHelper.Instance.gameObject.AddComponent<AudioSource> ();
			source.clip = Resources.Load (sound) as AudioClip;
			soundSources.Add (sound, source);
        }
        soundSources[sound].loop = loop;
        soundSources [sound].Play();
        
	}
    public static bool IsPlaying(string sound)
    {
        if (soundSources.ContainsKey(sound)) return soundSources[sound].isPlaying;
        return false;

    }
	public static void StopSound(string sound)
	{
		if (soundSources.ContainsKey (sound)) 
			soundSources [sound].Stop ();
	}
}
	