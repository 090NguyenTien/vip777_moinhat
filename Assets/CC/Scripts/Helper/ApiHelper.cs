﻿using UnityEngine;
using System.Collections;
using BaseCallBack;

public class ApiHelper : MonoBehaviour {

	static ApiHelper instance;

	public static ApiHelper Instance{
		get{
			if (instance == null) {
				GameObject container = new GameObject ("ApiHelper");
				instance = container.AddComponent<ApiHelper> ();

			}
			return instance;

		}
	}

	public void CallBack(string _name, string _url, onCallBackStringSprite _callBack){
//		Debug.Log ("___ URL ___ " + _url);
		StartCoroutine (Thread(_name, _url, _callBack));
	}
	public void CallBack(int _level, string _url, onCallBackIntSprite _callBack){
		//		Debug.Log ("___ URL ___ " + _url);
		StartCoroutine (Thread(_level, _url, _callBack));
	}
	public IEnumerator Thread(string _name, string _url, onCallBackStringSprite _spr)
	{
		string s = _name;
		WWW www = new WWW (_url);
		yield return www;
		Texture2D mainImage = www.texture;
        Sprite spr=null;
        if (mainImage != null)
        {
            spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));
        }
        Debug.Log("spr=======" + spr);

        _spr (s, spr);
	}
	public IEnumerator Thread(int _level, string _url, onCallBackIntSprite _spr)
	{
		int level = _level;
		WWW www = new WWW (_url);
		yield return www;
		Texture2D mainImage = www.texture;
		Sprite spr = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

		_spr (_level, spr);
	}
}
