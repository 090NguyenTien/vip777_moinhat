﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentEvent : MonoBehaviour
{

    public static TournamentEvent api;

    [SerializeField]
    private Button btnExit = null;

    [SerializeField]
    private Toggle togMaster = null, togGodGambler = null;

    [SerializeField]
    private Button btnTournament = null;

    [SerializeField]
    private TimeCountDownController countDownEvent = null;

    [SerializeField]
    private TournamentWaitRoom tournamentWaitRoom = null;

    public void initTournament()
    {
        api = this;

        if (GameHelper.isEnalbeTournament == false)
        {
            btnTournament.gameObject.SetActive(false);
            return;
        }
        else
        {
            btnTournament.gameObject.SetActive(true);
        }
        if (MyInfo.isInitTournament == false)
        {
            GamePacket gamePackage = new GamePacket(CommandKey.GET_EVENT_STATUS);
            SFS.Instance.SendRoomRequest(gamePackage);
            Debug.Log("send event status nekkkkkkkkkk");
        }
        else
        {
            startCountdownToActiveEvent();
            if (MyInfo.tournamentStatus != TournamentStatus.None)
            {
                showEvent(lstEventType1, lstEventType2);
            }
            
        }
    }

    private string alertTimeActiveEvent = "";
    public void responseEventStatus(GamePacket gamePackage)
    {
        MyInfo.tournamentCountDownToActive = gamePackage.GetLong("time") / 1000;
        MyInfo.tournamenToClose = gamePackage.GetLong("endtime") / 1000;

        alertTimeActiveEvent = gamePackage.GetString("alert");
        startCountdownToActiveEvent();
    }
    private void startCountdownToActiveEvent()
    {
        int countdownActive = (int)MyInfo.tournamentCountDownToActive;
        btnTournament.onClick.AddListener(eventNotActive);
        countDownEvent.initTimeCountDown(countdownActive, countdownActive, 0, countDownActiveComplete);

        if (MyInfo.tournamentStatus != TournamentStatus.None)//dang choi game va thoat ra
        {
            joinGameEvent();
        }
    }

    private void joinGameEvent()
    {
        GamePacket gamePackage = new GamePacket(CommandKey.JOIN_GAME_EVENT);
        gamePackage.Put("type", (int)MyInfo.tournamentType);
        gamePackage.Put("game_id", (int)MyInfo.tournamentStatus);
        SFS.Instance.SendZoneRequest(gamePackage); 
        Debug.Log("send join game event" + gamePackage.param.ToJson());
    }


    private void onSelectMasterKind(bool value)
    {
        if (value == true)
        {
            selectListEvent1();
        } 
    }

    private void onSelectGodGambler(bool value)
    {
        if (value == true)
        {
            selectListEvent2();
        }
    }
    public void onLeaderBoardResponse(GamePacket data)
    {
        tournamentWaitRoom.onLeaderBoardResponse(data);
    }
    private void eventNotActive()
    {
        AlertController.api.showAlert("Sự kiện chưa diễn ra, " + alertTimeActiveEvent);
    }
    private void countDownActiveComplete()
    {
        SFS.Instance.ClientOnAdminMessage(3, "Đã đến giờ diễn ra sự kiện Đấu trường - mọi người hãy cùng nhau vào tranh tài nhé");

        btnTournament.onClick.RemoveAllListeners();
        btnTournament.onClick.AddListener(joinEventTournament);
    }

    private void joinEventTournament()
    {
        GamePacket gamePackage = new GamePacket(CommandKey.JOIN_EVENT_REQUEST);
        SFS.Instance.SendRoomRequest(gamePackage);
    }

    [SerializeField]
    private GameObject gameList = null;
    [SerializeField]
    private List<Button> lstbtnEvent = null;

    private  static List<int> lstEventType1, lstEventType2;
    public void showEvent(List<int> lstEventType1Data, List<int> lstEventType2Data)
    {
        lstEventType1 = lstEventType1Data;
        lstEventType2 = lstEventType2Data;

        gameObject.SetActive(true);
        selectListEvent1();
        if (MyInfo.tournamentType == TournamentType.WinCount)
        {
            togMaster.isOn = true;
            togGodGambler.isOn = false;
        }
        else
        {
            togMaster.isOn = false;
            togGodGambler.isOn = true;
        }
        togMaster.onValueChanged.RemoveAllListeners();
        togGodGambler.onValueChanged.RemoveAllListeners();
        togMaster.onValueChanged.AddListener(onSelectMasterKind);
        togGodGambler.onValueChanged.AddListener(onSelectGodGambler);
        btnExit.onClick.AddListener(closeTournamentEvent);
    }

    private void selectListEvent1()
    {
        switch (GameHelper.gameKind)
        {
            case GameKind.Baucua:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    lstbtnEvent[i].gameObject.SetActive(lstEventType1.Contains(i + 1) == true);
                }
                break;
            case GameKind.XT:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.XiTo || activeGameID == (int)GAMEID.TaiXiu) && lstEventType1.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.TLMN:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.TLMN || activeGameID == (int)GAMEID.TaiXiu) && lstEventType1.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.Phom:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.Phom || activeGameID == (int)GAMEID.TaiXiu) && lstEventType1.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.TLDL:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.TLDL || activeGameID == (int)GAMEID.TaiXiu) && lstEventType1.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.MB:

                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.MauBinh || activeGameID == (int)GAMEID.TaiXiu) && lstEventType1.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
        }
       
        MyInfo.tournamentType = TournamentType.WinCount;
    }
    private void selectListEvent2()
    {

        switch (GameHelper.gameKind)
        {
            case GameKind.Baucua:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    lstbtnEvent[i].gameObject.SetActive(lstEventType2.Contains(i + 1) == true);
                }
                break;
            case GameKind.XT:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.XiTo || activeGameID == (int)GAMEID.TaiXiu) && lstEventType2.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.TLMN:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.TLMN || activeGameID == (int)GAMEID.TaiXiu) && lstEventType2.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.Phom:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.Phom || activeGameID == (int)GAMEID.TaiXiu) && lstEventType2.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.TLDL:
                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.TLDL || activeGameID == (int)GAMEID.TaiXiu) && lstEventType2.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
            case GameKind.MB:

                for (int i = 0; i < lstbtnEvent.Count; i++)
                {
                    int activeGameID = i + 1;
                    if ((activeGameID == (int)GAMEID.MauBinh || activeGameID == (int)GAMEID.TaiXiu) && lstEventType2.Contains(activeGameID) == true)
                    {
                        lstbtnEvent[i].gameObject.SetActive(true);
                    }
                    else
                    {
                        lstbtnEvent[i].gameObject.SetActive(false);
                    }
                }
                break;
        }
        MyInfo.tournamentType = TournamentType.MoneyWin;
    }

    public void onSelectGameEvent(int gameCode)//call tren editor
    {
        MyInfo.tournamentStatus = (TournamentStatus)gameCode;
        joinGameEvent();
    }

    public void showEventWaitingRoom(GamePacket gamePackage)
    {
        tournamentWaitRoom.showWaitRoom(gamePackage);
    }

    public void closeEventWaitingRoom()
    {
        tournamentWaitRoom.closeTournamentWaitRoom();
    }
    private void closeTournamentEvent()
    {
        MyInfo.tournamentStatus = TournamentStatus.None;
        gameObject.SetActive(false);
        togMaster.onValueChanged.RemoveAllListeners();
        togGodGambler.onValueChanged.RemoveAllListeners();
        btnExit.onClick.RemoveAllListeners();
    }

}
