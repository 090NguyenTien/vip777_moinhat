﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using BaseCallBack;


public class ItemBoomControll : MonoBehaviour
{

    RectTransform MyTransform;
    Vector3 TargetPos;
    [SerializeField]
    GameObject ObjBoomNo;
    [SerializeField]
    Transform trsfLuckyWheel;

    [SerializeField]
    Transform targetMove;
    onCallBack SendRequestBoom;

    Animator MyAnim;
    BoxCollider2D MyBox;

    bool BiHuy = false;


    // Use this for initialization
    void Awake()
    {
        MyAnim = gameObject.GetComponent<Animator>();
        MyBox = gameObject.GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Init(Transform _targetMove)
    {
        MyTransform = this.gameObject.GetComponent<RectTransform>();
        targetMove = _targetMove;
        TargetPos = targetMove.transform.position;
        //Destroy(this.gameObject, 30f);
    }

    public void NemBoom(onCallBack _SendRequestBoom, int LoaiBom = 0 , float speed = 1f, bool sent = true, bool chamvatthe = false)
    {
        MyTransform = this.gameObject.GetComponent<RectTransform>();
        SoundManager.PlaySound(SoundManager.NEM_BOM);
        // Debug.Log("targetMove " + targetMove);
        TargetPos = targetMove.transform.position;
        SendRequestBoom = _SendRequestBoom;
        // trsfLuckyWheel.DORotate(new Vector3(0, 0, -720), 5, RotateMode.FastBeyond360);

        Tweener myTweener = trsfLuckyWheel.DORotate(new Vector3(0, 0, -720), 5, RotateMode.FastBeyond360); // XOAY
        if (LoaiBom == 3 || LoaiBom == 5) // CaiThuyen
        {
            myTweener.Pause();
        }


        MyTransform.DOMove(TargetPos, speed).OnComplete(() =>
        {
            // trsfLuckyWheel.DORotate(new Vector3(0, 0, -900), 5, RotateMode.FastBeyond360).Pause();
            if (LoaiBom != 3 || LoaiBom != 5) // CaiThuyen
            {
                myTweener.Pause();
            }
            MyTransform.rotation = Quaternion.Euler(0, 0, 0);

            
            if (chamvatthe == true)
            {
                MyAnim.SetBool("ChamVatThe", true);
                BiHuy = true;
            }
            else
            {
                MyAnim.SetBool("End", true);
            }
            if (BiHuy == false)
            {

                if (LoaiBom == 0 || LoaiBom == 4)
                {
                    SoundManager.PlaySound(SoundManager.DAP_HEO);
                }
                else if (LoaiBom == 2)
                {
                    SoundManager.PlaySound(SoundManager.NUOCDA);
                }
                else if (LoaiBom == 3)
                {
                    SoundManager.PlaySound(SoundManager.COITAU);
                }
                else if (LoaiBom == 5 || LoaiBom == 6)
                {
                    SoundManager.PlaySound(SoundManager.NOBOM);
                }
                else if (LoaiBom >= 1)
                {
                    SoundManager.PlaySound(SoundManager.DANH_1_LA);
                }

                //if (gameObject.tag == "dan")
                //{
                //    MyAnim.SetBool("End", true);
                //}
                //else
                //{
                //    ObjBoomNo.SetActive(true);
                //}

            }

            if (sent == true)
            {
                SendRequestBoom();
            }

            
        });


    }


    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.tag == "kieng")
    //    {
    //        Debug.LogWarning("fuckkkk chammmmmmmm");
    //        //MyAnim.SetBool("ChamVatThe", true);
    //        //BiHuy = true;
    //        // StartCoroutine(PrintfAfter());

    //    }
    //}



    public string GetTypeByName(string name)
    {
        string ty = "";
        int i = name.IndexOf('_');
        if (i != -1)
        {
            ty = name.Substring(i);
        }
        return ty;
    }



    private void OnTriggerExit2D(Collider2D collision)
    {
        string name = collision.name;
        name = GetTypeByName(name);

        Debug.LogError("TenVatCan-------------- " + name);
        string thisname = GetTypeByName(this.name);

        if (name == "_efect(Clone)")
        {
            MyAnim.SetBool("ChamVatThe", true);
            BiHuy = true;
            // StartCoroutine(PrintfAfter());
            Debug.LogWarning("chammmmmmmm");
        }
        else if (thisname == "_efect(Clone)")
        {
            MyBox.enabled = false;
        }

        //if (collision.tag == "dan")
        //{
        //    MyAnim.SetBool("ChamVatThe", true);
        //    BiHuy = true;
        //    // StartCoroutine(PrintfAfter());
        //    Debug.LogWarning("chammmmmmmm");
        //}
        //else if (this.tag == "dan")
        //{
        //    MyBox.enabled = false;
        //}

    }

    public void BienMat()
    {
        // yield return new WaitForSeconds(0.2f);
        Debug.LogWarning(" ");
        MyAnim.SetBool("ChamVatThe", false);
        MyAnim.SetBool("BienMat", true);
    }

    public void Huy()
    {
        Destroy(this.gameObject);
    }
}
