﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using SimpleJSON;
using MiniJSON;
using UnityEngine.UI;
using System;

public enum LoginType
{
    NonLoginYet = -1,
    GameAccount = 0,
    Facebook = 1,
    QuickLogin = 2
}


public enum Gate
{
    Royal = 1,
    FPlay = 2,
    King88 = 3,
    BigWin = 4,
    Vip777 = 5,
    Pygo = 6,
    Win88 = 7
}

public class LoginController : MonoBehaviour
{

	[SerializeField]
	LoginView View;
	[SerializeField]
	PopupAlertManager popupAlert;
	[SerializeField]
	GameObject objDontDestroy;
	[SerializeField]
	PopupFogetPasswordManager FogetPassword;

    [SerializeField]
    Gate gate = Gate.FPlay;
    [SerializeField]
	Text txtVersion;

	[SerializeField]
	string apiBaucua, apiTLMN, apiMB, apiXT, apiPhom, apiTLDL;

	private SFS sfs;

	public const int MIN_CHAR_USERNAME = 6;
	public const int MAX_CHAR_USERNAME = 18;
	public const int MIN_CHAR_PASSWORD = 6;
	public const int MAX_CHAR_PASSWORD = 20;

	const string KEY_CODE = "code";

	bool isPanelSignIn = true;
	bool isRequestingSignIn = false;

	[SerializeField]
	UnityEngine.UI.InputField input1;

//	bool IsCheckMaintenance = false;

	bool IsFinishInitConfig = false;
	string JsonAssets = "";

	bool isRequestLogin = false;

	string Username = "";
	string Password = "";
    void Start()
	{
        GameHelper.IS_LOAD_LOGIN = true;
        //Debug.LogError((9 % 3) + "     "+ (8%3));
        GameHelper.gameKind = GameHelper.getGameKind();
		string keyIAP = "";
        //GameHelper.gameKind = GameHelper.getGameKind();

     	switch (GameHelper.gameKind)
        {
            case GameKind.MB:
                keyIAP = apiMB;
                break;
            case GameKind.XT:
                keyIAP = apiXT;
                break;
            case GameKind.TLMN:
                keyIAP = apiTLMN;
                break;
            case GameKind.Phom:
                keyIAP = apiPhom;
                break;
            case GameKind.TLDL:
                keyIAP = apiTLDL;
                break;
            case GameKind.Baucua:
                keyIAP = apiBaucua;
                break;
        }
        Debug.LogError("keyIAP===============" + keyIAP);
        Debug.LogError("GameHelper.gameKind===============" + GameHelper.gameKind);
        
        GameHelper.base64EncodedPublicKey = keyIAP;
        Init();
        StartCoroutine(getConfigGame());

        AlertController.api.CloseChatWhenLogout();
    }

    private IEnumerator getConfigGame()
    {
       
        string url = API.Instance.DOMAIN + "/GameBaiConfig.php";

        WWW service = new WWW(url);
        yield return service;
        if (string.IsNullOrEmpty(service.error) == false) //download error
        {
            yield break;
        }
        string[] configData = service.text.Split('\n');

        GameHelper.dicConfig = new Dictionary<string, string>();
        string[] dataElement;
        for (int i = 0; i < configData.Length; i++)
        {
            dataElement = configData[i].Split('\t');
            GameHelper.dicConfig.Add(dataElement[0], dataElement[1]);
        }

        hotlineNumber = GameHelper.getConfig("HotLine");
        //GameHelper.isEnalbeTournament = int.Parse(GameHelper.getConfig("enableTournament")) == 1;
        //int curVersion = int.Parse(Application.version.Split('.')[0]);
        //Debug.LogError("KHUONG=curVersion============"+ curVersion);
        //Debug.LogError("KHUONG=identifier============" + int.Parse(GameHelper.getConfig(Application.identifier)));
   
        //if (curVersion < int.Parse(GameHelper.getConfig(Application.identifier)))
        //{
        //    AlertController.api.showAlert("Phiên bản đã quá hạn, hãy update phiên bản mới!", showUpdatePage);
        //}
        //else
        //{
            if (sfs.isRelogin == false)//login lan dau => login thang vao luon
            {
                selectLoginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
                if (selectLoginType == LoginType.GameAccount)
                {
                    if (View.inputUsernameSignIn.text.Length > 0)
                    {
                        BtnSignInOnClick(View.inputUsernameSignIn.text, View.inputPasswordSignIn.text, false);
                    }
                    else//chua co ten dang nhap vao choi luon
                    {
                        autoSignUp();
                    }
                }else if(selectLoginType == LoginType.QuickLogin)
                {
                    LoginWithGuest();
                }
                else if (selectLoginType == LoginType.Facebook)
                {
                    loginWithFaceBook();
                }
            }
        //}
    }

    private LoginType selectLoginType = LoginType.NonLoginYet;
    public void loginWithFaceBook()
    {
        selectLoginType = LoginType.Facebook;
        isRequestLogin = true;
        if (IsFinishInitConfig == false)
        {
            Debug.Log("Chua Init, INIT thoi!");
            RequestInit();
        }
        else
        {
            FBController.api.loginWithFbAccount(onFacebookLoginComplete);
        }


    }

    public void loginWithGameAccount()
    {
        if (View.inputUsernameSignIn.text.Length > 0)
        {
            BtnSignInOnClick(View.inputUsernameSignIn.text, View.inputPasswordSignIn.text, true);
        }
        else//chua co ten dang nhap vao choi luon
        {
            autoSignUp();
        }
    }

    public void LoginWithGuest()
    {
        Debug.Log("LoadingManager.Instance " + LoadingManager.Instance);
        selectLoginType = LoginType.QuickLogin;
        isRequestLogin = true;
        MyInfo.AVATAR_FACEBOOK_LOAD = false;
        
        LoadingManager.Instance.ENABLE = true;
        if (IsFinishInitConfig == false)
        {
            Debug.Log("Chua Init, INIT thoi!");
            RequestInit();
        }
        else
        {
            BtnSignUpQuick();
        }
        
    }

    int numTimeSignFall = -1;
    private void autoSignUp()
    {
        ++numTimeSignFall;
        isAutoSignUp = true;
        /*
        int firstNumberLeng = 3;
        if (numTimeSignFall < 3)
        {
            firstNumberLeng = 3;
        }
        else if (numTimeSignFall < 6)
        {
            firstNumberLeng = 4;
        }
        else if (numTimeSignFall < 9)
        {
            firstNumberLeng = 5;
        }
        else 
        {
            firstNumberLeng = 6;
        }

        //string userName = "Player";
        string userName = "Player";

        for (int i = 0; i < firstNumberLeng; i++)
        {
            userName += Random.Range(0, 9);
        }
        string passText = Random.Range(100001, 999999).ToString();
        */
        //edit by khuong
        //BtnSignUpOnClick(userName, passText, passText);
        LoadingManager.Instance.ENABLE = true;
        View.inputUsernameSignIn.text = "";
        View.inputPasswordSignIn.text = "";
        Username = "";
        Password = "";
        BtnSignUpQuick();
    }
    
    private void BtnSignUpQuick()
    {
        API.Instance.RequestSignUpQuick(SystemInfo.deviceUniqueIdentifier, signUpQuickRespone); 
    }
    private void signUpQuickRespone(string signUpData)
    {
        Debug.Log("signUpData====" + signUpData);
        if (JSONNode.Parse(signUpData)["off"].AsInt == 1)
        {
            ShowMaintenance();
        }
        else
        {
            selectLoginType = LoginType.QuickLogin;
            PlayerPrefs.SetInt("LoginType", (int)LoginType.QuickLogin);
            RspSignIn(signUpData);
        }
    }
    private void autoSignComplete(string userName , string userPass)
    {
        Debug.Log("sign  up  complete neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        if (selectLoginType == LoginType.QuickLogin)
        {
            View.inputUsernameSignIn.text = "";
            View.inputPasswordSignIn.text = "";
            Username = "";
            Password = "";
            return;
        }
        View.inputUsernameSignIn.text = userName;
        View.inputPasswordSignIn.text = userPass;
        BtnSignInOnClick(userName, userPass, false);
    }

    private void showUpdatePage()
    {
        Application.OpenURL(GameHelper.getAppStoreURL());
        Application.Quit();
    }
    bool isFinishSpriteAvt;

	bool IsOkLogin{
		get{
//			Debug.Log ("Maintence: " + IsCheckMaintenance);
//			Debug.Log ("SprAVT: " + isFinishSpriteAvt);
//			Debug.Log ("SprVIP: " + isFinishSpriteVip);
//			Debug.Log ("ConfigVIP: " + isInitConfigVip);
//			Debug.Log ("PAYMENT: " + isFinishPayment);

			return isFinishSpriteAvt && IsFinishInitConfig; }
	}

	bool isRequestingInit = false;

    void RequestInit()
    {

		isRequestingInit = true;

		Debug.Log ("+++ REQUEST INIT +++");

		API.Instance.RequestInit (gate, RspInit);
	}

	void RspInit (string _json)
	{
        //MyInfo.IS_WHEEL_LUCKY = false;
        isRequestingInit = false;

		JSONNode node = JSONNode.Parse (_json);

		Debug.Log ("_RSP ASSETS_\n" + _json);

		if (!string.IsNullOrEmpty (_json)) {
			if (node ["off"].AsInt != 0) {
				ShowMaintenance ();
				LoadingManager.Instance.ENABLE = false;
			} else {
				JSONNode nodeUpdate = node ["update_game"] [Const.PLATFORM];
                int curVersion = int.Parse(Application.version.Split('.')[0]);
                string package = Application.identifier.Replace(".", "");
                int newVersion = nodeUpdate["all_version"][package].AsInt;
                if (newVersion > curVersion) {
					LoadingManager.Instance.ENABLE = false;
					popupAlert.Show ("Phiên bản của bạn đã quá hạn. Vui lòng cập nhật phiên bản mới nhất.", () => {
						popupAlert.Hide();
                        string url = nodeUpdate[package];
						Application.OpenURL (url);
                    });
					return;
				}
                //kiem tra xem user duoc x2 ,x3 the nap khong
                //Debug.LogError("Khuong==========promotion" + node["promotion"]);
                JSONNode promotionInapp = node["promotion"];
                setDataPromotion(promotionInapp);
                //end

                JSONNode nodeMaintenance = node ["show_config"] ["game"];

				if (nodeMaintenance [Const.PLATFORM].AsInt != 0) {
					ShowMaintenance ();
					LoadingManager.Instance.ENABLE = false;
					return;
				}
                
				//Lay Domain - Port
				RspConfigServer (node ["sfs"]);
      
				//Config Payment
				RspConfigPayment (node ["payment_status"].AsInt == 1, 
					node ["pkg_version"],
					node ["payment_vippoint"].AsInt);
                //Config SMS Payment and Card Payment
                PaymentSmsAndCard(node["payment_card"].AsInt == 1, node["payment_sms"].AsInt == 1, node["payment_card_mobi"].AsInt == 1,
                     node["payment_card_vina"].AsInt == 1, node["payment_card_viettel"].AsInt == 1, 
                     node["payment_sms_mobi"].AsInt == 1, node["payment_sms_vina"].AsInt == 1, node["payment_sms_viettel"].AsInt == 1);
			

				//Enable Game
				ConfigEnableGame (node["show_config"]["game_ids"]);
#if UNITY_ANDROID
				GameHelper.EnableCardWheel = node ["show_config"]["card_wheel"]["android"].AsInt != 0;
                GameHelper.EnableBarGame = node["show_config"]["game_ids"]["bar"]["android"].AsInt != 0;
#elif UNITY_IOS
				GameHelper.EnableCardWheel = node ["show_config"]["card_wheel"]["ios"].AsInt != 0;
#else
				GameHelper.EnableCardWheel = false;
#endif
                IsFinishInitConfig = true;


                RspVip(node["vipcollect"]);

                RspBooItem(node["bomb_item"]);

                RspMod(node["mod"]);
                RspUserColor(node["user_color"]);
                RspMaster(node["master"]);
                RspAvoidItem(node["avoid_item"]);
                RspShopColor(node["shop_color"]);
                Rsp_Aset_Level_Uprade(node["asset_level_upgrade"]);

                string zalo = node["zalo_admin"].Value;

                if (node["payment_domain"] != "")
                {
                    JSONArray paymentUrl = node["payment_domain"].AsArray;
                    MyInfo.PAYMENT_RANDOM = paymentUrl[UnityEngine.Random.Range(0, paymentUrl.Count)];
                }
                

                Debug.LogError("MyInfo.PAYMENT_RANDOM------" + MyInfo.PAYMENT_RANDOM);
                DataHelper.ZALO_ADD = zalo;

                if (!isFinishSpriteAvt)
                {
                    RspAssets(node["assets"]);
                }
                else if (IsOkLogin)
                {
                    if(selectLoginType == LoginType.QuickLogin){
                        LoginWithGuest();
                    }
                    else if (selectLoginType == LoginType.GameAccount)
                    {
                        RequestSignIn(Username, Password);
                    }
                    else if (selectLoginType == LoginType.Facebook)
                    {

                        loginWithFaceBook_fist();
                    }
                }

                if (!string.IsNullOrEmpty(node["max_friend"]))
                {
                    MyInfo.MAX_FRIEND = Int32.Parse(node["max_friend"].Value);
                }
                if (!string.IsNullOrEmpty(node["chat_status"]))
                {
                    MyInfo.CHAT_STATUS = Int32.Parse(node["chat_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["tlmn"]))
                {
                    MyInfo.CHANGE_TLMN = Int32.Parse(node["tlmn"].Value);
                }
                if (!string.IsNullOrEmpty(node["event_status"]))
                {
                    MyInfo.EVENT_STATUS = Int32.Parse(node["event_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["market_status"]))
                {
                    MyInfo.MARKET_STATUS = Int32.Parse(node["market_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["post_new_status"]))
                {
                    MyInfo.POST_NEWS_STATUS = Int32.Parse(node["post_new_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["clan_status"]))
                {
                    MyInfo.CLAN_STATUS = Int32.Parse(node["clan_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["is_clan_war"]))
                {
                    MyInfo.CLAN_WAR_STATUS = Int32.Parse(node["is_clan_war"].Value);
                }
                if (!string.IsNullOrEmpty(node["shop_status"]))
                {
                    MyInfo.SHOP_PHONGTHUY_STATUS = Int32.Parse(node["shop_status"].Value);
                }
                if (!string.IsNullOrEmpty(node["bundle_version"]))
                {
                    MyInfo.BUNDLE_VERSION = Int32.Parse(node["bundle_version"].Value);
                }
            }
		}
	}

    private void RspShopColor(JSONNode jSONNode)
    {
        DataHelper.AddShopColor(jSONNode);
    }

    private void RspAvoidItem(JSONNode jSONNode)
    {
        DataHelper.AddAvoidItem(jSONNode);
    }

    void RspBooItem(JSONNode _nodeAssets)
    {
        DataHelper.DicItemBoom.Clear();

        for (int i = 0; i < _nodeAssets.Count; i++)
        {
            int id_Bom = _nodeAssets[i]["id"].AsInt;
            int Vip = _nodeAssets[i]["vip_require"].AsInt;

            DataHelper.DicItemBoom.Add(id_Bom, Vip);
        }
    }




    public void loginWithFaceBook_fist()
    {
        selectLoginType = LoginType.Facebook;
        isRequestLogin = true;
        if (IsFinishInitConfig == false) // Truong Hop Chua Co Tai Khoan
        {
            Debug.Log("Chua Init, INIT thoi!");
            RequestInit();
        }
        else // Truong Hop Da Co Tai Khoan
        {
            FBController.api.loginWithFbAccount(onFacebookLoginComplete_fist);
        }


    }


    public void onFacebookLoginComplete_fist()
    {
        isRequestingSignIn = true;
      
        Debug.Log("**** Request API SignIn with FBID ");

        API.Instance.RequestSignIn("", "", RspSignIn, LoginType.Facebook);
        PlayerPrefs.SetInt("LoginType", (int)LoginType.Facebook);

        StartCoroutine(checkTextLogin());
    }





    void RspVip(JSONNode _nodeAssets)
    {       
        VIP_Controll.DicVipData.Clear();

        for (int i = 0; i < _nodeAssets.Count; i++)
        {
            GoiVip gv = new GoiVip();
            gv.TenVIP = _nodeAssets[i]["name"].Value;
            gv.MinPoint = _nodeAssets[i]["min"].Value;
            gv.MaxPoint = _nodeAssets[i]["max"].Value;
            gv.ThuongDatMoc = _nodeAssets[i]["benefit"]["bonus"]["chip"].Value;
            gv.ThuongNgay = _nodeAssets[i]["benefit"]["bonus_daily"]["chip"].Value;
            gv.ThuongPhanTram = _nodeAssets[i]["benefit"]["chip_commission"].Value;
            gv.HanMucTangChip = _nodeAssets[i]["benefit"]["max_transfer_perday"].Value;
            gv.HanMucTangGem = _nodeAssets[i]["benefit"]["max_transfer_gem_perday"].Value;
            gv.ColorChat = _nodeAssets[i]["benefit"]["color"].Value;
            VIP_Controll.DicVipData.Add(i, gv);
        }
    }

    
    void RspUserColor(JSONNode _nodeAssets)
    {
        DataHelper.UserColorChat.Clear();
        for (int i = 0; i < _nodeAssets.Count; i++)
        {
            string name = _nodeAssets[i]["name"].Value;
            string color = _nodeAssets[i]["color"].Value;
            string user_id = _nodeAssets[i]["user_id"]["$id"].Value;
            if(!DataHelper.UserColorChat.ContainsValue(color + ";" + user_id))
            {
                DataHelper.UserColorChat.Add(name, color + ";" + user_id);
            }            
        }
    }
    void RspMod(JSONNode _nodeAssets)
    {
        DataHelper.DanhSachMod.Clear();
        for (int i = 0; i < _nodeAssets.Count; i++)
        {            
            string name = _nodeAssets[i].Value;
            DataHelper.DanhSachMod.Add(i, name);
            Debug.LogWarning("fuck-------------- " + name);
        }
    }
    void RspMaster(JSONNode _nodeAssets)
    {
        DataHelper.MasterThanBai = new Hashtable();        
        
        string name = _nodeAssets["name"].Value;
        string user_id = _nodeAssets["user_id"].Value;
        string clan_id = _nodeAssets["clan_id"].Value;
        string image = _nodeAssets["image"].Value;
        DataHelper.MasterThanBai.Add("name", name);
        DataHelper.MasterThanBai.Add("clan_id", clan_id);
        DataHelper.MasterThanBai.Add("image", image);
        DataHelper.MasterThanBai.Add("user_id", user_id);
        
    }
    void Rsp_Aset_Level_Uprade(JSONNode Aset_Level_Uprade)
    {
        DataHelper.PhanTramDeNangCapSanPham.Clear();
        for (int i = 0; i < Aset_Level_Uprade.Count; i++)
        {
            float phantram = float.Parse(Aset_Level_Uprade[i].Value);
            DataHelper.PhanTramDeNangCapSanPham.Add(i, phantram);
        }
    }

    void RspDataBoderAvatar(string _json)
    {
        Debug.Log(" VO RspDataBoderAvata ------- " + _json);
        JSONNode node = JSONNode.Parse(_json);
        DataHelper.DicDataBoderAvatar.Clear();
        MyInfo.MY_BORDERS_AVATAR.Clear();
        for (int i = 0; i < node["user_avatar_border"].Count; i++)
        {
            int t = node["user_avatar_border"][i].AsInt;
         //   Debug.LogError("user_avatar_border ======== " + node["user_avatar_border"][i]);
            MyInfo.MY_BORDERS_AVATAR.Add(t);
        }

        for (int i = 0; i < node["vip_avatar_border"].Count; i++)
        {
            BoderAvatar bo = new BoderAvatar();
            bo.id = node["vip_avatar_border"][i]["id"].Value;
            bo.vip_require = node["vip_avatar_border"][i]["vip_require"].Value;

            if (node["vip_avatar_border"][i]["chip"].Value == "")
            {
                bo.chip = null;
                bo.gem = node["vip_avatar_border"][i]["gem"].Value;
            }
            else
            {
                bo.chip = node["vip_avatar_border"][i]["chip"].Value;
                bo.gem = null;
            }


            DataHelper.DicDataBoderAvatar.Add(bo.id, bo);
        }


        for (int i = 0; i < DataHelper.DicDataBoderAvatar.Count + 1; i++)
        {
            API.Instance.LoadBoderAvatar(i.ToString());

            API.Instance.LoadShopBoderAvatar(i.ToString());
        }

     /*   for (int i = 0; i < DataHelper.DicDataBoderAvatar.Count; i++)
        {
            API.Instance.LoadShopBoderAvatar(i.ToString());
        }

    */
    }






    private void PaymentSmsAndCard(bool card, bool sms, bool cardMobi, bool cardVina, bool cardVietel, bool smsMobi, bool smsVina, bool smsVietel)
    {
        //Debug.LogError("KKKKKKHHHHHH ====== sms:::: " + sms);
        GameHelper.CardPayment.card = card;
        GameHelper.CardPayment.sms = sms;
        GameHelper.CardPayment.cardMobi = cardMobi;
        GameHelper.CardPayment.cardVina = cardVina;
        GameHelper.CardPayment.cardVietel = cardVietel;
        GameHelper.CardPayment.smsMobi = smsMobi;
        GameHelper.CardPayment.smsVina = smsVina;
        GameHelper.CardPayment.smsVietel = smsVietel;
    }

    private void setDataPromotion(JSONNode promotionInapp)
    {
        //int value = promotionInapp["value"].AsInt;
        MyInfo.PROMOTION_IN_APP = promotionInapp;
        PlayerPrefs.SetInt("isShowed", -1);
    }

    void ConfigEnableGame(JSONNode _node)
    {
		string platform = "android";
#if UNITY_ANDROID
				        platform = "android";
#elif UNITY_IOS
				        platform = "ios";
#endif
        GameHelper.dictEnableGame = new Dictionary<string, bool> ();
        if (!string.IsNullOrEmpty (_node ["xito"].ToString())) {
            SetEnableGame ("xito", _node ["xito"] [platform].AsInt);
		}
		if (!string.IsNullOrEmpty (_node ["taixiu"].ToString())) {
            SetEnableGame ("taixiu", _node ["taixiu"] [platform].AsInt);
		}
		if (!string.IsNullOrEmpty (_node ["maubinh"].ToString())) {
            SetEnableGame ("maubinh", _node ["maubinh"] [platform].AsInt);
		}
		if (!string.IsNullOrEmpty (_node ["phom"].ToString())) {
            SetEnableGame ("phom", _node ["phom"] [platform].AsInt);
		}
		if (!string.IsNullOrEmpty (_node ["tienlen"].ToString())) {
            SetEnableGame ("tienlen", _node ["tienlen"] [platform].AsInt);
		}
        if (!string.IsNullOrEmpty(_node["bar"].ToString()))
        {
            SetEnableGame("bar", _node["bar"][platform].AsInt);
        }
    }
	void SetEnableGame(string _name, int _version){
        if (GameHelper.dictEnableGame.ContainsKey (_name))
			GameHelper.dictEnableGame [_name] = _version != 0;
				else
		GameHelper.dictEnableGame.Add (_name, _version != 0);

	}

	void RspConfigServer(JSONNode _nodeSFS){
		API.DomainSFS = _nodeSFS ["ip"].Value;
		API.PortSFS = _nodeSFS ["port"].AsInt;
	}

	bool EnablePayment = true;
	JSONNode NodeVersionPayment;

    
    //SetEnable sau khi lay thong tin user ktra
    void RspConfigPayment(bool _enable, JSONNode _nodeVersion, int _vippoint){
		EnablePayment = GameHelper.EnablePayment = _enable;
        NodeVersionPayment = _nodeVersion;
        //Debug.LogError("Cai Noi Gi Day===ConfigPayment"+ _enable);
	}
	void CheckConfigPayment(){

		bool isEnable = EnablePayment;


		if (isEnable) {
		
			
			
			if (isEnable) {
						
#if UNITY_ANDROID
				JSONNode nodePlatform = NodeVersionPayment;
				isEnable = nodePlatform ["payment"].AsInt == 1;
				
				if (isEnable) {
                    int curVersion = int.Parse(Application.version.Split('.')[0]);
                    isEnable = nodePlatform ["version"].AsInt >= curVersion;
                }
#elif UNITY_IOS
				JSONNode nodePlatform = NodeVersionPayment["ios"];
					isEnable = nodePlatform ["payment"].AsInt == 1;
		
					if (isEnable){
						isEnable = nodePlatform["version"].AsInt >= API.Version;
					}
#else
					isEnable = false;
#endif
			}
		}
		GameHelper.EnablePayment = isEnable;
        //Debug.LogError("Cai Noi Gi Day===CheckConfigPayment" + isEnable);
    }
    JSONNode nodeAvatar;
    /// <summary>
    /// CONFIG AVATAR & VIPPOINT
    /// </summary>
    /// <param name="_json">Json.</param>
    void RspAssets (JSONNode _nodeAssets)
	{
        Debug.Log("RSP ASSETS");
		//Luu hinh Avatar
		nodeAvatar = _nodeAssets ["avatar"];        
        //int countAvatar = 0;		
		//string name = nodeAvatar [countAvatar].Value;
        for(int i = 0; i < nodeAvatar.Count; i++)
        {
            DataHelper.AddAvatar(nodeAvatar[i]);
        }
        isFinishSpriteAvt = true;
        if (isRequestLogin && IsOkLogin)
        {
            Debug.Log("=== Avatar => Login ===");
            if (!isRequestingSignIn)
            {
                if (selectLoginType == LoginType.GameAccount)
                {
                    RequestSignIn(Username, Password);
                }
                else if (selectLoginType == LoginType.QuickLogin)
                {
                    LoginWithGuest();
                }
                else if (selectLoginType == LoginType.Facebook)
                {
                    loginWithFaceBook();
                }
            }
        }
    }    

	void ShowMaintenance(){
		popupAlert.Show ("Máy chủ hiện đang bảo trì. Vui lòng quay lại sau.", Application.Quit);
	}

				[Header("VersionUpdate . Version . BundleVersionCode")]
				[SerializeField]
				string sVersion;

	void Init()
	{
		isFinishSpriteAvt = false;
		sfs = SFS.Instance;

		View.Init (this);
		View.ShowInfoSignIn (GameHelper.GetUsername (), GameHelper.GetPass ());
		popupAlert.Init ();
		FogetPassword.Init ();


//		RequestInit ();

		if (DontDestroyManager.instance)
			return;
		
		GameObject obj = Instantiate (objDontDestroy) as GameObject;
		txtVersion.text = "v" + API.VersionUpdate.ToString () + "." + API.Version.ToString () + "." + sVersion; 

	}

	public void BtnFogetPasswordOnClick(){
		
		PlaySound (SoundManager.BUTTON_CLICK);

//		if (!IsCheckMaintenance) {
				API.Instance.RequestInit (gate, (_json0) => {

				if (JSONNode.Parse (_json0) ["off"].AsInt == 1) {
					ShowMaintenance ();
				}else{

				FogetPassword.ShowPanel();
				}
			});
//		}else
//
//			FogetPassword.Show ();
	}
	void PlaySound(string _sound){
		SoundManager.PlaySound (_sound);
	}

	void SaveAvatar(string _name){
		string url = API.PREFIX_AVT + _name;

		StartCoroutine (GameHelper.Thread (url, (_spr) => {
			DataHelper.AddAvatar(_name, _spr);
		}));
	}





  



    public void OnLoginSFS()
	{
	    GameHelper.ChangeScene (GameScene.HomeSceneV2);
	}

	public void BtnParentOnClick(bool isPlaySound = true)
	{
		isPanelSignIn = !isPanelSignIn;
		View.SetEnablePanel (isPanelSignIn);

		View.ShowTextParent (isPanelSignIn ? "- "+ ConstText.SignUp+" -" : "- "+ConstText.SignIn+" -");

		if(isPlaySound) PlaySound (SoundManager.BUTTON_CLICK);
	}


	public void BtnSignInOnClick(string _uname, string _pass, bool isPlaySound = true)
	{
        MyInfo.AVATAR_FACEBOOK_LOAD = false;
        Debug.Log ("===== SignIn OnClick =====");

		selectLoginType = LoginType.GameAccount;
		Username = _uname;
		Password = _pass;


		if(isPlaySound) PlaySound (SoundManager.BUTTON_CLICK);

		if (string.IsNullOrEmpty (Username) || string.IsNullOrEmpty (Password)) {
			View.ShowErrorSignIn ("Vui lòng nhập tài khoản và mật khẩu.");
			return;
		}

        isRequestLogin = true;

        LoadingManager.Instance.ENABLE = true;

        if (IsFinishInitConfig == false)
        {
			Debug.Log ("Chua Init, INIT thoi!");
			RequestInit ();

		}
        else
        {
			Debug.Log ("Init roi, dang nhap thoi");
			RequestSignIn (_uname, _pass);
		}
    }

				[SerializeField]
				bool IsOnline = true;
    [SerializeField]
    bool IsShopCardFromService = false;
    void Login()
    {
		if (GameHelper.EnablePayment)
			CheckConfigPayment ();
		SFS.Instance.InitSmartfox (IsOnline);
        MyInfo.SHOW_SHOP_FROM_SERVICE = IsShopCardFromService;
        //Debug.LogError("Cai Noi Gi Day Login=============" + GameHelper.EnablePayment);
    }
   
    void RequestSignIn(string _uname, string _pass)
    {

        if (isRequestingSignIn)
        {
            Debug.Log("Ko dang nhap dc - IsRequestingSignIn");
            return;
        }

        //		Debug.Log (" +++++ LOGIN CLICK +++++ ");
        View.ShowErrorSignIn();

        if (string.IsNullOrEmpty(_uname))
        {
            View.ShowErrorSignIn(ConstText.UsernameEmpty);
        }
        else if (_uname.Length < MIN_CHAR_USERNAME)
        {
            View.ShowErrorSignIn("Tên đăng nhập chứa ít nhất " + MIN_CHAR_USERNAME + " ký tự.");
        }
        else if (_uname.Length > MAX_CHAR_USERNAME)
        {
            View.ShowErrorSignIn("Tên đăng nhập chứa nhiều nhất " + MAX_CHAR_USERNAME + " ký tự.");
        }
        else if (_uname.Contains(" "))
        {
            View.ShowErrorSignIn(ConstText.UsernameSpecial);
        }
        else if (string.IsNullOrEmpty(_pass))
        {
            View.ShowErrorSignIn(ConstText.PasswordEmpty);
        }
        else if (_pass.Length < MIN_CHAR_PASSWORD)
        {
            View.ShowErrorSignIn("Mật khẩu chứa ít nhất " + MIN_CHAR_PASSWORD + " ký tự.");
        }
        else if (_pass.Length > MAX_CHAR_PASSWORD)
        {
            View.ShowErrorSignIn("Mật khẩu chứa nhiều nhất " + MAX_CHAR_PASSWORD + " ký tự.");
        }
        else
        {
            isRequestingSignIn = true;

            Debug.Log("**** Request API SignIn");
            Debug.Log(_uname + "   "  + _pass);

            API.Instance.RequestSignIn(_uname, _pass, RspSignIn, LoginType.GameAccount);
            GameHelper.SaveUsername(_uname);
            GameHelper.SavePass(_pass);
            PlayerPrefs.SetInt("LoginType", (int)LoginType.GameAccount);

            LoadingManager.Instance.ENABLE = true;


            StartCoroutine(checkTextLogin());
            LoadingManager.Instance.setHandleTimeOut(onConnectServerTimeOut);
        }
    }

    public void onFacebookLoginComplete()
    {
        isRequestingSignIn = true;
        LoadingManager.Instance.ENABLE = true;
        Debug.Log("**** Request API SignIn with FBID ");

        API.Instance.RequestSignIn("", "", RspSignIn, LoginType.Facebook);
        PlayerPrefs.SetInt("LoginType", (int)LoginType.Facebook);

        

        StartCoroutine(checkTextLogin());
        LoadingManager.Instance.setHandleTimeOut(onConnectServerTimeOut);
    }


    public void onConnectServerTimeOut()
    {
        Debug.Log("testttttttt");
        if (this != null)
        {
            StartCoroutine(noteSystemError());
        }
    }

    private IEnumerator noteSystemError()
    {
        string url = API.Instance.domainAPI + "/game/noticeSystemError";

        WWWForm form = new WWWForm();

        form.AddField(ParamKey.USER_NAME, View.inputUsernameSignIn.text);
        form.AddField("BugState", GameHelper.bugState);

        Dictionary<string, string> header = form.headers;
        header.Add(API.ApiKey, API.Instance.getAPIKey());


        WWW service = new WWW(url, form.data, header);
        yield return service;
        if (string.IsNullOrEmpty(service.error) == false) //download error
        {
            yield break;
        }
        Debug.LogWarning("send time outttttttttttttttttttttttttttttttttt");

    }
    private IEnumerator checkTextLogin()
    {
        WWW service = new WWW("http://google.com.vn");
        yield return service;
        if (string.IsNullOrEmpty(service.error) == false) //download error
        {
            yield break;
        }
        LoadingManager.Instance.setTextLoginTimeOut();
    }




    public void KiemTraTen(string ten)
    {
        isRequestingSignIn = true;
             LoadingManager.Instance.ENABLE = true;
        Debug.Log("**** Request API SignIn with FBID ");

        API.Instance.RequestSignIn(ten, "", RspSignIn, LoginType.Facebook);
        PlayerPrefs.SetInt("LoginType", (int)LoginType.Facebook);

        //    StartCoroutine(checkTextLogin());
          LoadingManager.Instance.setHandleTimeOut(onConnectServerTimeOut);
    }


    void RspSignIn(string _json)
	{
        JSONNode json = JSONNode.Parse(_json);
        Debug.LogWarning("Vo toi RspSignIn .................... json[status] = " + json["status"].AsInt);

        if (json["status"].AsInt == -1)
        {
            LoadingManager.Instance.ENABLE = false;
            string TenUser = json["facebook_name"].Value;
            List<string> DanhSachTen = new List<string>();
            int sl = json["available_name"].Count;
            for (int i = 0; i < sl; i++)
            {
                DanhSachTen.Add(json["available_name"][i].Value);
            }

            bool check = true;
            for (int i = 0; i < DanhSachTen.Count; i++)
            {
                if (TenUser.Equals(DanhSachTen[i]))
                {
                    check = false;
                }
            }

            if (check == true) // khoong trung
            {
                Debug.LogWarning("Ten Khong Kha Dung.................... ");
                View.ShowTenDangNhap(TenUser, DanhSachTen, false);

            }
            else // trung
            {
                Debug.LogWarning("Ten Kha Dung.................... ");
                View.ShowTenDangNhap(TenUser, DanhSachTen, true);

                PlayerPrefs.SetInt("LoginType", (int)LoginType.Facebook);
                //StartCoroutine(checkTextLogin());
                //LoadingManager.Instance.setHandleTimeOut(onConnectServerTimeOut);
            }

            return;
        }
        else
        {
            isRequestingSignIn = true;
            LoadingManager.Instance.ENABLE = true;
            Debug.Log("**** Request API SignIn with FBID ");
            StartCoroutine(checkTextLogin());
            LoadingManager.Instance.setHandleTimeOut(onConnectServerTimeOut);
        }

        isRequestingSignIn = false;
        
        
		View.ShowTest (json.ToString ());
        print("RspSignIn===XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX: " + json.ToString());
        if (json [KEY_CODE].AsInt == 601) {
			View.ShowErrorSignIn (ConstText.UsernameExist);
			LoadingManager.Instance.ENABLE = false;
		} else if (json [KEY_CODE].AsInt == 401) {
			View.ShowErrorSignIn ("Tên đăng nhập hoặc mật khẩu không đúng.");
			LoadingManager.Instance.ENABLE = false;
		} else {

			if (!string.IsNullOrEmpty (json ["bonus"] ["daily_vip"] ["chip"].Value)) {
				GameHelper.IsHaveBonusDaily = long.Parse (json ["bonus"] ["daily_vip"] ["chip"].Value);
			}

            if (!string.IsNullOrEmpty(json["bonus"]["revenue_assets_daily"]["chip"].Value))
            {
                GameHelper.IsHaveBonusDaily_item = long.Parse(json["bonus"]["revenue_assets_daily"]["chip"].Value);
            }

            MyInfo.MY_ID_VIP = Int32.Parse(json["current_vip"].Value);

            MyInfo.MY_BOR_AVATAR = Int32.Parse(json["vip_avatar_boder"].Value);

            MyInfo.MY_VIP_COLLECT = float.Parse(json["items"]["vip_collect"].Value);
           
            MyInfo.BorderAvatarName = json["vip_avatar_boder"].Value;
            if (!string.IsNullOrEmpty(json["first_payment"]))
            {
                MyInfo.FIRST_PAYMENT = Int32.Parse(json["first_payment"].Value);
            }
            
            
            string t = MyInfo.GetVipName();

            MyInfo.NAME = json [API.UNAME].Value;
            if(Report.report != null)
                Report.report.userName = MyInfo.NAME;
            MyInfo.CHIP = long.Parse (json [API.ITEM] [API.CHIP].Value);

            MyInfo.GEM = long.Parse(json[API.ITEM][API.GEM].Value);

            MyInfo.GOLD = long.Parse (json [API.ITEM] [API.GOLD].Value);
			MyInfo.VIPPOINT = int.Parse (json [API.ITEM] [API.VIPPOINT].Value);

            MyInfo.AvatarName = json [API.AVATAR].Value;
            Debug.Log("MyInfo.AvatarName "+ MyInfo.AvatarName);
			MyInfo.TOKEN = json [API.TOKEN] [API.KEY].Value;
			MyInfo.EXPIRED = json [API.TOKEN] [API.EXPIRED].Value;

			MyInfo.FULLNAME = json [API.INFO] [API.FULL_NAME].Value;
			MyInfo.PHONE = json [API.INFO] [API.PHONE].Value;
			MyInfo.EMAIL = json [API.INFO] [API.EMAIL].Value;
			MyInfo.CMND = json [API.INFO] [API.CMND].Value;
			MyInfo.ID = json [API.ID].Value;
            MyInfo.XAM_BOI_TOAN = int.Parse(json[API.ITEM]["xam"].Value);
            MyInfo.GEM = int.Parse(json[API.ITEM]["gem"].Value);
            MyInfo.BAR_POINT = int.Parse(json[API.ITEM]["bar_point"].Value);
            
            MyInfo.UNREAD_INBOX = int.Parse(json[API.UNREAD_INBOX].Value);
            //Debug.LogError("MyInfo.UNREAD_INBOX: " + MyInfo.UNREAD_INBOX + "   " + IsOkLogin);
            isRequestLogin = true;

            API.Instance.RequestGetDataBoderAvatar(RspDataBoderAvatar);


            //config Payment again
            if (GameHelper.EnablePayment)
				GameHelper.EnablePayment = json ["payment_status"].AsInt != 0;
            
            //Chua Init
            if (IsFinishInitConfig == false)
            {
				Debug.Log ("___ LOGIN => INIT ASSETS ___");
				RequestInit ();
			}
            else if (IsOkLogin)
            {//Da Init va tai hinh kip
                Login ();
			}
            else
            {
                Debug.Log (" Init va chua tai hinh kip => Nothing, cho` tai hinh xong => AutoLogin");
			}

		}
	}

    public void BtnSignUpOnClick(string _uname, string _pass, string _passConfirm)
	{
		PlaySound (SoundManager.BUTTON_CLICK);

        userNameSignUp = _uname;
        userPass = _pass;
        userPasConfirm = _passConfirm;
        API.Instance.RequestInit(gate, signUpRespone);
    }

    private string userNameSignUp = "", userPass = "", userPasConfirm = "";
    private void signUpRespone(string signUpData)
    {
        if (JSONNode.Parse(signUpData)["off"].AsInt == 1)
        {
            ShowMaintenance();
        }
        else
        {
            SignUp(userNameSignUp, userPass, userPasConfirm);
        }
    }

    private bool isAutoSignUp = false;
	void SignUp(string _uname, string _pass, string _passConfirm)
    {

		View.ShowErrorSignUp ();
		if (string.IsNullOrEmpty (_uname))
        {
			View.ShowErrorSignUp (ConstText.UsernameEmpty);
		}
        else if (_uname.Length < MIN_CHAR_USERNAME)
        {
			View.ShowErrorSignUp ("Tên đăng nhập tối thiểu " + MIN_CHAR_USERNAME + " ký tự");
		}
        else if (_uname.Length > MAX_CHAR_USERNAME)
        {
			View.ShowErrorSignUp ("Tên đăng nhập tối đa " + MAX_CHAR_USERNAME + " ký tự");
		}
        else if (_uname.Contains (" "))
        {
			View.ShowErrorSignUp (ConstText.UsernameSpecial);
		}
        else if (string.IsNullOrEmpty (_pass))
        {
			View.ShowErrorSignUp (ConstText.PasswordEmpty);
		}
        else if (_pass.Length < MIN_CHAR_PASSWORD)
        {
			View.ShowErrorSignUp ("Mật khẩu tối thiểu " + MIN_CHAR_PASSWORD + " ký tự");
		}
        else if (_pass.Length > MAX_CHAR_PASSWORD)
        {
			View.ShowErrorSignUp ("Mật khẩu tối đa " + MAX_CHAR_PASSWORD + " ký tự");
		}
        else if (!_pass.Equals (_passConfirm))
        {
			View.ShowErrorSignUp (ConstText.PasswordConfirmWrong);
		}
        else if (string.IsNullOrEmpty (_passConfirm))
        {
			View.ShowErrorSignUp (ConstText.PasswordConfirmWrong);
		}
        else
        {
			API.Instance.RequestSignUp (_uname, _pass, (_json) => {

				JSONNode json = JSONNode.Parse (_json);

				if (json ["code"].AsInt == 500)
                {
					View.ShowErrorSignUp (ConstText.UsernameExist); // Tài khoản đã tồn tại
                    if (isAutoSignUp == true)
                    {
                        autoSignUp();
                    }
                }
                else if (json ["code"].AsInt == 601)
                { 
					View.ShowErrorSignUp (ConstText.UsernameExist); // Tài khoản đã tồn tại
                    if (isAutoSignUp == true)
                    {
                        autoSignUp();
                    }
                }
                else if (json ["code"].AsInt == 602)
                {
				    View.ShowErrorSignUp (ConstText.LimitDevice); // Thiết bị này không thể đăng ký thêm tài khoản
                    AlertController.api.showAlert(ConstText.LimitDevice);

                    _uname = json["user_name"].ToString();
                    _pass = json["pass"].ToString();
                    View.ShowInfoSignIn(_uname, _pass);
                    autoSignComplete(_uname, _pass);

                }
                else if (!string.IsNullOrEmpty (json ["id"].Value)) // Đăng ký thành công
                {
                    if (isAutoSignUp == false)
                    {
                        BtnParentOnClick(false);
                        View.ShowInfoSignIn(_uname, _pass);
                        View.ShowInfoSignUp();
                        View.ShowErrorSignIn(ConstText.SignUpSuccess);
                    }
					
                    //										API.Instance.RequestSignIn(_uname, _pass, RspSignIn);
                    autoSignComplete(_uname , _pass);

                }

				LoadingManager.Instance.ENABLE = false;

			});
		}
	}


    public void TestPlayNow()
    {
        string Name = "";
        bool Success = true;
        string Pass = API.DEVICE_ID;
        
        int count = 0;
        do
        {
            if (count > 3)
            {
                Name = RandomName_2();
            }
            else
            {
                Name = RandomName();
                ++count;
            }           
            API.Instance.RequestSignUp(Name, Pass, (_json) => {

                JSONNode json = JSONNode.Parse(_json);

                if (json["code"].AsInt == 500)
                {
                    Debug.Log("Lỗi: Tài khoản đã tồn tại");
                    Success = false;
                }
                else if (json["code"].AsInt == 601)
                {
                    Debug.Log("Lỗi: Tài khoản đã tồn tại 2");
                    Success = false;
                }
                else if (json["code"].AsInt == 602)
                {
                    Debug.Log("Lỗi: Thiết bị này không thể đăng ký thêm tài khoản");
                    Success = false;
                }
                else if (string.IsNullOrEmpty(json["id"].Value))
                {
                    Debug.Log("Lỗi: không xác định");
                    Success = false;
                }                
            });

        } while (Success == false);
        
        BtnParentOnClick(false);
        View.ShowInfoSignIn(Name, Pass);
        View.ShowInfoSignUp();
        View.ShowErrorSignIn(ConstText.SignUpSuccess);

        LoadingManager.Instance.ENABLE = false;
        
        Debug.Log("Tao thanh cong --- Ten dang nhap: " + Name + " -- pass: " + Pass);

    }

    private string RandomName()
    {
        string Name = "";
        int number = UnityEngine.Random.Range(0, 10000);
        if (number < 10)
        {
            Name = "Player000" + number.ToString();
        }
        else if (number < 100)
        {
            Name = "Player00" + number.ToString();
        }
        else if (number < 1000)
        {
            Name = "Player0" + number.ToString();
        }
        else
        {
            Name = "Player" + number.ToString();
        }
        return Name;
    }

    private string RandomName_2()
    {
        string Name = "";
        int number = UnityEngine.Random.Range(0, 100000);
        if (number < 10)
        {
            Name = "Player0000" + number.ToString();
        }
        else if (number < 100)
        {
            Name = "Player000" + number.ToString();
        }
        else if (number < 1000)
        {
            Name = "Player00" + number.ToString();
        }
        else if (number < 10000)
        {
            Name = "Player0" + number.ToString();
        }
        else
        {
            Name = "Player" + number.ToString();
        }        
        return Name;
    }

    void RspSignUp(string _json)
		{
				JSONNode json = JSONNode.Parse (_json);

				View.ShowTest (json.ToString ());

		if (json ["code"].AsInt == 500) {
			View.ShowErrorSignUp ("Tài khoản đã tồn tại!");
		} else if (!string.IsNullOrEmpty (json ["id"].Value)) {
//						View.SetToogleSignIn(true);
//						View.ShowInfoSignIn(_uname, _pass);
			View.ShowInfoSignUp ();
			isPanelSignIn = true;
			View.SetEnablePanel (isPanelSignIn);

		}
		}

	public void ToggleSignInOnChange(bool _enable)
	{
		PlaySound (SoundManager.BUTTON_CLICK);

		View.SetEnablePanel (_enable);
		View.ShowErrorSignIn ();
		View.ShowErrorSignUp ();
	}

	public void OnSFSResponse(GamePacket param)
	{
		Debug.Log ("RSP - Lobby - CMD _ " + param.cmd + " - " + param.param.ToJson ());

		switch (param.cmd) {
		case CommandKey.GET_PLAY_BOARD_LIST:
						
			break;
		}
	}
    public void showFanPage()//call editor
    {
        Application.OpenURL("https://www.facebook.com/VipGame777-113486906690253/?modal=admin_todo_tour");
    }

    private string hotlineNumber = "01269.056.083";
    public void callHotLine()
    {
        Application.OpenURL("tel://" + hotlineNumber);
    }
    private static string extraMessage;

				//***
//	private static void HandleNotificationReceived(OSNotification notification) {
//		OSNotificationPayload payload = notification.payload;
//		string message = payload.body;
//
//		print("GameControllerExample:HandleNotificationReceived: " + message);
//		print("displayType: " + notification.displayType);
//		extraMessage = "Notification received with text: " + message;
//
//		Dictionary<string, object> additionalData = payload.additionalData;
//		if (additionalData == null) 
//			Debug.Log ("[HandleNotificationReceived] Additional Data == null");
//		else
//			Debug.Log("[HandleNotificationReceived] message "+ message +", additionalData: "+ Json.Serialize(additionalData) as string);
//	}
//
//	// Called when a notification is opened.
//	// The name of the method can be anything as long as the signature matches.
//	// Method must be static or this object should be marked as DontDestroyOnLoad
//	public static void HandleNotificationOpened(OSNotificationOpenedResult result) {
//		OSNotificationPayload payload = result.notification.payload;
//		string message = payload.body;
//		string actionID = result.action.actionID;
//
//		print("GameControllerExample:HandleNotificationOpened: " + message);
//		extraMessage = "Notification opened with text: " + message;
//
//		Dictionary<string, object> additionalData = payload.additionalData;
//		if (additionalData == null) 
//			Debug.Log ("[HandleNotificationOpened] Additional Data == null");
//		else
//			Debug.Log("[HandleNotificationOpened] message "+ message +", additionalData: "+ Json.Serialize(additionalData) as string);
//
//		if (actionID != null) {
//			// actionSelected equals the id on the button the user pressed.
//			// actionSelected will equal "__DEFAULT__" when the notification itself was tapped when buttons were present.
//			extraMessage = "Pressed ButtonId: " + actionID;
//		}
//	}

	public void OnConnectionLost()
	{
		popupAlert.Show (ConstText.ErrorConnection, () => {
			popupAlert.Hide();
		});
	}

}
