﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using BaseCallBack;
using SimpleJSON;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using System;

public class HomeControllerV2 : MonoBehaviour
{
    public static HomeControllerV2 api;
    [SerializeField]
	HomeViewV2 View;

    [SerializeField]
    GameObject PanelPopup;

    [SerializeField]
	PopupAlertManager popupAlert;
	[SerializeField]
	PopupFullManager popupFull;
	[SerializeField]
	PopupInvited popupInvited;
	//[SerializeField]
	//PopupAchivementManager Achivement;
    

    //	[SerializeField]
    //	PopupNotifyManager Notify;
    [SerializeField]
	PopupInfomationManager InfoUser;
	private SFS sfs{
		get{ return SFS.Instance; }
	}
	[SerializeField]
	PopupModuleManager Module;
	[SerializeField]
	PopupShopManager Shop;
	[SerializeField]
	PopupEventManager Event;
	[SerializeField]
	public PopupInboxManager Inbox;

    [SerializeField]
    private GiftCode giftCode = null;

    [SerializeField]
    Button BtnTLDL, BtnTLMN, BtnXT, BtnMB, BtnPhom, BtnTaiXiu;

    //[SerializeField]
    //private PopupMissionManager PanelMission = null;

    [SerializeField]
    private GameObject TitleGameLogo;
    [SerializeField]
    private List<Sprite> gamelogos;
    [SerializeField]
    SlotController SlotMachine;
    [SerializeField]
    GameObject BorderAvatarMark;
    //[SerializeField]
    //ParticleSystem ParticleLogo;
    [SerializeField]
    Button BtnXemBoi, BtnDapHeo, BtnSpecWheel, BtnLode, btnFacebook, BtnEvent, BtnTangChip, BtnCho, BtnTangGem, BtnSukien, BtnDangTin;

    [SerializeField]
    EstateControll Estate;

    [SerializeField]
    MadControll Mad;
    [SerializeField]
    GameObject PanelUI_Home, PanelWait;


    [SerializeField]
    GameObject BgMenu, GroupListIconTop, BtnExpand, BtnHide;
    [SerializeField]
    ChangeNameController PanelChangeName;
    [SerializeField]
    Button BtnBangHoi, BtnListBangHoi;
    [SerializeField]
    Button BtnThongBao;
[SerializeField]
    GameObject LogoThanBai, ImageThanBai, BtnChuPhucThanBai, TxtNameThanBai;


	[SerializeField]
    Button BtnShopPhongThuy;

    [SerializeField]
    TinTuc_Controll TinTucControll;
    enum ScreenHome
	{
		Home,
		//Achivement,
	}
	ScreenHome _screen;

	void Awake()
	{
        if (api != null)
        {
            Destroy(api.gameObject);
        }
        api = this;

        Init ();
        visibleGameIcon();
        //Enable Popup Panel
        SlotMachine.gameObject.SetActive(false);
        PanelPopup.SetActive(true);
        popupAlert.gameObject.SetActive(true);
        //panelNapTichLuy.SetActive(false);
        //Kiem Tra mở Promotion x2 thẻ nạp cho user
        PromotionPopup();
        BorderAvatarMark.SetActive(true);

        if (MyInfo.MY_BOR_AVATAR != -1)
        {
            StartCoroutine(WaitAndChageBorAvatar(1));
        }else
        {
            BorderAvatarMark.SetActive(false);
        }

        //set enable module home
        //Debug.LogError("Cai Noi Gi Day=============" + GameHelper.EnablePayment);
        //BtnXemBoi.gameObject.SetActive(GameHelper.EnablePayment);
        BtnDapHeo.gameObject.SetActive(GameHelper.EnablePayment);
        BtnSpecWheel.gameObject.SetActive(GameHelper.EnablePayment);
        BtnLode.gameObject.SetActive(GameHelper.EnablePayment);
        btnFacebook.gameObject.SetActive(GameHelper.EnablePayment);
		BtnEvent.gameObject.SetActive(GameHelper.EnablePayment);
        BtnTangChip.gameObject.SetActive(GameHelper.EnablePayment);
        BtnTangGem.gameObject.SetActive(GameHelper.EnablePayment);

        BtnSukien.gameObject.SetActive(GameHelper.EnablePayment);
        BtnDangTin.gameObject.SetActive(GameHelper.EnablePayment);
        if (MyInfo.POST_NEWS_STATUS == 0)
        {
            BtnDangTin.gameObject.SetActive(false);
        }

        //BtnCho.gameObject.SetActive(GameHelper.EnablePayment);
		BtnThongBao.gameObject.SetActive(GameHelper.EnablePayment);
		BtnShopPhongThuy.gameObject.SetActive(GameHelper.EnablePayment);
        if (MyInfo.SHOP_PHONGTHUY_STATUS == 0)
        {
            BtnShopPhongThuy.gameObject.SetActive(false);
        }

        BgMenu.SetActive(false);
        GroupListIconTop.SetActive(false);
        BtnExpand.SetActive(true);
        BtnHide.SetActive(false);
        PanelChangeName.gameObject.SetActive(false);

        if (MyInfo.EVENT_STATUS == 0)
        {
            BtnEvent.gameObject.SetActive(false);
        }

        if (MyInfo.MARKET_STATUS == 0)
        {
            //BtnCho.gameObject.SetActive(false);
        }
        if (MyInfo.CLAN_STATUS == 0)
        {
            BtnBangHoi.gameObject.SetActive(false);
            BtnListBangHoi.gameObject.SetActive(false);
        }


        if (GameHelper.EnablePayment == true && DataHelper.FIST_NEW == false)
        {
            TinTucControll.XemTinTuc_Test();
            DataHelper.FIST_NEW = true;
        }



        MyInfo.CheckMoney(null, CheckMoneyAndOpenShop);

        //show Chat Button
        AlertController.api.ShowChatButton();

        Estate.RequestLayDuLieuVatPham();




    }





    public void LayDuLieuTaiSan()
    {

        DataHelper.ClearData_TaiSan();

        Estate.RequestLayDuLieuVatPham_SoHuu();

        //   GameObject obj = Instantiate(objDontDetroy) as GameObject;

    }


    public void OpenMap()
    {
        Debug.LogError("vo toi day");
        PanelWait.SetActive(true);
        StartCoroutine(Wait(2f));
    }

    public void ClosePanelWait()
    {
        PanelWait.SetActive(false);
    }

    private IEnumerator Wait(float time)
    {

        yield return new WaitForSeconds(time);

        Mad.Init();
        Estate.InitLEFT();
        PanelUI_Home.SetActive(true);
        PanelWait.SetActive(false);

    }









    public void OpenPanelChangeName()
    {
        PanelChangeName.Show(OnCloseChangeName);
    }

    private void OnCloseChangeName(string name_key)
    {
        if (name_key != "")
        {
            UpdateInfoUser();
            SFS.Instance.UpdateUserName(name_key);
        }
       
    }

    public void ExpandMenu()
    {
        BgMenu.SetActive(true);
        GroupListIconTop.SetActive(true);
        BtnExpand.SetActive(false);
        BtnHide.SetActive(true);
    }
    public void HideAllMenu()
    {
        BgMenu.SetActive(false);
        GroupListIconTop.SetActive(false);
        BtnExpand.SetActive(true);
        BtnHide.SetActive(false);
    }

    public void LoadModuleChipToGem()
    {
        SceneManager.LoadScene(GameScene.ChipToGemScene.ToString());
    }
    public void showFacebookChoChip()
    {
        Application.OpenURL("https://www.facebook.com/groups/chototchip/");
    }

    private IEnumerator WaitAndChageBorAvatar(float waitTime)
    {     
        yield return new WaitForSeconds(waitTime);
        Sprite spr = DataHelper.GetBoderAvatar(MyInfo.BorderAvatarName);
        if (spr != null)
        {
            BorderAvatarMark.SetActive(false);
            View.ShowBorAvatar(spr);
        }
        else
        {
            BorderAvatarMark.SetActive(true);
        }
        
       
    }







    void PromotionPopup()
    {
        Debug.Log("Khuong============MyInfo.PROMOTION_IN_APP[value]:::" + MyInfo.PROMOTION_IN_APP["value"]);
        if (MyInfo.PROMOTION_IN_APP["value"].AsInt > 1)
        {
            popupAlert.showAlertPromotion();
        }
    }
#if UNITY_EDITOR

    void Update(){
		if (Input.GetKeyDown (KeyCode.Space)) {
			string token = "fdfjfeeofkmkhahpofdmfiba.AO-J1Ozhgv2rwlFOyFb0TSL6w58otdFVn5GnCUAjjpcJqAbqcQFrxLKmYR3_FfCmgMWN5cD7JghZlJg5hldtHXqTkgTWlv-ImB9Rj92qKATMm6nTfc2NrRkuaK6p18bWHcDgZgWnO9UK";
			API.Instance.RequestPaymentInAppAndroid (
				"googleplay", 
				"com.b3b.zinplay.1", 
				token, 
				RspPaymentInApp);
		}
	}
	#endif

	void RspPaymentInApp (string s)
	{
		Debug.Log (s);
	}

	#region Properties

	long chip;
	long CHIP {
		get {
			chip = MyInfo.CHIP;
			return chip;
		}
		set {
			chip = MyInfo.CHIP = value;            
		}
	}

    private void CheckMoneyAndOpenShop()
    {
        BtnShopInfoOnClick();
    }

    long gold;
	long GOLD {
		get {
			gold = MyInfo.GOLD;
			return gold;
		}
		set {
			gold = MyInfo.GOLD = value;
		}
	}


    long gem;
    long GEM
    {
        get
        {
            gem = MyInfo.GEM;
            return gem;
        }
        set
        {
            gem = MyInfo.GEM = value;
        }
    }


    int vippoint;
	int VIPPOINT{
		get{
			vippoint = MyInfo.VIPPOINT;
			return vippoint;
		}
		set{
			vippoint = MyInfo.VIPPOINT = value;
		}
	}

	#endregion

	public string FormatCash(long _value)
	{
		return Utilities.GetStringMoneyByLong (_value);
	}
	void PlaySound(string _sound){
        
    
		SoundManager.PlaySound (_sound);
	}
    [SerializeField]
    private GameObject showFillInfo = null;
    [SerializeField]
    private PopupSpecialWheelManager specWheelPopup;

    //[SerializeField]
    //private TournamentEvent tournamentEvent = null;
    void Init()
	{
        ShowThanBai();
        //		txtTest.text = "AAA";
        _screen = ScreenHome.Home;

		View.Init (this);
		View.ShowOrHideUnreadInbox();

        Debug.Log("MyInfo.BorderAvatarName luc dau = " + MyInfo.BorderAvatarName);


		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP),
			FormatCash(GOLD), MyInfo.AvatarName, MyInfo.BorderAvatarName,
			MyInfo.VIPPOINT,
            FormatCash(GEM)
        );





		popupInvited.Init (BtnCancelInvitedOnClick, BtnOkInvitedOnClick);
		//Achivement.Init (BackAchivementOnClick);
		popupAlert.Init ();
		popupFull.Init ();
		Module.InitHome (LogOutOnClick);
		Shop.Init (UpdateInfoUser, BtnBackShopOnClick);
		Event.Init (BackEventOnClick);
		//Inbox.Init(InboxExitOnClick);
		InfoUser.Init (BtnSaveInfoOnClick, BtnCloseInfoOnClick);
		
		if (PlayerPrefs.GetInt ("First", 0) == 0)
        {
            //popupAlert.Show ("Cập nhật thông tin để bảo mật tài khoản của bạn.", ()=>{
            //	InfoUser.Show();
            //	popupAlert.Hide();
            //});
            showFillInfo.gameObject.SetActive(true);
            PlayerPrefs.SetInt ("First", 1);
		}
        int rating = PlayerPrefs.GetInt("rating", 0);

        if (rating == 2)//login lan thu 2 => hien thi luon
        {
            if (GameHelper.EnablePayment)
            {
                AlertController.api.showAlert("Bạn có thích Vip777 không? hãy cho chúng tôi biết ý kiến của bạn về game nhé!", gotoStorePage, true);
            }
            
        }
        PlayerPrefs.SetInt("rating", rating + 1);
        if (MyInfo.CHIP < 1000000 && MyInfo.FIRST_PAYMENT==0)
        {
            ShowFirstPurchasePanel();
        }
        int specWheel = PlayerPrefs.GetInt("SpecWheel", 0);

        if (rating == 1)//login lan thu 2 => hien thi luon
        {
            if(GameHelper.EnablePayment) specWheelPopup.showPopupSpecWhell();
        }
        PlayerPrefs.SetInt("SpecWheel", specWheel + 1);


        //		View.SetEnableExchange (GameHelper.EnableExchange);

        LoadingManager.Instance.ENABLE = false;


        Debug.Log("GameHelper.IsHaveBonusDaily "+ GameHelper.IsHaveBonusDaily);

       


        if (GameHelper.IsHaveBonusDaily > 0) {

			popupAlert.Show ("Quà tặng VIP mỗi ngày\n" + Utilities.FormatVietnamCurrency(GameHelper.IsHaveBonusDaily)
				+ " chip", popupAlert.Hide);
			GameHelper.IsHaveBonusDaily = 0;
		}
		View.SetEnableCardWheel (GameHelper.EnableCardWheel);
        View.SetEnableBarGame(GameHelper.EnableBarGame);
		//Achivement.SetEnable (true);
        //tournamentEvent.initTournament();
    }
    bool isChucPhuc = false;
    public void ChucPhucThanBai()
    {
        if (isChucPhuc) return;
        GamePacket gp = new GamePacket("blessingGodCard");
        SFS.Instance.SendZoneRequest(gp);
    }
    void OnBlessingGodCard(GamePacket _param)
    {
        int status = _param.GetInt("status");
        if (status == 1)
        {
            isChucPhuc = true;
            AlertController.api.showAlert("Chúc Phúc Thành Công! Bạn Nhận Được " + FormatCash(_param.GetLong("received_chip")) + " chip");
            MyInfo.CHIP = _param.GetLong("chip");
            string chipuser = FormatCash(MyInfo.CHIP);
            View.UpdateChip(chipuser);            
        }
        else
        {
            AlertController.api.showAlert(_param.GetString("msg"));
        }
    }

    private void ShowThanBai()
    {
        TitleGameLogo.SetActive(true);
        LogoThanBai.SetActive(false);
        BtnChuPhucThanBai.SetActive(false);

        if (!GameHelper.EnablePayment) return;       

        if (DataHelper.MasterThanBai["name"].ToString() != "")
        {
            API.Instance.RequestLayIdBangHoi(MyInfo.ID, RspLayIdBangChinhMinh);            
        }
    }
    void RspLayIdBangChinhMinh(string _json)
    {
        Debug.LogWarning("RspLayIdBangHoi------------- " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string userBang = node["user_clan"]["$id"].Value;
        string BangMaster = node["master_clan"]["$id"].Value;

        Debug.Log("usserBang------------- " + userBang);
        Debug.Log("BangMaster------------- " + BangMaster);

        MyInfo.ID_BANG = userBang;
        MyInfo.ID_BANG_MASTER = BangMaster;
        
        LogoThanBai.SetActive(true);
        TitleGameLogo.SetActive(false);
        string Url = API.Instance.DOMAIN + "/" + DataHelper.MasterThanBai["image"];
        API.Instance.Load_Spr(Url, ImageThanBai.GetComponent<Image>());
        TxtNameThanBai.GetComponent<TMPro.TextMeshProUGUI>().text = DataHelper.MasterThanBai["name"].ToString();
        if(MyInfo.ID_BANG !="" && MyInfo.ID_BANG == DataHelper.MasterThanBai["clan_id"].ToString()) BtnChuPhucThanBai.SetActive(true);
    }
    void CheckBonusDaily_item()
    {
        if (GameHelper.IsHaveBonusDaily_item > 0)
        {

            popupAlert.Show("Doanh thu mỗi ngày\n" + Utilities.FormatVietnamCurrency(GameHelper.IsHaveBonusDaily_item)
                + " chip", popupAlert.Hide);
            GameHelper.IsHaveBonusDaily_item = 0;
        }
        else
        {
            popupAlert.Hide();
        }
    }


    public void ShowXemBoi()
    {
        //LoadingManager.Instance.ENABLE = true;
        //GameHelper.ChangeScene(GameScene.BoiToanScene);
    }
    public void ShowSlotMachine()
    {
        SlotMachine.Show();
    }
    public void ShowBarXengHoaQua()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.XengHoaQua);
    }
    private void gotoStorePage()
    {
        Application.OpenURL(GameHelper.getAppStoreURL());
    }
    public void BtnExchangeOnClick(){
//		Exchange.Show ();
	}
	void OnExChanged (string _msg)
	{
		popupAlert.Show (_msg, popupAlert.Hide);
		View.ShowInfoUser (
			Utilities.GetStringMoneyByLong (CHIP),
			Utilities.GetStringMoneyByLong (GOLD));
	}

	void OnBackExchange ()
	{
//		Exchange.Hide ();

	}

	void BtnBackShopOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		Shop.Hide ();
		View.SetEnablePanelUI (true);
        MyInfo.CheckMoney(null, CheckMoneyAndOpenShop);
    }

	void BtnShopInfoOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
		Shop.Show ();
		View.SetEnablePanelUI (false);
	}

	void BtnBackCardWheelOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		UpdateInfoUser ();
//		CardWheel.Hide ();
	}

	int GetGameID()
    {
        //if (GameHelper.gameKind == GameKind.Baucua) GameHelper.gameKind = GameKind.TLDL;

        switch (GameHelper.gameKind)
        {
		case GameKind.TLMN:
			return 5;
		case GameKind.MB:
			return 2;
		case GameKind.XT:
			return 1;
		case GameKind.Phom:
			return 4;
            case GameKind.TLDL:
                return 6;
                //		case GameKind.TX:
                //			return 3;
        }
		return 0;
	}
    private void visibleGameIcon()
    {
        return;
        switch (GameHelper.gameKind)
        {
            case GameKind.TLMN:
                BtnTLMN.gameObject.SetActive(false);
                //Image imglogo = TitleGameLogo.transform.GetChild(0).GetComponent<Image>();
                //imglogo.sprite = gamelogos.ElementAt((int)GameHelper.gameKind - 1);
                break;
            case GameKind.MB: BtnMB.gameObject.SetActive(false); break;
            case GameKind.XT: BtnXT.gameObject.SetActive(false); break;
            case GameKind.Phom: BtnPhom.gameObject.SetActive(false); break;
            case GameKind.TLDL: BtnTLDL.gameObject.SetActive(false); break;               
        }
    }
	public void RequestJoinGameLobbyRoom()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		int gid = GetGameID();
		GameHelper.currentGid = gid;
		SFS.Instance.RequestJoinGameLobbyRoom (gid);

		LoadingManager.Instance.ENABLE = true;
	}

    public void RequestJoinTaiXiuRoom()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        GameHelper.currentGid = (int)GAMEID.TaiXiu;
        SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);

        LoadingManager.Instance.ENABLE = true;
    }

    public void RequestJoinBauCuaRoom()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        GameHelper.currentGid = (int)GAMEID.BauCua;
        SFS.Instance.RequestJoinGameLobbyRoom(GameHelper.currentGid);

        LoadingManager.Instance.ENABLE = true;
    }

    public void RequestJoinGameLobbyRoom(int gid)
	{
        Debug.Log("RequestJoinGameLobbyRoom============" + gid);
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
        //khuong edit haha
        if(MyInfo.CHANGE_TLMN == 0)
        {
            if (gid == 5) gid = 6;
        }
		GameHelper.currentGid = gid;

		SFS.Instance.RequestJoinGameLobbyRoom (gid);

		LoadingManager.Instance.ENABLE = true;
	}

	#region QUICK GAME

	public void BtnQuickGameOnClick()
    {
        //RqQuickGame ();
        RequestJoinGameLobbyRoom();
	}

	void RqQuickGame()
    {
		int gameID = GetGameID ();
        GameHelper.currentGid = gameID;

        GamePacket packet = new GamePacket (CommandKey.QUICK_GAME);
		packet.Put (ParamKey.GAME_ID, gameID);

		sfs.SendZoneRequest (packet);
	}

	#endregion

	public void BtnAchivementOnClick()
	{
        Debug.Log("click btn achivement");
		//Achivement.SetEnable (true);
		//SetEnableContent (false);
		//_screen = ScreenHome.Achivement;       
       // HomeViewV2.ParticleLogo.Stop();
    }
	void BackAchivementOnClick()
	{

		View.SetEnablePanelUI (true);
		//Achivement.SetEnable (false);
	}
	void SetEnableContent(bool _enable)
	{
		View.SetEnablePanelUI (_enable);
	}

	public void UpdateInfoUser(){


		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP), 
			FormatCash(GOLD), 
			MyInfo.AvatarName, 
            MyInfo.BorderAvatarName,
			VIPPOINT,
            FormatCash(GEM));
	}
	void UpdateInfoUser(long _chip, long _gold, int _vippoint){


		CHIP = _chip;
		GOLD = _gold;
		VIPPOINT = _vippoint;

		View.ShowInfoUser (MyInfo.NAME, 
			FormatCash(CHIP), 
			FormatCash(GOLD), 
			MyInfo.AvatarName,
            MyInfo.BorderAvatarName,
            VIPPOINT,
            FormatCash(GEM));
	}

	#region Log Out

	public void LogOutOnClick()
	{
        MyInfo.BangChatRoom = null;
        MyInfo.BangChatRoomName = "";
		PlaySound (SoundManager.BUTTON_CLICK);
		SFS.Instance.LogOut ();
    }

	#endregion

	#region SHOP

	public void ShopOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		Shop.Show ();
		View.SetEnablePanelUI (false);
	}

    public void ShopOnClick_GEM()
    {
        PlaySound(SoundManager.BUTTON_CLICK);
        Shop.Show(true);
        View.SetEnablePanelUI(false);
    }


    #endregion

    #region MODULE

    public void ModuleOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		Module.Show ();
	}

	#endregion

	#region EVENT

	public void EventOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		View.SetEnablePanelUI (false);
	}

	void BackEventOnClick ()
	{
		//Event.Hide ();
		View.SetEnablePanelUI (true);

		UpdateInfoUser ();
	}

	#endregion

	#region Inbox

	public void InboxOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		Inbox.Init(InboxExitOnClick);
		Inbox.Show();
	}

	void InboxExitOnClick()
	{
		Inbox.Hide();
		UpdateInfoUser();
	}

	#endregion

	#region Invite

	void BtnCancelInvitedOnClick ()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		popupInvited.Hide ();
	}
	void BtnOkInvitedOnClick (int _roomID, string _pass)
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		if (_roomID == -1) {
			popupAlert.Show (ConstText.ErrorNoMoneyToPlay, popupAlert.Hide);
			popupInvited.Hide ();
		} else {
			RqJoinGameRoom (_roomID, _pass);
			LoadingManager.Instance.ENABLE = true;
		}
	}
	private void RqJoinGameRoom(int roomId, string _pass)
	{
		GamePacket param = new GamePacket (CommandKey.JOIN_GAME_ROOM);

		if (!string.IsNullOrEmpty (_pass))
			param.Put (ParamKey.ROOM_PASSWORD, _pass);

		param.Put (ParamKey.ROOM_ID, roomId);
		param.Put (ParamKey.GAME_ID, GameHelper.currentGid);
		sfs.SendZoneRequest (param);
	}
	void RspInvited(GamePacket _param)
	{
		string s = _param.GetString ("ivn");
		string[] info = s.Split ('#');

		string name = info [0];
		int roomID = int.Parse (info [1]);
		int gameID = int.Parse (info [2]);
		int roomLv = int.Parse (info [3]);
		long bet = long.Parse (info [4]);
		long betRequire = long.Parse (info [5]);
		string pass = info [6];

		GameHelper.currentGid = gameID;
		popupInvited.Show (name, roomID, gameID, roomLv, bet, betRequire, pass);
	}

	#endregion

	#region Info User

	public void BtnAvatarOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Show ();
	}
	void BtnSaveInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
		View.UpdateAvatar ();
	}
	void BtnCloseInfoOnClick()
	{
		PlaySound (SoundManager.BUTTON_CLICK);
		InfoUser.Hide ();
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
       // Debug.LogError("KhuongTest==========" + loginType);
        if (loginType != LoginType.Facebook)
        {
            View.UpdateAvatar();
        }
	}

	#endregion

	#region Lucky Wheel

	private void OnResultLuckyWheel (){
		View.UpdateChip (FormatCash (CHIP));
	}

    #endregion

    [SerializeField]
    private PopupVideoReward videoReward = null;
    [SerializeField]
    private PopupSpecialWheelManager specWheel = null;

    [SerializeField]
    PopupLotteryManager Lottery;
    public void OnSFSResponse(GamePacket param)
	{
		    Debug.Log ("Home rsp - " + param.ToString());
		    Debug.Log("Home cmd - " + param.cmd);

		    switch (param.cmd) {

		    case CommandKey.JOIN_GAME:
                Debug.Log("GameHelper.currentGid===============" + GameHelper.currentGid);
                if(GameHelper.currentGid == (int)GAMEID.DuaNgua)
                {
                    GameHelper.ChangeScene(GameHelper.currentGid);
                }
                else
                {
                    bool isRoomEvent = param.GetBool("is_room_event");
                    if (isRoomEvent == false)
                    {
                        if (GameHelper.currentGid != (int)GAMEID.Lottery)
                            GameHelper.ChangeScene(GameHelper.currentGid);
                        else
                        {
                            LoadingManager.Instance.ENABLE = false;
                            Lottery.Init();
                        }
                    }
                    else
                    {
                        GameHelper.currentGid = (int)MyInfo.tournamentStatus;
                        GameHelper.ChangeScene(GameHelper.currentGid);
                    }
                }                
                break;
		    case CommandKey.INVITE:
			    RspInvited (param);
			    break;
		    case CommandKey.JOIN_GAME_LOBBY_ROOM:
			    LoadWatingRoom (param);
			    break;
		    case CommandKey.ERROR:
			    RspError (param);
			    LoadingManager.Instance.ENABLE = false;
			    break;
		    case CommandKey.REFRESH:
			    //			txtTest.text = " RSP Refresh";
			    RspRefreshUserInfo (param);
			    break;
		    case CommandKey.START_ARENA:
    //			RspStartArena (param);
			    break;
		    case CommandKey.FINISH_ARENA:
    //			RspFinishArena (param);
			    break;
			    //        case CommandKey.JOIN_ARENA_GAME:
			    //            print("JOIN_ARENA_GAME ID: " + param.GetInt(ParamKey.GAME_ID));
			    //            GameHelper.ChangeScene(param.GetInt(ParamKey.GAME_ID));
			    //            break;
		    case CommandKey.POPUP_PRIZE:
			    print ("Message: " + param.GetString (ParamKey.Message));
			    //GameHelper.ChangeScene(param.GetInt(ParamKey.GAME_ID));

			    popupAlert.Show (param.GetString (ParamKey.Message), popupFull.Hide);

			    UpdateInfoUser (param.GetLong (ParamKey.CHIP), param.GetLong (ParamKey.GOLD), param.GetInt (ParamKey.VIPPOINT));

			    break;
		    case CommandKey.QUICK_GAME:
			    GameHelper.ChangeScene (GetGameID ());
			    break;
            //case CommandKey.REQUEST_ADS:
            //    videoReward.onResponseRequestAds(param);
            //    break;
            //case CommandKey.REWARD_ADS:
            //    videoReward.onResponseRewardAds(param);
            //    break;
            case CommandKey.GET_SPEC_WHEEL:
                specWheel.resGetSpecWheel(param);
                break;            
            case CommandKey.START_ROLL_SPEC_WHEEL:
                specWheel.onStartWheelResponse(param);
                break;
            case CommandKey.GET_EVENT_STATUS:
                //tournamentEvent.responseEventStatus(param);
                break;
            case CommandKey.JOIN_EVENT_REQUEST:

                List<int> lstEventType1 = param.GetIntArray("event1").ToList();
                List<int> lstEventType2 = param.GetIntArray("event2").ToList();

                //tournamentEvent.showEvent(lstEventType1, lstEventType2);
                break;
            case CommandKey.JOIN_GAME_EVENT:
                //tournamentEvent.showEventWaitingRoom(param);
                break;
            case CommandKey.LEADERBOARD:
                //tournamentEvent.onLeaderBoardResponse(param);
                break;
            case CommandKey.EXIT_GAME_EVENT:
                //tournamentEvent.closeEventWaitingRoom();
                break;
            //case CommandKey.MISSION:
            //    PanelMission.OnSFSResponse(param);
            //    break;
            //case CommandKey.CLAIM_MISSION:
            //    PanelMission.OnSFSResponse(param);
            //    break;

            case "pirateAttackHistory":
                Estate.OnSFSResponse(param);
                break;
            case "blessingGodCard":
                OnBlessingGodCard(param);
                break;
        }
	}

	void RspRefreshUserInfo (GamePacket param)
	{

		//		txtTest.text = param.param.ToJson();

		CHIP = param.GetLong (ParamKey.CHIP);
		
		UpdateInfoUser ();


	}
	//	[SerializeField]
	//	UnityEngine.UI.Text txtTest;
	//	string test1 = "";
	void OnApplicationPause(bool _pause){
		if (!_pause) {
			//			txtTest.text = "Waiting!";
			//			StartCoroutine (GameHelper.Thread (2, () => {
			//				txtTest.text = "Requesting!";
			sfs.SendZoneRefreshData ();
			//			}));
		}
	}
	void RspError(GamePacket _param)
	{
		int reason = _param.GetInt (ParamKey.REASON);

		switch (reason) {
		case 1:
			popupAlert.Show (ConstText.ErrorRoomFull, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 2:
			popupAlert.Show (ConstText.ErrorRoomNotExist, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 3:
			popupAlert.Show (ConstText.ErrorNoMoneyToPlay, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		case 4:
			popupAlert.Show (ConstText.ErrorCantFindRoom, () => {
				HidePopupAlert();
				popupInvited.Hide();
			});
			break;
		}
	}
    public void ShowFirstPurchasePanel()
    {
        AlertController.api.ShowFirstPurchase(() => {
            ShopOnClick();
        });
    }
	void HidePopupAlert()
	{
		popupAlert.Hide ();
	}
	void HidePopupInvited()
	{
		popupInvited.Hide ();
	}
    public void ShowNapTichLuy()
    {
        //panelNapTichLuy.SetActive(true);
        //panelNapTichLuy.GetComponent<PopupNapTichLuy>().Init();
    }

    public void RequestJoinTaiXiuMiniRoom()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.TaiXiuMiniScene);
    }
    public void RequestJoinEventRoom()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.EventScene);
    }
    public void ShowChangeGiftSence()
    {
        LoadingManager.Instance.ENABLE = true;
        GameHelper.ChangeScene(GameScene.ChangeChipScene);
    }
    private void LoadWatingRoom(GamePacket param)
	{
		int gid = param.GetInt(ParamKey.GAME_ID);
		GameHelper.currentGid = gid;

		if (gid == 3)
			GameHelper.ChangeScene (GameScene.TaiXiuScene);
		else
			SceneManager.LoadScene(GameScene.WaitingRoom.ToString());

	}
	public void OnConnectionLost()
	{
		popupAlert.Show (ConstText.ErrorConnection, () => {
			GameHelper.ChangeScene(GameScene.LoginScene);
		});
	}
}
