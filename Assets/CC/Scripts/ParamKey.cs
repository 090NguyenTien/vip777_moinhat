﻿public class ParamKey
{
    public const string GAME_ID = "gid";
	public const string PLAY_BOARD_LIST = "pbl";
	public const string BET_MONEY = "bm";
	public const string BET_MONEY_TOTAL = "ttb";
	public const string RESULT_GAME = "grs";
	public const string RESULT = "result";

    public const string ROOM_ID = "rid";
    public const string ROOM_PASSWORD = "pwr";
    public const string ROOM_LIST_LEVEL = "rlv";

    public const string HOST_ID = "h";
    public const string HOST_NAME = "hn";
    public const string USER_INFO = "ui";
    public const string USER_ACTIVE = "ua";
    public const string USER_ID = "uid";
	public const string USER_LIST = "ul";
	public const string USER_FIRST_RAISE = "auid";

    public const string POSITION = "ps";
    public const string PASS_USER_ID = "puid";
    public const string FIRST_GAME = "ifg";
    public const string NEW_ROUND = "nr";
    public const string RANK = "ugr";

	public const string CARD_LIST = "cl";
	public const string CARD_TYPE = "t";

	public const string CARD_ID = "cid";
	public const string CARD_1 = "c1";
	public const string CARD_2 = "c2";
    public const string KILL_INFORMATION = "wsc";
    public const string RETURN_MONEY = "rtm";
    public const string IMMEDIATE_WIN_RESULT = "wp";
    public const string TIME = "ti";

    public const string ACTION_RAISE_XT = "i";

	public const string REASON = "rs";

	public const string ACHIVEMENT = "acv";
	public const string DEVICE = "device";
	public const string PACKAGE_NAME = "pkg";



    public const string ResultBet = "ResultBet";
  
    // Phom
    public const string TurnTime = "TurnTime";
    public const string RoomId = "RoomId";
    public const string Slots = "Slots";
    public const string HostId = "HostId";
    public const string BetMoney = "BetMoney";
    public const string Position = "Position";
    public const string StartSlotId = "StartSlotId";
    public const string DeckSize = "DeckSize";
    public const string Combination = "Combination";
    public const string DiscardedCardId = "DiscardedCardId";
    public const string DiscardSlotId = "DiscardSlotId";
    public const string StartTurnSlotId = "StartTurnSlotId";
    public const string PreviousSlotId = "PreviousSlotId";
    public const string RecommendedEatenCardIds = "RecommendedEatenCardIds";
    public const string DrawnCardId = "DrawnCardId";
    public const string DrawnCardSlotId = "DrawnCardSlotId";
    public const string EatenSlotId = "eatenSlotId";
    public const string EatenCardIds = "EatenCardIds";
    public const string BeEatenSlotId = "beEatenSlotId";
    public const string BeEatenCardId = "beEatenCardId";
    public const string ChangedMoney = "changedMoney";
    public const string EatenSlotNewMoney = "EatenSlotNewMoney";
    public const string BeEatenSlotNewMoney = "BeEatenSlotNewMoney";
    public const string LaidOffRound = "laidOffRound";
    public const string OldRoundStarterId = "oldRoundStarterId";
    public const string NewRoundStarterId = "NewRoundStarterId";
    public const string BeMovedCardId = "beMovedCardId";
    public const string Message = "Message";
    public const string SelectedCardIds ="SelectedCardIds";
    public const string GiftCode ="code";
    public const string LaidDownSlotId = "LaidDownSlotId";
    public const string LaidDownCombination = "LaidDownCombination";
    public const string UpperLaidDownCards = "UpperLaidDownCards";
    public const string LowerLaidDownCards = "LowerLaidDownCards";
    //public const string RecommendedSentGroups = "RecommendedSentGroups";
    public const string RecommendedSentCards = "RecommendedSentCards";
    public const string SentSlotId = "SentSlotId";
    public const string SentGroups = "SentGroups";
    public const string GameRound = "GameRound";
    public const string CurrentTurnPlayerId = "CurrentTurnPlayerId";
    public const string ExitedPlayerId = "ExitedPlayerId";
    public const string SecondsUntilStartGame = "SecondsUntilStartGame";
    public const string CanChangeCombo = "CanChangeCombo";
    public const string IsWinByOppExit = "IsWinByOppExit";
    public const string Round = "Round";

    public const string SLOTS = "ss";
    public const string START_SLOT_ID = "ssi";
    public const string SORTED_COMBINATION = "sc";
    public const string DECK_SIZE = "ds";

    public const string EATEN_CARD_IDS = "ecis";

    public const string DISCARDER_ID = "di";
    public const string DISCARDED_CARD_ID = "dci";
    public const string CAN_MELD = "cm";
    public const string END_TURN_SLOT_ID = "etsi";
    public const string START_TURN_SLOT_ID = "stsi";
    public const string DRAWN_CARD_SLOT_ID = "dcsi";
    public const string DRAWN_CARD_ID = "dci";
    public const string RECOMMENDED_EATEAN_CARD_IDS = "recis";


    public const string START_CARD_SETS = "scss";
    public const string NextCardId = "NextCardId";

    // Tai Xiu
    public const string CHIP = "chip";
	public const string GOLD = "gold";
	public const string VIPPOINT = "vp";
    public const string CURRENT_TIME = "ct";
    public const string ONLINE_USER_NUMBER = "oun";
    public const string TOTAL_BET = "tlb";
    public const string DETAIL_TOTAL_BETS = "tlbd";
    public const string DICE_RESULT = "dr";
    public const string GAME_RESULT = "gr";
    public const string BET_TYPE = "i";
    public const string CHEATED_DICE_RESULT = "cdr";
    public const string USER_NAME = "uN";
    public const string BET_DATA = "bd";

    public const string IS_ARENA_GAME = "iag";


    //vidieo reward
    public const string ADS_SECRET_KEY = "adsk";
    public const string ADS_ID_UNITY = "adsIdU";
    public const string ADS_ID_ADMOB = "adsIdA";
    public const string ADS_DELAY = "adsd";
    public const string CUR_ADS_REWARD = "car";
    public const string NEXT_ADS_REWARD = "nar";
    public const string MAX_ADS = "amax";
    public const string ADS_SUCCESS_COUNT = "asc";
    public const string REMAIN_TIME = "remaintime";
    public const string LOTTERY_ENABLE = "lotteryEnable";


    public const string TYPE = "type";
    public const string MINE = "mine";
    public const string BETTING_MONEY = "betting_money";
}