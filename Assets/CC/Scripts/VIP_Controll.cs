﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VIP_Controll : MonoBehaviour {

    public static Dictionary<int, GoiVip> DicVipData = new Dictionary<int, GoiVip>();
    [SerializeField]
    GameObject ItemVip, Content, PanelDanhSachVip;
    [SerializeField]
    Text TxtVip;

    public void MoDanhSachVip()
    {
        for (int i = 0; i < DicVipData.Count; i++)
        {
            GameObject obj = Instantiate(ItemVip) as GameObject;
            obj.transform.SetParent(Content.transform);
            obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            ItemVipControll item = obj.GetComponent<ItemVipControll>();

            
            GoiVip gv = new GoiVip();
            gv = DicVipData[i];
            item.Init(gv.TenVIP, gv.MinPoint, gv.ThuongDatMoc, gv.ThuongNgay, gv.ThuongPhanTram);
            if (i == MyInfo.MY_ID_VIP)
            {
                item.SoHuu();
            }
            else if (i > MyInfo.MY_ID_VIP)
            {
                item.ChuaDatDuoc();
            }

        }
        TxtVip.text = MyInfo.MY_VIP;
        PanelDanhSachVip.SetActive(true);
    }

    public void DongDanhSachVip()
    {
        for (int i = 0; i < Content.transform.childCount; i++)
        {
            GameObject obj = Content.transform.GetChild(i).gameObject;
            Destroy(obj);
        }
        PanelDanhSachVip.SetActive(false);
    }
	
}

public class GoiVip
{
    public string TenVIP;
    public string MinPoint;
    public string MaxPoint;
    public string ThuongDatMoc;
    public string ThuongNgay;
    public string ThuongPhanTram;
    public string HanMucTangChip;
    public string HanMucTangGem;
    public string ColorChat;
}