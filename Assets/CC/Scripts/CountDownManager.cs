﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountDownManager : MonoBehaviour {

	[SerializeField]
	Text txtTime;

	bool isRun;
	float timer;

	public void InitNew(float _time)
	{
		timer = _time;
		isRun = true;
	}

	void Update()
	{
		if (!isRun)
			return;

		if (timer > 0) {
			int _time = (int)timer;
			timer -= Time.deltaTime;

//			if (_time > (int)timer)
//				SoundManager.PlaySound (SoundManager.COUNT_DOWN);

			ShowTime ();
		} else
			Stop ();
	}

	public void Show()
	{
		txtTime.gameObject.SetActive (true);
	}

	void ShowTime()
	{
		int _time = (int)timer;
		txtTime.text = (_time).ToString ();
	}

	public void Stop ()
	{
		isRun = false;
		txtTime.gameObject.SetActive (false);
	}
}
