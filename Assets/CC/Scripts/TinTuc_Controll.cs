﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class TinTuc_Controll : MonoBehaviour
{
    [SerializeField]
    GameObject PanelTinTuc, Content, item_, PanelLoad;
    [SerializeField]
    Image ImgTinTuc;
    [SerializeField]
    Sprite SprAn, SprHien, SprThongBaoMacDinh;

    Dictionary<int, string> LinkHinh = new Dictionary<int, string>();
    Dictionary<int, Image> DanhMuc = new Dictionary<int, Image>();
  
    int Page = 0;
    int CurPage = -1;
    int max;
    

    private void ShowTinTuc(string url)
    {
        
        string Url = API.Instance.DOMAIN + "/" + url;
        API.Instance.Load_Spr(Url, ImgTinTuc, CloseLoad);   

    }

    IEnumerator Wait_TatLoiNguyetLao(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        PanelLoad.SetActive(false);
    }

    public void XemTinTuc()
    {
        WWWForm form = new WWWForm();
        form.AddField("", "");
      //  API.Instance.BaseCallService_TinTuc(LinkHinh[Page], form, ShowTinTuc);

    }

    public void XemTinTuc_Test()
    {
      
        API.Instance.RequestGet_New(RspLayThongTin);

      
    }

    void CloseLoad()
    {
        PanelLoad.SetActive(false);
    }
   
   
    void RspLayThongTin(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("data get info NEW  " + _json);

        int cou = node.Count;
        max = cou - 1;
        for (int i = 0; i < cou; i++)
        {
            string link = node[i].Value;
            GameObject Obj = Instantiate(item_) as GameObject;
            Obj.GetComponent<RectTransform>().SetParent(Content.GetComponent<RectTransform>().transform);
            Obj.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            Image img = Obj.GetComponent<Image>();

            LinkHinh.Add(i, link);
            DanhMuc.Add(i, img);
        }
        PanelLoad.SetActive(true);
        ShowPage(Page);
        PanelTinTuc.SetActive(true);
    }

    public void ShowPage(int page)
    {
        string url = LinkHinh[page];

        Debug.LogError("CurPage = " + CurPage + " ------page----- " + page);

        if (CurPage != -1)
        {
            Image t = DanhMuc[CurPage];
            t.sprite = SprAn;
        }
        

        Image p = DanhMuc[page];
        // p.color = new Color(p.color.r, p.color.g, p.color.b, 255f);
        //  XemTinTuc();

        p.sprite = SprHien;


        ShowTinTuc(LinkHinh[Page]);
        CurPage = page;
    }

    public void BtnRightClick()
    {
       
       // StartCoroutine(Wait_TatLoiNguyetLao(2f));
        if (Page < max)
        {
            PanelLoad.SetActive(true);
            CurPage = Page;
            Page++;
            ShowPage(Page);
        }       
    }

    public void BtnLeftClick()
    {
        
       // StartCoroutine(Wait_TatLoiNguyetLao(2f));
        if (Page > 0)
        {
            PanelLoad.SetActive(true);
            CurPage = Page;
            Page--;
            ShowPage(Page);
        }
    }

    public void CloseTinTuc()
    {

        foreach (var item in DanhMuc)
        {
            GameObject obj = item.Value.gameObject;
            Destroy(obj);
        }
        DanhMuc.Clear();
        LinkHinh.Clear();
        CurPage = -1;
        Page = 0;
        ImgTinTuc.sprite = SprThongBaoMacDinh;
        CloseLoad();
        PanelTinTuc.SetActive(false);
    }

}
