﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LoadingManager : MonoBehaviour {

	static LoadingManager instance;

	[SerializeField]
	GameObject panelError;
	[SerializeField]
	Button btnClose;
	[SerializeField]
	Text txtError;

	float time;

	static Camera camMain;

    [SerializeField]
    private AudioSource homeMusic = null;
	void Awake()
	{
		instance = this;
		panelError.SetActive (false);
		gameObject.SetActive (false);
		btnClose.onClick.AddListener (CloseOnClick);

		txtError.text = ConstText.ErrorConnectionLow;
		btnClose.transform.GetChild (0).GetComponent<Text> ().text = ConstText.Close;

        //Debug.LogError(PlayerPrefs.GetInt(KEYSETTING.ENABLE_BACKGROUND_MUSIC, 0).ToString());
        setPlayMusic( PlayerPrefs.GetInt(KEYSETTING.ENABLE_BACKGROUND_MUSIC, 1) == 1);


    }

    public void setPlayMusic(bool isPlay)
    {
        if (isPlay == true)
        {
            //Debug.LogError("playyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy music ");
            homeMusic.Play();
        }
        else
        {
            //Debug.LogError("stoppppppppppppppppppppppppppppppp music ");
            homeMusic.Stop();
        }
    }

	public static LoadingManager Instance
	{
		get{ return instance; }
	}
//	Coroutine loadingThread;
	public bool ENABLE
    {
		set
        {
            txtError.text = ConstText.ErrorConnectionLow;
            actionTimeOut = null;

            gameObject.SetActive (value);

			if (value)
            {
				time = 30;

                //				loadingThread = StartCoroutine (LoadingThread ());
                //if (camMain == null)
                //{
                //    	camMain = GameObject.Find ("CamMain").GetComponent<Camera>();
                //        camMain.gameObject.SetActive(true);
                //}
                //				camMain.depth = 4;
            }
            else
            {
                    //camMain.gameObject.SetActive(false);
                    time = 0;
			}
			btnClose.gameObject.SetActive (false);
			txtError.gameObject.SetActive (false);
			panelError.SetActive (false);
		}
	}

    private Action actionTimeOut = null;
    public void setHandleTimeOut(Action actionTimeOutSet)
    {
        actionTimeOut = actionTimeOutSet;
    }

    public void setTextLoginTimeOut()
    {
        txtError.text = ConstText.Server_Was_Maintenence;
    }



//	IEnumerator LoadingThread(){
//		iconLoading.SetActive (true);
//		yield return new WaitForSeconds (10);
//
//
//	}

	void Update()
	{
        
		if (time > 0)
        {
			time -= Time.deltaTime;
		}
        else
        {
            if (actionTimeOut != null)
            {
                actionTimeOut();
                actionTimeOut = null;
            }
			btnClose.gameObject.SetActive (true);
			txtError.gameObject.SetActive (true);
//			iconLoading.SetActive (false);
			panelError.SetActive (true);

//			if (loadingThread != null)
//				StopCoroutine (loadingThread);


//			camMain.depth = 99;
		}
	}

	void CloseOnClick()
	{
		ENABLE = false;
	}
}
