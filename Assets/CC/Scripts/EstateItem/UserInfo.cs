﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInfo : MonoBehaviour {
    [SerializeField]
    GameObject PanelUI_Home;
    public MadControll Mad;
    [SerializeField]
    EstateControll Estate;


    public void BtnMadClick()
    {
       // Mad.Init();
       // Estate.InitLEFT();
       // PanelUI_Home.SetActive(true);
        
        if (DataHelper.IsInitShopVatPham == true)
        {
            Mad.Init();
            Estate.InitLEFT();
            PanelUI_Home.SetActive(true);
        }
        else
        {
            Debug.LogWarning("DataHelper.IsInitShopVatPham =" + DataHelper.IsInitShopVatPham);
        }    
            
    }

    public string DataInMad_Change = "";
    public string StringHomeUnClock_Change = "";
    public string StringShopUnClock_Change = "";
    public string StringCarUnClock_Change = "";

}
