﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class TestItemInHome : MonoBehaviour {
    [SerializeField]
    Text TxtNameItem, TxtInfoItem, TxtDiamonPrice, TxtChipPrice;
    [SerializeField]
    Image ImgItem;
    string MyId = "";
    [SerializeField]
    MadControll Mad;
    [SerializeField]
    EstateControll estate;
    private int ChangeType = -1;

    public long GiaChip, GiaGem;
    [SerializeField]
    GameObject Alert;
    [SerializeField]
    Text TxtAlert, TxtPoint, TxtLevel, TxtDoanhThu; 
    [SerializeField]
    GameObject BtnCHIP;
    [SerializeField]
    ShopBorderAvatar ShopBorder;
    public int Point = 0;
    public int Level = 1;
    public int DoanhThu = 0;

    public void Init(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point, long DoanhThu)
    {
        MyId = ID;
       // ImgItem.sprite = DataHelper.dictSprite_House[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);

        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }

        TxtPoint.text = point.ToString();
        Point = point;
        ChangeType = 1;

        TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        TxtLevel.text = "1";

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }

    public void InitShop(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point, long DoanhThu)
    {
        MyId = ID;
        //    ImgItem.sprite = DataHelper.dictSprite_Shop[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        TxtLevel.text = "1";
        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        ChangeType = 2;
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }


    public void InitCar(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point, long DoanhThu)
    {
        MyId = ID;
       // ImgItem.sprite = DataHelper.dictSprite_Car[ID];
        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);
        TxtNameItem.text = _name;
        TxtInfoItem.text = info;

        TxtDoanhThu.text = Utilities.GetStringMoneyByLong(DoanhThu);
        TxtLevel.text = "1";
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        ChangeType = 3;
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }


    public void InitItemInShop(string ID, string _name, string info, long DiamonPrice, long ChipPrice, int point, Sprite spr, string Doanhthu)
    {
        MyId = ID;
        //ImgItem.sprite = spr;

        string Img = DataHelper.DuLieuTenImgTheoID[ID];
        API.Instance.Load_SprItem(Img, ImgItem);

        TxtNameItem.text = _name;
        TxtInfoItem.text = info;
        TxtDoanhThu.text = Doanhthu;
        TxtDiamonPrice.text = Utilities.GetStringMoneyByLong(DiamonPrice);
        if (ChipPrice > 0)
        {
            TxtChipPrice.text = Utilities.GetStringMoneyByLong(ChipPrice);
            BtnCHIP.SetActive(true);
        }
        else
        {
            BtnCHIP.SetActive(false);
        }
        
        TxtPoint.text = point.ToString();
        Point = point;

        GiaChip = ChipPrice;
        GiaGem = DiamonPrice;
    }




    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void BtnBackOnClick()
    {
        this.gameObject.SetActive(false);
        ChangeType = -1;
    }

    public void BtnTestOnClick(string loaitien)
    {
        // this.gameObject.SetActive(false);


        estate.RequestMuaVatPham(MyId, loaitien);


        /*
        if (ChangeType == 1)
        {
           // Mad.ChangeHome(MyId);
          //  Mad.BuyHome(MyId);

            estate.RequestMuaVatPham(MyId, loaitien);
            
        }
        else if (ChangeType == 2)
        {
           
            Mad.ChangeShop(MyId);
            Mad.BuyShop(MyId);
        }
        else if (ChangeType == 3)
        {

            Mad.ChangeCar(MyId);
            Mad.BuyCar(MyId);
        }
        */
    }

    public void ThongBaoLoi(string msg)
    {
        TxtAlert.text = msg;
        Alert.SetActive(true);
    }

    public void MuaBangChip()
    {
        if (MyInfo.CHIP >= GiaChip)
        {
            BtnTestOnClick("chip");
        }
        else
        {
            TxtAlert.text = "Bạn không đủ CHIP mua vật phẩm này";
            Alert.SetActive(true);
        }


    }


    public void MuaBangGem()
    {
        if (MyInfo.GEM >= GiaGem)
        {
            BtnTestOnClick("gem");
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM mua vật phẩm này";
            Alert.SetActive(true);
        }

    }


    public void MuaBangChip_InShop()
    {
        if (MyInfo.CHIP >= GiaChip)
        {
            API.Instance.RequestMuaVatPham(MyId, "chip", RspLayMuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ CHIP mua vật phẩm này";
            Alert.SetActive(true);
        }


    }


    public void MuaBangGem_InShop()
    {
        if (MyInfo.GEM >= GiaGem)
        {
            API.Instance.RequestMuaVatPham(MyId, "gem", RspLayMuaVatPham);
        }
        else
        {
            TxtAlert.text = "Bạn không đủ GEM mua vật phẩm này";
            Alert.SetActive(true);
        }

    }


    void RspLayMuaVatPham(string _json)
    {
        Debug.LogWarning("DU LIEU mua SAN PHAM: " + _json);
        JSONNode node = JSONNode.Parse(_json);
        string sta = node["status"].Value;
        long chip = long.Parse(node["chip"].Value);
        long gem = long.Parse(node["gem"].Value);
        string id = node["user_asset_id"]["$id"].Value;

        string idTONG = node["asset_id"]["$id"].Value;





        if (sta == "1")
        {
            MyInfo.CHIP = chip;
            MyInfo.GEM = gem;

            long mau = DataHelper.TongGiaTri_CuaSanPham_TaiLevel(idTONG, 1);

            long mauhientai = mau;

            long mautong = mau;

            ThemDuLieuSoHuu(id, idTONG, 1, mauhientai, mautong);

            ShopBorder.ThongBaoMuaThanhCong();
            this.gameObject.SetActive(false);
            
        }
        else
        {
            string MSG = node["msg"].Value;
            ThongBaoLoi(MSG);
        }

        Debug.LogWarning("st= " + sta + "  chip= " + chip + "   gem= " + gem + "   id= " + id);
    }




    void ThemDuLieuSoHuu(string id, string idTong, int level, long mauhientai, long mautong)
    {
        string key = id;
        My_Item_SoHuu it = new My_Item_SoHuu();
        it.idTong = idTong;
        it.idRieng = id;
        it.Level = level;
        it.InMad = false;
        it.MauHienTai = mauhientai;
        it.MauTong = mautong;
        string type = CheckType(idTong);
        it.type = type;
        it.gem = DataHelper.GetGemByIDTong(idTong, type);
        if (DataHelper.DuLieuSoHuu_V2.ContainsKey(key))
        {
            DataHelper.DuLieuSoHuu_V2[key] = it;
            //   Debug.LogWarning("Da Them vao du lieu so huu CU");
        }
        else
        {

            DataHelper.DuLieuSoHuu_V2.Add(key, it);
            DataHelper.Array_DuLieuSoHuu.Add(it);
            //  Debug.LogWarning("Da Them moi du lieu so huu");
        }

        //DataHelper.DuLieuSoHuu_V2.Add(key, it);
        //DataHelper.Array_DuLieuSoHuu.Add(it);




        //if (DataHelper.DanhSach_LoaiVatPham_SoHuu.ContainsKey(idTong))
        //{
        //    DataHelper.DanhSach_LoaiVatPham_SoHuu[idTong] = idTong;
        //    //   Debug.LogWarning("Da Them vao du lieu so huu CU");
        //}
        //else
        //{

        //    DataHelper.DanhSach_LoaiVatPham_SoHuu.Add(idTong, idTong);
        //    //  Debug.LogWarning("Da Them moi du lieu so huu");
        //}


        DataHelper.Array_DuLieuSoHuu.Sort(new SortDuLieuSoHuu());

        int diemVatPham = DataHelper.DuLieuDiemTaiSan[idTong];
        //int DiemCongThem = diemVatPham * sl;
        DataHelper.MY_DiemTaiSan += diemVatPham;
    }


    string CheckType(string idTong)
    {
        string Type = "";

        foreach (var item in DataHelper.DuLieuNha)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "house";
                return Type;
            }

        }

        foreach (var item in DataHelper.DuLieuShop)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "shop";
                return Type;
            }

        }

        foreach (var item in DataHelper.DuLieuXe)
        {
            My_Item it = new My_Item();
            it = item.Value;
            if (it.id == idTong)
            {
                Type = "car";
                return Type;
            }

        }

        return Type;
    }




    public void CloseAlert()
    {
        TxtAlert.text = "";
        Alert.SetActive(false);
    }

}
