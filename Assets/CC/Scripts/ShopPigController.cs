﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using SimpleJSON;
using BaseCallBack;
using UnityEngine.Purchasing;
#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

public class ShopPigController : MonoBehaviour {
    //[SerializeField]
    //PopupAlertManager AlertResult;
    [SerializeField]
    EvenDapHeo EvenDH;

    int Id_BuyInApp = 0;
    public int TypeShop = 0;

    
    #region InApp

    public void showPopupShop()
    {
        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction += OnPuchased;
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.CallServiceGetProductInAppId();
    }


    public void resGetHammer(string dataStoreId)
    {
        

        //UM_InAppPurchaseManager.Instance.Init();
    }


    public void BuyTurnBuyInApp(int id, string storeId)
    {
        resGetHammer(storeId);

        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        //UM_InAppPurchaseManager.Instance.Purchase(storeId);
        UnityPurchasinghelp.UnityPurchasingHelper.Instance.OnPurchaseClicked(storeId, OnPuchased);
    }


    void OnPuchased(string productId, string purchaseToken, bool isSuccess)
    {
        
        if (isSuccess)
        {

            //if (_result.Google_PurchaseInfo != null)
            //    Debug.Log(_result.Google_PurchaseInfo);
           // else if (_result.IOS_PurchaseInfo != null)
            //    Debug.Log(_result.IOS_PurchaseInfo);

#if UNITY_ANDROID
            //JSONNode node = JSONNode.Parse(_result.Google_PurchaseInfo.originalJson);

            string store = "googleplay";
            string productID = productId; //node["productId"].Value;
            string token = purchaseToken;//node["purchaseToken"].Value;

            API.Instance.RequestPaymentHammerInAppAndroid(store, productID, token, RspPaymentHammerInApp);

#elif UNITY_IOS

//			JSONNode node = JSONNode.Parse(_result.IOS_PurchaseInfo.Receipt);
//
			string store = "appstore";
			string receipt = _result.IOS_PurchaseInfo.Receipt;

//			s += "TOKEN: " + token
//			+ "\n packageName " + productID + "\n";

//			s+= "AppUname: " + _result.IOS_PurchaseInfo.ApplicationUsername
//				+"\nProductID: " + _result.IOS_PurchaseInfo.ProductIdentifier
//				+"\nReceipt: " + _result.IOS_PurchaseInfo.Receipt
//				+"\nTransactionId: " + _result.IOS_PurchaseInfo.TransactionIdentifier
//				;
            API.Instance.RequestPaymentInAppIOS(store, receipt, RspPaymentWheelInApp);
#endif

        }
        else
        {
            AlertController.api.showAlert("Giao dịch không thành công.");
        }
    }



    void RspPaymentHammerInApp(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" RspHammerPig " + _json);
        if (node["status"].AsInt == 1)
        {
            if (TypeShop == 2)
            {
                int Count_silver_hammer = node["silver_hammer"].AsInt;
                EvenDH.CapNhatBuaSauKhiMua(2, Count_silver_hammer);
            }
            else if (TypeShop == 3)
            {
                int Count_gold_hammer = node["gold_hammer"].AsInt;
                EvenDH.CapNhatBuaSauKhiMua(3, Count_gold_hammer);
            }

            AlertController.api.showAlert(node["msg"].Value);
           // closeBuyTurn();
        }
        else if (node["status"].AsInt == 0)
        {
            AlertController.api.showAlert(node["sms"]);
        }
        
    }



    public void BtnBackOnClick()
    {
        //UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction -= OnPuchased;
        gameObject.SetActive(false);
    }
    #endregion

    #region ReWar
    private string adsSecretkey = "";

   
    // GỌI XEM VIDEO
    public void ShowAdsForIronHammer()
	{
        //AlertResult.Init();
        SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
        API.Instance.RequestAdsFreeHammer((_json) => 
        {
            JSONNode node = JSONNode.Parse(_json);
            Debug.Log(" RspRequestAds " + _json);
            if (node["status"].AsInt == 1)
            {
                adsSecretkey = node["adsk"].Value;
                ShowUnityVideoRewardAd();
            }
        });
	}


    public string placementId = "rewardedVideo";
    void ShowUnityVideoRewardAd()
    {
#if UNITY_ANDROID
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
#endif
    }

 #if UNITY_ANDROID
    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched(); // GOI NHẬN QUÀ
        }
        else if (result == ShowResult.Skipped)
        {
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            AlertController.api.showAlert("Hiện không có video nào khả dụng, vui lòng quay lại sau");
        }
    }
#endif

    // bắt đầu gọi nhận quà
    private void callReceiveGiftWatched()
    {
        API.Instance.RequestGetHammerIron(adsSecretkey, RspIronHammer);
    }

    // Nhận quà
	void RspIronHammer (string _json)
	{
		Debug.Log (_json);

		JSONNode jsonNode = JSONNode.Parse (_json);
        if (jsonNode ["status"].AsInt == 1)
        {
            int Count_HammerIron = int.Parse(jsonNode["iron_hammer"].Value);
            Debug.Log(" Nhận Được Búa = " + Count_HammerIron);
            EvenDH.CapNhatBuaSauKhiMua(1, Count_HammerIron);
        }      
	}


    void ShowError(string _msg)
    {
        AlertController.api.showAlert(_msg);
        //AlertResult.Show(_msg, AlertResult.Hide);
    }

    void ShowError(string _msg, onCallBack _callBack)
    {
        AlertController.api.showAlert(_msg, () => {            
            _callBack();
        });
    }
    #endregion
}
