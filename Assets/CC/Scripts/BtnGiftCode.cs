﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtnGiftCode : MonoBehaviour {

    public GameObject PanelPopup;
    public GameObject PanelGiftCode;
    
	// Use this for initialization
	public void OnBtnClick()
    {
        PanelPopup.SetActive(true);
        PanelGiftCode.SetActive(true);
        Transform panelEvent = PanelPopup.transform.Find("EVENT");
        panelEvent.gameObject.SetActive(true);
    }
}
