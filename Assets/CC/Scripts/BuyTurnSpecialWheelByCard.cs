﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class BuyTurnSpecialWheelByCard : MonoBehaviour {
    [SerializeField]
    private GameObject PopupCard;
    [SerializeField]
    private Text txtTurnCount;
    [SerializeField]
    private GameObject panelProcessingPayment;
    [SerializeField]
    private GameObject PopupConfrim;

    [SerializeField]
    private InputField CardPrice;
    [SerializeField]
    private Dropdown CardType;
    [SerializeField]
    private InputField Pin;
    [SerializeField]
    private InputField Serial;
    [SerializeField]
    private Button BtnNap;

    [SerializeField]
    private Text PriceConfrim;

    [SerializeField]
    private Text Warning;

    [SerializeField]
    private GameObject MobieCard;
    [SerializeField]
    private GameObject Title;
    [SerializeField]
    private GameObject lineC;
    [SerializeField]
    Button btnOne, btnFive;


    private int Price;
    // Use this for initialization
    void Start () {
        Pin.contentType = InputField.ContentType.IntegerNumber;
        Pin.characterLimit = 20;
        Serial.contentType = InputField.ContentType.IntegerNumber;
        Serial.characterLimit = 20;
        Warning.text = "";

        if (!MyInfo.PC_VERSION)
        {
            //set enable Payment Card
            if (MyInfo.SHOW_SHOP_FROM_SERVICE)
            {
                if (!GameHelper.EnablePayment)
                {
                    CardActive(false);
                }
                else
                {
                    //set nha mang vao` combobox
                    setDataCardCombobox();
                    CardActive(GameHelper.CardPayment.card);
                }
            }
            else
            {
                setDataCardCombobox();
                CardActive(true);
                //btnOne.gameObject.SetActive(false);
                //btnFive.gameObject.SetActive(false);
            }
        }else
        {
            setDataCardCombobox();
            CardActive(true);
            btnOne.gameObject.SetActive(false);
            btnFive.gameObject.SetActive(false);
        }




    }

   
    private void CardActive(bool c)
    {
        MobieCard.SetActive(c);
        Title.SetActive(c);
        lineC.SetActive(c);
    }




    private void setDataCardCombobox()
    {
        //Debug.LogError(name + "Mobifone::::::" + GameHelper.dicConfig["Mobifone"]);
        //Debug.LogError(name + "Viettel::::::" + GameHelper.dicConfig["Viettel"]);
        //Debug.LogError(name + "Vinaphone::::::" + GameHelper.dicConfig["Vinaphone"]);

        bool haveViettel = true;
        bool haveMobi = true;
        bool haveVina = true;
        if (int.Parse(GameHelper.dicConfig["Mobifone"]) == 0 || !GameHelper.CardPayment.cardMobi)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Mobifone")
                {
                    CardType.options.RemoveAt(i);
                    haveMobi = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Viettel"]) == 0 || !GameHelper.CardPayment.cardVietel)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Viettel")
                {
                    CardType.options.RemoveAt(i);
                    haveViettel = false;
                    break;
                }
            }
        }
        if (int.Parse(GameHelper.dicConfig["Vinaphone"]) == 0 || !GameHelper.CardPayment.cardVina)
        {
            for (int i = 0; i < CardType.options.Count; i++)
            {
                if (CardType.options[i].text == "Vinaphone")
                {
                    CardType.options.RemoveAt(i);
                    haveVina = false;
                    break;
                }
            }
        }
        if (!haveMobi)
        {
            CardType.captionText.text = "Viettel";
            if (!haveViettel)
            {
                CardType.captionText.text = "Vinaphone";
                if (!haveVina)
                {
                    CardType.captionText.text = "";
                }
            }

        }
    }





    // Update is called once per frame
    void Update () {
		
	}

    private bool CheckInput()
    {      
        if (Pin.text.Length <= 6)
        {
            Warning.text = "Mã nạp thẻ không hợp lệ!";
            return false;
        }
        if (Serial.text.Length <= 6)
        {
            Warning.text = "Mã serial không hợp lệ!";
            return false;
        }
        return true;
    }

    public void CheckConfrim()
    {
        bool check = CheckInput();
        if (check == true)
        {
            ShowPopupConfrim();
            Warning.text = "";
            Warning.gameObject.SetActive(false);
        }
        else
        {
            Warning.gameObject.SetActive(true);
        }
    }


    public void ShowPopupCard()
    {
        PopupCard.SetActive(true);
    }

    public void ClosePopupCard()
    {
        Warning.gameObject.SetActive(false);
        PopupCard.SetActive(false);
        Price = 0;
        Pin.text = "";
        Serial.text = "";
        CardType.value = 0;
        Warning.text = "";
    }

    public void buyPackOneByMobieCard()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = "10.000";

        ShowPopupCard();
    }

    public void buyPackTwoByMobieCard()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = "20.000";
        
        ShowPopupCard();
    }

    public void buydPackFiveByMobieCard()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = "50.000";
        ShowPopupCard();
    }

    public void buydPackTenByMobieCard()//call editor
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        CardPrice.text = "100.000";
        ShowPopupCard();
    }

    public void BuyTurn()
    {
        string price = Price.ToString();
        string Type = CardType.captionText.text.ToLower();
        string pin = Pin.text;
        string serial = Serial.text;
        Debug.Log("gia = " + price + " --------loai the = " + Type + " --------pin = " + pin + " --------seri = " + serial);
        //call service
        API.Instance.RequestBuySpecwheelByCard(Type, price, pin, serial,BuyTurnComplete);
        panelProcessingPayment.gameObject.SetActive(true);
    }

    private void BuyTurnComplete(string s)
    {
        panelProcessingPayment.gameObject.SetActive(false);
        JSONNode node = JSONNode.Parse(s);
        Debug.Log(name + "====s====" + s);
        Debug.Log(name + "====node====" + node);
        bool success = node["status"].Value.ToString().Equals("1");
        if (success)
        {
            UpdateChip(
                long.Parse(node["items"]["chip"].Value),
                int.Parse(node["items"]["wheel"].Value)
            );
            AlertController.api.showAlert("Giao dịch thành công.",ClosePopupComplete);
        }
        else
        {
            AlertController.api.showAlert("Giao dịch không thành công.", ClosePopupComplete);
        }       
    }

    private void ClosePopupComplete()
    {
        ClosePopupConfrim();
        ClosePopupCard();
    }

    void UpdateChip(long _chip, int _wheel)
    {
        MyInfo.CHIP = _chip;
        MyInfo.SPEC_WHEEL = _wheel;
        txtTurnCount.text = MyInfo.SPEC_WHEEL.ToString();
    }
    public void ShowPopupConfrim()
    {
        string T = CardPrice.text;
        if (T == "20.000")
        {
            PriceConfrim.text = "20.000";
            Price = 20000;
        }
        else if (T == "100.000")
        {
            PriceConfrim.text = "100.000";
            Price = 100000;
        }
        else if (T == "10.000")
        {
            PriceConfrim.text = "10.000";
            Price = 10000;
        }
        else if (T == "50.000")
        {
            PriceConfrim.text = "50.000";
            Price = 50000;
        }
        PopupConfrim.SetActive(true);
    }

    public void ClosePopupConfrim()
    {
      //  PriceConfrim.text = "";
        PopupConfrim.SetActive(false);
    }
}
