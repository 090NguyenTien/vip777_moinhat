﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemHisCard : MonoBehaviour
{
    [SerializeField]
    Text  MenhGia, Loai, TrangThai, ThongTin;
    [SerializeField]
    Text Thoigian;

    public string token = "";
    [SerializeField]
    Button BtnCheck;

    public void Init(string ThoiGian, string Gia, string loai, string trangthai, string pin, string seri )
    {
        Thoigian.text = ThoiGian;
        MenhGia.text = Gia;
        Loai.text = loai;
        TrangThai.text = trangthai;

        ThongTin.text = "Pin:" + pin + "\n" + "Seri:" + seri;


    }
}
