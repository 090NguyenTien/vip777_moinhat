﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ItemInboxView : MonoBehaviour {

    [SerializeField]
    Text txtTitle, txtFromName, txtCreatedDate, txtCreatedTime;
    [SerializeField]
    Image imgBackground, imgMail;
    [SerializeField]
    Button btnSelect;
    [SerializeField]
    Button btnDelete;

    private Inbox inbox;

    public void Init(Inbox inbox)
    {
        this.inbox = inbox;
        btnSelect.onClick.AddListener(BtnSelectOnClick);
        btnDelete.onClick.AddListener(BtnDeleteOnClick);
    }

    public void Show(int index)
    {
        txtTitle.text = inbox.Title;
        txtFromName.text = inbox.Author;
        if (inbox.CreatedDate != "null")
        {
            txtCreatedDate.text = inbox.CreatedDate.Split(' ')[0].ToString();//.ToString("dd/MM/yyyy");
            txtCreatedTime.text = inbox.CreatedDate.Split(' ')[1].ToString();//.TimeOfDay.ToString();
        }
        else
        {
            txtCreatedDate.text = "";
            txtCreatedTime.text = "";
        }
        //if (index % 2 == 0)
        //    imgBackground.gameObject.SetActive(false);
        //else
        //    imgBackground.gameObject.SetActive(true);
    }

    void BtnSelectOnClick()
    {
        PopupInboxManager.Instance.BtnSelectOnClick(inbox);
    }

    void BtnDeleteOnClick()
    {
        PopupInboxManager.Instance.OpenConfrimDelete(inbox.Id);
    }
}
