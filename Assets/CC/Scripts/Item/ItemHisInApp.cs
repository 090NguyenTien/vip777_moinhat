﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseCallBack;

public class ItemHisInApp : MonoBehaviour
{
    [SerializeField]
    Text MenhGia, TrangThai;
    public string token = "";
    public string store = "";
    public string productID = "";
    public onCallBackString RspPaymentInApp;
    [SerializeField]
    Button BtnCheck;

    public void Init(string Gia, string Token, string Store, onCallBackString rspPaymentInApp)
    {
        MenhGia.text = Gia;//productID
        productID = Gia;
        token = Token;
        store = Store;
        RspPaymentInApp = rspPaymentInApp;

        TrangThai.text = "Đang xử lý";

        BtnCheck.onClick.RemoveAllListeners();
        BtnCheck.onClick.AddListener(BtnClick);
    
    }
     public void BtnClick()
    {
        API.Instance.RequestPaymentInAppAndroid(store, productID, token, RspPaymentInApp);
    }
}
