﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class ItemCardValueExchangeView : MonoBehaviour {

	Text txtValueCard, txtGold;
	Image imgProvider;
	Button btnItem;

	GameObject objLock;
	Text txtVipLevelLock;

	onCallBackInt itemClick;

	int Index;

	public void Init(int _index, int _valueCard, int _valueGold, Sprite _sprProvider, onCallBackInt _itemClick)
	{
		txtGold = transform.Find ("TxtValueGold").GetComponent<Text> ();
		txtGold.text = Utilities.GetStringMoneyByLong (_valueGold);

		imgProvider = transform.Find ("ImgProvider").GetComponent<Image> ();
		imgProvider.sprite = _sprProvider;

		txtValueCard = imgProvider.transform.Find ("TxtValueCost").GetComponent<Text> ();
		txtValueCard.text = Utilities.GetStringMoneyByLong (_valueCard);


		btnItem = transform.GetChild(0).GetComponent<Button> ();
		btnItem.onClick.AddListener (ItemOnClick);

		objLock = transform.Find ("Lock").gameObject;
		txtVipLevelLock = objLock.transform.GetChild (0).GetComponent<Text> ();

		itemClick = _itemClick;

		Index = _index;
	}
	public void ShowSpriteProvider(Sprite _spr){
		imgProvider.sprite = _spr;
	}

	void ItemOnClick()
	{
		itemClick (Index);
	}

	public void ShowLock(int _vipLevel){
		objLock.SetActive (true);
		txtVipLevelLock.text = "VIP " + _vipLevel.ToString ();
	}
	public void HideLock(){
		objLock.SetActive (false);
	}

}
