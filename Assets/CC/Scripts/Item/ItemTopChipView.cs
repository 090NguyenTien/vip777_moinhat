﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemTopChipView : MonoBehaviour
{
    [SerializeField]
    GameObject ImgChip;
    [SerializeField]
    Sprite SprTaiSan;
    GameObject bg;
    Text txtOrder, txtName, txtChip;
    Image imgOrder;
    [SerializeField]
    Button BtnXemTaiSan;
    public string IdUser;
    PopupAchivementManager Achivement;

    public void Init()
    {
        bg = transform.GetChild(0).gameObject;
        txtOrder = transform.GetChild(1).GetComponent<Text>();
        txtName = transform.GetChild(2).GetComponent<Text>();
        txtChip = transform.GetChild(3).GetComponent<Text>();
        //		imgOrder = transform.GetChild (4).GetComponent<Image> ();
        imgOrder = transform.Find("ImgOrder").GetComponent<Image>();
    }

    public void Show(string id, int _index, string _name, long _chip, bool taisan = false)
    {
        gameObject.SetActive(true);
        //		bg.GetComponent<Image> ().color = new Color (0, 0, 0, _index % 2 == 0 ? 0.2f : .4f);
        if (_index < 5)
        {
            txtOrder.gameObject.SetActive(false);
            imgOrder.gameObject.SetActive(true);

            imgOrder.sprite = DataHelper.GetIconAchive(_index + 1);
        }
        else
        {
            txtOrder.gameObject.SetActive(true);
            imgOrder.gameObject.SetActive(false);

            txtOrder.text = (_index + 1).ToString();
        }

        if (taisan == true)
        {
            ImgChip.GetComponent<Image>().sprite = SprTaiSan;
        }

        txtName.text = _name;
        txtChip.text = Utilities.GetStringMoneyByLong(_chip);
        IdUser = id;
        Achivement = GameObject.Find("ACHIVEMENT V2").GetComponent<PopupAchivementManager>();

        //BtnXemTaiSan.onClick.RemoveAllListeners();
        //BtnXemTaiSan.onClick.AddListener(TaiSan);
    }


    public void TaiSan()
    {
        Achivement.XemTaiSan(IdUser);
    }
}
