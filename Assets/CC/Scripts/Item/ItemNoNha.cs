﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemNoNha : MonoBehaviour
{
    public Image Img_item_nha;
    [SerializeField]
    Sprite spr_Khung;
   
    
    public void Init(Image _img_item_nha)
    {
        Img_item_nha = _img_item_nha;
        this.gameObject.SetActive(true);
        //StartCoroutine(PrintfAfter());
    }

   public void MatNha()
    {
        Img_item_nha.sprite = spr_Khung;
    }

    public void InitThanhMau()
    {

        this.gameObject.SetActive(true);
    }
   
    public void An()
    {
        this.gameObject.SetActive(false);
    }

}
