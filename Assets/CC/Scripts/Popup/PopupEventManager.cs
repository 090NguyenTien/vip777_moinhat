﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using BaseCallBack;
using SimpleJSON;
using UnityEngine.EventSystems;
using System;

public class PopupEventManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	Transform trsfItemParent;
	[SerializeField]
	Image imgMain, imgAction;
	[SerializeField]
	Button btnAction, btnWebView, btnBack;
	[SerializeField]
	GameObject objEventThumb,objEvenDapHeo;
    //[SerializeField]
    //PopupAlertManager popupAlertManager;
    [SerializeField]
    private Button btnFreeWheel = null;

    //[SerializeField]
    //private GameObject firstTimePurchasePanel = null;
	static bool isLoad = false;
	static List<Sprite> lstSprItem;

	bool isLoadCurrentScene = false;

	onCallBack backClick;

	int CurrentIndex;

//	List<Sprite> lstSprSmall, lstSprBig, lstSprButton;
//	List<string> lstLink;

	ItemEventThumbView[] lstItemThumb;
	ItemEventMainView[] lstItemMain;

	[SerializeField]
	PopupLuckyWheelManager Wheel;

	int CurrentIndexEvent;
	int IndexWheel;

	int EventCount{
		get{ return GameHelper.nodeEventData.Count; }
	}
	static JSONNode nodeEvent{
		get{ return GameHelper.nodeEventData; }
	}

	public void Init(onCallBack _callBack)
	{
		btnBack.onClick.AddListener (BtnBackOnClick);

        btnFreeWheel.onClick.AddListener(showFreeWheel);
        //btnFirstTimePurchase.onClick.AddListener(showFirstTimePurchase);

        backClick = _callBack;

		//btnAction.onClick.AddListener (BtnActionOnClick);
		CurrentIndex = 0;

        //RspEvent();
        Wheel.Init();
        //Wheel.RequestLuckyWheelData();
    }
    private GameObject curEvent = null;
    private void showFreeWheel()
    {
        Wheel.Show();
        curEvent = Wheel.gameObject;
    }

    public void showFirstTimePurchase()
    {
        //firstTimePurchasePanel.gameObject.SetActive(true);
        //popupAlertManager.ShowFirstPurchase(OpenFirstPurchasePanel);
    }

    private void OpenFirstPurchasePanel()
    {
        throw new NotImplementedException();
    }

    void OnSetEnableBtnBack (bool _enable)
	{
		btnBack.gameObject.SetActive (_enable);
	}

	#region THUMB

	void OnGetSprThumb (int i, Sprite spr)
	{
		lstItemThumb [i].ShowSprite (spr);
	}

	void ItemThumbOnClick (int _index)
	{
//		Debug.Log ("Index: " + _index);
		if (CurrentIndex == _index || Wheel.IsRolling)
			return;

		//Phong to nho hinh thumb
		for (int i = 0; i < lstItemThumb.Length; i++) {
			lstItemThumb [i].IsSelected = i == _index;
		}
		CurrentIndex = _index;

		//Show Sprite Main
		if (lstItemMain [_index].SprBig == null)
			StartCoroutine (GameHelper.Thread (lstItemMain [_index].UrlSpr, OnShowSprMain));
		else
			OnShowSprMain ();

        //Neu index == indexWHeel thi show Wheel
        if (IndexWheel == _index)
        {
            Wheel.Show();
        }
        else
        {
            Wheel.Hide();
        }
		
		bool _isAction = lstItemMain [_index].IsAction;

		//Neu index nay co Action thi show buttonAction
		if (_isAction) {
			if (lstItemMain [_index].SprAction == null)
				StartCoroutine (GameHelper.Thread (lstItemMain [_index].UrlSpr, OnShowSprAction));
			else
				OnShowSprAction ();
		}

		btnAction.gameObject.SetActive (_isAction);

	}

	#endregion

#region MAIN

	void OnShowSprMain()
	{
		imgMain.sprite = lstItemMain [CurrentIndex].SprBig;
	}
	void OnShowSprMain(Sprite _spr)
	{
		lstItemMain [CurrentIndex].SprBig = _spr;
		imgMain.sprite = _spr;
	}
	void OnShowSprAction()
	{
		imgAction.gameObject.SetActive (true);
		imgAction.sprite = lstItemMain [CurrentIndex].SprAction;
	}
	void OnShowSprAction(Sprite _spr)
	{
		lstItemMain [CurrentIndex].SprAction = _spr;
		imgAction.gameObject.SetActive (true);
		imgAction.sprite = _spr;
	}

	#endregion


	public void Show()
	{
        panel.SetActive(true);
	}

	void BtnActionOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		Debug.Log (lstItemMain [CurrentIndex].UrlAction);
		InAppBrowser.OpenURL (lstItemMain [CurrentIndex].UrlAction);
	}

	void BtnWebViewOnClick(string _link)
	{
		InAppBrowser.OpenURL (_link);
	}

	public void BtnBackOnClick()
	{

        SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		if (Wheel.IsRolling)
			return;
		
		backClick ();
        curEvent.gameObject.SetActive(false);

    }

	public void Hide()
	{
		panel.SetActive (false);
	}

	public IEnumerator Thread(string _url1, onCallBackSprite _spr1)
	{
		WWW www1 = new WWW (_url1);
		yield return www1;

		Texture2D mainImage1 = www1.texture;
		Sprite spr = Sprite.Create(mainImage1, new Rect(0, 0, mainImage1.width, mainImage1.height), new Vector2(0.5f, 0.5f));

		_spr1 (spr);
	}

}

public class ItemShopDapHeo
{
    public string id;
    public string price_vnd;
    public string price_usd;
    public string turn;
    public string StoreId;
}