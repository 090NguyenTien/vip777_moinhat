﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System;

public class GiftCode : MonoBehaviour
{
    public Text txtGoldReceive;
    public Button btnOKReceive;
    [SerializeField]
    Text txtChipUser;
    public Button btnOK;
    public InputField txtGiftCode;
    public Button btnClose;
    public GameObject panelGiftCode;
    public GameObject panelPopup;
    public GameObject panelReceiveGift;
    public Text txtError;
    // Use this for initialization
    void Start()
    {
        panelReceiveGift.SetActive(false);
		panelGiftCode.SetActive(true);
        btnOK.onClick.AddListener(BtnOKOnClick);
        btnClose.onClick.AddListener(BtnCloseOnClick);
        txtError.text = "";

        //init panelReceiveGift
        btnOKReceive.onClick.AddListener(BtnOKReceiveOnClick);
        txtGoldReceive.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }
    void BtnOKReceiveOnClick()
    {
        panelReceiveGift.SetActive(false);
    }
    void BtnCloseOnClick()
    {
       // panelPopup.SetActive(false);
		gameObject.SetActive(false);
        panelGiftCode.SetActive(false);
        panelReceiveGift.SetActive(false);
        txtError.text = "";
    }
    void BtnOKOnClick()
    {
        if(txtGiftCode.text.Length > 0)
            API.Instance.UseGiftCode(txtGiftCode.text, RspGiftCodeCallback);
    }

    void RspGiftCodeCallback(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        if (node["status"].AsInt == -1)
        {
            AlertController.api.showAlert(node["msg"]);
            return;
        }
        long chipReceive = long.Parse(node["chip_receive"].Value);
        int wheelReceive = int.Parse(node["wheel_receive"].Value);
        int wheel = int.Parse(node["wheel"].Value);
        long chip = long.Parse(node["chip"].Value);
        
        txtChipUser.text = MyInfo.CHIP.ToString();
        string receive = "";
        if (chipReceive > 0)
            receive = chipReceive + " chip";
        if (wheelReceive > 0)
            receive = " và " + wheelReceive + " vòng quay đặc biệt";
        
        AnimHomeController.api.playGold((int)chipReceive, 20, Vector3.one, HomeViewV2.api.getGoldTranform(),null,false);
        StartCoroutine(showGiftAlertReceive(receive));
        MyInfo.CHIP = chip;
    }

    private IEnumerator showGiftAlertReceive(string receive)
    {
        yield return new WaitForSeconds(2f);
        AlertController.api.showAlert("Bạn vừa nhận được " + receive);
    }
}
