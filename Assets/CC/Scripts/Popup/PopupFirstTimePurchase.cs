﻿using BaseCallBack;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupFirstTimePurchase : MonoBehaviour
{
    //[SerializeField]
    //private HomeView homeview = null;
	//[SerializeField]
	//private HomeViewV2 homeviewv2 = null;
    onCallBack cb;
    public void purchaseNow()
    {        
        closePopup();
        //if(homeview != null)
        //      	homeview.ShopOnClick();
        //if(homeviewv2 != null)
        //homeviewv2.ShopOnClick();
        if (cb != null)
        {
            cb();
        }
    }
    public void Show(onCallBack _cb)
    {
        cb = null;
        cb = _cb;
        gameObject.SetActive(true);
    }
    public void closePopup()
    {
        gameObject.SetActive(false);
    }

}
