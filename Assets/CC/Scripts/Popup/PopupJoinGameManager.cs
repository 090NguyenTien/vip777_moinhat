﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using BaseCallBack;

public class PopupJoinGameManager : MonoBehaviour {

	[SerializeField]
	GameObject panel;
	[SerializeField]
	Text txtID, txtHost, txtContent, txtError;
	[SerializeField]
	InputField inputPass;
	[SerializeField]
	Button btnCancel, btnOk;

	onCallBackIntString _okOnClick;

	int roomID;

	public void Init()
	{
		btnCancel.onClick.AddListener (CancelOnClick);
		btnOk.onClick.AddListener (OkOnClick);
	}

	public void Show(int _id, long _bet, string _host, onCallBackIntString _okClick)
	{
		panel.SetActive (true);

		txtID.text = _bet.ToString ();
		txtHost.text = _host;

		txtContent.text = ConstText.RoomID + ": " + _id.ToString();
		txtError.text = "";
		roomID = _id;
		_okOnClick = _okClick;
	}
	public void Hide()
	{
		panel.SetActive (false);
	}
	public void RefreshError()
	{
		txtError.text = "";
	}
	public void ShowError (string _msg)
	{
		txtError.text = _msg;
	}

	void CancelOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		panel.SetActive (false);
	}
	void OkOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		_okOnClick (roomID, inputPass.text);
		txtError.text = "";
	}
}
