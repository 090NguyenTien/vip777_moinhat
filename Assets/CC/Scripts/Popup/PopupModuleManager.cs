﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*

[SerializeField]
	PopupModuleManager Module;

Module.Init();

Module.Show();



 */
using BaseCallBack;

public class PopupModuleManager : MonoBehaviour {

	[SerializeField]
	bool SOUND, INVITED, LOGOUT, TUTORIAL, BACKGROUND_MUSIC;
	[SerializeField]
	GameObject panel;

	[SerializeField]
	GameObject SoundOn, SoundOff, InvitedOn, InvitedOff, MusicOn,MusicOff;
	[SerializeField]
	Button btnSound, btnInvited, btnLogOut, btnClose, btnTutorial, btnBgMusic, btnHotline;

	onCallBack logOutClick, tutorialClick;

    private string hotlineNumber = "";
    private void Awake()
    {
        hotlineNumber = GameHelper.getConfig("HotLine");
        btnHotline.transform.GetChild(0).GetComponent<Text>().text = "Hotline          " + hotlineNumber;
    }

    public void InitHome(onCallBack _logOut)
	{
		SettingHelper.Instance.Init ();
		if (SOUND)
        {
			btnSound.onClick.AddListener (SoundOnClick);
			ShowSound (SettingHelper.SOUND.ENABLE);
		}
        if (BACKGROUND_MUSIC)
        {
            btnBgMusic.onClick.AddListener(BackgroundMusicOnClick);
            ShowBackgroundMusic(SettingHelper.SOUND.ENABLE);
        }
        if (INVITED)
        {
			btnInvited.onClick.AddListener (InvitedOnClick);
			ShowInvited (SettingHelper.Instance.INVITED);
		}
		
		if (LOGOUT)
        {
			btnLogOut.onClick.AddListener (LogOutOnClick);
			logOutClick = _logOut;
		}
        btnHotline.onClick.RemoveAllListeners();
        btnHotline.onClick.AddListener(callHotLine);

		btnClose.onClick.AddListener (CloseOnClick);
	}

    private void callHotLine()
    {
        Application.OpenURL("tel://" + hotlineNumber);
    }
	public void InitLobby(onCallBack _tutorial)
	{
		SettingHelper.Instance.Init ();
		if (SOUND) {
			btnSound.onClick.AddListener (SoundOnClick);
			ShowSound (SettingHelper.SOUND.ENABLE);
		}

        if (BACKGROUND_MUSIC)
        {
            btnBgMusic.onClick.AddListener(BackgroundMusicOnClick);
            ShowBackgroundMusic(SettingHelper.BACKGROUND_MUSIC.ENABLE);
        }

        if (INVITED) {
			btnInvited.onClick.AddListener (InvitedOnClick);
			ShowInvited (SettingHelper.Instance.INVITED);
		}

		if (TUTORIAL)
        {
			btnTutorial.onClick.AddListener (BtnTutorialOnClick);
			tutorialClick = _tutorial;
		}
        btnHotline.onClick.RemoveAllListeners();
        btnHotline.onClick.AddListener(callHotLine);

        btnClose.onClick.AddListener (CloseOnClick);

	}

	void BtnTutorialOnClick ()
	{
		tutorialClick ();
	}

	public void Show()
	{
		panel.SetActive (true);
	}
	public void Hide()
	{
		panel.SetActive (false);
	}

	void CloseOnClick()
	{
        Debug.Log("ABCDEGFFFFFFFFFFFFFFFFFFF===========");
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);
		Hide ();
	}
    void BackgroundMusicOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

        SettingHelper.BACKGROUND_MUSIC.ENABLE = !SettingHelper.BACKGROUND_MUSIC.ENABLE;

        ShowBackgroundMusic(SettingHelper.BACKGROUND_MUSIC.ENABLE);

    }

    void SoundOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		SettingHelper.SOUND.ENABLE = !SettingHelper.SOUND.ENABLE;

		ShowSound (SettingHelper.SOUND.ENABLE);

	}
	void InvitedOnClick()
	{
		SoundManager.PlaySound (SoundManager.BUTTON_CLICK);

		SettingHelper.Instance.INVITED = !SettingHelper.Instance.INVITED;

		ShowInvited (SettingHelper.Instance.INVITED);
	}

	void ShowSound(bool _enable)
	{
		SoundOff.SetActive (!_enable);
		SoundOn.SetActive (_enable);
	}
    void ShowBackgroundMusic(bool _enable)
    {
        MusicOff.SetActive(!_enable);
        MusicOn.SetActive(_enable);
    }
    void ShowInvited(bool _invited)
	{
		InvitedOff.SetActive (_invited);
		InvitedOn.SetActive (!_invited);
	}

	void LogOutOnClick ()
	{
		logOutClick ();
	}

    public void showFacebookFanpage()
    {
        Application.OpenURL("https://www.facebook.com/VipGame777-113486906690253/?modal=admin_todo_tour");
    }
}
