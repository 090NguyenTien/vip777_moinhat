﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimHomeController : MonoBehaviour 
{
    public static AnimHomeController api;


    void Awake()
    {
        api = this;
        init();
    }
    public void init()
    {
        iniGold();
    }
    [SerializeField]
    private GameObject cointPrefab = null;
  
    #region Effect Gold
    private ObjectPool goldPool;
    private List<RectTransform> goldList;
    private int numGoldComplete;
    private bool isAnimatingGold;
    private long curGold;
    private float heightGoldWorld;
    void iniGold()
    {
        GameObject goldPrefab = cointPrefab;
        RectTransform goldLayerRect = new GameObject("Gold").AddComponent<RectTransform>();
        goldLayerRect.SetParent(transform);
        goldLayerRect.localPosition = Vector3.zero;
        goldLayerRect.localScale = Vector3.one;
        goldLayerRect.anchorMin = Vector3.zero;
        goldLayerRect.anchorMax = Vector3.one;
        goldLayerRect.offsetMin = Vector3.zero;
        goldLayerRect.offsetMax = Vector3.zero;
        goldPool = new ObjectPool(15, goldLayerRect, goldPrefab);

        RectTransform goldRectPrefab = goldPool.GetPool().GetComponent<RectTransform>();
        Vector3[] cornerGold = new Vector3[4];
        goldRectPrefab.GetWorldCorners(cornerGold);
        heightGoldWorld = cornerGold[1].y - cornerGold[0].y;
        goldPool.AddPool(goldRectPrefab.gameObject);
        goldList = new List<RectTransform>();
    }


    private Transform targetMove;
    public void playGold(int addedGold,int numPrefab, Vector3 originPos, Transform targetMove , Action cb = null,bool isAddChip=true)
    {
        //Debug.LogError("playGold=====addedGold======"+ addedGold);
        if (addedGold == 0)
        {
            //cong gold = 0 ma dien anima cai gi, dien ak
            return;
        }

        long firstTimeUpdateGold = MyInfo.CHIP;
        curGold = firstTimeUpdateGold + addedGold;

        int indexCurFirst = goldList.Count;
       

        int valuePerGold = addedGold / numPrefab;

        this.targetMove = targetMove;

        isAnimatingGold = true;
        Vector3 bound = Vector3.one * 3;
        for (int i = 0; i < numPrefab; i++)
        {
            RectTransform goldTran = goldPool.GetPool().GetComponent<RectTransform>();
            goldTran.localScale = Vector3.zero;
            goldTran.position = originPos;            
            goldList.Add(goldTran);
        }
        //Debug.LogError("playGold=====goldList.Count======" + goldList.Count);
        //Debug.LogError("playGold=====indexCurFirst======" + indexCurFirst);
        int indexTmp = 0;
        for (int i = indexCurFirst; i < goldList.Count; i++)
        {
            int index = indexTmp;
            int index1 = i;
            ++indexTmp;
            RectTransform goldTran = goldList[i];
            Vector3 originPosTmp = originPos;
            originPosTmp.x = UnityEngine.Random.Range(originPosTmp.x - bound.x / 2, originPosTmp.x + bound.x / 2);
            originPosTmp.y = UnityEngine.Random.Range(originPosTmp.y - bound.y / 2 , originPosTmp.y + bound.y / 2);

            goldTran.transform.localScale = Vector3.one * 0.3f;

            float delayFirstTween = 0 ;
           
            goldTran.DOScale(Vector3.one * 0.7f, 0.65f).SetEase(Ease.OutSine).SetDelay(delayFirstTween)/*.OnStart(() => goldTran.gameObject.SetActive(true))*/;
            Vector3[] cornerGold = new Vector3[4];
            goldTran.GetWorldCorners(cornerGold);

            goldTran.transform.position = originPos;
            //originPosTmp.y = Mathf.Max(heightGoldWorld, originPosTmp.y);
           
            goldTran.DOMove(originPosTmp, 0.2f).SetEase(Ease.OutBack).SetDelay(delayFirstTween).OnComplete(() =>
            {
                Vector3 targetPos = targetMove.transform.position;
                //index * 0.01f + 0.05f)
                goldTran.DOMove(targetPos, 0.5f).SetEase(Ease.OutQuad).SetDelay(index * 0.01f + 0.05f).OnComplete(() =>
                {
                    goldTran.gameObject.SetActive(false);
                    if (HomeViewV2.api != null) HomeViewV2.api.effectReceiveGold();
                    else effectReceiveGold(targetMove.gameObject);
                    SoundManager.PlaySound(SoundManager.BUTTON_CLICK);

                    if (HomeViewV2.api != null) HomeViewV2.api.UpdateChip(Utilities.GetStringMoneyByLong(firstTimeUpdateGold + valuePerGold));
                    firstTimeUpdateGold = firstTimeUpdateGold + valuePerGold;
                   
                    ++numGoldComplete;
                    //Debug.LogError("playGold=====goldTran======" + goldTran + i);
                    if (numGoldComplete >= goldList.Count)
                    {
                        //Debug.LogError("playGold=====numGoldComplete======" + numGoldComplete);
                        if (HomeViewV2.api != null)
                        {
                            HomeViewV2.api.UpdateChip(Utilities.GetStringMoneyByLong(curGold));
                            if(isAddChip) MyInfo.CHIP = curGold;
                        }                        
                        resetGold();
                        if (cb != null)
                        {
                            cb();
                        }
                    }
                });
            });
        }
    }

    public void effectReceiveGold(GameObject txtChip)
    {
        float time1 = 0.15f;
        txtChip.transform.DOKill();
        txtChip.transform.localScale = Vector3.one;
        txtChip.transform.DOScale(Vector3.one * 0.8f, time1).SetEase(Ease.Linear).OnComplete(() => {
            txtChip.transform.DOScale(Vector3.one * 1.1f, time1).SetEase(Ease.Linear).OnComplete(() => {
                txtChip.transform.DOScale(Vector3.one * 0.9f, time1).SetEase(Ease.Linear).OnComplete(() =>
                {
                    txtChip.transform.DOScale(Vector3.one, time1).SetEase(Ease.Linear);
                });
            }); ;
        });
    }






    private void resetGold()
    {
        //UserInfo myInfo = MainGameController.api.getMyInfo();
        if (goldList != null)
        {
            int length = goldList.Count;
            for (int i = 0; i < length; i++)
            {
                Transform goldTran = goldList[i];
                goldTran.localPosition = Vector3.one * 5000;
                goldPool.AddPool(goldTran.gameObject);
                goldTran.DOKill();
            }
            numGoldComplete = 0;
            goldList = new List<RectTransform>();
            isAnimatingGold = false;
            if (curGold > 0)
            {
                if(HomeViewV2.api!=null) HomeViewV2.api.UpdateChip(Utilities.GetStringMoneyByLong(curGold));
            }
            curGold = 0;
        }
    }
    #endregion



    public void scaleRect(RectTransform target)
    {
        float time1 = 0.15f;
        target.DOKill();
        target.localScale = Vector3.one;
        target.DOScale(Vector3.one * 0.8f, time1).SetEase(Ease.Linear).OnComplete(() => {
            target.DOScale(Vector3.one * 1.1f, time1).SetEase(Ease.Linear).OnComplete(() => {
                target.DOScale(Vector3.one * 0.9f, time1).SetEase(Ease.Linear).OnComplete(() =>
                {
                    target.DOScale(Vector3.one, time1).SetEase(Ease.Linear);
                });
            }); ;
        });

    }
    public bool checkIsAnimatingAll()
    {
        return isAnimatingGold ;
    }



    private void OnDisable()
    {
        resetGold();
    }
}
