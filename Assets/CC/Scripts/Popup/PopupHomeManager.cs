﻿using UnityEngine;
using System.Collections;

public class PopupHomeManager : MonoBehaviour {

	[SerializeField]
	Transform trsfParent;

	[SerializeField]
	GameObject objInvited, objAchivement;

	PopupInvited Invited;
	PopupAchivementManager Achivement;

	public void ShowInvited()
	{
		if (Invited == null) {
			GameObject obj = Instantiate (objInvited) as GameObject;
			obj.transform.SetParent (trsfParent);
			obj.transform.localScale = Vector3.one;
			obj.transform.position = Vector3.zero;

			Invited = obj.GetComponent<PopupInvited> ();
//			Invited.Ini
		}
	}
}
