﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif
using UnityEngine.UI;

public class PopupVideoReward : MonoBehaviour
{
    [SerializeField]
    private Button btnWatchVideo = null;
    //[SerializeField]
    //private Button btnWatchDoubleVideo = null;
    [SerializeField]
    private Text txtTextBtnReceive = null, txtCurGoldReward = null, txtNextGoldReward = null, txtCountText = null;

    [SerializeField]
    private TimeCountDownController timeCountDown = null, timeHomeCountDown = null;
    [SerializeField]
    private GameObject timehomeCountdownBg = null;

    [SerializeField]
    private Animator btnFreeChipAnim = null;

    [SerializeField]
    private GameObject doubleChip = null;

    private int adsType = 1;
    private string vungleRewardId = "";
    bool adInited = false;
    private void OnEnable()
    {
        
        waiting.gameObject.SetActive(false);
        initFirstTime();
    }

    void initializeEventHandlers()
    {
        // Event triggered during when an ad is about to be played
        //Vungle.onAdStartedEvent += (placementID) => {
        //    Debug.Log("Ad " + placementID + " is starting!  Pause your game  animation or sound here.");
        //};
        //// Event is triggered when a Vungle ad finished and provides the entire information about this event
        //// These can be used to determine how much of the video the user viewed, if they skipped the ad early, etc.
        //Vungle.onAdFinishedEvent += (placementID, args) => {
        //    Debug.Log("Ad finished - placementID " + placementID + ", was call to action clicked:" + args.WasCallToActionClicked + ", is completed view:"
        //        + args.IsCompletedView);
        //    if (args.IsCompletedView == true)
        //    {
        //        callReceiveGiftWatched();
        //    }
        //    else
        //    {
        //        btnWatchVideo.interactable = true;
        //        AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        //    }
        //};

        //// Event is triggered when the ad's playable state has been changed
        //// It can be used to enable certain functionality only accessible when ad plays are available
        //Vungle.adPlayableEvent += (placementID, adPlayable) => {
        //    Debug.Log("Ad's playable state has been changed! placementID " + placementID + ". Now: " + adPlayable);
        //    //placements[placementID] = adPlayable;
        //};

        ////Fired initialize event from sdk
        //Vungle.onInitializeEvent += () => {
        //    adInited = true;
        //    Debug.Log("SDK initialized");
        //};

    }

    public static event Action onInitializeEvent;
    public void initFirstTime()
    {
        API.Instance.RequestAds(1, RspRequestAds);
    }

    void RspRequestAds(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.Log(" RspRequestAds " + _json);
        if (node["status"].AsInt == 1)
        {
            txtCurGoldReward.text = node[ParamKey.CUR_ADS_REWARD].Value;
            txtNextGoldReward.text = node[ParamKey.NEXT_ADS_REWARD].Value;
            txtCountText.text = "Bạn còn " + (int.Parse(node[ParamKey.MAX_ADS].Value) - int.Parse(node[ParamKey.ADS_SUCCESS_COUNT].Value)).ToString() + " lượt xem";
            adsType = int.Parse(node["at"].Value);
            if (node[ParamKey.ADS_DELAY] != null)//dang delay
            {
                int delayTime = int.Parse(node[ParamKey.ADS_DELAY].Value);
                startDelay(delayTime);
            }
            else
            {
                onCountdownComplete();
                adsSecretkey = node[ParamKey.ADS_SECRET_KEY].Value;
            }
        }
        else if (node["status"].AsInt != 1)
        {
            txtCountText.text = "Bạn còn đã dùng hết lượt xem hôm nay";
            txtCurGoldReward.text = "Max";
            txtNextGoldReward.text = "Max";
        }
    }
    public void Start()
    {
        btnWatchVideo.onClick.AddListener(showAdmobVideo);
        //btnWatchDoubleVideo.onClick.AddListener(showDoubleVideo);
    }

    private void showDoubleVideo()
    {
        waiting.gameObject.SetActive(true);
        btnWatchVideo.interactable = false;
        //if (Vungle.isAdvertAvailable(vungleRewardId))
        //{
        //    Vungle.playAd(vungleRewardId);
        //}
        //else
        //{
#if UNITY_IOS || UNITY_ANDROID
            ShowUnityVideoRewardAd();
#endif
        //}
    }

    private string adsSecretkey = "";

    private void startDelay(int secondDelay)
    {
        Debug.Log("startDelay ");
        btnFreeChipAnim.Play("CountDownFreeChip");
        btnWatchVideo.interactable = false;
        txtTextBtnReceive.gameObject.SetActive(false);
        timeCountDown.initTimeCountDown(secondDelay, secondDelay, 0, onCountdownComplete);
        timeHomeCountDown.initTimeCountDown(secondDelay, secondDelay, 0, onCountdownHomeComplete);

    }

    private void onCountdownHomeComplete()
    {
        timehomeCountdownBg.SetActive(false);
        //Debug.LogError("timehomeCountdownBg "+ timehomeCountdownBg.activeInHierarchy);
        btnFreeChipAnim.Play("FreeChipCanReceive");
    }

    private void onCountdownComplete()
    {
        txtTextBtnReceive.gameObject.SetActive(true);
        btnWatchVideo.interactable = true;
        timeCountDown.gameObject.SetActive(false);
        //btnFreeChipAnim.Play("FreeChipCanReceive");
        btnFreeChipAnim.Play("FreeChipCanReceive");
    }

    [SerializeField]
    private GameObject waiting;
    private void showAdmobVideo()
    {
        waiting.gameObject.SetActive(true);
        btnWatchVideo.interactable = false;
#if UNITY_IOS || UNITY_ANDROID
        ShowUnityVideoRewardAd();
#endif
    }

    public void closePopup()
    {
        //gameObject.SetActive(false);
    }

    public void close()
    {
        //doubleChip.SetActive(false);
        gameObject.SetActive(false);
    }

    private void callReceiveGiftWatched()
    {
        waiting.gameObject.SetActive(false);
        if (adsSecretkey != "")
        {
            API.Instance.RewardAds(adsSecretkey, RspRewardCallback);
        }
    }

    void RspRewardCallback(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        if (node["status"].AsInt == 1)
        {
            if (int.Parse(node[ParamKey.MAX_ADS].Value) > int.Parse(node[ParamKey.ADS_SUCCESS_COUNT].Value))
            {
                int delayTime = int.Parse(node[ParamKey.ADS_DELAY].Value);
                Debug.Log("RspRewardCallback ");
                startDelay(delayTime);
                txtCurGoldReward.text = node[ParamKey.CUR_ADS_REWARD].Value;
                txtNextGoldReward.text = node[ParamKey.NEXT_ADS_REWARD].Value;
                txtCountText.text = "Bạn còn " + (int.Parse(node[ParamKey.MAX_ADS].Value) - int.Parse(node[ParamKey.ADS_SUCCESS_COUNT].Value)).ToString() + " lượt xem";
            }
            else
            {
                txtCountText.text = "Bạn đã hết lượt xem hôm nay";
            }
            double mychip = double.Parse(node[ParamKey.CHIP].Value);
            int chipReward = (int)(mychip - MyInfo.CHIP);
            if (HomeViewV2.api != null)
            {
                AnimHomeController.api.playGold(chipReward, 20, btnWatchVideo.transform.position, HomeViewV2.api.getGoldTranform());
            }
            //int adsType = int.Parse(node["at"].Value);
            //if (adsType == 1)
            //{
                //doubleChip.gameObject.SetActive(true);
                //API.Instance.RequestAds(2, RspRequestAds);
            //}
            //else if (adsType == 2)
            //{
            //    doubleChip.gameObject.SetActive(false);
            //}
        }
        else if (node["status"].AsInt != 1)
        {
            btnWatchVideo.interactable = true;
            AlertController.api.showAlert("Có lỗi khi xử lý thưởng xem video, vui lòng liên hệ admin");
        }

    }
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////

    public string placementId = "rewardedVideo";
#if UNITY_IOS || UNITY_ANDROID

    void ShowUnityVideoRewardAd()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;
        Advertisement.Show(placementId, options);
    }

    void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            callReceiveGiftWatched();
        }
        else if (result == ShowResult.Skipped)
        {
            btnWatchVideo.interactable = true;
            AlertController.api.showAlert("Bạn phải xem hết video mới nhận được quà!");
        }
        else if (result == ShowResult.Failed)
        {
            //if (Vungle.isAdvertAvailable(vungleRewardId))
            //{
            //    Vungle.playAd(vungleRewardId);
            //}
            //{
                waiting.gameObject.SetActive(false);
                btnWatchVideo.interactable = true;
                AlertController.api.showAlert("Hiện không có video nào khả dụng, vui lòng quay lại sau");
            //}
        }
    }

#endif
}
