﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TangChipTrongChat : MonoBehaviour
{

    public void ChangeChipToUser(string TenNguoiNhan, string txtchipReceive, long chipTang, long minChip, long limitChip, long min_transfer)
    {
        string uReceive = TenNguoiNhan;
        string strChipIsReceive = txtchipReceive;
        long chipIsReceive = 0;
        if (strChipIsReceive != "")
        {
            chipIsReceive = chipTang;
        }
        if (uReceive == "")
        {
            AlertController.api.showAlert("Vui Lòng Nhập Chính Xác Tên Người Được Tặng!");
            return;
        }
        else if (MyInfo.CHIP < minChip)
        {
            AlertController.api.showAlert("Số Chip Của Bạn Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(minChip) + " Mới Có Thể Chuyển Được!");
            return;
        }
        else if (limitChip == 0)
        {
            AlertController.api.showAlert("Bạn Phải Đạt Vip 1 Trở Lên Mới Có Thể Tặng Chip Được!");
            return;
        }
        else if (chipIsReceive < min_transfer)
        {
            AlertController.api.showAlert("Số Chip Tặng Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(min_transfer) + " !");
            return;
        }
        else if (chipIsReceive > limitChip)
        {
            AlertController.api.showAlert("Vượt Hạn Mức Cho Phép. Số Chip Tặng Của Bạn Phải Thấp Hơn " + Utilities.GetStringMoneyByLong(limitChip) + " !");
            return;
        }
        else if (MyInfo.CHIP - chipIsReceive < minChip)
        {
            AlertController.api.showAlert("Vượt Hạn Mức Cho Phép. Số Chip Còn Lại Của Bạn Sau Khi Chuyển Phải Lớn Hơn " + Utilities.GetStringMoneyByLong(minChip) + " !");
            return;
        }

        //API.Instance.RequestChangeChip(MsgContent.text, uReceive, chipIsReceive, ChangeChipComplete);
        GamePacket gp = new GamePacket(CommandKey.TRANSFER_ITEM);
        gp.Put("type", "chip");
        gp.Put("amount", chipIsReceive);
        gp.Put("to_user_name", uReceive);

        Debug.LogError("chipIsReceive---------- " + chipIsReceive + "-----------to_user_name---------- " + uReceive);
        SFS.Instance.SendZoneRequest(gp);
    }

    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.TRANSFER_ITEM:
                ChangeChipFromServer(gp);
                break;
        }
    }

    private void ChangeChipFromServer(GamePacket gp)
    {
        int status = gp.GetInt("status");
        if (status == 1)
        {
            AlertController.api.showAlert("Tặng Chip Thành Công, Chip Tặng Sẽ Được Gửi Vào Hộp Thư Người Được Tặng!");
            MyInfo.CHIP = gp.GetLong("chip");
        }
        else
        {
            AlertController.api.showAlert(gp.GetString("msg"));
        }
    }



}
