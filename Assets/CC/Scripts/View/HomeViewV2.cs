﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using Facebook.Unity;
using System;

public class HomeViewV2 : MonoBehaviour {
    [HideInInspector]
	public HomeControllerV2 Controller;

    public static HomeViewV2 api;
    private void Awake()
    {
        if (api != null)
        {
            return;
        }
        api = this;
    }
    void Start()
    {
        //if (GameHelper.gameKind == GameKind.Baucua)
        //{
        //    girlLeft.SetActive(true);
        //    MainGame.SetActive(false);
        //}
        //else
        //{
            girlLeft.SetActive(false);
            MainGame.SetActive(true);
        //}
    }
    public Transform getGoldTranform()
    {
        return txtChip.transform;
    }

    [SerializeField]
	GameObject panelUI;
	[SerializeField]
	Text txtUsername, txtChip, txtGem;
	[SerializeField]
	Image imgAvatar, imgBorderAvt, imgBorderAvt_Df;
	[SerializeField]
	Button btnJoinLobby, btnShop;
	[SerializeField]
	Button btnAvatar;

	[SerializeField]
	Button btnModule, btnEvent, btnInbox;

	[SerializeField]
	Image ImgUnreadInbox;
	[SerializeField]
	Text TxtUnreadInbox;
    [SerializeField]
    GameObject girlLeft, MainGame, PanelTinTuc;
    [SerializeField]
    Text TxtVipInHome;


    [SerializeField]
    GameObject PanelShopBorAvatar, ScollBorderAvatar;
    [SerializeField]
    PopupInfomationManager InfomationManager;
    //[SerializeField]
    //ParticleSystem ParticleLogo;
    [SerializeField]
    Button btnBarXengHoaQua;
    [SerializeField]
    Image ImgTinTuc;

    public void OpenPanelShopBorAvatar()
    {
        PanelShopBorAvatar.SetActive(true);
        ShopBorderAvatar sh = PanelShopBorAvatar.GetComponent<ShopBorderAvatar>();
        sh.PanelMua.SetActive(false);
        sh.InitPanelBuyBorAvatar();
        sh.InitTop();

    }

    public void XemTinTuc()
    {        
        WWWForm form = new WWWForm();
        form.AddField("", "");       
        API.Instance.BaseCallService("/event/noticeimage", form, ShowTinTuc);
        
    }

    private void ShowTinTuc(string url)
    {
        string Url = API.Instance.DOMAIN + "/" + url;
        API.Instance.Load_Spr(Url, ImgTinTuc);
        PanelTinTuc.SetActive(true);
    }

    public void CloseTinTuc()
    {
        PanelTinTuc.SetActive(false);
    }


    public void ClosePanelShopBorAvatar()
    {
        if (ScollBorderAvatar.activeInHierarchy == true)
        {
            InfomationManager.CreateItemBorderAvatar();
        }


        PanelShopBorAvatar.SetActive(false);
        ShopBorderAvatar sh = PanelShopBorAvatar.GetComponent<ShopBorderAvatar>();
        sh.PanelMua.SetActive(false);
    }

    public void Init(HomeControllerV2 _controller)
	{        
        Controller = _controller;

		btnAvatar.onClick.AddListener (BtnAvatarOnClick);

		btnModule.onClick.AddListener (ModuleOnClick);

        
        btnShop.onClick.AddListener (ShopOnClick);
        //btnShowRank.onClick.AddListener (onShowRank);

        btnEvent.onClick.AddListener (rewardVideo);
        panelVideoReward.GetComponent<PopupVideoReward>().initFirstTime();
        btnInbox.onClick.AddListener(InboxOnClick);

		btnJoinLobby.onClick.AddListener (BtnJoinLobbyOnClick);

        TxtVipInHome.text = MyInfo.MY_VIP;
		
		//Scale Item game, Show Particle
//		Intro ();
	}
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            AlertController.api.showAlert("Bạn có muốn thoát game?", Application.Quit);
        }
    }
    void BtnJoinLobbyOnClick ()
	{
		
		Controller.RequestJoinGameLobbyRoom ();
	}


	public void SetEnableArena(bool _enable){
		//		Arena.SetActive (_enable);
	}

    //[SerializeField]
    //private Button btnShowRank = null;

    [SerializeField]
    private GameObject panelVideoReward = null;
    private void onShowRank()
    {
        //goRank.gameObject.SetActive(true);
        //ParticleLogo.Stop();
        //BtnAchivementOnClick();
    }
	void LogOutOnClick ()
	{
		Controller.LogOutOnClick ();
	}

	public void ModuleOnClick()
	{
		Controller.ModuleOnClick ();
        //ParticleLogo.Stop();
    }

	void EventOnClick ()
	{
		Controller.EventOnClick ();
    }
    public void rewardVideo()
    {
        panelVideoReward.SetActive(true);
        //ParticleLogo.Stop();
    }

    void InboxOnClick()
	{
		Controller.InboxOnClick();
        //ParticleLogo.Stop();
    }

	public void ShopOnClick()
	{
		Controller.ShopOnClick ();
        //ParticleLogo.Stop();
    }

    public void Shop_GEM_OnClick()
    {
        Controller.ShopOnClick_GEM();
        //ParticleLogo.Stop();
    }



    void BtnAchivementOnClick()
	{
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Controller.BtnAchivementOnClick ();
        //ParticleLogo.Stop();
    }
	void BtnAvatarOnClick()
	{
        //if (PanelDapHeo.activeInHierarchy == false)
        //{
            Controller.BtnAvatarOnClick();
        //}

		
        //ParticleLogo.Stop();
    }

	public void SetEnablePanelUI(bool _enable)
	{
		panelUI.SetActive (_enable);
        //if (_enable)
        //{
            //ParticleLogo.Play();
        //}
	}
	public void SetEnableExchange(bool _enable){
		//		btnExchange.gameObject.SetActive (_enable);
	}
	public void SetEnableCardWheel(bool _enable){
		//		btnCardWheel.gameObject.SetActive (_enable);
	}
    public void SetEnableBarGame(bool _enable)
    {
        btnBarXengHoaQua.gameObject.SetActive(_enable);
    }

    public void ShowInfoUser(string _chip, string _gold){
		txtChip.text = _chip;
		//		txtGold.text = _gold;
	}
	public void ShowInfoUser(string _uname, string  _chip, string _gold,  string _avtName, string Bor_avtName, int _vipPoint, string _gem)
	{
		txtUsername.text = _uname;
		txtChip.text = _chip;
        txtGem.text = _gem;
		Sprite avatar = null;
		
		LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);
		if (loginType == LoginType.Facebook)
		{
			Debug.LogError("MyInfo.AVATAR_FACEBOOK_LOAD " + MyInfo.AVATAR_FACEBOOK_LOAD);
			MyInfo.AVATAR_FACEBOOK_LOAD = true;
			StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=normal&access_token=" + AccessToken.CurrentAccessToken.TokenString, this.imgAvatar));
		}
		
        if (!MyInfo.AVATAR_FACEBOOK_LOAD)//
        {
            //avatar = DataHelper.GetAvatar(_avtName);
            StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + MyInfo.AvatarName, imgAvatar));
        }
        //ShowAvatar(avatar, _avtName);

        Sprite BorAvatar = null;
        if (!MyInfo.AVATAR_FACEBOOK_LOAD)//
        {
            BorAvatar = DataHelper.GetBoderAvatar(Bor_avtName);
        }

        ShowBorAvatar(BorAvatar);


        ChatController.RegistTextChip(txtChip);
    }
	public void UpdateAvatar(){
        //imgAvatar.sprite = DataHelper.GetAvatar (MyInfo.AvatarName);
        StartCoroutine(UpdateAvatarThread(API.PREFIX_AVT + MyInfo.AvatarName, imgAvatar));
	}
	public void UpdateChip(string _chip)
	{
		txtChip.text = _chip;
	}

    public void UpdateGem(string _gem)
    {
        txtGem.text = _gem;
    }


    public void UpdateGold(string _gold)
	{
		//		txtGold.text = _gold;
	}

    void ShowAvatar(Sprite _spr, string urlAvatar = "")
    {
        LoginType loginType = (LoginType)PlayerPrefs.GetInt("LoginType", -1);

        if (loginType == LoginType.Facebook)
        {
            if (!MyInfo.AVATAR_FACEBOOK_LOAD)
            {
                MyInfo.AVATAR_FACEBOOK_LOAD = true;
                StartCoroutine(UpdateAvatarThread("https://graph.facebook.com/me/picture?type=normal&access_token=" + AccessToken.CurrentAccessToken.TokenString, this.imgAvatar));
            }
            else
            {
                imgAvatar.sprite = MyInfo.sprAvatar;
            }
        }
        else
        {
            MyInfo.sprAvatar = _spr;
            imgAvatar.sprite = _spr;
        }
    }

    internal void SetEnableBarGame()
    {
        throw new NotImplementedException();
    }

    public void ShowBorAvatar(Sprite _spr)
    {
        if (_spr != null)
        {
            MyInfo.sprBorAvatar = _spr;
            imgBorderAvt.sprite = _spr;

            imgBorderAvt.gameObject.SetActive(true);
            imgBorderAvt_Df.gameObject.SetActive(false);

            imgBorderAvt.transform.localScale = new Vector2(1.1f, 1.1f);
        }
        
    }


   // public void 



    public static IEnumerator UpdateAvatarThread(string _url, Image avatar)
    {
        Texture2D mainImage;
        WWW www = new WWW(_url);
        yield return www;

        mainImage = www.texture;
        MyInfo.sprAvatar = avatar.sprite = Sprite.Create(mainImage, new Rect(0, 0, mainImage.width, mainImage.height), new Vector2(0.5f, 0.5f));

    }



    public void effectReceiveGold()
    {
        float time1 = 0.15f;
        txtChip.transform.DOKill();
        txtChip.transform.localScale = Vector2.one;
        txtChip.transform.DOScale(Vector2.one * 0.8f, time1).SetEase(Ease.Linear).OnComplete(() => {
            txtChip.transform.DOScale(Vector2.one * 1.1f, time1).SetEase(Ease.Linear).OnComplete(() => {
                txtChip.transform.DOScale(Vector2.one * 0.9f, time1).SetEase(Ease.Linear).OnComplete(() =>
                {
                    txtChip.transform.DOScale(Vector2.one, time1).SetEase(Ease.Linear);
                });
            }); ;
        });
    }
    public void ShowOrHideUnreadInbox()
	{
		if (MyInfo.UNREAD_INBOX == 0)
		{
			TxtUnreadInbox.gameObject.SetActive(false);
			ImgUnreadInbox.gameObject.SetActive(false);
		}
		else
		{
			TxtUnreadInbox.text = MyInfo.UNREAD_INBOX + "";
			TxtUnreadInbox.gameObject.SetActive(true);
			ImgUnreadInbox.gameObject.SetActive(true);
		}
	}

    public void ShowGlobalAds() {
        VideoAdsManager.Instance.ShowAds();
    }
}
