﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertPromotion : MonoBehaviour {

    [SerializeField]
    private GameObject TxtValue, TxtDateEnd;
    [SerializeField]
    private GameObject BtnNap, BtnClose;
    [SerializeField]
    private PopupShopManager PopupShopManager;
    private JSONNode dataPromote;
    public void OnClickNapTien()
    {
        PopupShopManager.Show();
        gameObject.SetActive(false);
    }
    public void OnClickClose()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);        
    }
    void OnEnable()
    {   
        PlayerPrefs.SetInt("isShowed", 1);
        dataPromote = MyInfo.PROMOTION_IN_APP;        
        TxtValue.GetComponent<Text>().text = "X"+dataPromote["value"];
        TxtDateEnd.GetComponent<Text>().text = dataPromote["end_date"];
    }

}
