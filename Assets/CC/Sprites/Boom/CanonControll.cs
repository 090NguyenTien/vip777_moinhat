﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonControll : MonoBehaviour
{
    Animator MyAnim;

    void Awake()
    {
        MyAnim = gameObject.GetComponent<Animator>();
    }


    public void BatDaiBang()
    {
        MyAnim.SetBool("DaiBang", true);
    }

    public void Huy()
    {
        Destroy(this.gameObject);
    }

    public void BatThanBai()
    {
        MyAnim.SetBool("ThanBai", true);
    }

    public void BatLogoThanBai()
    {
        MyAnim.SetBool("LogoThanBai", true);
        //StartCoroutine(WAITING(f));
    }

    public void BatLogoQuyPhi()
    {
        MyAnim.SetBool("LogoQuyPhi", true);
        //StartCoroutine(WAITING(f));
    }

    public void BatLogoSoaiCa()
    {
        MyAnim.SetBool("LogoSoaiCa", true);
    }

    public void BatLogoHaiPhuong()
    {
        MyAnim.SetBool("LogoHaiPhuong", true);
    }

    public void BatLogoThietPhien()
    {
        MyAnim.SetBool("LogoThietPhien", true);
    }

    IEnumerator WAITING(float time)
    {
        yield return new WaitForSeconds(time);
        MyAnim.SetBool("LogoThanBai", true);
    }
}
