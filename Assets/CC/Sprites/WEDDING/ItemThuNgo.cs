﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemThuNgo : MonoBehaviour
{
    public string ID_USER;
    public string TEN_USER;
    public string TIN_NHAN;
    public string TEN_NHAN;
    public string URL;

    [SerializeField]
    Text txtTen;
    [SerializeField]
    Button BtnXoa, BtnDoc;
    [SerializeField]
    WEDDINGCONTROLL Wedding;

    public void Init(string id_user, string tenuser, string tinnhan, string url, WEDDINGCONTROLL _wedding)
    {
        ID_USER = id_user;
        TEN_USER = tenuser;
        TIN_NHAN = tinnhan;
       // TEN_NHAN = ten_nhan;
        URL = url;


        Wedding = _wedding;// GameObject.Find("PanelWedding").GetComponent<WEDDINGCONTROLL>();
        txtTen.text = tenuser;

        BtnXoa.onClick.RemoveAllListeners();
        BtnXoa.onClick.AddListener(BtnXoaClick);

        BtnDoc.onClick.RemoveAllListeners();
        BtnDoc.onClick.AddListener(BtnDocClick);
    }

    void BtnXoaClick()
    {
        Wedding.XoaThu(ID_USER);
    }

    void BtnDocClick()
    {
        Wedding.DocThu(ID_USER);
    }
}
