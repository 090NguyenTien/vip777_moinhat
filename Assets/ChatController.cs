﻿using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.SceneManagement;

public class ChatController : MonoBehaviour
{
    private bool isInit = false;
    [SerializeField]
    Toggle chatAll, friends, beInvite, bangHoi, GiaiDau, TienIch;
    [SerializeField]
    GameObject warningChat, warningFriend, warningInvite, warningBtnChat, warningBangHoi;
    int maxMessage = 50;
    List<Messager> messageList= new List<Messager>();
    List<Messager> messageBangList= new List<Messager>();
    List<FriendItem> FriendItemList = new List<FriendItem>();
    [SerializeField]
    GameObject ChatAllContainer, FriendsContainer, FriendsChatContainer, textObject, textSend, chatInputGroup, FriendItem, InviteFriend, InviteFriendContainer, ChatBangContainer;
    [SerializeField]
    ScrollRect ScrollViewChatAll, ScrollViewFriends, ScrollViewFriendsChat, ScrollViewInviteFriend, ScrollViewChatBang;
    [SerializeField]
    GameObject Content;
    [SerializeField]
    EstateControll Estate;
    [SerializeField]
    PopupInfoEnemy _PopupInfoEnemy;
    
    //[SerializeField]
    //Toggle BtnGiaiDau, BtnTienIch;

  
    int curTabActive = 0; //0 all 1 ban be
    bool isGetFriend = false;
    // Start is called before the first frame update
    void Start()
    {
        Content.gameObject.SetActive(false);
        warningBtnChat.SetActive(false);
        warningChat.SetActive(false);
        warningFriend.SetActive(false);
        warningInvite.SetActive(false);
        warningBangHoi.SetActive(false);
        ActiveScreenTab(true, false,false,false, false, false);
        chatAll.onValueChanged.AddListener(isToggleChange);
        beInvite.onValueChanged.AddListener(isToggleChange);
        friends.onValueChanged.AddListener(isToggleChange);
        bangHoi.onValueChanged.AddListener(isToggleChange);

        GiaiDau.onValueChanged.AddListener(isToggleChange);
        TienIch.onValueChanged.AddListener(isToggleChange);

        GiaiDau.gameObject.SetActive(false);
    }
    public void AllowGetFriendStatus()
    {
        isGetFriend = false;       
    }
    public bool CheckActiveSelfContent()
    {
        return Content.activeSelf;
    }
    internal void Hide()
    {
        Content.gameObject.SetActive(false);
        CloseTienIch();
    }
    internal void Show()
    {
        Content.gameObject.SetActive(true);
        if (isInit == false)
        {
            isInit = true;
            InitChat();
        }
        if (curTabActive == 5)
        {
            Content.GetComponent<BaseController>().OpenModuleTienIch(this.gameObject);
        }
        //SFS.Instance.RequestJoinChatRoom();
    }

    private void InitChat()
    {
        curTabActive = 0;
        chatAll.isOn = true;
        isGetFriend = false;

        for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        {
            string mod = DataHelper.DanhSachMod[i];
            string ten = MyInfo.NAME;
            if (ten == mod)
            {
                textSend.GetComponent<InputField>().characterLimit = 0;
                GiaiDau.gameObject.SetActive(true);
                break;
            }
        }
        //get 20 tin nhắn từ server chat tong
        GamePacket gp = new GamePacket("getBackUpChat");
        SFS.Instance.SendRoomChatRequest(gp);
        //get 20 tin nhắn từ chat bang
        if (MyInfo.BangChatRoom != null)
        {
            GamePacket gp2 = new GamePacket("getBackUpClanChat");
            SFS.Instance.SendRoomBangHoiRequest(gp2);
        }
    }

    static List<Text> LstTextChip = new List<Text>();
    public static void RegistTextChip(Text txtChip)
    {
        Debug.Log("=========UpdateChipUser========" + LstTextChip.Count);
        LstTextChip.RemoveAll(item => item == null);
        Debug.Log("=========UpdateChipUser========" + LstTextChip.Count);
        if (!LstTextChip.Contains(txtChip))
        {
            LstTextChip.Add(txtChip);
        }
    }
    private void UpdateChipUser(GamePacket param)
    {        
        long NewChipUser = param.GetLong(ParamKey.CHIP);
        MyInfo.CHIP = NewChipUser;
        foreach (Text chip in LstTextChip)
        {
            if (chip != null)
            {
                chip.text = Utilities.GetStringMoneyByLongBigSmall(NewChipUser);
            }
        }
    }

    private void ActiveScreenTab(bool isChatAll, bool isFriends, bool invite, bool banghoi, bool GiaiDau, bool TienIch)
    {
        ScrollViewChatAll.gameObject.SetActive(isChatAll);
        ScrollViewChatBang.gameObject.SetActive(banghoi);
        ScrollViewFriendsChat.gameObject.SetActive(isFriends);
        ScrollViewFriends.gameObject.SetActive(isFriends);
        ScrollViewInviteFriend.gameObject.SetActive(invite);
        chatInputGroup.gameObject.SetActive(!invite);
    }
    public void isToggleChange(bool val)
    {
        if (val == true)
        {
            //chatInputGroup.SetActive(true);
            ActiveScreenTab(chatAll.isOn, friends.isOn, beInvite.isOn, bangHoi.isOn, GiaiDau.isOn, TienIch.isOn);
            RectTransform tem = ChatAllContainer.transform.GetComponent<RectTransform>();
            tem = ChatAllContainer.GetComponent<RectTransform>();
            if (chatAll.isOn && curTabActive != 0)
            {
                CloseTienIch();
                warningChat.SetActive(false);
                curTabActive = 0;
            }
            
            else if (friends.isOn && curTabActive!=1)
            {
                CloseTienIch();
                chatInputGroup.SetActive(false);
                warningChat.SetActive(false);
                curTabActive = 1;
                if (!isGetFriend)
                {
                    SFS.Instance.GetListFriend();
                    isGetFriend = true;
                }
            }else if (beInvite.isOn && curTabActive != 2)
            {
                CloseTienIch();
                warningInvite.SetActive(false);
                curTabActive = 2;
                if (!isGetFriend)
                {
                    SFS.Instance.GetListFriend();
                    isGetFriend = true;
                }
            }
            else if (bangHoi.isOn && curTabActive != 3)
            {
                CloseTienIch();
                warningBangHoi.SetActive(false);
                curTabActive = 3;
            }
            else if (GiaiDau.isOn && curTabActive != 4)
            {
              //  warningBangHoi.SetActive(false);
                curTabActive = 4;
                Content.GetComponent<BaseController>().OpenModuleDanhSachGiaiDau(this.gameObject);
            }
            else if (TienIch.isOn && curTabActive != 5)
            {
              //  warningBangHoi.SetActive(false);
                curTabActive = 5;
                Content.GetComponent<BaseController>().OpenModuleTienIch(this.gameObject);
            }
        }
    }

    private void CloseTienIch()
    {
        Transform panelTieniIch = gameObject.transform.Find("PanelTienIch");
        if (panelTieniIch != null)
        {
            panelTieniIch.gameObject.GetComponent<TienIchController>().OnCloseTienIch();
        }
    }
    
    public void SendMessageToServer()
    {
        if (MyInfo.MY_ID_VIP == 0)
        {
            AlertController.api.showAlert("Bạn cần đạt VIP 1 để kích hoạt tính năng này!");
            return;
        }

        InputField txt = textSend.GetComponent<InputField>();
       // string msg = txt.textComponent.text;
        string msg = txt.text;
        if (msg == "") return;
        int idUser_sfs = MyInfo.SFS_ID;
        string IdUser = MyInfo.ID;

        int tab = chatAll.isOn == true ? 1 : 0;
        if (bangHoi.isOn)
        {
            tab = 2;
        }
        Debug.LogError("txt.textComponent.text=====" + txt.text);
        //string msg = idUser.ToString() + "#" + textSend.GetComponent<Text>().text +"#" +tab+ "#" + Const.CHAT_CHAR_COMPONENT;
        
        string uName = MyInfo.NAME;
        if (tab == 1)
        {
            SFS.Instance.SendPublicChatRoom(MyInfo.MY_ID_VIP, msg, idUser_sfs, uName, tab, IdUser);
        }else if (tab == 2)
        {
            if (MyInfo.BangChatRoom == null)
            {
                AddMassgeBangAction("Bạn Chưa Gia Nhập Bang Hội!");
            }
            else
            {
                SFS.Instance.SendPublicChatBang(MyInfo.MY_ID_VIP, msg, idUser_sfs, uName, tab, IdUser);
            }
            
        }
        else
        {            
            if (curIdFriendChat == -1)
            {
                FriendItem friend = GetFriendById(curIdFriendChat);
                if (friend != null)
                {
                    friend.ShowOfflineMessage("Người dùng đang không trực tuyến!");
                }
            }
            else
            {
                SFS.Instance.SendPrivateMessageChatRoom(msg, idUser_sfs, uName, tab, curIdFriendChat, IdUser);
            }
        }

        txt.text = "";
    }
    public void ResponseChatFromServer(IDictionary param)
    {
        if (messageList.Count > maxMessage)
        {
            Destroy(messageList[0].textObject.gameObject);
            messageList.Remove(messageList[0]);
        }
        warningChat.SetActive(true);
        warningBtnChat.SetActive(true);
        string msg = (string)param["message"];
        Room room = (Room)param["room"];
        User sender = (User)param["sender"];
        SFSObject data = (SFSObject)param["data"];


        //Debug.LogError("msg------------ " + msg);

        Messager newMsg = new Messager();
        
        newMsg.text = msg;
        GameObject newText=null;       

            //if (data.GetInt("tab") == 1)
            //{
            newText = Instantiate(textObject, ChatAllContainer.transform);
            newMsg.textObject = newText.GetComponent<TextMeshProUGUI>();
        newMsg.BtnInfo = newText.GetComponent<Button>();

        string IdUser = data.GetUtfString("userId");
        newMsg.UserId = IdUser;

        string _name = data.GetUtfString("uName");
        newMsg.Name = _name;

        Debug.LogError("data_ IdUser  msg------------ " + IdUser);

        //string vipShow = data.GetInt("vip") > 0 ? data.GetInt("vip").ToString() : "";
        	if (data.GetInt("vip") == 0)
            {
                newMsg.textObject.text = data.GetUtfString("uName") + " : " + newMsg.text;
            }
            else
            {
                newMsg.textObject.text = data.GetUtfString("uName") + "[VIP" + data.GetInt("vip") + "]" + " : " + newMsg.text;
            }
            int vipChat = data.GetInt("vip");
            GoiVip gv = VIP_Controll.DicVipData[vipChat];
            string[] colorPoint;
            if (vipChat > 0)
            {
                colorPoint = gv.ColorChat.Split(',');
                newMsg.textObject.color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
            }
        //         if (data.GetInt("idSFS") == MyInfo.SFS_ID)
        //         {
        //             newMsg.textObject.color = Color.yellow;
        //         }
        //if (data.GetInt("vip") >= 10)
        //         {
        //             newMsg.textObject.color = Color.red;
        //         }
        //         else if(data.GetInt("vip") >= 7)
        //         {
        //             newMsg.textObject.color = new Color(255.0f/255.0f, 20.0f/255.0f, 147.0f/255.0f);
        //         }
        //         else if (data.GetInt("vip") >= 4)
        //         {
        //             newMsg.textObject.color = new Color(255.0f/255.0f, 165.0f/255.0f, 0);
        //         }
        for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        {
            string mod = DataHelper.DanhSachMod[i];
            string ten = data.GetUtfString("uName");
            if (ten == mod)
            {
                newMsg.textObject.color = Color.green;
            }
        }
        //add color cho user buy color
        foreach(KeyValuePair<string, string> entry in DataHelper.UserColorChat)
        {
            string ten = entry.Key;
            var valuekey = entry.Value.Split(';');
            var uid = valuekey[1];
            string color = valuekey[0];
            if (uid == data.GetUtfString("userId"))
            {
                string[] colorP = color.Split(',');
                newMsg.textObject.color = new Color(float.Parse(colorP[0]) / 255.0f, float.Parse(colorP[1]) / 255.0f, float.Parse(colorP[2]) / 255.0f);
                break;
            }
        }
        newMsg.Init();
        messageList.Add(newMsg);
        //}        
    }
    public void AddMassgeBangAction(string msg)
    {
        Messager newMsg = new Messager();
        newMsg.text = msg;
        GameObject newText = null;
        newText = Instantiate(textObject, ChatBangContainer.transform);
        newMsg.textObject = newText.GetComponent<TextMeshProUGUI>();
        newMsg.textObject.text = msg;
        newMsg.textObject.color = Color.yellow;

        newMsg.BtnInfo = newText.GetComponent<Button>();
        newMsg.Init();
        messageBangList.Add(newMsg);
    }
    public void ResponseChatBangFromServer(IDictionary param)
    {
        warningBtnChat.SetActive(true);
        warningBangHoi.SetActive(true);
        if (messageBangList.Count > maxMessage)
        {
            Destroy(messageBangList[0].textObject.gameObject);
            messageBangList.Remove(messageBangList[0]);
        }
        string msg = (string)param["message"];
        Room room = (Room)param["room"];
        User sender = (User)param["sender"];
        SFSObject data = (SFSObject)param["data"];        
        Messager newMsg = new Messager();
        newMsg.text = msg;
        GameObject newText = null;
        newText = Instantiate(textObject, ChatBangContainer.transform);

        string IdUser = data.GetUtfString("userId");
        newMsg.UserId = IdUser;

        string _name = data.GetUtfString("uName");
        newMsg.Name = _name;

        newMsg.textObject = newText.GetComponent<TextMeshProUGUI>();
        newMsg.BtnInfo = newText.GetComponent<Button>();
        if (data.GetInt("vip") == 0)
        {
            newMsg.textObject.text = data.GetUtfString("uName") + " : " + newMsg.text;
        }
        else
        {
            newMsg.textObject.text = data.GetUtfString("uName") + "[VIP" + data.GetInt("vip") + "]" + " : " + newMsg.text;
        }
        int vipChat = data.GetInt("vip");
        GoiVip gv = VIP_Controll.DicVipData[vipChat];
        string[] colorPoint;
        if (vipChat > 0)
        {
            colorPoint = gv.ColorChat.Split(',');
            newMsg.textObject.color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
        }

        //if (data.GetInt("idSFS") == MyInfo.SFS_ID)
        //{
        //    newMsg.textObject.color = Color.yellow;
        //}
        //if (data.GetInt("vip") >= 10)
        //{
        //    newMsg.textObject.color = Color.red;
        //}
        //else if (data.GetInt("vip") >= 7)
        //{
        //    newMsg.textObject.color = new Color(255.0f / 255.0f, 20.0f / 255.0f, 147.0f / 255.0f);
        //}
        //else if (data.GetInt("vip") >= 4)
        //{
        //    newMsg.textObject.color = new Color(255.0f / 255.0f, 165.0f / 255.0f, 0);
        //}
        for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        {
            string mod = DataHelper.DanhSachMod[i];
            string ten = data.GetUtfString("uName");
            if (ten == mod)
            {
                newMsg.textObject.color = Color.green;
            }
        }


        foreach (KeyValuePair<string, string> entry in DataHelper.UserColorChat)
        {
            string ten = entry.Key;
            var valuekey = entry.Value.Split(';');
            var uid = valuekey[1];
            string color = valuekey[0];
            if (uid == data.GetUtfString("userId"))
            {
                string[] colorP = color.Split(',');
                newMsg.textObject.color = new Color(float.Parse(colorP[0]) / 255.0f, float.Parse(colorP[1]) / 255.0f, float.Parse(colorP[2]) / 255.0f);
                break;
            }
        }
        newMsg.Init();    
        messageBangList.Add(newMsg);
    }
    private void OnChatFriendResponse(IDictionary param)
    {
        SFSObject data = (SFSObject)param["data"];
        int idReceive = data.GetInt("idReceive");
        int idSFSSender = data.GetInt("idSFS");
        FriendItem friend = GetFriendById(idReceive);
        if (friend != null)
        {
            friend.AddChatFromServer(param);
        }
        else
        {
            friend = GetFriendById(idSFSSender);
            if (friend != null)
            {
                friend.AddChatFromServer(param);
            }
        }
        
    }
    private FriendItem GetFriendById(int id)
    {
        foreach(FriendItem friend in FriendItemList)
        {
            if(friend.sfsId == id)
            {
                return friend;
            }
        }
        return null;
    }
    public void OnExitChatRoom(string uName)
    {
        foreach (FriendItem friend in FriendItemList)
        {
            if (friend.uId == uName)
            {
                friend.ChangeStatusOff();
                break;
            }
        }
    }
    public void OnEnterChatRoom(Hashtable data)
    {
        //string uName = data["uN"].ToString();
        int sfsid = (int)data["sfsid"];
        string uid = data["uid"].ToString();
        string avatar = data["avatar"].ToString();
        int avatarborder = (int)data["avatarborder"];
        
        foreach (FriendItem friend in FriendItemList)
        {
            if (friend.uId == uid)
            {
                friend.ChangeStatusON(uid, sfsid, avatar, avatarborder);
                break;
            }
        }
    }
    public void OnChatResponse(GamePacket param)
    {
        Debug.Log("OnChatResponse rsp - " + param.ToString());
        Debug.Log("OnChatResponse cmd - " + param.cmd);

        switch (param.cmd)
        {

            case CommandKey.JOIN_CHAT_ROOM:
                JoinChatComplete(param);
                break;
            case CommandKey.GET_LIST_FRIEND:
                GetListFriend(param);
                break;
            case CommandKey.ADD_FRIEND_REQUEST://gửi về cho user được mời
                AddFriendBeInvite(param);
                break;
            case CommandKey.ADD_FRIEND_RESULT://gửi về cho user đã gửi lời mời
                AddFriendRes(param);
                break;
            case CommandKey.ADD_FRIEND_CONFIRM:
                AddFriendConfirm(param);
                break;
            case CommandKey.ADD_FRIEND_SUCCESS:
                CheckGetListFriendAgain();
                break;
            case CommandKey.ADD_FRIEND_IGNORE:
                //uid: id user ko đồng ý kết bạn
                //uN: username user ko đồng ý kết ban
                break;
            case CommandKey.UNFRIEND_FRIEND_REQUEST:
                UnFriendRes(param);
                break;

            case CommandKey.UPGRADE_DEPUTY:
                UpGradeDeputyRes(param);
                break;
            case CommandKey.DOWN_GRADE_DEPUTY:
                DownGradeDeputyRes(param);
                break;
            case CommandKey.KICK_MEMBER_BANG_HOI:
                KickMemberRes(param);
                break;
            case CommandKey.REQUEST_LEAVE_CLAN:
                LeaveClanRes(param);
                break;
            case CommandKey.RECEIVE_HELP_BANGHOI:
                RecieveHelpRes(param);
                break;
            case CommandKey.HELP_MEMBER_BANGHOI:
                HelpMemberRes(param);
                break;
            case CommandKey.CONTRIBUTE_CLAN:
                ContributeClanRes(param);
                break;
            case "getBackUpChat":
                GetBackUpChat(param);
                break;
            case "getBackUpClanChat":
                GetBackUpClanChat(param);
                break;
            case CommandKey.REFRESH:
                UpdateChipUser(param);
                break;
            case "sendClanDuel":
                SendClanDueRsp(param);
                break;
            case "receiveClanDue":
                ReceiveClanDueRsp(param);
                break;
           case "getClanWarTime"://thời gian còn lại của của bang chiến 2 bang phải gọi lên mới lấy được
                GetClanWarTime(param);
                break;
            case "startClanWarMatch"://thời gian 2 user chiến bang lúc start game và băt đầu đếm ngược server tự trả về
                StartClanWarMatch(param);
                break;
            case "endClanWarMatch":
                EndClanWarMatch(param);
                break;
            case "clanWarMatchPause":
                ClanWarMatchPause(param);
                break;
            case "cancelClanDueCauseNotFight":
                CancelClanDueCauseNotFight(param);
                break;
            case "matchPause":
                MatchPause(param);
                break;
        }
    }

    private void MatchPause(GamePacket param)
    {        
        AlertController.api.PauseCountDownBangChien();
    }

    private void CancelClanDueCauseNotFight(GamePacket param)
    {
        if (param.ContainKey("noti"))
        {
            AlertController.api.ShowNotiAllGame(param.GetString("noti"));
        }
    }

    private void ClanWarMatchPause(GamePacket param)
    {
        if(MyInfo.NAME== param.GetString("exitusername"))
        {
            AlertController.api.ShowCountDownBangChien(Int32.Parse(param.GetLong("time").ToString()), "Thời Gian Vào Lại");
        }
    }

    private void EndClanWarMatch(GamePacket param)
    {
        if (param.ContainKey("noti"))
        {
            AlertController.api.ShowNotiAllGame(param.GetString("noti"));
            AlertController.api.HideCountDownBangChien();
        }
    }

    private void StartClanWarMatch(GamePacket param)
    {
        AlertController.api.ShowCountDownBangChien(param.GetInt("time"), "Bang Chiến Kết Thúc Sau");
    }

    private void GetClanWarTime(GamePacket param)
    {
        AlertController.api.ShowCountDownBangChien(param.GetInt("time"), "Bang Chiến Kết Thúc Sau");
        AlertController.api.PauseCountDownBangChien();        
    }

    private void SendClanDueRsp(GamePacket param)
    {
        if (param.ContainKey("noti"))
        {
            AlertController.api.ShowNotiAllGame(param.GetString("noti"));
        }        
    }
    private void ReceiveClanDueRsp(GamePacket param)
    {
        if (param.ContainKey("noti"))
        {
            AlertController.api.ShowNotiAllGame(param.GetString("noti"));
        }
    }

    private void GetBackUpClanChat(GamePacket param)
    {
        ISFSArray lstMsg = param.GetSFSArray("message");
        foreach (ISFSObject message in lstMsg)
        {
            string msg = message.GetUtfString("message");
            int vip = message.GetInt("vip");
            string uId = message.GetUtfString("userId");
            string uName = message.GetUtfString("username");
            AddMessageBangInit(msg, vip, uName, uId);
        }
    }
    private void GetBackUpChat(GamePacket param)
    {
        ISFSArray lstMsg = param.GetSFSArray("message");
        foreach(ISFSObject message in lstMsg)
        {
            string msg = message.GetUtfString("message");
            int vip = message.GetInt("vip");
            string uId = message.GetUtfString("userId");
            string uName = message.GetUtfString("username");
            AddMessageInit(msg, vip, uName, uId);
        }
    }
    void AddMessageBangInit(string msg, int vip, string uName, string uId)
    {
        if (messageBangList.Count > maxMessage)
        {
            Destroy(messageBangList[0].textObject.gameObject);
            messageBangList.Remove(messageBangList[0]);
        }


        Debug.LogError("id user trong bang   " + uId);

        Messager newMsg = new Messager();
        newMsg.text = msg;
        GameObject newText = null;
        newText = Instantiate(textObject, ChatBangContainer.transform);
        newMsg.textObject = newText.GetComponent<TextMeshProUGUI>();
        newMsg.Name = uName;
        newMsg.UserId = uId;
        newMsg.BtnInfo = newText.GetComponent<Button>();





        if (vip == 0)
        {
            newMsg.textObject.text = uName + " : " + newMsg.text;
        }
        else
        {
            newMsg.textObject.text = uName + "[VIP" + vip + "]" + " : " + newMsg.text;
        }
        int vipChat = vip;
        GoiVip gv = VIP_Controll.DicVipData[vipChat];
        string[] colorPoint;
        if (vipChat > 0)
        {
            colorPoint = gv.ColorChat.Split(',');
            newMsg.textObject.color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
        }

        for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        {
            string mod = DataHelper.DanhSachMod[i];
            string ten = uName;
            if (ten == mod)
            {
                newMsg.textObject.color = Color.green;
            }
        }


        foreach (KeyValuePair<string, string> entry in DataHelper.UserColorChat)
        {
            string ten = entry.Key;
            var valuekey = entry.Value.Split(';');
            var uid = valuekey[1];
            string color = valuekey[0];
            if (uid == uId)
            {
                string[] colorP = color.Split(',');
                newMsg.textObject.color = new Color(float.Parse(colorP[0]) / 255.0f, float.Parse(colorP[1]) / 255.0f, float.Parse(colorP[2]) / 255.0f);
                break;
            }
        }
        newMsg.Init();
        messageBangList.Add(newMsg);
    }


    #region INFO_ENEMY

    string IDUSER = "";

    public void ShowInfo(string userId, string name)
    {
        if (userId != MyInfo.ID)
        {
            if (SceneManager.GetActiveScene().name.Equals(GameScene.HomeSceneV2.ToString()) || SceneManager.GetActiveScene().name.Equals(GameScene.WaitingRoom.ToString()))
            {
                Debug.LogError("LAY DDC IDUSER ---------------- " + userId + " -----name-----  " + name);

                if (userId != "" && userId != null)
                {
                    TenNguoiNhan = name;
                    IDUSER = userId;
                    API.Instance.RequestGet_Info_ByUserId(userId, RspLayThongTin);
                }

            }

        }



    }

    void RspLayThongTin(string _json)
    {
        JSONNode node = JSONNode.Parse(_json);
        Debug.LogError("data get info by userid  " + _json);
        string VIP = node["vip"].Value;
        string IDBANG = node["user_clan"]["$id"].Value;
        int IDGIOITINH = int.Parse(node["gender"].Value);
        string PARTNER = node["partner"].Value;
        _PopupInfoEnemy.InitInChat(IDUSER, TenNguoiNhan, VIP, IDBANG, IDGIOITINH, PARTNER);
    }




    [SerializeField]
    GameObject PopupTangChip;


    [SerializeField]
    Text chipReceive;


    [SerializeField]
    TMPro.TextMeshProUGUI userReceive;

    long chipTang;
    string TenNguoiNhan;

    [SerializeField]
    Text txtPercent, txtHanMuc;
    long minChip = 10000000;
    long limitChip = 0;
    List<long> lstChipLimit;
    int transfer_fee;
    long min_transfer;


    [SerializeField]
    TangChipTrongChat tangchiptrongchat;




    public void XacNhanTangChip()
    {
        tangchiptrongchat.ChangeChipToUser(TenNguoiNhan, chipReceive.text, chipTang, minChip, limitChip, min_transfer);
    }




    public void TangChipClick()
    {
        chipTang = 1000000;
        userReceive.text = "Gửi tặng\n" + TenNguoiNhan;
        chipReceive.text = Utilities.GetStringMoneyByLongBigSmall(chipTang);
        API.Instance.RequestInitChangeChip(InitChangeChipComplete);
    }

    public void CloseTangChip()
    {
        TenNguoiNhan = "";
        limitChip = 0;
        minChip = 10000000;
        chipTang = 1000000;
        userReceive.text = "";
        chipReceive.text = "";
        txtPercent.text = "";
        txtHanMuc.text = "";
        transfer_fee = 0;
        min_transfer = 0;
        PopupTangChip.SetActive(false);
    }


    public void AddChipTang(int val = 0)
    {
        chipTang += val;
        chipReceive.text = Utilities.GetStringMoneyByLongBigSmall(chipTang);
    }

    void InitChangeChipComplete(string json)
    {
        JSONNode node = JSONNode.Parse(json);
        minChip = long.Parse(node["min_chip"].Value);
        transfer_fee = int.Parse(node["transfer_fee"]);
        min_transfer = long.Parse(node["min_transfer"]);
        txtPercent.text = transfer_fee + "%";
   
        HanMucChuyenTheoVip();
    }
    void HanMucChuyenTheoVip()
    {
        int vip = MyInfo.MY_ID_VIP;
        GoiVip gv;
        //Description.text = "";
        //for (int i = 0; i < VIP_Controll.DicVipData.Count; i++)
        //{
        //    gv = VIP_Controll.DicVipData[i];
        //    if (gv.HanMucTangChip != "")
        //    {
        //        Description.text += "Vip" + i + " : " + Utilities.GetStringMoneyByLong(long.Parse(gv.HanMucTangChip)) + "/lần \n";
        //    }
        //}
        gv = VIP_Controll.DicVipData[MyInfo.MY_ID_VIP];
        if (gv.HanMucTangChip == "")
        {
            limitChip = 0;
        }
        else
        {
            limitChip = long.Parse(gv.HanMucTangChip);
        }

        txtHanMuc.text = Utilities.GetStringMoneyByLong(limitChip);
        PopupTangChip.SetActive(true);
    }



    #endregion







    void AddMessageInit(string msg, int vip, string uName, string uId)
    {
        Messager newMsg = new Messager();

        newMsg.text = msg;
        GameObject newText = null;

        //if (data.GetInt("tab") == 1)

        // THRESH

        Debug.LogError("uId chat --- " + uId);
        newMsg.UserId = uId;
        newMsg.Name = uName;
        //{
        newText = Instantiate(textObject, ChatAllContainer.transform);
        newMsg.textObject = newText.GetComponent<TextMeshProUGUI>();

        newMsg.BtnInfo = newText.GetComponent<Button>();

        

        //string vipShow = data.GetInt("vip") > 0 ? data.GetInt("vip").ToString() : "";
        if (vip == 0)
        {
            newMsg.textObject.text = uName + " : " + newMsg.text;
        }
        else
        {
            newMsg.textObject.text = uName + "[VIP" + vip + "]" + " : " + newMsg.text;
        }
        int vipChat = vip;
        GoiVip gv = VIP_Controll.DicVipData[vipChat];
        string[] colorPoint;
        if (vipChat > 0)
        {
            colorPoint = gv.ColorChat.Split(',');
            newMsg.textObject.color = new Color(float.Parse(colorPoint[0]) / 255.0f, float.Parse(colorPoint[1]) / 255.0f, float.Parse(colorPoint[2]) / 255.0f);
        }
       
        for (int i = 0; i < DataHelper.DanhSachMod.Count; i++)
        {
            string mod = DataHelper.DanhSachMod[i];
            string ten = uName;
            if (ten == mod)
            {
                newMsg.textObject.color = Color.green;
            }
        }
        //add color cho user buy color
        foreach (KeyValuePair<string, string> entry in DataHelper.UserColorChat)
        {
            string ten = entry.Key;
            var valuekey = entry.Value.Split(';');
            var uid = valuekey[1];
            string color = valuekey[0];
            if (uid == uId)
            {
                string[] colorP = color.Split(',');
                newMsg.textObject.color = new Color(float.Parse(colorP[0]) / 255.0f, float.Parse(colorP[1]) / 255.0f, float.Parse(colorP[2]) / 255.0f);
                break;
            }
        }
        newMsg.Init();
        messageList.Add(newMsg);
    }
    private void ContributeClanRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            long chip = param.GetLong("chip");
            string membername = param.GetString("membername");
            AddMassgeBangAction(membername + " Đã Đóng Góp " + Utilities.GetStringMoneyByLongBigSmall(chip) + " Vào Bang");
        }
    }
    private void HelpMemberRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            long chip = param.GetLong("receive_chip");
            string membername = param.GetString("membername");
            AddMassgeBangAction(membername + " Đã Được Cứu Trợ " + Utilities.GetStringMoneyByLong(chip) + " Từ Bang Chủ");
        }        

    }
    private void RecieveHelpRes(GamePacket param)
    {       
        long receive_chip = param.GetLong("receive_chip");
        AlertController.api.showAlert("Bạn Được Cứu Trợ " + Utilities.GetStringMoneyByLong(receive_chip) + " Từ Bang Chủ!");               
    }
    private void LeaveClanRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberid = param.GetString("memberid");
            string membername = param.GetString("membername");
            AddMassgeBangAction(membername + " Đã Rời Khỏi Bang!");
        }
    }
    private void KickMemberRes(GamePacket param)
    {
        int status = param.GetInt("status");        
        if (status == 1)
        {
            string memberid = param.GetString("memberid");
            string membername = param.GetString("membername");
            if (memberid == MyInfo.ID)
            {
                AlertController.api.showAlert("Bạn Đã Bị Kick Ra Khỏi Bang!");
            }
            AddMassgeBangAction(membername + " Đã Bị Kick Ra Khỏi Bang!");
        }
        
    }
    private void UpGradeDeputyRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberid = param.GetString("memberid");
            string membername = param.GetString("membername");
            if (memberid == MyInfo.ID)
            {
                AlertController.api.showAlert("Bạn Đã Được Thăng Cấp Thành Bang Phó!");
            }
            AddMassgeBangAction(membername + " Đã Được Thăng Cấp Thành Bang Phó!");
        }
        
    }
    private void DownGradeDeputyRes(GamePacket param)
    {
        int status = param.GetInt("status");
        if (status == 1)
        {
            string memberid = param.GetString("memberid");
            string membername = param.GetString("membername");
            if (memberid == MyInfo.ID)
            {
                AlertController.api.showAlert("Bạn Bị Giáng Cấp Từ Bang Chủ!");
            }
            AddMassgeBangAction(membername + " Đã Bị Giáng Cấp Từ Bang Chủ!");
        }
        
    }

    private void UnFriendRes(GamePacket param)
    {
        Debug.Log("AddFriendRes rsp - ");
        //user_id_1,user_name_1,user_id_2,user_name_2
        string user_id_1, user_name_1, user_id_2, user_name_2;

        user_id_1 = param.GetString("user_id_1");
        user_name_1 = param.GetString("user_name_1");
        user_id_2 = param.GetString("user_id_2");
        user_name_2 = param.GetString("user_name_2");

        if (user_id_1 == MyInfo.ID)
        {
            foreach (FriendItem friend in FriendItemList)
            {
                if (friend.uId == user_id_2)
                {
                    friend.RemoveFriend();
                    FriendItemList.Remove(friend);
                    Destroy(friend.gameObject);
                    break;
                }
            }
        }
        if(user_id_2 == MyInfo.ID)
        {
            foreach (FriendItem friend in FriendItemList)
            {
                if (friend.uId == user_id_1)
                {
                    friend.RemoveFriend();
                    FriendItemList.Remove(friend);
                    Destroy(friend.gameObject);
                    break;
                }
            }
        }
    }

    private void CheckGetListFriendAgain()
    {
        if (friends.isOn)
        {
            SFS.Instance.GetListFriend();
            isGetFriend = true;
        }else
        {
            isGetFriend = false;
        }
    }
    private void AddFriendConfirm(GamePacket param)
    {
        CheckGetListFriendAgain();
        warningFriend.SetActive(true);
        warningBtnChat.SetActive(true);
    }
    private void AddFriendRes(GamePacket param)//gửi về cho user đã gửi lời mời
    {
        Debug.Log("AddFriendRes rsp - ");
        int result = param.GetInt("grs");
        if (result == 1)
        {
            string uName = param.GetString("uN");
        }else
        {
            Debug.Log("User Này Không Tồn Tại");
        }
    }

    private void AddFriendBeInvite(GamePacket param)//gửi về cho user được mời
    {
        //{ "avatarborder":-1,"uid":"5cef876b8ead77e5338b4583","uN":"tfczzz","grs":1,"avatar":"12.png"}
        warningInvite.SetActive(true);
        warningBtnChat.SetActive(true);
        Debug.Log("AddFriendBeInvite rsp - ");        
        string uid = param.GetString("uid");
        string uName = param.GetString("uN");
        string avatar = param.GetString("avatar");
        int avatarborder = param.GetInt("avatarborder");

        GameObject friendInvite = Instantiate(InviteFriend, InviteFriendContainer.transform);
        friendInvite.GetComponent<FriendInvite>().Init(uid, uName, avatar, avatarborder);
    }

    private void GetListFriend(GamePacket param)
    {
        curIdFriendChat = -2;
        foreach (Transform child in FriendsContainer.transform)
        {
            Destroy(child.gameObject);
        }

        //SFS.Instance.RequestAddFriend(MyInfo.ID);
        Debug.Log("GetListFriend rsp - ");
        ISFSObject data = param.GetSFSObject("dt");
        ISFSArray lstoff = data.GetSFSArray("list_off");
        ISFSArray lston = data.GetSFSArray("list_onl");
        GameObject friendItem=null;
        string avatar = "";
        int avatarborder=-1;
        int sfsId = -1;
        if (lston.Count > 0)
        {
            foreach (SFSObject item in lston)
            {
                avatar = "";
                if (item.ContainsKey("avatar"))
                {
                    avatar = item.GetUtfString("avatar");
                }
                avatarborder = -1;
                if (item.ContainsKey("avatarborder"))
                {
                    avatarborder = item.GetInt("avatarborder");
                }
                sfsId = item.GetInt("sfsid");
                friendItem = Instantiate(FriendItem, FriendsContainer.transform);
                friendItem.GetComponent<FriendItem>().Init(FriendsChatContainer,textObject, OnClickFriendItem, item.GetUtfString("user_id"), item.GetUtfString("user_name"), avatar, avatarborder, true, sfsId);
                FriendItemList.Add(friendItem.GetComponent<FriendItem>());
            }
        }
        avatar = "";
        avatarborder = -1;
        sfsId = -1;
        if (lstoff.Count > 0)
        {            
            foreach(SFSObject item in lstoff)
            {
                avatar = "";
                if (item.ContainsKey("avatar"))
                {
                    avatar = item.GetUtfString("avatar");
                }
                avatarborder = -1;
                if (item.ContainsKey("avatarborder"))
                {
                    avatarborder = item.GetInt("avatarborder");
                }
                friendItem = Instantiate(FriendItem, FriendsContainer.transform);                
                friendItem.GetComponent<FriendItem>().Init(FriendsChatContainer, textObject, OnClickFriendItem, item.GetUtfString("user_id"), item.GetUtfString("user_name"), avatar, avatarborder, false,sfsId);
                FriendItemList.Add(friendItem.GetComponent<FriendItem>());
            }
            
        }

        foreach (Transform child in InviteFriendContainer.transform)
        {
            Destroy(child.gameObject);
        }
        ISFSArray lstRequestAddFriend = data.GetSFSArray("request_add");

        foreach (SFSObject item in lstRequestAddFriend)
        {
            string uid = item.GetUtfString("user_id");
            string uName = item.GetUtfString("user_name");
            avatar = item.GetUtfString("avatar");
            avatarborder = item.GetInt("avatar_border");
            GameObject friendInvite = Instantiate(InviteFriend, InviteFriendContainer.transform);
            friendInvite.GetComponent<FriendInvite>().Init(uid, uName, avatar, avatarborder);
        }        
    }
    int curIdFriendChat=-2;
    private void OnClickFriendItem(GameObject obj)
    {
        FriendItem item = obj.GetComponent<FriendItem>();
        curIdFriendChat = item.sfsId;
        item.ShowMessage();
        chatInputGroup.SetActive(true);
    }
    private void JoinChatComplete(GamePacket param)
    {
        
    }
}

[System.Serializable]
public class Messager
{
    public string text;
    public TextMeshProUGUI textObject;
    public string UserId = "";
    public Button BtnInfo;
    public string Name = "";

    ChatController Chat;

    public void Init()
    {
        GameObject obj = GameObject.Find("ChatComponent");
        Chat = obj.GetComponent<ChatController>();
         

        BtnInfo.onClick.RemoveAllListeners();
        BtnInfo.onClick.AddListener(BtnInfoClick);
    }

    void BtnInfoClick()
    {
        Chat.ShowInfo(UserId, Name);
    }
}
