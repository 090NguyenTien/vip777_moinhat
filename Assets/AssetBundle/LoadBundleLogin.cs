﻿using BaseCallBack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class LoadBundleLogin : MonoBehaviour
{
    public static LoadBundleLogin api { get; private set; }
    string BundleURL = "/files/assetbundle/avoid_item/android";
    public static string AssetName = "kingbundle";//"myassetBundle";
    int versionBundle = 5;//đổi từng asset bundle trong assetbundle đã lưu về máy.
    int versionLocalLoaded = 5;//đổi version khi muốn thay đổi toàn bộ asset bundle mới
    int downloaded = 0;
    static AssetBundle bundle;

    void Awake()
    {
        if (api == null)
        {
            DontDestroyOnLoad(this);
            api = this;
            //LoadingManager.Instance.ENABLE = true;
            LoadBundleFromServer();
        }
    }
    void Start()
    {
        versionBundle = MyInfo.BUNDLE_VERSION;
        versionLocalLoaded = MyInfo.BUNDLE_VERSION;        
    }
    public void LoadBundleFromServer()
    {
        BundleURL = API.Instance.DOMAIN + "/" + BundleURL + "/" + AssetName;
        StartCoroutine(DownloadAndCache());
    }
    private void LoadBundleComplete()
    {
        //LoadingManager.Instance.ENABLE = false;
        Debug.Log("LOAD BUNDLE COMPLETE======");
    }
    IEnumerator DownloadAndCache()
    {
        if (PlayerPrefs.GetInt("AsetLoaded" + versionLocalLoaded, 0) == 0)
        {
            Debug.Log("Asset has NOT been downloaded. Downloading....");
            WWW www;
            using (www = WWW.LoadFromCacheOrDownload(BundleURL, versionBundle))
            {
                yield return www;
                if (www.error != null)
                    throw new Exception("WWW download had an error:" + www.error);
                if (www.error == null)
                {
                    bundle = www.assetBundle;
                    bundle.Unload(false);
                    //www.Dispose();
                }
            }
            if (Caching.ready == true)
            {
              //  bundle = www.assetBundle;
                StartCoroutine(downloadAsset());
                downloaded = 1;
                //Save that we have down loaded Asset 
                PlayerPrefs.SetInt("AsetLoaded" + versionLocalLoaded, 1);
                yield return InitializeLevelAsync();

            }
        }

        else
        {            
            Debug.Log("Asset already loaded. Can't download it again! Loading it instead");            
            yield return InitializeLevelAsync();
        }
    }


    private object InitializeLevelAsync()
    {
        LoadBundleComplete();
        throw new NotImplementedException();
    }




    IEnumerator downloadAsset()
    {

        UnityWebRequest www = UnityWebRequest.Get(BundleURL);
        DownloadHandler handle = www.downloadHandler;
        UnityEngine.Debug.Log("BundleURL: " + BundleURL);
        //Send Request and wait
        yield return www.Send();

        if (www.isNetworkError)
        {

            UnityEngine.Debug.Log("Error while Downloading Data: " + www.error);
        }
        else
        {
            UnityEngine.Debug.Log("Success");

            //handle.data

            //Construct path to save it
            string dataFileName = "kingbundle";
            string tempPath = Path.Combine(Application.persistentDataPath, "");
            tempPath = Path.Combine(tempPath, dataFileName);

            //Save
            save(handle.data, tempPath);
        }
    }

    void save(byte[] data, string path)
    {
        //Create the Directory if it does not exist
        if (!Directory.Exists(Path.GetDirectoryName(path)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(path));
        }

        try
        {
            File.WriteAllBytes(path, data);
            Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
        }
        catch (Exception e)
        {
            Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
            Debug.LogWarning("Error: " + e.Message);
        }
    }

    public GameObject GetAssetBundleByName(string bundleName)
    {
        Debug.Log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + Application.persistentDataPath);
        //AssetBundle aaa = AssetBundleUltility.ReadAssetBunldStreaming(AssetName);
        bundle = AssetBundleUltility.ReadAssetBunld(AssetName);
        //object[] aa = bbb.LoadAllAssets();
        GameObject obj = bundle.LoadAsset(bundleName) as GameObject;
        //DemoThietPhien demoClass = obj.GetComponent<DemoThietPhien>();
        //Instantiate(obj, container.transform);        
        return obj;
    }
    public void UnloadBundle()
    {
        if (bundle)
        {
            Debug.Log("Unload Bundle ======");
            bundle.Unload(false);
        }
    }
}
