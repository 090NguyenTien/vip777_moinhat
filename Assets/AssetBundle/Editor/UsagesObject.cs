﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class UsagesObject : EditorWindow
{
    public GameObject[] objsFound;
    private static bool show = false;
    void OnGUI()
    {
        if (show == true)
        {
            show = false;
            objsFound = getList(Selection.gameObjects[0]).ToArray();
        }
        GUILayout.Label("Select layer", EditorStyles.boldLabel);
        

        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);

        SerializedProperty objs = so.FindProperty("objsFound");
        if (objs != null)
        {
            EditorGUILayout.PropertyField(objs, true); // True means show children

        }
        
        so.ApplyModifiedProperties(); // Remember to apply modified properties


        GUILayout.Space(20);
        if (GUILayout.Button("OK"))
        {
            Close();
        }
    }






    [MenuItem("GameObject/FindUsages", false, 0)]
    public static void findUsages()
    {
        show = true;
        UsagesObject window = (UsagesObject)GetWindow(typeof(UsagesObject));
        window.Show();
    }

    private static List<GameObject> getList(GameObject objToCompare)
    {
        List<GameObject> lst = new List<GameObject>();


        Debug.Log("finding...");

        //GameObject[] objs = Object.FindObjectsOfType<GameObject>();
        GameObject[] objs = Resources.FindObjectsOfTypeAll<GameObject>();

        foreach (var item in objs)
        {
            //if (item.transform.getFullPath() != "MainGameContainer/Maingame(Clone)/MainCanvasController/ModulController/CampaignNew/Popups/PopupGroupDetail/Box/TabBoxInfoBoss/Mask/BoxInfoBossCurrent")
            //{
            //    continue;
            //}
            //Debug.Log(item.transform.getFullPath());
            var components = item.GetComponents<Component>();

            foreach (var c in components)
            {

                if (c != null)
                {

                    if (c.GetType().FullName.Contains("UnityEngine.") == false)
                    {

                        SerializedObject so = new SerializedObject(c);
                        var sp = so.GetIterator(); //tung field
                        
                        while (sp.NextVisible(true) )
                        {
                            if (sp.editable == true /*&&sp.isExpanded == false */&& sp.propertyType == SerializedPropertyType.ObjectReference)
                            {
                                try
                                {
                                    if (sp.objectReferenceValue == null)
                                    {
                                        continue;
                                    }
                                    Component com = (sp.objectReferenceValue as Component);
                                    if (com != null)
                                    {
                                        if (com.gameObject == objToCompare)
                                        {
                                            lst.Add(c.gameObject);
                                            Debug.LogWarning("add");
                                            break;
                                        }

                                    }
                                }
                                catch (System.Exception) { }
                            }
                            




                        }

                    }

                }
            }
        }
        
        return lst;

    }
}
