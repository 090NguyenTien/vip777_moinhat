﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KetQuaType : MonoBehaviour {

    [SerializeField]
    int Type = 0;

    public Text Type_txt, Number_1_Txt, Number_2_Txt, Number_3_Txt, Number_4_Txt, Number_5_Txt, Number_6_Txt;

    private DataItemResult item;

    public void Init(DataItemResult item)
    {
        this.item = item;
    }



    public void Show(string type, string num_1, string num_2 = "", string num_3 = "", string num_4 = "", string num_5 = "", string num_6 = "")
    {
        Type_txt.text = type;
        Number_1_Txt.text = num_1;

        if (Number_2_Txt != null)
        {
            Number_2_Txt.text = num_2;
        }

        if (Number_3_Txt != null)
        {
            Number_3_Txt.text = num_3;
        }

        if (Number_4_Txt != null)
        {
            Number_4_Txt.text = num_4;
        }

        if (Number_5_Txt != null)
        {
            Number_5_Txt.text = num_5;
        }

        if (Number_6_Txt != null)
        {
            Number_6_Txt.text = num_6;
        }


    }


}
