﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sfs2X.Entities.Data;
using System.Collections.Generic;

public class BetLotteryManager : MonoBehaviour {
  
    [SerializeField]
    Button btnDanhLo;
    [SerializeField]
    Button btn3Cang;
    [SerializeField]
    Button btnDanhDe;
    [SerializeField]
    Button btnDauDuoi;
    [SerializeField]
    Button btnXacNhanDat;
    [SerializeField]
    Button btnXacNhanHuy;
    [SerializeField]
    Button btnType1;
    [SerializeField]
    Button btnType2;
    [SerializeField]
    Button btnXacNhan;
    [SerializeField]
    Text txtHuongDan;
    [SerializeField]
    Text txtLoaiDe;
    [SerializeField]
    Text txtSoDanh;
    [SerializeField]
    Text txtTongTien;
    [SerializeField]
    Text txtTienCon;
    [SerializeField]
    Text txtTienThang;
    [SerializeField]
    Dropdown DrNum_1;
    [SerializeField]
    Dropdown DrNum_2;
    [SerializeField]
    Dropdown DrNum_3;
    [SerializeField]
    Scrollbar ScrMoney;
    [SerializeField]
    GameObject PanelXacNhan;
    [SerializeField]
    Text txtLoaiDeXacNhan;
    [SerializeField]
    Text txtSoDanhXacNhan;
    [SerializeField]
    Text txtTongTienXacNhan;
    [SerializeField]
    Text txtConLaiXacNhan;
    [SerializeField]
    GameObject MidPanelLottery;
    [SerializeField]
    Text Date;

    public PopupLotteryManager LotteryManager;
    private int Pase;
    private int Type;
    private string strType;
    private Text txtBtnType1;
    private Text txtBtnType2;
    private string strBetMoney;
    private string strBetMoneyForSendRequest;
    public long BetMoney;
    private string NotePase_0;
    private string NotePase_1;
    private string NotePase_2;
    private string NotePase_3;
    private string NotePase_4;
    private string NotePase_5;
    private string NotePase_6;
    private int Num_1;
    private int Num_2;
    private int Num_3;
    private int intBetNumber;
    private long MaxMoney = 0;
    long UserMoney;
    float SoTienConLai;
    bool IsInited = false;
    public void showPanelBet()
    {
        if (LotteryManager.EndBetTime == false)
        {           
            Init();
        }
        else
        {
            AlertController.api.showAlert("Đã hết thời gian đặt cược vui lòng quay lại vào ngày mai");
            Debug.Log("LotteryManager.EndBetTime = " + LotteryManager.EndBetTime);
        }     
    }

    public void Init()
    {
        MaxMoney = MyInfo.CHIP;
        PopupLotteryManager.Panel = "DatCuoc";
        PopupLotteryManager.OnDatCuoc = true;       
        gameObject.SetActive(true);
        LotteryManager.BtnBet.gameObject.SetActive(false);
        LotteryManager.OffMID();
        NotePase_0 = "Hướng dẫn: Đánh 2 chữ số cuối trong lô 27 giải ( chỉ cần thanh toán cho 22 giải ). Thắng gấp 78 lần, nếu số đó về N lần thì tính kết quả x N lần. Ví dụ: đánh lô 79 - 1 con 1k, Tổng thanh toán: 1k x 22 = 22k. Nếu trong lô có 2 chữ số cuối là 79 thì Tiền thắng: 1k x 78 = 78k, nếu có N lần 2 chữ số cuối là 79 thì Tiền thắng là: 1k x 78 x N";
        NotePase_1 = "Hướng dẫn: Đánh 3 chữ số cuối trong lô 23 giải. Thắng gấp 770 lần, nếu số đó về N lần thì tính kết quả x N lần. Ví dụ: đánh lô 789 - 1 con 1k, Tổng thanh toán: 1k x 23 = 23k. Nếu trong lô có 3 chữ số cuối là 789 thì Tiền thắng: 1k x 770 = 770k, nếu có N lần 3 chữ số cuối là 789 thì Tiền thắng là: 1k x 770 x N";
        NotePase_2 = "Hướng dẫn: Đánh 3 chữ số cuối của giải ĐB. Thắng gấp 700 lần. Ví dụ: đánh 1k cho số 879, Tổng thanh toán: 1k. Nếu giải ĐB là xx879 thì Tiền thắng: 1k x 700 = 700K";
        NotePase_3 = "Hướng dẫn: Đánh lô giải 7 ( có 4 giải, thanh toán đủ ). Thắng gấp 82 lần. Ví dụ : đánh 1k cho số 79, Tổng thanh toán: 1k x 4 =4k. Nếu trong lô giải 7 có 1 số 79 thì Tiền thắng: 1k x 82 = 82k.";
        NotePase_4 = "Hướng dẫn: Đánh 2 chữ số cuối trong giải ĐB. Thắng gấp 82 lần. Ví dụ: đánh 1k cho số 79. Tổng thanh toán: 1k. Nếu giải ĐB là xxx79 thì Tiền thắng: 1k x 82 = 82k";
        NotePase_5 = "Hướng dẫn: Đánh 1 chữ số ở hàng chục của giải ĐB. Thắng gấp 8.7 lần. Ví dụ: đánh 1k cho số 7. Tổng thanh toán: 1K. Nếu giải ĐB là xxx7x thì Tiền thắng: 1k x 8.7 = 8.7k";
        NotePase_6 = "Hướng dẫn: Đánh 1 chữ số cuối của giải ĐB. Thắng gấp 8.7 lần. Ví dụ: đánh 1k cho số 7. Tổng thanh toán: 1K. Nếu giải ĐB là xxxx7 thì Tiền thắng: 1k x 8.7 = 8.7k";
        Pase = 0;
        Type = 0;

         
        Date.text = "<color=#0DF648FF>" + System.DateTime.Now.ToString("dd/MM/yyyy") + "</color>";

        btnDanhLo.onClick.RemoveAllListeners();
        btnDanhLo.onClick.AddListener(BtnDanhLoOnClick);
        btn3Cang.onClick.RemoveAllListeners();
        btn3Cang.onClick.AddListener(Btn3CangOnClick);
        btnDanhDe.onClick.RemoveAllListeners();
        btnDanhDe.onClick.AddListener(BtnDanhDeOnClick);
        btnDauDuoi.onClick.RemoveAllListeners();
        btnDauDuoi.onClick.AddListener(BtnDauDuoiOnClick);
        btnType1.onClick.RemoveAllListeners();
        btnType1.onClick.AddListener(BtnType_1_OnClick);
        btnType2.onClick.RemoveAllListeners();
        btnType2.onClick.AddListener(BtnType_2_OnClick);
        btnXacNhan.onClick.RemoveAllListeners();
        btnXacNhan.onClick.AddListener(ConFirmBet);
        btnXacNhanDat.onClick.RemoveAllListeners();
        btnXacNhanDat.onClick.AddListener(SendLotteryInfoRequest);
        btnXacNhanHuy.onClick.RemoveAllListeners();
        btnXacNhanHuy.onClick.AddListener(OffPanelXacNhan);
        DrNum_1.onValueChanged.RemoveAllListeners();
        DrNum_1.onValueChanged.AddListener(CheckNumberDropdown);
        DrNum_2.onValueChanged.RemoveAllListeners();
        DrNum_2.onValueChanged.AddListener(CheckNumberDropdown_2);
        DrNum_3.onValueChanged.RemoveAllListeners();
        DrNum_3.onValueChanged.AddListener(CheckNumberDropdown_3);
        ScrMoney.onValueChanged.RemoveAllListeners();
        ScrMoney.onValueChanged.AddListener(ChangeValueScrollbarMoney);
        txtBtnType1 = btnType1.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        txtBtnType2 = btnType2.gameObject.transform.GetChild(0).gameObject.GetComponent<Text>();
        txtHuongDan.text = NotePase_0;

        txtBtnType1.text = "Lô 2 Số";
        txtBtnType2.text = "Lô 3 Số";

        Num_1 = 0;
        Num_2 = 0;
        Num_3 = 0;
        strBetMoney = "1000";
        strBetMoneyForSendRequest = strBetMoney;
        BetMoney = 1000;
        ScrMoney.value = 0;
        btnType1.gameObject.SetActive(true);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        btnType2.gameObject.SetActive(true);
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckStateType();       
    }

    public void BackUpInit()
    {
        Pase = 0;
        Type = 0;
        txtHuongDan.text = NotePase_0;
        txtBtnType1.text = "Lô 2 Số";
        txtBtnType2.text = "Lô 3 Số";
        Num_1 = 0;
        Num_2 = 0;
        Num_3 = 0;
        DrNum_1.value = Num_1;
        DrNum_2.value = Num_2;
        DrNum_3.value = Num_3;
        strBetMoney = "1000";
        strBetMoneyForSendRequest = strBetMoney;
        BetMoney = 1000;
        ScrMoney.value = 0;
        btnType1.gameObject.SetActive(true);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        btnType2.gameObject.SetActive(true);
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckStateType();
        txtSoDanh.text = "00";
    }

    #region EVEN

    public void BtnBackOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        gameObject.SetActive(false);
    }

    public void BtnDanhLoOnClick()
    {
        Pase = 0;
        Type = 0;
        Debug.Log("Click");
        btnType1.gameObject.SetActive(true);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        btnType2.gameObject.SetActive(true);
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckStateType();
    }

    public void Btn3CangOnClick()
    {
        Pase = 1;
        Type = 2;

        btnType1.gameObject.SetActive(false);
        btnType2.gameObject.SetActive(false);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");

        CheckStateType();
    }

    public void BtnDanhDeOnClick()
    {
        Pase = 2;
        Type = 3;
        btnType1.gameObject.SetActive(true);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        btnType2.gameObject.SetActive(true);
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckStateType();
    }

    public void BtnDauDuoiOnClick()
    {
        Pase = 3;
        Type = 5;
        btnType1.gameObject.SetActive(true);
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");

        btnType2.gameObject.SetActive(true);
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        CheckStateType();
    }

    public void BtnType_1_OnClick()
    {
       

        if (Pase == 0)
        {
            Type = 0;
        }
        else if (Pase == 1)
        {
            Type = 2;
        }
        else if (Pase == 2)
        {
            Type = 3;
        }
        else
        {
            Type = 5;
        }

        CheckStateType();
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
    }


    public void BtnType_2_OnClick()
    {
       

        if (Pase == 0)
        {
            Type = 1;
        }
        else if (Pase == 2)
        {
            Type = 4;
        }
        else if (Pase == 3)
        {
            Type = 6;
        }
        CheckStateType();
        btnType1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So");
        btnType2.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/btn_Lo2So_2");
    }

    public void CheckNumberDropdown(int ValueEnd)
    {
        Num_1 = ValueEnd;
        ChangeTxtBetNumber();
    }


    public void CheckNumberDropdown_2(int ValueEnd)
    {
        Num_2 = ValueEnd;
        ChangeTxtBetNumber();
    }


    public void CheckNumberDropdown_3(int ValueEnd)
    {
        Num_3 = ValueEnd;
        ChangeTxtBetNumber();
    }

    public void ChangeValueScrollbarMoney(float value)
    {
        if (ScrMoney.value == 0)
        {
            BetMoney = 1000;
        }
        else
        {
            float y = ScrMoney.value * (float)MaxMoney;
            if (y > 1000)
            {
                BetMoney = (long)y;
            }
            else
            {
                BetMoney = 1000;
            }
            
        }
        ShowBetMoney();
        ProcessWinMoney();
    }



    #endregion

    #region HIỂN THỊ

    public void CheckStateType()
    {
        txtHuongDan.text = "";
        if (Pase == 0)
        {
            btnType2.gameObject.SetActive(true);
            txtBtnType1.text = "Lô 2 Số";
            txtBtnType2.text = "Lô 3 Số";
            btnDanhLo.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topTrai_2");
            btn3Cang.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            btnDanhDe.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            btnDauDuoi.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topPhai_1");
            if (Type == 0)
            {
                txtHuongDan.text += NotePase_0;
                txtLoaiDe.text = "Đánh Lô 2 Số";
                strType = "LÔ 2 SỐ";
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(true);
                DrNum_3.gameObject.SetActive(false);
                Num_3 = 0;
                DrNum_3.value = 0;
            }
            else if (Type == 1)
            {
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(true);
                DrNum_3.gameObject.SetActive(true);
                txtHuongDan.text += NotePase_1;
                txtLoaiDe.text = "Đánh Lô 3 Số";
                strType = "LÔ 3 SỐ";
            }
        }
        else if (Pase == 1)
        {
            btn3Cang.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_2");
            btnDanhLo.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topTrai_1");
            btnDanhDe.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            btnDauDuoi.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topPhai_1");
            DrNum_1.gameObject.SetActive(true);
            DrNum_2.gameObject.SetActive(true);
            DrNum_3.gameObject.SetActive(true);
            txtLoaiDe.text = "Đánh 3 Càng";
            strType = "3 CÀNG";
            txtHuongDan.text += NotePase_2;
        }
        else if (Pase == 2)
        {
            btnDanhDe.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_2");
            btnDanhLo.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topTrai_1");
            btn3Cang.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            btnDauDuoi.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topPhai_1");
            txtBtnType1.text = "Đề Đầu";
            txtBtnType2.text = "Đề Đặc Biệt";
            if (Type == 3)
            {
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(true);
                DrNum_3.gameObject.SetActive(false);
                Num_3 = 0;
                DrNum_3.value = 0;
                txtHuongDan.text += NotePase_3;
                txtLoaiDe.text = "Đánh Đề Đầu";
                strType = "ĐỀ ĐẦU";
            }
            else if (Type == 4)
            {
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(true);
                DrNum_3.gameObject.SetActive(false);
                Num_3 = 0;
                DrNum_3.value = 0;
                txtHuongDan.text += NotePase_4;
                txtLoaiDe.text = "Đánh Đề Đặc Biệt";
                strType = "ĐỀ ĐẶC BIỆT";
            }
        }
        else if (Pase == 3)
        {
            btnDauDuoi.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topPhai_2");
            btnDanhLo.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topTrai_1");
            btn3Cang.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            btnDanhDe.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icon/topGiua_1");
            txtBtnType1.text = "Đầu";
            txtBtnType2.text = "Đuôi";
            if (Type == 5)
            {
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(false);
                DrNum_3.gameObject.SetActive(false);
                Num_2 = 0;
                Num_3 = 0;
                DrNum_2.value = 0;
                DrNum_3.value = 0;
                txtHuongDan.text += NotePase_5;
                txtLoaiDe.text = "Đánh Đầu";
                strType = "ĐÁNH ĐẦU";
            }
            else if (Type == 6)
            {
                DrNum_1.gameObject.SetActive(true);
                DrNum_2.gameObject.SetActive(false);
                DrNum_3.gameObject.SetActive(false);
                Num_2 = 0;
                Num_3 = 0;
                DrNum_2.value = 0;
                DrNum_3.value = 0;
                txtHuongDan.text += NotePase_6;
                txtLoaiDe.text = "Đánh Đuôi";
                strType = "ĐÁNH ĐUÔI";
            }
        }
        ChangeTxtBetNumber();
        ShowBetMoney();
        ProcessWinMoney();
    }

    private void ShowBetMoney()
    {
        BetMoney = Mathf.FloorToInt(BetMoney);
        strBetMoneyForSendRequest = BetMoney.ToString();
        strBetMoney = string.Format("{0:0,0}", BetMoney);
        txtTongTien.text = strBetMoney;
    }

    private void ChangeTxtBetNumber()
    {
        string strBetNumber;
        if (Type == 0 || Type == 3 || Type == 4)
        {
            strBetNumber = Num_1.ToString() + Num_2.ToString();
        }
        else if (Type == 1 || Type == 2)
        {
            strBetNumber = Num_1.ToString() + Num_2.ToString() + Num_3.ToString();
        }
        else
        {
            strBetNumber = Num_1.ToString();
        }
        txtSoDanh.text = strBetNumber;
        intBetNumber = int.Parse(strBetNumber);
    }

    public void ProcessWinMoney()
    {
        txtTienCon.text = "";
        txtTienThang.text = "";
        long a = BetMoney;
        long t, b;
        if (Type == 0)
        {
            t = a / 22;
            b = a * 78 / 22;
        }
        else if (Type == 1)
        {
            t = a / 23;
            b = a * 770 / 23;
        }
        else if (Type == 2)
        {
            t = a;
            b = a * 700;
        }
        else if (Type == 3)
        {
            t = a / 4;
            b = a * 82 / 4;
        }
        else if (Type == 4)
        {
            t = a;
            b = a * 82;
        }
        else if (Type == 5)
        {
            t = a;
            float c = a * 8.7f;
            b = (long)c;
        }
        else
        {
            t = a;
            float c = a * 8.7f;
            b = (long)c;
        }        
        txtTienCon.text += string.Format("{0:0,0}", t);
        txtTienThang.text += string.Format("{0:0,0}", b);
    }

    #endregion

    #region XÁC NHẬN ĐẶT CƯỢC

    public void ConFirmBet()
    {
        Debug.Log("Loai de = " + Type + "So Danh = " + intBetNumber + "Tien Dat = " + BetMoney);
        ActivePanelXacNhan();
    }

    private void GetUserMoney(GamePacket gp)
    {
        UserMoney = gp.GetLong("chip");
        SoTienConLai = float.Parse(UserMoney.ToString()) - BetMoney;
    }


    public void XacNhanDaDat(GamePacket gp)
    {
        int grp = gp.GetInt("grp");
        if (grp == 1)
        {
            Debug.Log("Dat thanh cong");
        }
        else if (grp == -1)
        {
            Debug.Log("Khong du tien");
        }
        else if (grp == -2)
        {
            Debug.Log("Dat cuoc khong dung");
        }
        else if (grp == -3)
        {
            Debug.Log("Tien cuoc toi thieu la 1000");
        }
    }

    private void ActivePanelXacNhan()
    {
        PanelXacNhan.SetActive(true);
        txtLoaiDeXacNhan.text = strType;
        txtSoDanhXacNhan.text = intBetNumber.ToString();
        txtTongTienXacNhan.text = strBetMoney;
        MaxMoney = MyInfo.CHIP;
        txtConLaiXacNhan.text = string.Format("{0:0,0}", (MaxMoney - BetMoney));
    }

    private void SendLotteryInfoRequest()
    {
        GamePacket gp = new GamePacket(CommandKey.LotteryBet);
        gp.Put("Type", Type);
        string NumBer = intBetNumber.ToString();
        if (Type == 1 || Type == 2)
        {
            char[] array = NumBer.ToCharArray();
            if (array.Length == 2)
            {
                string temp = "0" + NumBer;
                NumBer = temp;
            }
            else if (array.Length == 1)
            {
                string temp = "00" + NumBer;
                NumBer = temp;
            }
        }
        else if (Type == 0 || Type == 3 || Type == 4)
        {
            char[] array = NumBer.ToCharArray();
            if (array.Length == 1)
            {
                string temp = "0" + NumBer;
                NumBer = temp;
            }
        }
        gp.Put("BetNumber", NumBer);
        long BM = long.Parse(strBetMoneyForSendRequest);
        gp.Put("BetMoney", BM);
        SFS.Instance.SendRoomRequest(gp);
        MyInfo.CHIP -= BetMoney;
        LotteryManager.ChangeTextUserChip();
        PanelXacNhan.SetActive(false);
    }

    public void OffPanelXacNhan()
    {
        PanelXacNhan.SetActive(false);
        BackUpInit();
    }

    #endregion








}
