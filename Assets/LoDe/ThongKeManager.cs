﻿using Sfs2X.Entities.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatisticalLotteryManager4 : MonoBehaviour {

    private SFS sfs;
    private static StatisticalLotteryManager instance;
    public static StatisticalLotteryManager Instance { get { return instance; } }
    [SerializeField]
    GameObject Panel;
    [SerializeField]
    GameObject List_SoXuatHien;
    [SerializeField]
    GameObject Module_SoXuatHien;
    [SerializeField]
    Transform Parent_SoXuatHien;

    [SerializeField]
    GameObject List_SoKhongXuatHien;
    [SerializeField]
    GameObject Module_SoKhongXuatHien;
    [SerializeField]
    Transform Parent_SoKhongXuatHien;


    [SerializeField]
    GameObject List_SoDat;
    [SerializeField]
    Transform Parent_SoDat;


    [SerializeField]
    Button BtnBack;
    [SerializeField]
    Button BtnExit;
    [SerializeField]
    GameObject Panel_Xuat;
    [SerializeField]
    GameObject Panel_An;
    [SerializeField]
    GameObject Panel_Dat;
    [SerializeField]
    Button BtnXuat;
    [SerializeField]
    Button BtnAn;
    [SerializeField]
    Button BtnDat;


    private string[] ArrayAppearRepones;
    private string[] ArrayNotAppearRepones;
    private string[] ArrayHotNumberRepones;

    List<string> LstTyLe;
    List<string> LstNumber;

    private SFSObject obj;

    ISFSArray myArray;

    private int[] DiceResult;
    private string GameResult;

    private bool IsInited = false;

    void Awake()
    {
      //  instance = this;

    }







    public void ShowSoXuatHien()
    {
        PopupLotteryManager.Panel = "ThongKe";

        if (IsInited == false)
        {
            List_SoXuatHien.SetActive(false);
            Panel.SetActive(true);
            Panel_Xuat.SetActive(true);
            Panel_An.SetActive(false);
            Panel_Dat.SetActive(false);

            BtnBack.onClick.RemoveAllListeners();
            BtnBack.onClick.AddListener(ButtonBackOnClick);

            //  BtnExit.onClick.RemoveAllListeners();
            //  BtnExit.onClick.AddListener(ButtonBackOnClick);

            BtnXuat.onClick.RemoveAllListeners();
            BtnXuat.onClick.AddListener(ButtonXuatOnClick);

            BtnAn.onClick.RemoveAllListeners();
            BtnAn.onClick.AddListener(ButtonAnOnClick);

            BtnDat.onClick.RemoveAllListeners();
            BtnDat.onClick.AddListener(ButtonDatOnClick);

            ArrayAppearRepones = new string[10];
            ArrayNotAppearRepones = new string[10];
            ArrayHotNumberRepones = new string[10];

            LstTyLe = new List<string>();
            LstNumber = new List<string>();

            List_SoXuatHien.SetActive(true);

            for (int i = 0; i < Parent_SoXuatHien.childCount; i++)
            {
                GameObject.Destroy(Parent_SoXuatHien.GetChild(i).gameObject);
            }




            HienThiSoXuatHien();

            IsInited = true;
        }
        else
        {
            List_SoXuatHien.SetActive(false);
            Panel.SetActive(true);
            Panel_Xuat.SetActive(true);
            Panel_An.SetActive(false);
            Panel_Dat.SetActive(false);

            ArrayAppearRepones = new string[10];
            ArrayNotAppearRepones = new string[10];
            ArrayHotNumberRepones = new string[10];

            LstTyLe = new List<string>();
            LstNumber = new List<string>();

            List_SoXuatHien.SetActive(true);

            for (int i = 0; i < Parent_SoXuatHien.childCount; i++)
            {
                GameObject.Destroy(Parent_SoXuatHien.GetChild(i).gameObject);
            }

            HienThiSoXuatHien();
        }





        //  testNhanSoKhongXuatHien();
    }


    public void ShowSoKhongXuatHien()
    {
        List_SoKhongXuatHien.SetActive(true);

        for (int i = 0; i < Parent_SoKhongXuatHien.childCount; i++)
        {
            GameObject.Destroy(Parent_SoKhongXuatHien.GetChild(i).gameObject);
        }
        //  ShowItemSoKhongXuatHien(0, "14", "78");

        //  SendSoKhongXuatHienRequest();
        HienThiSoKhongXuatHien();
    }



    public void ShowSoDatCuoc()
    {
        List_SoDat.SetActive(true);

        for (int i = 0; i < Parent_SoDat.childCount; i++)
        {
            GameObject.Destroy(Parent_SoDat.GetChild(i).gameObject);
        }

        // ShowItemSoDat(0, "15");

        // SendSoDatCuocRequest();

        HienThiNhanSoDat();
    }



    #region REQUEST



    #endregion


    #region RESPONSE

    /*

    public void OnSFSResponse(GamePacket gp)
    {
        switch (gp.cmd)
        {
            case CommandKey.AppearNumber:
                NhanDuLieuSoXuatHien(gp);
                break;
            case CommandKey.NotAppearNumber:
                NhanDuLieuSoKhongXuatHien(gp);
                break;
            case CommandKey.HotNumber:
                NhanDuLieuSoDatCuoc(gp);
                break;
        }
    }

        */


    private void HienThiSoXuatHien()
    {
        /*
        string[] array = new string[] { "12", "78", "95", "16", "75", "41", "32", "05", "78", "99" } ;

        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < array.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = array[i];
                dem = 1;
            }
            else
            {
                ShowItem(i - 1, SoTruoc, array[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
        */


        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < ArrayAppearRepones.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = ArrayAppearRepones[i];
                dem = 1;
            }
            else
            {
                ShowItem(i - 1, SoTruoc, ArrayAppearRepones[i]);
                dem = 0;
                SoTruoc = "";
            }
        }



    }



    public void NhanDuLieuSoXuatHien(GamePacket gp)
    {

        ArrayAppearRepones = gp.GetStringArray("ArrayAppearNumber");



        /*

        string[] ArrayNumBer = gp.GetStringArray("ArrayAppearNumber");

        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < ArrayNumBer.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = ArrayNumBer[i];
                dem = 1;
            }
            else
            {
                ShowItem(i - 1, SoTruoc, ArrayNumBer[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
        */
    }



    private void HienThiSoKhongXuatHien()
    {
        /*
        string[] array = new string[] { "2", "14", "45", "32", "97", "55", "31", "02", "07", "33" };
        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < array.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = array[i];
                dem = 1;
            }
            else
            {
                ShowItemSoKhongXuatHien(i - 1, SoTruoc, array[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
        */


        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < ArrayNotAppearRepones.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = ArrayNotAppearRepones[i];
                dem = 1;
            }
            else
            {
                ShowItemSoKhongXuatHien(i - 1, SoTruoc, ArrayNotAppearRepones[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
    }


    public void NhanDuLieuSoKhongXuatHien(GamePacket gp)
    {
        ArrayNotAppearRepones = gp.GetStringArray("ArrayNotAppearNumber");

        /*
        string[] ArrayNumBer = gp.GetStringArray("ArrayNotAppearNumber");
        int dem = 0;
        string SoTruoc = "";

        for (int i = 0; i < ArrayNumBer.Length; i++)
        {
            if (dem == 0)
            {
                SoTruoc = ArrayNumBer[i];
                dem = 1;
            }
            else
            {
                ShowItemSoKhongXuatHien(i - 1, SoTruoc, ArrayNumBer[i]);
                dem = 0;
                SoTruoc = "";
            }
        }
        */
    }



    private void HienThiNhanSoDat()
    {
        /*

         string[] ArrayReponse = new string[] { "12|35%", "78|7%", "95|74%", "16|3%", "75|14%", "41|9%", "32|12%", "05|31%", "78|84%", "99|4%" };

         List<string> LstTyLe = new List<string>();
         List<string> LstNumber = new List<string>();

         for (int t = 0; t < ArrayReponse.Length; t++)
         {
             string[] k = ArrayReponse[t].Split('|');
             LstNumber.Add(k[0]);
             LstTyLe.Add(k[1]);
         }

         int dem = 0;
         string SoTruoc = "";
         string TyLeTruoc = "";

         for (int i = 0; i < LstNumber.Count; i++)
         {
             if (dem == 0)
             {
                 SoTruoc = LstNumber[i];
                 TyLeTruoc = LstTyLe[i];
                 dem = 1;
             }
             else
             {
                 ShowItemSoDat(i - 1, SoTruoc, TyLeTruoc, LstNumber[i], LstTyLe[i]);
                 dem = 0;
                 SoTruoc = "";
                 TyLeTruoc = "";
             }
         }

         LstNumber.Clear();
         LstTyLe.Clear();
         */



        for (int t = 0; t < ArrayHotNumberRepones.Length; t++)
        {
            string[] k = ArrayHotNumberRepones[t].Split('|');
            LstNumber.Add(k[0]);
            LstTyLe.Add(k[1]);
        }

        int dem = 0;
        string SoTruoc = "";
        string TyLeTruoc = "";

        for (int i = 0; i < LstNumber.Count; i++)
        {
            if (dem == 0)
            {
                SoTruoc = LstNumber[i];
                TyLeTruoc = LstTyLe[i];
                dem = 1;
            }
            else
            {
               // ShowItemSoDat(i - 1, SoTruoc, TyLeTruoc, LstNumber[i], LstTyLe[i]);
                dem = 0;
                SoTruoc = "";
                TyLeTruoc = "";
            }
        }

        LstNumber.Clear();
        LstTyLe.Clear();

    }





    public void NhanDuLieuSoDatCuoc(GamePacket gp)
    {
        ArrayHotNumberRepones = gp.GetStringArray("ArrayHotNumber");

        /*
        string[] ArrayReponse = gp.GetStringArray("ArrayHotNumber");

        List<string> LstTyLe = new List<string>();
        List<string> LstNumber = new List<string>();

        for (int t = 0; t < ArrayReponse.Length; t++)
        {
            string[] k = ArrayReponse[t].Split('|');
            LstNumber.Add(k[0]);
            LstTyLe.Add(k[1]);
        }
        
         

       
        int dem = 0;
        string SoTruoc = "";
        string TyLeTruoc = "";

        for (int i = 0; i < LstNumber.Count; i++)
        {
            if (dem == 0)
            {
                SoTruoc = LstNumber[i];
                TyLeTruoc = LstTyLe[i];
                dem = 1;
            }
            else
            {
                ShowItemSoDat(i - 1, SoTruoc, TyLeTruoc, LstNumber[i], LstTyLe[i]);
                dem = 0;
                SoTruoc = "";
                TyLeTruoc = "";
            }
        }
        */
    }







    void ShowItem(int index, string number_1, string number_2)
    {
        SoKhongXuatHienModule itemView;
        GameObject obj = Instantiate(Module_SoKhongXuatHien) as GameObject;
        ItemNotAppearNumber item = new ItemNotAppearNumber();

        obj.transform.SetParent(Parent_SoXuatHien);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<SoKhongXuatHienModule>();



        itemView.Init(item);
        itemView.Show(number_1, number_2);
    }


    /*
    void ShowItemSoDat(int index, string number_1, string percent_1, string number_2, string percent_2)
    {
        SoXuatHienNhieuNhat itemView;
        GameObject obj = Instantiate(Module_SoXuatHien) as GameObject;
        ItemHotNumberBet item = new ItemHotNumberBet();

        obj.transform.SetParent(Parent_SoDat);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<SoXuatHienNhieuNhat>();



        itemView.Init(item);
        itemView.Show(number_1, percent_1, number_2, percent_2);
    }


    */

    void ShowItemSoKhongXuatHien(int index, string number_1, string number_2)
    {
        SoKhongXuatHienModule itemView;
        GameObject obj = Instantiate(Module_SoKhongXuatHien) as GameObject;
        ItemNotAppearNumber item = new ItemNotAppearNumber();

        obj.transform.SetParent(Parent_SoKhongXuatHien);
        obj.transform.SetSiblingIndex(index);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = Vector3.zero;
        itemView = obj.GetComponent<SoKhongXuatHienModule>();



        itemView.Init(item);
        itemView.Show(number_1, number_2);

    }





    #endregion



    #region BUTTON


    public void ButtonBackOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel.SetActive(false);
        Panel_Xuat.SetActive(false);
        Panel_An.SetActive(false);
        Panel_Dat.SetActive(false);

        List_SoDat.SetActive(false);
        List_SoKhongXuatHien.SetActive(false);
        List_SoXuatHien.SetActive(false);
    }

    public void ButtonXuatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Xuat.SetActive(true);
        Panel_An.SetActive(false);
        Panel_Dat.SetActive(false);
    }

    public void ButtonAnOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Xuat.SetActive(false);
        Panel_An.SetActive(true);
        Panel_Dat.SetActive(false);

        ShowSoKhongXuatHien();
    }

    public void ButtonDatOnClick()
    {
        SoundManager.PlaySound(SoundManager.BUTTON_CLICK);
        Panel_Xuat.SetActive(false);
        Panel_An.SetActive(false);
        Panel_Dat.SetActive(true);

        ShowSoDatCuoc();
    }



    #endregion




    public void Hide()
    {
        Panel.SetActive(false);
    }

    public void UpdateHistory(int[] diceResult, string gameResult)
    {
        DiceResult = diceResult;
        GameResult = gameResult;

        if (Panel.activeSelf)
            ShowSoXuatHien();
    }
}


